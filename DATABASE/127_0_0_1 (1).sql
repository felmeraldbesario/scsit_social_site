-- phpMyAdmin SQL Dump
-- version 4.4.12
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 31, 2016 at 12:59 PM
-- Server version: 5.6.25
-- PHP Version: 5.6.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `sss_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `aboutus`
--

CREATE TABLE IF NOT EXISTS `aboutus` (
  `id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `address` varchar(100) NOT NULL,
  `body` varchar(1000) NOT NULL,
  `image` varchar(500) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `aboutus`
--

INSERT INTO `aboutus` (`id`, `title`, `address`, `body`, `image`) VALUES
(1, 'Salazar Colleges of Science and Institute of Technology', '211. N. Bacalso., Avenue Cebu City', 'Salazar Colleges of Science and Institute of Technology are now enrolling new students hahaha Unsay About us sa Salazar or SCSIT , cuz I realy don''t know', 'logo1.png');

-- --------------------------------------------------------

--
-- Table structure for table `add_friend`
--

CREATE TABLE IF NOT EXISTS `add_friend` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `admin_announcement`
--

CREATE TABLE IF NOT EXISTS `admin_announcement` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `announcement` varchar(5000) NOT NULL,
  `date` varchar(50) NOT NULL,
  `date_update` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_announcement`
--

INSERT INTO `admin_announcement` (`id`, `admin_id`, `announcement`, `date`, `date_update`) VALUES
(2, 1, 'When: March 30, 2016\r\nTime: 7am\r\nWhere: Salazar Colleges of Science and Institute of Technology (SCSIT GROUND)\r\nWhat: fourth year graduation ', '11:03:43 pm 03/01/16', '11:03:43 pm 03/06/16');

-- --------------------------------------------------------

--
-- Table structure for table `admin_block_list`
--

CREATE TABLE IF NOT EXISTS `admin_block_list` (
  `id` int(11) NOT NULL,
  `reason_id` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL,
  `blocked_id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_block_list`
--

INSERT INTO `admin_block_list` (`id`, `reason_id`, `status`, `blocked_id`, `admin_id`, `date`) VALUES
(1, 5, 1, 20, 1, '07:03:07 pm 03/12/16');

-- --------------------------------------------------------

--
-- Table structure for table `admin_delete_messages`
--

CREATE TABLE IF NOT EXISTS `admin_delete_messages` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `deleter_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_delete_messages`
--

INSERT INTO `admin_delete_messages` (`id`, `message_id`, `deleter_id`) VALUES
(1, 27, 0),
(2, 19, 0),
(3, 40, 0),
(4, 36, 0),
(5, 44, 0),
(11, 53, 1),
(12, 52, 1),
(13, 48, 1),
(14, 47, 1),
(15, 42, 1),
(16, 54, 1);

-- --------------------------------------------------------

--
-- Table structure for table `admin_logs`
--

CREATE TABLE IF NOT EXISTS `admin_logs` (
  `id` int(11) NOT NULL,
  `admin_id` int(100) NOT NULL,
  `date_login` varchar(100) NOT NULL,
  `date_logout` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_logs`
--

INSERT INTO `admin_logs` (`id`, `admin_id`, `date_login`, `date_logout`) VALUES
(20, 1, '10:45:40 am 03/10/16', '10:03:55 am 03/10/16'),
(21, 1, '10:46:17 am 03/10/16', '10:03:25 am 03/10/16'),
(22, 13, '10:46:52 am 03/10/16', '10:03:00 am 03/10/16'),
(23, 13, '02:24:42 pm 03/10/16', ''),
(24, 13, '02:26:49 pm 03/10/16', ''),
(25, 13, '02:33:55 pm 03/10/16', ''),
(26, 13, '02:36:36 pm 03/10/16', ''),
(27, 13, '03:12:40 pm 03/10/16', ''),
(28, 13, '03:18:14 pm 03/10/16', ''),
(29, 13, '03:29:36 pm 03/10/16', '03:03:39 pm 03/10/16'),
(30, 13, '03:39:34 pm 03/10/16', '03:03:44 pm 03/10/16'),
(31, 13, '03:40:36 pm 03/10/16', '03:03:21 pm 03/10/16'),
(32, 13, '03:57:53 pm 03/10/16', '03:03:30 pm 03/10/16'),
(33, 13, '04:20:34 pm 03/10/16', ''),
(34, 1, '11:01:41 pm 03/10/16', '01:03:48 am 03/11/16'),
(35, 1, '10:45:32 am 03/11/16', '11:03:43 am 03/11/16'),
(36, 1, '11:18:50 am 03/11/16', '11:03:03 am 03/11/16'),
(37, 13, '11:33:20 am 03/11/16', '11:03:28 am 03/11/16'),
(38, 1, '11:33:43 am 03/11/16', '12:03:03 pm 03/11/16'),
(39, 1, '01:02:42 pm 03/11/16', '01:03:05 pm 03/11/16'),
(40, 1, '02:42:55 pm 03/11/16', ''),
(41, 1, '07:10:17 pm 03/11/16', ''),
(42, 1, '08:16:22 am 03/12/16', ''),
(43, 13, '09:43:11 am 03/12/16', '10:03:15 am 03/12/16'),
(44, 1, '02:09:48 pm 03/12/16', '02:03:47 pm 03/12/16'),
(45, 1, '02:11:36 pm 03/12/16', '02:03:57 pm 03/12/16'),
(46, 1, '02:22:31 pm 03/12/16', '02:03:03 pm 03/12/16'),
(47, 1, '02:38:39 pm 03/12/16', '02:03:30 pm 03/12/16'),
(48, 1, '06:57:39 pm 03/12/16', '07:03:25 pm 03/12/16'),
(49, 1, '07:12:05 pm 03/12/16', ''),
(50, 1, '07:13:22 pm 03/12/16', '07:03:47 pm 03/12/16'),
(51, 1, '07:21:45 pm 03/12/16', '07:03:15 pm 03/12/16'),
(52, 1, '04:27:39 pm 03/13/16', '04:03:58 pm 03/13/16'),
(53, 1, '04:47:23 pm 03/13/16', ''),
(54, 1, '04:47:24 pm 03/13/16', '04:03:02 pm 03/13/16');

-- --------------------------------------------------------

--
-- Table structure for table `admin_messaging`
--

CREATE TABLE IF NOT EXISTS `admin_messaging` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `message` varchar(500) NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` varchar(100) NOT NULL,
  `date_forward` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_messaging`
--

INSERT INTO `admin_messaging` (`id`, `sender_id`, `receiver_id`, `message`, `image`, `date`, `date_forward`) VALUES
(36, 1, 0, '                this is my message super admin', 'no image', '12:27:40 am 03/04/16', ''),
(37, 1, 0, 'this is my second message super admin', 'no image', '12:28:00 am 03/04/16', ''),
(38, 1, 5, 'dd', 'no image', '12:28:17 am 03/04/16', ''),
(39, 1, 0, 'ddd', 'no image', '12:28:26 am 03/04/16', ''),
(40, 1, 0, 'dd', 'no image', '12:33:20 am 03/04/16', '12:33:20 am 03/04/16'),
(41, 1, 2, 'hi', 'no image', '01:29:19 pm 03/04/16', ''),
(42, 1, 4, 'dddd', 'abstract-anime-free-ps3-wallpaper-1080p-83-16.jpg', '01:29:36 pm 03/04/16', ''),
(43, 1, 0, 'dd', 'no image', '05:04:13 pm 03/05/16', ''),
(44, 1, 0, 'type your text', 'no image', '05:04:24 pm 03/05/16', ''),
(45, 1, 0, 'type your text', 'no image', '05:05:59 pm 03/05/16', '05:05:59 pm 03/05/16'),
(46, 0, 1, 'hehe', '', '05:03:33 pm 03/05/16', ''),
(47, 1, 0, 'lol', 'no image', '10:06:47 am 03/05/16', ''),
(48, 1, 2, 'lol', 'no image', '05:41:19 pm 03/05/16', '05:41:19 pm 03/05/16'),
(49, 1, 0, 'dddd', 'no image', '05:41:31 pm 03/05/16', '05:41:31 pm 03/05/16'),
(50, 0, 6, ' lol', '', '06:03:25 pm 03/05/16', ''),
(51, 0, 9, ' dd', '', '06:03:35 pm 03/05/16', ''),
(52, 1, 0, 'hello\r\nder?\r\n<3\r\nmusta?', 'no image', '08:38:46 pm 03/05/16', ''),
(53, 1, 0, 'hello\r\nder?\r\n<3\r\nmusta?', 'no image', '11:19:28 pm 03/06/16', '11:19:28 pm 03/06/16'),
(54, 1, 0, 'ddd', 'no image', '02:56:42 am 03/12/16', '');

-- --------------------------------------------------------

--
-- Table structure for table `admin_personel`
--

CREATE TABLE IF NOT EXISTS `admin_personel` (
  `id` int(11) NOT NULL,
  `fname` varchar(20) NOT NULL,
  `lname` varchar(20) NOT NULL,
  `Mname` varchar(20) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `email` varchar(30) NOT NULL,
  `image` varchar(1000) NOT NULL,
  `assigned_department` int(11) NOT NULL,
  `admin_id_number` varchar(30) NOT NULL,
  `date` varchar(500) NOT NULL,
  `date_profile_change` varchar(100) NOT NULL,
  `date_pass_change` varchar(50) NOT NULL,
  `account_date_update` varchar(50) NOT NULL,
  `date_login` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_personel`
--

INSERT INTO `admin_personel` (`id`, `fname`, `lname`, `Mname`, `username`, `password`, `email`, `image`, `assigned_department`, `admin_id_number`, `date`, `date_profile_change`, `date_pass_change`, `account_date_update`, `date_login`) VALUES
(1, 'felmerald', 'besario', 'c', 'felmerald', 'ec158a9218473b34f83884eceff5e015', 'felmeraldb@gmail.com', 'IMG_20141216_145214.jpg', 1, '2123854', '03:46:33 am 02/17/16', '04:03:56 pm 03/09/16', '07:13:05 pm 03/12/16', '03:03:30 am 03/02/16', '0'),
(13, 'chrislane', 'abasanta', 'daan', 'chrislaneabasanta', 'ec158a9218473b34f83884eceff5e015', 'chrislaneabasanta@yahoo.com', 'scsit-cover1.png', 6, '2323', '03:29:21 am 03/04/16', '04:03:12 pm 03/09/16', '', '', '0');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_comment`
--

CREATE TABLE IF NOT EXISTS `announcement_comment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement_comment`
--

INSERT INTO `announcement_comment` (`id`, `topic_id`, `user_id`, `comments`, `date`) VALUES
(12, 4, 1, 'lols', 'February 28, 2016 01:41pm'),
(13, 3, 1, 'wetwew', 'February 28, 2016 01:44pm'),
(14, 3, 1, 'ngeks', 'February 28, 2016 01:44pm'),
(15, 4, 1, 'ngeks', 'February 28, 2016 02:21pm'),
(16, 4, 1, 'ngeks sure oi', 'February 28, 2016 02:35pm'),
(17, 3, 1, 'wako wako', 'February 28, 2016 02:56pm'),
(18, 3, 1, 'lol', 'February 28, 2016 03:12pm');

-- --------------------------------------------------------

--
-- Table structure for table `announcement_feedback`
--

CREATE TABLE IF NOT EXISTS `announcement_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement_feedback`
--

INSERT INTO `announcement_feedback` (`id`, `topic_id`, `user_id`) VALUES
(4, 4, 1),
(5, 3, 1),
(6, 6, 20);

-- --------------------------------------------------------

--
-- Table structure for table `announcement_modal_like`
--

CREATE TABLE IF NOT EXISTS `announcement_modal_like` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement_modal_like`
--

INSERT INTO `announcement_modal_like` (`id`, `topic_id`, `user_id`) VALUES
(4, 4, 1),
(5, 3, 1),
(6, 6, 20);

-- --------------------------------------------------------

--
-- Table structure for table `announcement_modalcomment`
--

CREATE TABLE IF NOT EXISTS `announcement_modalcomment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `announcement_modalcomment`
--

INSERT INTO `announcement_modalcomment` (`id`, `topic_id`, `user_id`) VALUES
(3, 4, 1),
(6, 4, 12);

-- --------------------------------------------------------

--
-- Table structure for table `block_reason`
--

CREATE TABLE IF NOT EXISTS `block_reason` (
  `id` int(11) NOT NULL,
  `reason` varchar(90) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `block_reason`
--

INSERT INTO `block_reason` (`id`, `reason`) VALUES
(1, 'Publishing Scandal Status alright'),
(2, 'Publishing Sex Video'),
(4, 'Publishing Gambling'),
(5, 'Commenting Scandal');

-- --------------------------------------------------------

--
-- Table structure for table `campus_post`
--

CREATE TABLE IF NOT EXISTS `campus_post` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_id_number` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `campus_post` varchar(500) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campus_post`
--

INSERT INTO `campus_post` (`id`, `user_id`, `user_id_number`, `image`, `campus_post`, `date`) VALUES
(3, 27, '3', '12395638_1185233994824046_1917492748_n_-_Copy1.jpg', '', 'March 12,2016 03:32:19 AM'),
(4, 20, '1', 'Stephen-Curry-Wallpapers-016.jpg', 'what''s on my mind..', 'March 12,2016 02:09:17 PM'),
(5, 20, '1', '', 'sfsdf', 'March 12,2016 05:29:00 PM');

-- --------------------------------------------------------

--
-- Table structure for table `campus_post_comment`
--

CREATE TABLE IF NOT EXISTS `campus_post_comment` (
  `id` int(11) NOT NULL,
  `campus_post_id` int(11) NOT NULL,
  `campus_commenter_id` int(11) NOT NULL,
  `comment` varchar(150) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campus_post_comment`
--

INSERT INTO `campus_post_comment` (`id`, `campus_post_id`, `campus_commenter_id`, `comment`, `date`) VALUES
(1, 28, 22, ' sdfsf', '08-03-2016 04:16:26 PM'),
(2, 28, 22, ' sdfsf', '08-03-2016 04:16:34 PM'),
(3, 6, 22, ' roy', '08-03-2016 04:17:04 PM'),
(9, 3, 27, ' ggggg', '12-03-2016 03:33:24 AM'),
(12, 3, 27, 'sdfsdf', '12-03-2016 05:36:54 AM'),
(13, 3, 27, 'sdfsdf', '12-03-2016 05:37:03 AM'),
(15, 3, 20, 'jermaine', '12-03-2016 02:08:38 PM'),
(16, 4, 27, 'sherly ni', '12-03-2016 02:14:41 PM'),
(17, 4, 27, 'sdfsdf', '12-03-2016 02:19:10 PM'),
(20, 4, 0, 'ddd', '03:03:10 pm 03/12/16'),
(23, 4, 0, 'asd', '03:03:55 pm 03/12/16'),
(26, 4, 20, 'jermaine', '12-03-2016 04:34:22 PM'),
(27, 5, 27, 'sdfsdf', '12-03-2016 07:07:16 PM');

-- --------------------------------------------------------

--
-- Table structure for table `campus_post_feedback`
--

CREATE TABLE IF NOT EXISTS `campus_post_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `student_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `campus_post_feedback`
--

INSERT INTO `campus_post_feedback` (`id`, `topic_id`, `student_id`) VALUES
(2, 6, 22),
(10, 6, 20),
(15, 3, 20);

-- --------------------------------------------------------

--
-- Table structure for table `course`
--

CREATE TABLE IF NOT EXISTS `course` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `course` varchar(50) NOT NULL,
  `date_added` varchar(100) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `course`
--

INSERT INTO `course` (`id`, `department_id`, `course`, `date_added`, `date_update`) VALUES
(6, 3, 'BEED', '12:51:41 am 03/02/16', ''),
(10, 1, 'BSACT', '11:37:38 pm 03/06/16', '11:03:38 pm 03/06/16');

-- --------------------------------------------------------

--
-- Table structure for table `deleted_messages`
--

CREATE TABLE IF NOT EXISTS `deleted_messages` (
  `id` int(11) NOT NULL,
  `message_id` int(11) NOT NULL,
  `deleter_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `deleted_messages`
--

INSERT INTO `deleted_messages` (`id`, `message_id`, `deleter_id`) VALUES
(7, 6, 27);

-- --------------------------------------------------------

--
-- Table structure for table `department`
--

CREATE TABLE IF NOT EXISTS `department` (
  `id` int(11) NOT NULL,
  `department` varchar(50) NOT NULL,
  `image` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department`
--

INSERT INTO `department` (`id`, `department`, `image`) VALUES
(1, 'COMPUTER', 'IT.png'),
(2, 'NURSING', 'nursing.png'),
(3, 'EDUCATION', 'education.png'),
(4, 'HRM', 'hrm.png'),
(5, 'MARITIME', 'marine.png'),
(6, 'ENGINEERING', 'engineer.png'),
(7, 'FACULTY', 'faculty.png'),
(8, 'CRIMINOLOGY', 'criminology.png'),
(9, 'BUSINESS', 'business_administrator.png');

-- --------------------------------------------------------

--
-- Table structure for table `department_topic`
--

CREATE TABLE IF NOT EXISTS `department_topic` (
  `id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `user_id_number` varchar(50) NOT NULL,
  `image` varchar(50) NOT NULL,
  `department_topic` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_topic`
--

INSERT INTO `department_topic` (`id`, `department_id`, `user_id`, `user_id_number`, `image`, `department_topic`, `date`) VALUES
(3, 6, 13, '2323', 'no image', 'hello there', '10:02:34 am 03/12/16'),
(4, 1, 0, '0', 'no image', 'super admin', '02:03:18 pm 03/12/16'),
(6, 1, 0, '0', 'no image', 'asdad', '04:03:07 pm 03/12/16');

-- --------------------------------------------------------

--
-- Table structure for table `department_topic_comments`
--

CREATE TABLE IF NOT EXISTS `department_topic_comments` (
  `id` int(11) NOT NULL,
  `admin_id_number` int(11) NOT NULL,
  `department_topic_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `commenter_id` int(11) NOT NULL,
  `comment` varchar(150) NOT NULL,
  `date` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_topic_comments`
--

INSERT INTO `department_topic_comments` (`id`, `admin_id_number`, `department_topic_id`, `department_id`, `commenter_id`, `comment`, `date`) VALUES
(5, 2123854, 2, 0, 1, 'why?man', '09:58:57 am 03/12/16'),
(11, 2123854, 6, 0, 1, 'hello there', '07:00:25 pm 03/12/16');

-- --------------------------------------------------------

--
-- Table structure for table `department_topic_feedback`
--

CREATE TABLE IF NOT EXISTS `department_topic_feedback` (
  `id` int(11) NOT NULL,
  `department_topic_id` int(11) NOT NULL,
  `department_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `feedback` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `department_topic_feedback`
--

INSERT INTO `department_topic_feedback` (`id`, `department_topic_id`, `department_id`, `user_id`, `feedback`) VALUES
(1, 6, 0, 20, 0);

-- --------------------------------------------------------

--
-- Table structure for table `editorial_comment`
--

CREATE TABLE IF NOT EXISTS `editorial_comment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `editorial_feedback`
--

CREATE TABLE IF NOT EXISTS `editorial_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorial_feedback`
--

INSERT INTO `editorial_feedback` (`id`, `topic_id`, `user_id`) VALUES
(4, 3, 1),
(5, 4, 1),
(6, 1, 27);

-- --------------------------------------------------------

--
-- Table structure for table `editorial_modal_like`
--

CREATE TABLE IF NOT EXISTS `editorial_modal_like` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorial_modal_like`
--

INSERT INTO `editorial_modal_like` (`id`, `topic_id`, `user_id`) VALUES
(4, 3, 1),
(5, 4, 1),
(6, 1, 27);

-- --------------------------------------------------------

--
-- Table structure for table `editorial_modalcomment`
--

CREATE TABLE IF NOT EXISTS `editorial_modalcomment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `editorial_new`
--

CREATE TABLE IF NOT EXISTS `editorial_new` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(200) NOT NULL,
  `editorial` varchar(5000) NOT NULL,
  `image` varchar(500) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorial_new`
--

INSERT INTO `editorial_new` (`id`, `admin_id`, `title`, `editorial`, `image`, `date`, `date_update`) VALUES
(1, 1, '5 Idiots', 'dfdfdf', 'model2.jpg', '04:03:14 pm 03/13/16', '04:03:58 pm 03/13/16');

-- --------------------------------------------------------

--
-- Table structure for table `editorial_views`
--

CREATE TABLE IF NOT EXISTS `editorial_views` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `editorial_views`
--

INSERT INTO `editorial_views` (`id`, `topic_id`, `user_id`) VALUES
(1, 1, 27);

-- --------------------------------------------------------

--
-- Table structure for table `edp`
--

CREATE TABLE IF NOT EXISTS `edp` (
  `id` int(11) NOT NULL,
  `department` varchar(50) NOT NULL,
  `course` varchar(50) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `id_number` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `edp`
--

INSERT INTO `edp` (`id`, `department`, `course`, `firstname`, `middlename`, `lastname`, `id_number`) VALUES
(1, 'computer', 'bsit', 'jermaine', 'artajo', 'maturan', '212-3844'),
(2, 'computer', 'bsit', 'Antonio', 'An', 'Canillas', '212-3845'),
(3, 'nursing', 'nursing', 'jim', 'royjim', 'roy', '212-212'),
(4, 'computer', 'bsit', 'charity', 'cha', 'tapic', '212-3'),
(5, 'education', 'beed', 'sherly', 'sher', 'kaquilala', '212-900');

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_comment`
--

CREATE TABLE IF NOT EXISTS `entertainment_comment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_feedback`
--

CREATE TABLE IF NOT EXISTS `entertainment_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_modal_like`
--

CREATE TABLE IF NOT EXISTS `entertainment_modal_like` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_modalcomment`
--

CREATE TABLE IF NOT EXISTS `entertainment_modalcomment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_news`
--

CREATE TABLE IF NOT EXISTS `entertainment_news` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `title` varchar(500) NOT NULL,
  `entertainment` varchar(5000) NOT NULL,
  `image` varchar(200) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entertainment_news`
--

INSERT INTO `entertainment_news` (`id`, `admin_id`, `title`, `entertainment`, `image`, `date`, `date_update`) VALUES
(1, 1, 'dfd', 'dfd', 'Stephen-Curry-Wallpapers-017.jpg', '04:03:50 pm 03/13/16', '');

-- --------------------------------------------------------

--
-- Table structure for table `entertainment_views`
--

CREATE TABLE IF NOT EXISTS `entertainment_views` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `entertainment_views`
--

INSERT INTO `entertainment_views` (`id`, `topic_id`, `user_id`) VALUES
(1, 3, 27);

-- --------------------------------------------------------

--
-- Table structure for table `friend_list`
--

CREATE TABLE IF NOT EXISTS `friend_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `friends_id` int(11) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `friend_list`
--

INSERT INTO `friend_list` (`id`, `user_id`, `friends_id`, `date`) VALUES
(1, 20, 21, '15-02-2016'),
(2, 20, 22, '16-02-2016'),
(3, 22, 23, '18-02-2016'),
(4, 20, 24, '18-02-2016'),
(5, 24, 22, '22-02-2016'),
(6, 20, 25, '22-02-2016'),
(7, 20, 27, '12-03-2016');

-- --------------------------------------------------------

--
-- Table structure for table `locator`
--

CREATE TABLE IF NOT EXISTS `locator` (
  `id` int(11) NOT NULL,
  `contact` varchar(1000) NOT NULL,
  `locator` varchar(1000) NOT NULL,
  `information` varchar(1000) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `locator`
--

INSERT INTO `locator` (`id`, `contact`, `locator`, `information`) VALUES
(1, '09433465298', 'Salazar Colleges of Science and Institute of Technology', 'scsit_edu_email@hotmail.com');

-- --------------------------------------------------------

--
-- Table structure for table `messaging`
--

CREATE TABLE IF NOT EXISTS `messaging` (
  `id` int(11) NOT NULL,
  `sender_id` int(11) NOT NULL,
  `receiver_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `message` varchar(150) NOT NULL,
  `status` tinyint(4) NOT NULL,
  `date` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messaging`
--

INSERT INTO `messaging` (`id`, `sender_id`, `receiver_id`, `image`, `message`, `status`, `date`) VALUES
(4, 20, 21, '', 'dae', 0, '12-03-2016 05:37:02 PM'),
(6, 27, 20, '', 'sdfsdf', 1, '12-03-2016 06:43:06 PM');

-- --------------------------------------------------------

--
-- Table structure for table `news`
--

CREATE TABLE IF NOT EXISTS `news` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `news_body` varchar(5000) NOT NULL,
  `title` varchar(200) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news`
--

INSERT INTO `news` (`id`, `admin_id`, `image`, `news_body`, `title`, `date`, `date_update`) VALUES
(1, 1, 'abstract-anime-free-ps3-wallpaper-1080p-83-19.jpg', 'add', 'salazar', '04:03:52 pm 03/13/16', '04:03:16 pm 03/13/16');

-- --------------------------------------------------------

--
-- Table structure for table `news_comment`
--

CREATE TABLE IF NOT EXISTS `news_comment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_comment`
--

INSERT INTO `news_comment` (`id`, `topic_id`, `user_id`, `comments`, `date`) VALUES
(1, 13, 1, 'kigwahon', '02/13/16 01:26 pm');

-- --------------------------------------------------------

--
-- Table structure for table `news_feedback`
--

CREATE TABLE IF NOT EXISTS `news_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_modal_like`
--

CREATE TABLE IF NOT EXISTS `news_modal_like` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `news_modalcomment`
--

CREATE TABLE IF NOT EXISTS `news_modalcomment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `news_modalcomment`
--

INSERT INTO `news_modalcomment` (`id`, `topic_id`, `user_id`) VALUES
(1, 13, 1);

-- --------------------------------------------------------

--
-- Table structure for table `news_views`
--

CREATE TABLE IF NOT EXISTS `news_views` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `pre_signup`
--

CREATE TABLE IF NOT EXISTS `pre_signup` (
  `id` int(11) NOT NULL,
  `dept_id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `bdate` varchar(30) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(10) NOT NULL,
  `username` varchar(30) NOT NULL,
  `password` varchar(50) NOT NULL,
  `year` int(2) NOT NULL,
  `id_number` varchar(50) NOT NULL,
  `address` varchar(50) NOT NULL,
  `mobilenumber` int(20) NOT NULL,
  `email` varchar(30) NOT NULL,
  `course_id` int(20) NOT NULL,
  `image` varchar(50) NOT NULL,
  `verification` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports`
--

CREATE TABLE IF NOT EXISTS `sports` (
  `id` int(11) NOT NULL,
  `admin_id` int(11) NOT NULL,
  `image` varchar(50) NOT NULL,
  `sports_body` varchar(5000) NOT NULL,
  `title` varchar(200) NOT NULL,
  `date` varchar(1000) NOT NULL,
  `date_update` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sports`
--

INSERT INTO `sports` (`id`, `admin_id`, `image`, `sports_body`, `title`, `date`, `date_update`) VALUES
(1, 1, 'programming-hd-wallpaper-for-iphone-02c5.jpg', 'sdsdsd', 'antonio', '04:03:20 pm 03/13/16', '04:03:32 pm 03/13/16');

-- --------------------------------------------------------

--
-- Table structure for table `sports_comment`
--

CREATE TABLE IF NOT EXISTS `sports_comment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `comments` varchar(150) NOT NULL,
  `date` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_feedback`
--

CREATE TABLE IF NOT EXISTS `sports_feedback` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_modal_like`
--

CREATE TABLE IF NOT EXISTS `sports_modal_like` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_modalcomment`
--

CREATE TABLE IF NOT EXISTS `sports_modalcomment` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `sports_views`
--

CREATE TABLE IF NOT EXISTS `sports_views` (
  `id` int(11) NOT NULL,
  `topic_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `sports_views`
--

INSERT INTO `sports_views` (`id`, `topic_id`, `user_id`) VALUES
(1, 4, 27);

-- --------------------------------------------------------

--
-- Table structure for table `super_admin_info`
--

CREATE TABLE IF NOT EXISTS `super_admin_info` (
  `sa_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `username` varchar(500) NOT NULL,
  `password` varchar(500) NOT NULL,
  `firstname` varchar(200) NOT NULL,
  `lastname` varchar(200) NOT NULL,
  `middlename` varchar(200) NOT NULL,
  `faculty_id` int(11) NOT NULL,
  `email` varchar(200) NOT NULL,
  `contact_number` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `super_admin_info`
--

INSERT INTO `super_admin_info` (`sa_id`, `image`, `username`, `password`, `firstname`, `lastname`, `middlename`, `faculty_id`, `email`, `contact_number`) VALUES
(0, 'k129118221.jpg', 'antonio', 'password23', 'Canillas', 'antonio', 'aparente', 23984, 'antoniojr.canillas@gmial.com', 9433465298);

-- --------------------------------------------------------

--
-- Table structure for table `system_status`
--

CREATE TABLE IF NOT EXISTS `system_status` (
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `system_status`
--

INSERT INTO `system_status` (`status`) VALUES
(1);

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(11) NOT NULL,
  `id_number` varchar(20) NOT NULL,
  `firstname` varchar(50) NOT NULL,
  `lastname` varchar(50) NOT NULL,
  `middlename` varchar(50) NOT NULL,
  `age` int(11) NOT NULL,
  `gender` varchar(20) NOT NULL,
  `address` varchar(100) NOT NULL,
  `birthdate` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `course` int(20) NOT NULL,
  `year` int(11) NOT NULL,
  `mobile_number` bigint(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(100) NOT NULL,
  `department_id` int(20) NOT NULL,
  `token` varchar(20) NOT NULL,
  `date_created` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `id_number`, `firstname`, `lastname`, `middlename`, `age`, `gender`, `address`, `birthdate`, `email`, `course`, `year`, `mobile_number`, `username`, `password`, `department_id`, `token`, `date_created`) VALUES
(20, '1', 'jermaine', 'Maturan', 'artajo', 11, 'male', 'Labangon', '0011-11-12', 'jam@yahoo.com', 7, 1, 9231008842, 'jermaine', '96c83b667abab07f8c4fdee87e7f8f7d', 1, 'VBQG271paZs1b9Jfo6Yi', '25-01-2016'),
(21, '1', 'antonio', 'canillas', 'an', 12, 'male', 'Labangon', '0001-11-11', 'an@yahoo.com', 7, 1, 22, 'antonio', '84987ee48b5744e293c462b75aca964c', 1, 'vz7EPQJgBjIwyixySDbV', '25-01-2016'),
(22, '2', 'jim', 'roy', 'royjim', 22, 'male', 'pardo', '2016-02-02', 'roy@yahoo.com', 9, 1, 212, 'royjim', 'cb896f8939d67ad49331675072cc55da', 2, 'iysVwBICicnxGyhDjoLo', '16-02-2016'),
(27, '3', 'sherly', 'kaquilala', 'sher', 2, 'male', 'manila', '2016-03-01', 'sherly@gmail.com', 6, 1, 9, 'sherly', 'f6bed9117eb8cea593064770fce01c33', 3, 'TV1KrGd0xQDUofe1Jeus', '12-03-2016');

-- --------------------------------------------------------

--
-- Table structure for table `user_block_list`
--

CREATE TABLE IF NOT EXISTS `user_block_list` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `blocked_user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user_profile_pic`
--

CREATE TABLE IF NOT EXISTS `user_profile_pic` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `image` varchar(200) NOT NULL,
  `date` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_profile_pic`
--

INSERT INTO `user_profile_pic` (`id`, `user_id`, `image`, `date`) VALUES
(1, 27, '12431360_1185235621490550_1161773171_n1.jpg', '12-03-2016'),
(2, 20, '12404349_1185255248155254_713943476_n_-_Copy5.jpg', '12-03-2016');

-- --------------------------------------------------------

--
-- Table structure for table `user_status`
--

CREATE TABLE IF NOT EXISTS `user_status` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `status` tinyint(2) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user_status`
--

INSERT INTO `user_status` (`id`, `user_id`, `status`) VALUES
(1, 20, 0),
(2, 21, 0),
(3, 22, 0),
(4, 23, 1),
(5, 24, 0),
(6, 25, 1),
(7, 26, 0),
(8, 27, 1);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `aboutus`
--
ALTER TABLE `aboutus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `add_friend`
--
ALTER TABLE `add_friend`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_announcement`
--
ALTER TABLE `admin_announcement`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_block_list`
--
ALTER TABLE `admin_block_list`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `blocked_id` (`blocked_id`);

--
-- Indexes for table `admin_delete_messages`
--
ALTER TABLE `admin_delete_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_logs`
--
ALTER TABLE `admin_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_messaging`
--
ALTER TABLE `admin_messaging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `admin_personel`
--
ALTER TABLE `admin_personel`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`,`email`,`assigned_department`,`admin_id_number`);

--
-- Indexes for table `announcement_comment`
--
ALTER TABLE `announcement_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_feedback`
--
ALTER TABLE `announcement_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_modal_like`
--
ALTER TABLE `announcement_modal_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `announcement_modalcomment`
--
ALTER TABLE `announcement_modalcomment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `block_reason`
--
ALTER TABLE `block_reason`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campus_post`
--
ALTER TABLE `campus_post`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campus_post_comment`
--
ALTER TABLE `campus_post_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `campus_post_feedback`
--
ALTER TABLE `campus_post_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `course`
--
ALTER TABLE `course`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `deleted_messages`
--
ALTER TABLE `deleted_messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department`
--
ALTER TABLE `department`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `department` (`department`);

--
-- Indexes for table `department_topic`
--
ALTER TABLE `department_topic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_topic_comments`
--
ALTER TABLE `department_topic_comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `department_topic_feedback`
--
ALTER TABLE `department_topic_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_comment`
--
ALTER TABLE `editorial_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_feedback`
--
ALTER TABLE `editorial_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_modal_like`
--
ALTER TABLE `editorial_modal_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_modalcomment`
--
ALTER TABLE `editorial_modalcomment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_new`
--
ALTER TABLE `editorial_new`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `editorial_views`
--
ALTER TABLE `editorial_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `edp`
--
ALTER TABLE `edp`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_comment`
--
ALTER TABLE `entertainment_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_feedback`
--
ALTER TABLE `entertainment_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_modal_like`
--
ALTER TABLE `entertainment_modal_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_modalcomment`
--
ALTER TABLE `entertainment_modalcomment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_news`
--
ALTER TABLE `entertainment_news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `entertainment_views`
--
ALTER TABLE `entertainment_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `friend_list`
--
ALTER TABLE `friend_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `locator`
--
ALTER TABLE `locator`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messaging`
--
ALTER TABLE `messaging`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news`
--
ALTER TABLE `news`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_comment`
--
ALTER TABLE `news_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_feedback`
--
ALTER TABLE `news_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_modal_like`
--
ALTER TABLE `news_modal_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_modalcomment`
--
ALTER TABLE `news_modalcomment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `news_views`
--
ALTER TABLE `news_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pre_signup`
--
ALTER TABLE `pre_signup`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`),
  ADD UNIQUE KEY `id_number` (`id_number`),
  ADD UNIQUE KEY `email` (`email`);

--
-- Indexes for table `sports`
--
ALTER TABLE `sports`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_comment`
--
ALTER TABLE `sports_comment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_feedback`
--
ALTER TABLE `sports_feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_modal_like`
--
ALTER TABLE `sports_modal_like`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_modalcomment`
--
ALTER TABLE `sports_modalcomment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sports_views`
--
ALTER TABLE `sports_views`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `super_admin_info`
--
ALTER TABLE `super_admin_info`
  ADD PRIMARY KEY (`sa_id`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id_number` (`id_number`,`email`,`username`),
  ADD UNIQUE KEY `token` (`token`);

--
-- Indexes for table `user_block_list`
--
ALTER TABLE `user_block_list`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_profile_pic`
--
ALTER TABLE `user_profile_pic`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_status`
--
ALTER TABLE `user_status`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `user_id` (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `aboutus`
--
ALTER TABLE `aboutus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `add_friend`
--
ALTER TABLE `add_friend`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `admin_announcement`
--
ALTER TABLE `admin_announcement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `admin_block_list`
--
ALTER TABLE `admin_block_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `admin_delete_messages`
--
ALTER TABLE `admin_delete_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `admin_logs`
--
ALTER TABLE `admin_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `admin_messaging`
--
ALTER TABLE `admin_messaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `admin_personel`
--
ALTER TABLE `admin_personel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `announcement_comment`
--
ALTER TABLE `announcement_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `announcement_feedback`
--
ALTER TABLE `announcement_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `announcement_modal_like`
--
ALTER TABLE `announcement_modal_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `announcement_modalcomment`
--
ALTER TABLE `announcement_modalcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `block_reason`
--
ALTER TABLE `block_reason`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `campus_post`
--
ALTER TABLE `campus_post`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `campus_post_comment`
--
ALTER TABLE `campus_post_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `campus_post_feedback`
--
ALTER TABLE `campus_post_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `course`
--
ALTER TABLE `course`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `deleted_messages`
--
ALTER TABLE `deleted_messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `department`
--
ALTER TABLE `department`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `department_topic`
--
ALTER TABLE `department_topic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `department_topic_comments`
--
ALTER TABLE `department_topic_comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `department_topic_feedback`
--
ALTER TABLE `department_topic_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `editorial_comment`
--
ALTER TABLE `editorial_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `editorial_feedback`
--
ALTER TABLE `editorial_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `editorial_modal_like`
--
ALTER TABLE `editorial_modal_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `editorial_modalcomment`
--
ALTER TABLE `editorial_modalcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `editorial_new`
--
ALTER TABLE `editorial_new`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `editorial_views`
--
ALTER TABLE `editorial_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `edp`
--
ALTER TABLE `edp`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `entertainment_comment`
--
ALTER TABLE `entertainment_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entertainment_feedback`
--
ALTER TABLE `entertainment_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entertainment_modal_like`
--
ALTER TABLE `entertainment_modal_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entertainment_modalcomment`
--
ALTER TABLE `entertainment_modalcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `entertainment_news`
--
ALTER TABLE `entertainment_news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `entertainment_views`
--
ALTER TABLE `entertainment_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `friend_list`
--
ALTER TABLE `friend_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `locator`
--
ALTER TABLE `locator`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `messaging`
--
ALTER TABLE `messaging`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `news`
--
ALTER TABLE `news`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news_comment`
--
ALTER TABLE `news_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news_feedback`
--
ALTER TABLE `news_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_modal_like`
--
ALTER TABLE `news_modal_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `news_modalcomment`
--
ALTER TABLE `news_modalcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `news_views`
--
ALTER TABLE `news_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `pre_signup`
--
ALTER TABLE `pre_signup`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sports`
--
ALTER TABLE `sports`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `sports_comment`
--
ALTER TABLE `sports_comment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sports_feedback`
--
ALTER TABLE `sports_feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sports_modal_like`
--
ALTER TABLE `sports_modal_like`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sports_modalcomment`
--
ALTER TABLE `sports_modalcomment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sports_views`
--
ALTER TABLE `sports_views`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `super_admin_info`
--
ALTER TABLE `super_admin_info`
  MODIFY `sa_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `user_block_list`
--
ALTER TABLE `user_block_list`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `user_profile_pic`
--
ALTER TABLE `user_profile_pic`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user_status`
--
ALTER TABLE `user_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
