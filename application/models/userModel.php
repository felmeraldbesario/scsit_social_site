<?php

/**
 * authour: Jermaine Maturan
 * Name: UserModel
 * Date Created: December 24, 2015
 */
class userModel extends CI_Model {

    function __construct(){
        parent::__construct();
        $this->load->library('verification');
    }
    public function system_status(){
		$query = $this->db->query("SELECT status FROM system_status WHERE status=0");
		if($query->num_rows() != 0){
			$this->session->set_flashdata('system_status','This page is not available due to examination');
			redirect(base_url().'auth');
			exit();
		}
	}
    
    /**
     * Name: select_department
     * @param: null
     * return: String
     * purpose: for viewing department on select option in index
     */
    public function select_department() {
        $query = $this->db->query('SELECT id, department FROM department ORDER BY id DESC');
        return $query->result();
    }
    
    /**
     *Name: select_course
     * @param: null
     * @return: String
     * pupose: for viewing course in signup page 
     */
    public function select_course(){
        $dept_id = $this->session->userdata('pre_deptId');
        $query = $this->db->query("SELECT id, course FROM course WHERE department_id = '".$dept_id."'");
        return $query->result();
    }
    
    /**
     * Name: verification
     * @param: null
     * @return String
     * purpose: for verification
     */
    public function question(){
        return $this->verification->question();
    }

    /**
     * Name: select_department
     * @param: department_id, firstname, lastname, birthdate, age, gender, username, password
     * return: void
     * purpose: inserting pre-signup data to database
     */
    public function preSignUp($dept_id, $firstname, $middlename, $lastname, $bdate, $age, $gender, $username, $password) {
		if($this->checkUsername($username)){
				$data = array(
					'dept_id' => strtolower($dept_id),
					'firstname' => strtolower($firstname),
					'middlename' => strtolower($middlename),
					'lastname' => strtolower($lastname),
					'bdate' => $bdate,
					'age' => $age,
					'gender' => strtolower($gender),
					'username' => strtolower($username),
					'password' => md5(md5($password))
				);
				$query = $this->db->get_where('pre_signup', $data);
				if ($query->num_rows() == 0) {
					$checkUser = $this->db->query("SELECT username FROM user WHERE username='".$username."'");
					if($checkUser->num_rows() == 0){

						$this->db->insert('pre_signup', $data);
						$sesion_data = array(
							'pre_firstname' => $firstname,
							'pre_middlename' => $middlename,
							'pre_lastname' => $lastname,
							'pre_username' => $username,
							'pre_deptId' => $dept_id,
							'login' => TRUE
						);
						$this->session->set_userdata($sesion_data);
					    redirect(base_url().'signup');
						exit(0);
					}else{
						$this->session->set_flashdata('usernameExist','Username already exist..');
						redirect(base_url().'index');
					}
				} else {
					$sesion_data = array(
							'pre_firstname' => $firstname,
							'pre_lastname' => $lastname,
							'pre_username' => $username,
							'pre_deptId' => $dept_id,
							'login' => TRUE
						);
					$this->session->set_userdata($sesion_data);
					$this->session->set_flashdata('exist','Please proceed to next step...');
					//redirect(base_url().'signup');
					exit(0);
				}
		}else{
			$this->session->set_flashdata('usernameExist','username already exist....');
			redirect(base_url().'index');
		}
    }
	
	/**
     * Name: checkUsername
     * @param: username
     * return: boolean
     * purpose: check if username already exist
     */
	 
	 public function checkUsername($username){
		$query = $this->db->query("SELECT username FROM pre_signup WHERE username = '".$username."'");
		if($query->num_rows() == 0){
			return true;
		}else{
			return false;
		}
	 }


	/**
     * Name: do_upload
     * @param: year, id_number, address, mobilenumber, email, course_id, answer, user_answer, image
     * return: boolean
     * purpose: update presignup table..second step
     */
	 public function do_upload($year, $id_number, $address, $mobilenumber, $email, $course_id, $answer, $user_answer, $image){

	 	if(strtolower($answer) == strtolower($user_answer)){

	 		$data = array(
	 				'year' => $year,
	 				'id_number' => trim($id_number),
	 				'address' => $address,
	 				'mobilenumber' => $mobilenumber,
	 				'email' => $email,
	 				'course_id' => $course_id,
	 				'image' => $image
	 			);

	 		$this->db->where('username',$this->session->userdata('pre_username'));
	 		$this->db->update('pre_signup', $data);
	 		$check = $this->checkEdp($this->session->userdata('pre_deptId'), $course_id, $this->session->userdata('pre_firstname'), $this->session->userdata('pre_middlename'),$this->session->userdata('pre_lastname'), $id_number);
			if($check){

					$insertVerification = array(
							'verification' => $this->verification->verificationCode()
						);
					$this->db->where('username',$this->session->userdata('pre_username'));
					$this->db->update('pre_signup', $insertVerification);
					
					redirect(base_url().'verification');				
			}else{
				
				$this->db->where('username', $this->session->userdata('pre_username'));
				$query = $this->db->delete('pre_signup');
				$this->session->sess_destroy();
				redirect(base_url().'index');

			}
	 	}else{
	 		
	 		$this->session->set_flashdata('wrong', 'Incorrect answer..');
	 		redirect(base_url().'signup');
	 		exit(0);
	 	}
	 }


	 /**
     * Name: checkEdp
     * @param: department, course, firstname, lastname, id_number
     * return: boolean
     * purpose: check user to edp database if he/she is an official scsit personnel
     */

     public function checkEdp($department_id, $course_id, $firstname, $middlename, $lastname, $id_number){
     	$departmentQuery = $this->db->query("SELECT department FROM department WHERE id='".$department_id."'");
     	$departmentResult = $departmentQuery->row();
     	$dept = $departmentResult->department;
		
		$courseQuery = $this->db->query("SELECT course FROM course WHERE id='".$course_id."'");
		$courseResult = $courseQuery->row();
		$course = $courseResult->course;
		
		$data = array(
				'department' => strtolower($dept),
				'course' => strtolower($course),
				'id_number' => trim($id_number),
				'firstname' => strtolower($firstname),
				'middlename' => strtolower($middlename),
				'lastname' => strtolower($lastname)
		);
		
		$query = $this->db->get_where('edp', $data);
		if($query->num_rows() == 0){
			return false;
		}else{
			return true;
		}
     }


      /**
     * Name: verify
     * @param: verificationCode
     * return: void
     * purpose: check verification match
     */
     public function verify($verificationCode){
     	$username = $this->session->userdata('pre_username');
     	$query = $this->db->query("SELECT * FROM pre_signup WHERE username='".$username."'");
     	$result = $query->row();
     	if($result->verification == $verificationCode){
     		date_default_timezone_set('Asia/Manila');
     		$date_created = date("d-m-Y");
     		$data = array(
						'id_number' => $result->dept_id,
						'firstname' => $result->firstname,
						'lastname' =>  $result->lastname,
						'middlename' => $result->middlename,
						'age' => $result->age,
						'gender' => $result->gender,
						'address' => $result->address,
						'birthdate' => $result->bdate,
						'email' => $result->email,
						'course' => $result->course_id,
						'year' => $result->year,
						'mobile_number'=> $result->mobilenumber,
						'username' => $result->username,
						'password' => $result->password,
						'department_id'=> $result->dept_id,
						'token' => $this->verification->tokenizer(50),
						'date_created' => $date_created
					);

     		$checkUser = $this->db->get_where('user', $data);
     		if($checkUser->num_rows() == 0){
     			$this->db->insert('user', $data);
     			$this->userSession($result->username, $date_created);
     		}else{
     			$this->session->set_flashdata('alreadyExistUser','You have already an account on this site...');
     			redirect(base_url().'index');
     		}
     	}else{
     		$this->session->set_flashdata('IncorrectVerification', 'Incorrect verification code..Please check your email..');
     		redirect(base_url().'verification');
     	}
     }

     /**
     * Name: userSession
     * @param: username
     * return: void
     * purpose: sessioning on user data
     */
     public function userSession($username, $date_created){
     	//$this->session->sess_destroy();
     	$checkImage = $this->db->query("SELECT image FROM pre_signup WHERE username='".$username."'");
     	$imageResult = $checkImage->row();

     	$query = $this->db->query("SELECT id, username ,token FROM user WHERE username='".$username."'");
	    $result = $query->row();

	    if($imageResult->image != ''){
	    	$imageData = array(
	    			'user_id' => $result->id,
	    			'image' => $imageResult->image,
	    			'date' => $date_created
	    		);

	    	$this->db->insert('user_profile_pic', $imageData);
	    }

	    $sess_data = array(
	    		'login_id' => $result->id,
	    		'login_token' => $result->token,
	    		'login' => TRUE
	    	);

	    $this->db->where('username', $username);
	    $this->db->delete('pre_signup');
	    $this->session->set_userdata($sess_data);
	    $status = array(
	    		'user_id' => $result->id,
	    		'status' =>1
	    	);
	    $this->db->insert('user_status', $status);
	    redirect(base_url().'home');

     }

      /**
     * Name: login
     * @param: username, password
     * return: void
     * purpose:for login
	*/
     public function login($username, $password){
     	$this->system_status();
     	$data = array(
     			'username' => $username,
     			'password' => md5(md5($password))
     		);

     	$query = $this->db->get_where('user', $data);
     	if($query->num_rows() == 0){
     		$this->session->set_flashdata("incorrectPassOrUsername", "Incorrect password or username...");
     		redirect(base_url().'auth');
     	}else{
     		$token = array(
     				'token' => $this->verification->tokenizer(50)
     			);
     		$result = $query->row();
     		$checkAdminBlock = $this->db->query("SELECT blocked_id FROM admin_block_list WHERE blocked_id='".$result->id."'");
     		if($checkAdminBlock->num_rows() != 0){
     			$this->session->set_flashdata("admin_block", "Your account has been blocked by your admin..Please contact you admin immediately..");
     			redirect(base_url().'auth');
     		}
     		$this->db->where('id', $result->id);
     		$this->db->update('user', $token);
     		
     		$userQuery = $this->db->query("SELECT id, username, token FROM user WHERE id='".$result->id."'");
     		$resultQuery = $userQuery->row();
     		$session_data = array(
     				'login_id' => $resultQuery->id,
     				'login_token' => $resultQuery->token
     			);
     		$this->status($resultQuery->id, 1);
     		$this->session->set_userdata($session_data);
     		redirect(base_url().'home');
     		exit(0);
     	}
     }

	 /**
     * Name: status
     * @param: user_id, status
     * return: void
     * purpose:for update or insert status
	*/
	 public function status($user_id, $status){
	 	$data = array(
	 			'user_id' => $user_id,
	 			'status' => $status
	 		);
	 	$this->db->where('user_id', $user_id);
	 	$query = $this->db->get('user_status');
	 	if($query->num_rows() == 0){
	 		$this->db->insert('user_status', $data);
	 	}else{
	 		$this->db->where('user_id', $user_id);
	 		$this->db->update('user_status', $data);
	 	}
	 }


}	
