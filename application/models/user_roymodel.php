<?php

class user_roymodel extends CI_Model{

	function login_auth($data){

		$username = $data['username'];
		$password = $data['password'];

		$this->db->where('username',$username);
		$this->db->where('password',$password);
		$selectUser = $this->db->get('user');
		if($selectUser->num_rows() > 0){

			foreach ($selectUser->result() as $userRow) {
				
				$user_id = $userRow->id;
			}
				$sessionData = array('login_id'=>$user_id,
									 'logged_in'=>TRUE);
				$this->session->set_userdata($sessionData);

				redirect(base_url().'home');
		}else{

			redirect(base_url().'index');
		}
	}
	// *news functionalities*
	function countviews($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$news_views = $this->db->get('news_views');
		if($news_views->num_rows() == 0){

			$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
			$insert_views = $this->db->insert('news_views',$data);
			if(isset($insert_views)){

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('news_views');
			}
		}else{

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('news_views');
		}
	}
	function countcomments($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$view_comments = $this->db->get('news_modalcomment');
			//num_rows function para ma determined if ni exist na sa news_modalcomment
			if($view_comments->num_rows() == 0){
				//insert if wala pa sa table nga news_modalcomment
				$data_newsmodal = array('topic_id'=>$topic_id,
										'user_id'=>$user_id);
				$insert_modalcomment = $this->db->insert('news_modalcomment',$data_newsmodal);
				if(isset($insert_modalcomment)){

					$this->db->where('topic_id',$topic_id);
					echo $countcomments = $this->db->count_all_results('news_modalcomment');
				}
			}else{

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('news_modalcomment');
			}
	}
	function uncountcomment($data){

		//$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		//$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectNewscomment = $this->db->get('news_comment');
		// if true wala nay ni exist nga comment ang
		// user sa table nga news_comment atung
		// e delete pud sa news_modalcomment nga table
		// ang user nga wala na mo exist sa 
		// news_comment table
		if($selectNewscomment->num_rows() == 0){

			// e delete ang topic_id og user_id sa news_modalcomment nga table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$uncountcomment = $this->db->delete('news_modalcomment');
			if(isset($uncountcomment)){

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('news_modalcomment');
			}
		}else{
				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('news_modalcomment');

		}
	}
	function insert_like($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectLike = $this->db->get('news_feedback');

		if($selectLike->num_rows == 1){
			//if true ni dislike ang user e delete ang data sa news_feedback table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('news_feedback');
			//if true ni dislike ang user e update ang news_dislike table ang value kay 0
			// $data = array('topic_id'=>$topic_id,
			// 			  'user_id'=>$user_id,
			// 			  'like'=>0);
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('news_modal_like');
			//e echo ang result
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('news_feedback');

		}else{
			//else false ni like ang user e insert sa news_feedback table
			$data = array('topic_id'=>$topic_id,
						  'user_id'=>$user_id);
			$this->db->insert('news_feedback',$data);
			//if true ni like ang user e insert ang data sa news_dislike table ang value kay 1
			$news_like = array('topic_id'=>$topic_id,
							   'user_id'=>$user_id);
			$this->db->insert('news_modal_like',$news_like);
			//e echo ang result 	
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('news_feedback');
			
		}
	}
	function insert_comment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comments'=>$comment,
					  'date'=>$date);
		$this->db->insert('news_comment',$data);
	}
	function select_twocommentnews(){

		$this->db->select('news_comment.id as id, news_comment.topic_id as topic_id, news_comment.comments as comment, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('news_comment');
        $this->db->join('user','user.id = news_comment.user_id');
        $this->db->order_by('id','asc');
        // $this->db->limit(2);
        // $this->db->where('topic_id',$Row_news->news_id);
        $selectTwocommentnews = $this->db->get();

        return $selectTwocommentnews;
	}
	function callwhocomment_modal($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('news_modalcomment.id as id, news_modalcomment.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('news_modalcomment');
        $this->db->join('user','user.id = news_modalcomment.user_id');
        $this->db->where("news_modalcomment.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $Allcommentnews = $this->db->get();

        return $Allcommentnews->result();
	}
	function callwholikes_modal($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('news_modal_like.id as id, news_modal_like.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('news_modal_like');
        $this->db->join('user','user.id = news_modal_like.user_id');
        $this->db->where("news_modal_like.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $All_likenews = $this->db->get();

        return $All_likenews->result();
	}
	function select_likes(){

		$this->db->select('news_modal_like.*, user.*');
		$this->db->from('news_modal_like');
		$this->db->join('user','user.id = news_modal_like.user_id','left');
		$this->db->order_by('news_modal_like.id','asc');
		$select_newslike = $this->db->get();

		return $select_newslike;
	}
	function update_comment($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];

		$data = array('comments'=>$comment,
					  'date'=>$date);

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$editNewscomment = $this->db->update('news_comment',$data);
		if(isset($editNewscomment)){

			return true;
		}else{
			return false;
		}
	}
	function delete_comment($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$this->db->delete('news_comment');
		
	}
	// end sa news functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ 
	// *sports functionalities*
	function insert_sportscomment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comments'=>$comment,
					  'date'=>$date);
		$this->db->insert('sports_comment',$data);
	}
	function sportscountcomments($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$view_comments = $this->db->get('sports_modalcomment');
			//num_rows function para ma determined if ni exist na sa news_modalcomment
			if($view_comments->num_rows() == 0){
				//insert if wala pa sa table nga news_modalcomment
				$data_sportsmodal = array('topic_id'=>$topic_id,
										'user_id'=>$user_id);
				$insert_modalcomment = $this->db->insert('sports_modalcomment',$data_sportsmodal);
				if(isset($insert_modalcomment)){

					$this->db->where('topic_id',$topic_id);
					echo $countcomments = $this->db->count_all_results('sports_modalcomment');
				}
			}else{

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('sports_modalcomment');
			}
	}
	function update_commentsports($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];

		$data = array('comments'=>$comment,
					  'date'=>$date);

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$editSportscomment = $this->db->update('sports_comment',$data);
		if(isset($editSportscomment)){

			return true;
		}else{
			return false;
		}
	}
	function delete_commentsports($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$this->db->delete('sports_comment');
		
	}
	function uncountcommentsports($data){

		//$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		//$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectSportscomment = $this->db->get('sports_comment');
		// if true wala nay ni exist nga comment ang
		// user sa table nga news_comment atung
		// e delete pud sa news_modalcomment nga table
		// ang user nga wala na mo exist sa 
		// news_comment table
		if($selectSportscomment->num_rows() == 0){

			// e delete ang topic_id og user_id sa news_modalcomment nga table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$uncountcomment = $this->db->delete('sports_modalcomment');
			if(isset($uncountcomment)){

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('sports_modalcomment');
			}
		}else{
				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('sports_modalcomment');

		}
	}
	function insert_likesports($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectLike = $this->db->get('sports_feedback');

		if($selectLike->num_rows == 1){
			//if true ni dislike ang user e delete ang data sa news_feedback table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('sports_feedback');
			//if true ni dislike ang user e update ang news_dislike table ang value kay 0
			// $data = array('topic_id'=>$topic_id,
			// 			  'user_id'=>$user_id,
			// 			  'like'=>0);
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('sports_modal_like');
			//e echo ang result
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('sports_feedback');

		}else{
			//else false ni like ang user e insert sa news_feedback table
			$data = array('topic_id'=>$topic_id,
						  'user_id'=>$user_id);
			$this->db->insert('sports_feedback',$data);
			//if true ni like ang user e insert ang data sa news_dislike table ang value kay 1
			$news_like = array('topic_id'=>$topic_id,
							   'user_id'=>$user_id);
			$this->db->insert('sports_modal_like',$news_like);
			//e echo ang result 	
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('sports_feedback');
			
		}
	}
	function callwholikes_modalsports($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('sports_modal_like.id as id, sports_modal_like.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('sports_modal_like');
        $this->db->join('user','user.id = sports_modal_like.user_id');
        $this->db->where("sports_modal_like.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $All_likesports = $this->db->get();

        return $All_likesports->result();
	}
	function callwhocomment_modalsports($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('sports_modalcomment.id as id, sports_modalcomment.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('sports_modalcomment');
        $this->db->join('user','user.id = sports_modalcomment.user_id');
        $this->db->where("sports_modalcomment.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $Allcommentsports = $this->db->get();

        return $Allcommentsports->result();
	}
	function countviews_sports($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$news_views = $this->db->get('sports_views');
		if($news_views->num_rows() == 0){

			$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
			$insert_views = $this->db->insert('sports_views',$data);
			if(isset($insert_views)){

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('sports_views');
			}
		}else{

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('sports_views');
		}
	}
	// end sa sports functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// *entertainment functionalities*
	function insert_entertainmentcomment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comments'=>$comment,
					  'date'=>$date);
		$this->db->insert('entertainment_comment',$data);
	}
	function entertainmentcountcomments($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$view_comments = $this->db->get('entertainment_modalcomment');
			//num_rows function para ma determined if ni exist na sa news_modalcomment
			if($view_comments->num_rows() == 0){
				//insert if wala pa sa table nga news_modalcomment
				$data_entertainmentmodal = array('topic_id'=>$topic_id,
										'user_id'=>$user_id);
				$insert_modalcomment = $this->db->insert('entertainment_modalcomment',$data_entertainmentmodal);
				if(isset($insert_modalcomment)){

					$this->db->where('topic_id',$topic_id);
					echo $countcomments = $this->db->count_all_results('entertainment_modalcomment');
				}
			}else{

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('entertainment_modalcomment');
			}
	}
	function update_commententertainment($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];

		$data = array('comments'=>$comment,
					  'date'=>$date);

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$editEntertainmentcomment = $this->db->update('entertainment_comment',$data);
		if(isset($editEntertainmentcomment)){

			return true;
		}else{
			return false;
		}
	}
	function delete_commententertainment($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$this->db->delete('entertainment_comment');
		
	}
	function uncountcommententertainment($data){

		//$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		//$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectEntertainmentcomment = $this->db->get('entertainment_comment');
		// if true wala nay ni exist nga comment ang
		// user sa table nga news_comment atung
		// e delete pud sa news_modalcomment nga table
		// ang user nga wala na mo exist sa 
		// news_comment table
		if($selectEntertainmentcomment->num_rows() == 0){

			// e delete ang topic_id og user_id sa news_modalcomment nga table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$uncountcomment = $this->db->delete('entertainment_modalcomment');
			if(isset($uncountcomment)){

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('entertainment_modalcomment');
			}
		}else{
				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('entertainment_modalcomment');

		}
	}
	function insert_likeentertainment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectLike = $this->db->get('entertainment_feedback');

		if($selectLike->num_rows == 1){
			//if true ni dislike ang user e delete ang data sa news_feedback table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('entertainment_feedback');
			//if true ni dislike ang user e update ang news_dislike table ang value kay 0
			// $data = array('topic_id'=>$topic_id,
			// 			  'user_id'=>$user_id,
			// 			  'like'=>0);
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('entertainment_modal_like');
			//e echo ang result
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('entertainment_feedback');

		}else{
			//else false ni like ang user e insert sa news_feedback table
			$data = array('topic_id'=>$topic_id,
						  'user_id'=>$user_id);
			$this->db->insert('entertainment_feedback',$data);
			//if true ni like ang user e insert ang data sa news_dislike table ang value kay 1
			$news_like = array('topic_id'=>$topic_id,
							   'user_id'=>$user_id);
			$this->db->insert('entertainment_modal_like',$news_like);
			//e echo ang result 	
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('entertainment_feedback');
			
		}
	}
	function callwholikes_modalentertainment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('entertainment_modal_like.id as id, entertainment_modal_like.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('entertainment_modal_like');
        $this->db->join('user','user.id = entertainment_modal_like.user_id');
        $this->db->where("entertainment_modal_like.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $All_likeentertainment = $this->db->get();

        return $All_likeentertainment->result();
	}
	function callwhocomment_modalentertainment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('entertainment_modalcomment.id as id, entertainment_modalcomment.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('entertainment_modalcomment');
        $this->db->join('user','user.id = entertainment_modalcomment.user_id');
        $this->db->where("entertainment_modalcomment.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $Allcommententertainment = $this->db->get();

        return $Allcommententertainment->result();
	}
	function countviews_entertainment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$entertainment_views = $this->db->get('entertainment_views');
		if($entertainment_views->num_rows() == 0){

			$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
			$insert_views = $this->db->insert('entertainment_views',$data);
			if(isset($insert_views)){

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('entertainment_views');
			}
		}else{

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('entertainment_views');
		}
	}
	// end sa entertainment functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// *editorial functionalities*
	function insert_editorialcomment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comments'=>$comment,
					  'date'=>$date);
		$this->db->insert('editorial_comment',$data);
	}
	function editorialcountcomments($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$view_comments = $this->db->get('editorial_modalcomment');
			//num_rows function para ma determined if ni exist na sa news_modalcomment
			if($view_comments->num_rows() == 0){
				//insert if wala pa sa table nga news_modalcomment
				$data_editorialmodal = array('topic_id'=>$topic_id,
										'user_id'=>$user_id);
				$insert_modalcomment = $this->db->insert('editorial_modalcomment',$data_editorialmodal);
				if(isset($insert_modalcomment)){

					$this->db->where('topic_id',$topic_id);
					echo $countcomments = $this->db->count_all_results('editorial_modalcomment');
				}
			}else{

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('editorial_modalcomment');
			}
	}
	function update_commenteditorial($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];

		$data = array('comments'=>$comment,
					  'date'=>$date);

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$editEditorialcomment = $this->db->update('editorial_comment',$data);
		if(isset($editEditorialcomment)){

			return true;
		}else{
			return false;
		}
	}
	function delete_commenteditorial($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$this->db->delete('editorial_comment');
		
	}
	function uncountcommenteditorial($data){

		//$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		//$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectEditorialcomment = $this->db->get('editorial_comment');
		// if true wala nay ni exist nga comment ang
		// user sa table nga news_comment atung
		// e delete pud sa news_modalcomment nga table
		// ang user nga wala na mo exist sa 
		// news_comment table
		if($selectEditorialcomment->num_rows() == 0){

			// e delete ang topic_id og user_id sa news_modalcomment nga table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$uncountcomment = $this->db->delete('editorial_modalcomment');
			if(isset($uncountcomment)){

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('editorial_modalcomment');
			}
		}else{
				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('editorial_modalcomment');

		}
	}
	function insert_likeeditorial($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectLike = $this->db->get('editorial_feedback');

		if($selectLike->num_rows == 1){
			//if true ni dislike ang user e delete ang data sa news_feedback table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('editorial_feedback');
			//if true ni dislike ang user e update ang news_dislike table ang value kay 0
			// $data = array('topic_id'=>$topic_id,
			// 			  'user_id'=>$user_id,
			// 			  'like'=>0);
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('editorial_modal_like');
			//e echo ang result
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('editorial_feedback');

		}else{
			//else false ni like ang user e insert sa news_feedback table
			$data = array('topic_id'=>$topic_id,
						  'user_id'=>$user_id);
			$this->db->insert('editorial_feedback',$data);
			//if true ni like ang user e insert ang data sa news_dislike table ang value kay 1
			$news_like = array('topic_id'=>$topic_id,
							   'user_id'=>$user_id);
			$this->db->insert('editorial_modal_like',$news_like);
			//e echo ang result 	
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('editorial_feedback');
			
		}
	}
	function callwholikes_modaleditorial($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('editorial_modal_like.id as id, editorial_modal_like.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('editorial_modal_like');
        $this->db->join('user','user.id = editorial_modal_like.user_id');
        $this->db->where("editorial_modal_like.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $All_likeeditorial = $this->db->get();

        return $All_likeeditorial->result();
	}
	function callwhocomment_modaleditorial($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('editorial_modalcomment.id as id, editorial_modalcomment.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('editorial_modalcomment');
        $this->db->join('user','user.id = editorial_modalcomment.user_id');
        $this->db->where("editorial_modalcomment.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $Allcommenteditorial = $this->db->get();

        return $Allcommenteditorial->result();
	}
	function countviews_editorial($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$editorial_views = $this->db->get('editorial_views');
		if($editorial_views->num_rows() == 0){

			$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
			$insert_views = $this->db->insert('editorial_views',$data);
			if(isset($insert_views)){

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('editorial_views');
			}
		}else{

			$this->db->where('topic_id',$topic_id);
			echo $countviews = $this->db->count_all_results('editorial_views');
		}
	}
	// end sa editorial functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//*announcement functionalities*
	function select_oneannouncement(){

		$count = 0;
		$select_announcement = $this->db->get('admin_announcement');
		$select_allcomments = $this->db->get('announcement_comment');
		$count_all = $select_allcomments->num_rows();
		foreach ($select_announcement->result() as $Rowannouncement) {
			
			$this->db->select('announcement_comment.*,user.firstname,user.lastname,user_profile_pic.image');
			$this->db->from('announcement_comment');
			$this->db->join('user','user.id = announcement_comment.user_id','left');
			$this->db->join('user_profile_pic','user_profile_pic.user_id = announcement_comment.user_id','left');
			$this->db->where('topic_id',$Rowannouncement->id);
			$this->db->limit(1);
			$select_onecomment = $this->db->get();
			foreach ($select_onecomment->result() as $Row_oneannouncement) {
				
				$count++;
				$select_oneannouncement[$count] = array('id'=>$Row_oneannouncement->id,
									  'topic_id'=>$Row_oneannouncement->topic_id,
									  'user_id'=>$Row_oneannouncement->user_id,
									  'comment'=>$Row_oneannouncement->comments,
									  'date'=>$Row_oneannouncement->date,
									  'firstname'=>$Row_oneannouncement->firstname,
									  'lastname'=>$Row_oneannouncement->lastname,
									  'image'=>$Row_oneannouncement->image);
			}
		}
		if($count_all == 0){

			return 0;
		}else{

			return $select_oneannouncement;
		}
	}
	function select_allannouncement(){

		$count = 0;
		$select_announcement = $this->db->get('admin_announcement');
		$select_allcomments = $this->db->get('announcement_comment');
		$count_all = $select_allcomments->num_rows();
		foreach ($select_announcement->result() as $Rowannouncement) {
			
			$topic_id = $Rowannouncement->id;
			$select_allcomment = $this->db->query("SELECT announcement_comment.*, user.firstname, user.lastname, user_profile_pic.image FROM announcement_comment LEFT JOIN user ON user.id = announcement_comment.user_id LEFT JOIN user_profile_pic ON user_profile_pic.user_id = announcement_comment.user_id WHERE topic_id = '$topic_id' LIMIT $count_all OFFSET 1");
			if($select_allcomment->num_rows() == 0){

				$count++;
				$select_allannouncement[$count] = 0;
			}else{
					foreach ($select_allcomment->result() as $Row_allannouncement) {
					
					$count++;
						$select_allannouncement[$count] = array('id'=>$Row_allannouncement->id,
										  'topic_id'=>$Row_allannouncement->topic_id,
										  'user_id'=>$Row_allannouncement->user_id,
										  'comment'=>$Row_allannouncement->comments,
										  'date'=>$Row_allannouncement->date,
										  'firstname'=>$Row_allannouncement->firstname,
										  'lastname'=>$Row_allannouncement->lastname,
										  'image'=>$Row_allannouncement->image);
					
				}
			}
		}
			return $select_allannouncement;
			// echo "<pre>";
			// print_r($select_allannouncement);
			// die();
	}
	function insert_announcementcomment($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$comment = $data['comment'];
		$date = $data['date'];
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comments'=>$comment,
					  'date'=>$date);
		$result = $this->db->insert('announcement_comment',$data);
	}
	function announcementcountcomments($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$view_comments = $this->db->get('announcement_modalcomment');
			//num_rows function para ma determined if ni exist na sa news_modalcomment
			if($view_comments->num_rows() == 0){
				//insert if wala pa sa table nga news_modalcomment
				$data_announcementmodal = array('topic_id'=>$topic_id,
										'user_id'=>$user_id);
				$insert_modalcomment = $this->db->insert('announcement_modalcomment',$data_announcementmodal);
				if(isset($insert_modalcomment)){

					$this->db->where('topic_id',$topic_id);
					echo $countcomments = $this->db->count_all_results('announcement_modalcomment');
				}
			}else{

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('announcement_modalcomment');
			}
	}
	function callwhocomment_modalannouncement($topic_id){

		$topic_ids = $topic_id;
		$this->db->select('announcement_modalcomment.id,user.firstname,user.lastname');
		$this->db->from('announcement_modalcomment');
		$this->db->join('user','user.id = announcement_modalcomment.user_id','left');
		$this->db->where('topic_id',$topic_ids);
		$select_announcement_modalcomment = $this->db->get();

		return $select_announcement_modalcomment;
	}
	function insert_likeannouncement($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectLike = $this->db->get('announcement_feedback');

		if($selectLike->num_rows == 1){
			//if true ni dislike ang user e delete ang data sa news_feedback table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('announcement_feedback');
			//if true ni dislike ang user e update ang news_dislike table ang value kay 0
			// $data = array('topic_id'=>$topic_id,
			// 			  'user_id'=>$user_id,
			// 			  'like'=>0);
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$this->db->delete('announcement_modal_like');
			//e echo ang result
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('announcement_feedback');

		}else{
			//else false ni like ang user e insert sa news_feedback table
			$data = array('topic_id'=>$topic_id,
						  'user_id'=>$user_id);
			$this->db->insert('announcement_feedback',$data);
			//if true ni like ang user e insert ang data sa news_dislike table ang value kay 1
			$news_like = array('topic_id'=>$topic_id,
							   'user_id'=>$user_id);
			$this->db->insert('announcement_modal_like',$news_like);
			//e echo ang result 	
			$this->db->where('topic_id',$topic_id);
			echo $likes = $this->db->count_all_results('announcement_feedback');
			
		}
	}
	function callwholikes_modalannouncement($data){

		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->select('announcement_modal_like.id as id, announcement_modal_like.topic_id as topic_id, user.firstname as firstname, user.lastname as lastname, user.id as id_user');
        $this->db->from('announcement_modal_like');
        $this->db->join('user','user.id = announcement_modal_like.user_id');
        $this->db->where("announcement_modal_like.topic_id",$topic_id);
        $this->db->order_by('id','asc');
        $this->db->group_by('user.id',$user_id);
        $All_likeannouncement = $this->db->get();

        return $All_likeannouncement->result();
	}
	function callmodal_update($data){

		$id = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('id',$id);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$select_announcement_comment = $this->db->get('announcement_comment');

		return $select_announcement_comment->result();
	}
	function update_commentannouncement($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$comment = $data['comment'];
		$date = $data['date'];

		$data = array('comments'=>$comment,
					  'date'=>$date);

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$editEditorialcomment = $this->db->update('announcement_comment',$data);
		if(isset($editEditorialcomment)){

			return true;
		}else{
			return false;
		}
	}
	function callmodal_delete($data){

		$count = 0;
		$id = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];
		$this->db->where('id',$id);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$select_announcement_comment = $this->db->get('announcement_comment');
		if(!isset($select_announcement_comment)){

			$count++;
			$data_announcement_comment[$count] = 0;
		}else{

			foreach ($select_announcement_comment->result() as $Rowcomment) {

			$count++;
			$this->db->where('topic_id',$topic_id);
			$count_all = $this->db->get('announcement_comment');
			$num_of_comments = $count_all->num_rows();
			$data_announcement_comment[$count] = array('id'=>$Rowcomment->id,
													   'topic_id'=>$Rowcomment->topic_id,
													   'num_of_comments'=>$num_of_comments);
			}
		}

		return $data_announcement_comment;
		// echo "<pre>";
		// print_r($data_announcement_comment);
		// die();
	}
	function delete_commentannouncement($data){

		$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$this->db->delete('announcement_comment');
		
	}
	function announcementuncountcomments($data){

		//$id_comment = $data['id_comment'];
		$topic_id = $data['topic_id'];
		$user_id = $data['user_id'];

		//$this->db->where('id',$id_comment);
		$this->db->where('topic_id',$topic_id);
		$this->db->where('user_id',$user_id);
		$selectAnnouncementcomment = $this->db->get('announcement_comment');
		// if true wala nay ni exist nga comment ang
		// user sa table nga news_comment atung
		// e delete pud sa news_modalcomment nga table
		// ang user nga wala na mo exist sa 
		// news_comment table
		if($selectAnnouncementcomment->num_rows() == 0){

			// e delete ang topic_id og user_id sa news_modalcomment nga table
			$this->db->where('topic_id',$topic_id);
			$this->db->where('user_id',$user_id);
			$uncountcomment = $this->db->delete('announcement_modalcomment');
			if(isset($uncountcomment)){

				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('announcement_modalcomment');
			}
		}else{
				$this->db->where('topic_id',$topic_id);
				echo $countcomments = $this->db->count_all_results('announcement_modalcomment');

		}
	}
	// end sa announcement functionalities
}
?>