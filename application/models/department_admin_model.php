<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class department_admin_model extends CI_Model
 {

 	// @param
 	// login with password is MD5
 	// if user name and password matched to thier username and password user can access their account
 	// if username and password doesn't matched to thier username and password user will be redirect to admin login in other words user can't login
 	// using flash data if false the flash data will print Invalid Username or Password..Register First
public function login($username,$password){
		$data = array(
			'username'=>$username,
			'password'=>md5(md5($password))
			);
	
		$admin_query = $this->db->get_where('admin_personel',$data);
		if($admin_query->num_rows() != 0){

			foreach($admin_query->result() as $admin_row){
				$user_id=$admin_row->id;
				$sessionData = array(
						'login_id'=>$admin_row->id,
						'login_username'=>$admin_row->username,
						'login_password'=>$admin_row->password,
						'login'=>TRUE
					);
				$this->session->set_userdata($sessionData);

				$this->session->set_userdata(array('login_welcome'=>'Welcome! Administrator'));

				date_default_timezone_set('Asia/Manila');
   				$date_login=date('h:i:s a m/d/y');
				$data = array(
					'admin_id'=>$user_id,
					'date_login'=>$date_login
					);
				$this->db->insert('admin_logs',$data);
				redirect(base_url().'administrator');
			}
				
		}


		else{

			 $this->session->set_userdata(array('loginError'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-warning-sign"></span> Error..! Invalid Username or Password..Register First</div>'));  
			 redirect(base_url().'admin_login');
		}	




        }

  
  
 		// registration for dept_admin
		// @param
        // admin registration
        // insert data to admin personel and password encrepted to MD5
        // using flash data if success registration process to success.php page and flash data will print You are now Register, Login now..!
public function admin_registration($fname,$lname,$Mname,$username,$password,$email,$assigned_department,$admin_id_number,$image,$date){
	$data = array(
			'fname'=>$fname,
			'lname'=>$lname,
			'Mname'=>$Mname,
			'username'=>$username,
			'password'=>md5(md5($password)),
			'email'=>$email,
			'assigned_department'=>$assigned_department,
			'admin_id_number'=>$admin_id_number,
			'image'=>$image,
			'date'=>$date

			);
		$this->db->insert('admin_personel',$data);
		$this->session->set_flashdata(array('reg_success'=>'<div class="alert alert-sucess alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> You are now Register, Login now..!</div>'));
		redirect(base_url().'success');
}
	// @param
	// inserting department message to messaging table
	// where the ID is in the array
public function dept_message($receiver_id,$sender_id,$message,$image,$date){
	date_default_timezone_set('Asia/Manila');
    $date=date('h:i:s a m/d/y');
	$data = array(
		'receiver_id'=>$receiver_id,
		'sender_id'=>$sender_id,
		'message'=>$message,
		'image'=>$image,
		'date'=>$date
		);
	$this->db->insert('admin_messaging',$data);
	$this->session->set_userdata(array('costumer_message_sent'=>'Message successfuly sent.'));            
	redirect(base_url().'department_message');
	exit();
}
	// @param
	// message forward or insert message forward
	// with flash data
public function dept_message_forward($receiver_id,$sender_id,$message,$image,$date_forward,$date){
	date_default_timezone_set('Asia/Manila');
    $date=date('h:i:s a m/d/y');
    $date_forward=date('h:i:s a m/d/y');

$data = array(
		'receiver_id'=>$receiver_id,
		'sender_id'=>$sender_id,
		'message'=>$message,
		'image'=>$image,
		'date_forward'=>$date_forward,
		'date' =>$date
		);
	$this->db->insert('admin_messaging',$data);
	$this->session->set_flashdata(array('forward_success'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Message Forwarded Successfully !</div>'));           
	redirect(base_url().'department_message');
	exit();
}
	public function reply_message($receiver_id,$sender_id,$message,$image,$date){
		date_default_timezone_get('Asia/Manila');
		$date=date('h:i:s a m/d/y');
		$data = array(
			'receiver_id'=>$receiver_id,
			'sender_id'=>$sender_id,
			'message'=>$message,
			'image'=>$image,
			'date'=>$date
			);
		$this->db->insert('admin_messaging',$data);
		$this->session->set_flashdata(array('reply_success'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Message successfully send </div>'));
		redirect(base_url().'department_message');
		exit();
	}
// insert
// @param
// insert course to department selected to course table
// with userdata like flash data
// the alert design is in the page
	public function add_course($department_id,$course,$date_added){
		date_default_timezone_set('Asia/Manila');
        $date_added=date('h:i:s a m/d/y');
		$data=array(
			'department_id'=>$department_id,
			'course'=>$course,
			'date_added'=>$date_added
			);
		$this->db->insert('course',$data);
		$this->session->set_userdata(array('course_success_added'=>'Success.! Course Added'));
		redirect(base_url().'department_selected');

	}
		// @param
		// inserting news to news table
		// with userdata. alert html is in the page
	public function insert_news($admin_id,$title,$news_body,$image,$date){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');

		$data=array(
			'admin_id'=>$admin_id,
			'title'=>$title,
			'news_body'=>$news_body,
			'image'=>$image,
			'date'=>$date
			);
		$this->db->insert('news',$data);
		$this->session->set_userdata(array('news_inserting'=>'One data successfuly inserted.'));  
		redirect(base_url().'admin_news');
		exit();

	}
	// @param
	// inserting data for intertainment
	// insert to entertainment_news table
	// with userdata alert html design is in the page
	public function insert_intertainment($admin_id,$title,$entertainment,$image,$date){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'admin_id'=>$admin_id,
			'title'=>$title,
			'entertainment'=>$entertainment,
			'image'=>$image,
			'date'=>$date
			);
		$this->db->insert('entertainment_news',$data);
		$this->session->set_userdata(array('entertainment_inserting_data'=>'Success.! One Data Added.'));
		redirect(base_url().'admin_entertainment');
		exit();

	}// @param
	// inserting data for sports
	// insert to sports table
	// with userdata alert html design is in the page
	public function insert_sports($admin_id,$title,$sports_body,$image,$date){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'admin_id'=>$admin_id,
			'title'=>$title,
			'sports_body'=>$sports_body,
			'image'=>$image,
			'date'=>$date
			);
		$this->db->insert('sports',$data);
		$this->session->set_userdata(array('sports_added'=>'Success! One Data Added!'));
		redirect(base_url().'admin_sports');
		exit();

	}
	// @param
	// inserting data for editorial
	// insert to editorial_new table
	// with userdata alert html design is in the page
	public function insert_editorial($admin_id,$title,$editorial,$image,$date){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'admin_id'=>$admin_id,
			'title'=>$title,
			'editorial'=>$editorial,
			'image'=>$image,
			'date'=>$date
			);
		$this->db->insert('editorial_new',$data);
		$this->session->set_userdata(array('editorial_added_data'=>'Success.! One Data Added'));
		redirect(base_url().'admin_editorial');
		exit();
	}
	// @param
	// insertin data for announcement to admin_announcement table
	// with flashdata
	public function publish_announcement($admin_id,$announcement,$date){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');

		$data = array(
			'admin_id'=>$admin_id,
			'announcement'=>$announcement,
			'date'=>$date
			);
		$this->db->insert('admin_announcement',$data);
		$this->session->set_flashdata(array('announcement_published_success'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> <span class="glyphicon glyphicon-ok"></span> Announcement Successfully Published</div>'));
		redirect(base_url().'department_announcement');
		exit();
	}


// update
	// @param
	// update profile image
	// condition if true ,select new image the flash data will print profile successfully updates if false if no selected image flash data will print you havent change your photo

	public function Update_profile_dept_admin($id,$image,$date_profile_change){
		date_default_timezone_set('Asia/Manila');
		$date_profile_change = date('h:m:s a m/d/y');
		if($image=="no image"){
			$this->session->set_userdata(array('update_profile_failed'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-warning-sign"></span> Profile not Change </div>'));
			redirect(base_url().'administrator');
			exit();
		}else{
			$data = array(
				'image'=>$image,
				'date_profile_change'=>$date_profile_change
				);
			$this->db->where('id',$id);
			$this->db->update('admin_personel',$data);
			$this->session->set_userdata(array('update_pic_pro_success'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Profile Successfully Update! </div>')); 
			redirect(base_url().'administrator');
			exit();
		}
	}
	// @param
	// updating set news
	// flash data
	// condition if news update with image print if in the condition
	// if condition has a image print else in the condiion
	public function fel_updating_news($id,$image,$title,$news_body,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');
		if($image=="no image"){
			$data = array(
				'title'=>$title,
				'news_body'=>$news_body,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('news',$data);
			$this->session->set_userdata(array('news_updating_event'=>'One Item successfuly updated.'));
			redirect(base_url().'admin_news');
			exit();
		}else{
			$data = array(
				'image'=>$image,
				'title'=>$title,
				'news_body'=>$news_body,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('news',$data);
			$this->session->set_userdata(array('news_updating_event'=>'One Item successfuly updated.'));
			redirect(base_url().'admin_news');
			exit();
		}
	}
	// @param
	// updating set sports
	// flash data
	// condition if news update with image print if in the condition
	// if condition has a image print else in the condiion
	public function fel_updating_sports($id,$image,$title,$sports_body,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');
		if($image=="no image"){
			$data = array(
				'title'=>$title,
				'sports_body'=>$sports_body,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('sports',$data);
			$this->session->set_userdata(array('sports_updated_file_default'=>'Success.! One Item Updated'));
			redirect(base_url().'admin_sports');
			exit();
		}else{
			$data = array(
				'image'=>$image,
				'title'=>$title,
				'sports_body'=>$sports_body,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('sports',$data);
			$this->session->set_userdata(array('sports_updated_file_default'=>'Success.! One Item Updated'));
			redirect(base_url().'admin_sports');
			exit();
		}
	}
	// @param
	// updating set entertainment_news
	// flash data
	// condition if news update with image print if in the condition
	// if condition has a image print else in the condiion
	public function fel_updating_entertainment($id,$image,$title,$entertainment,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');
		if($image=="no image"){
			$data = array(
				'title'=>$title,
				'entertainment'=>$entertainment,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('entertainment_news',$data);
			$this->session->set_userdata(array('entertainment_update'=>'Success.! One Data Updated'));
			redirect(base_url().'admin_entertainment');
			exit();
		}else{
			$data = array(
				'image'=>$image,
				'title'=>$title,
				'entertainment'=>$entertainment,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('entertainment_news',$data);
			$this->session->set_userdata(array('entertainment_update'=>'Success.! One Data Updated'));
			redirect(base_url().'admin_entertainment');
			exit();
		}
	}
	// @param
	// updating set editorial_new
	// flash data
	// condition if news update with image print if in the condition
	// if condition has a image print else in the condiion
	public function fel_updating_editorial($id,$image,$title,$editorial,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');
		if($image=="no image"){
			$data = array(
				'title'=>$title,
				'editorial'=>$editorial,
				'date_update'=>$date_update
				);
			$this->db->where('id',$id);
			$this->db->update('editorial_new',$data);
			$this->session->set_userdata(array('Updating_editorial_event'=>'Success.! One Data Updated'));
			redirect(base_url().'admin_editorial');
			exit();
		}else{
			$data = array(
			'image'=>$image,
			'title'=>$title,
			'editorial'=>$editorial,
			'date_update'=>$date_update
			);
		$this->db->where('id',$id);
		$this->db->update('editorial_new',$data);
		$this->session->set_userdata(array('Updating_editorial_event'=>'Success.! One Data Updated'));
		redirect(base_url().'admin_editorial');
		exit();
		}
	}
	// @param
	// edit department post from department select
	// with flash data
	// condition if no image if in the condition will print
	// if having image else in the condition will print
	public function edit_departmentPost($id,$image,$department_topic)
		{
			if($image == "no image"){
			$data=array(
				
				'department_topic'=>$department_topic
				);
			$this->db->where('id',$id);
			$this->db->update('department_topic',$data);
			$this->session->set_flashdata(array('walayImage' => '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Status Successfully Updated </div>'));
			redirect(base_url().'department_selected');
			exit();
		}else{
			$data=array(
				'image'=>$image,
				'department_topic'=>$department_topic
				);
			$this->db->where('id',$id);
			$this->db->update('department_topic',$data);
			$this->session->set_flashdata(array('walayImage' => '<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Status Successfully Updated </div>'));
			redirect(base_url().'department_selected');
			exit();
		}

		}
		// @param
		// updating course to course table
		// using flashdata
	public function Updating_course($id,$course,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');
		$data = array('course'=>$course, 'date_update'=>$date_update);
		$this->db->where('id',$id);
		$this->db->update('course',$data);
		$this->session->set_flashdata(array('Updatez_course'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Success .! Course Update .! </div>'));
		redirect(base_url().'department_selected');
		exit();
	}


	// @param
	// updating announcemnt
	// update admin_announcement in the table
	// with flashdata
	public function update_announcement($id,$announcement,$date_update){
		date_default_timezone_set('Asia/Manila');
		$date_update = date('h:m:s a m/d/y');

		$data = array('announcement' => $announcement, 'date_update' => $date_update);
		$this->db->where('id',$id);
		$this->db->update('admin_announcement',$data);
		$this->session->set_flashdata(array('announce_update_success' => '<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Announcement successfuly update </div>'));
		redirect(base_url().'department_announcement');
		exit();
	}
	public function account_update($id,$account_date_update,$fname,$lname,$Mname,$email){
		date_default_timezone_set('Asia/Manila');
		$account_date_update = date('h:m:s a m/d/y');

		$data = array(
			'account_date_update'=>$account_date_update,
			'fname'=>$fname,
			'lname'=>$lname,
			'Mname'=>$Mname,
			'email'=>$email
			);
		$this->db->where('id',$id);
		$this->db->update('admin_personel',$data);
		redirect(base_url().'administrator');
		exit();
	}


	// deleting data
	// @param
	// delete data from news
	// with userdata
	public function delete_news($id){
		$this->db->where('id',$id);
		$this->db->delete('news');
		$this->session->set_userdata(array('news_deleting'=>'One Item successfuly deleted.'));
		redirect(base_url().'admin_news');
		exit();
	}
	// @param
	// delete data from sports
	// with userdata
	public function delete_sports($id){
		$this->db->where('id',$id);
		$this->db->delete('sports');
		$this->session->set_userdata(array('sports_deleting'=>'Success.! One Item Deleted'));
		redirect(base_url().'admin_sports');
		exit();
	}
	// @param
	// delete data from entertainment_news
	// with userdata
	public function delete_entertainment($id){
		$this->db->where('id',$id);
		$this->db->delete('entertainment_news');
		$this->session->set_userdata(array('entertainment_deleting_data'=>'Success.! One Item Deleted'));
		redirect(base_url().'admin_entertainment');
		exit();
	}
	// @param
	// delete data from editorial_new
	// with userdata
	public function delete_enditorial($id){
		$this->db->where('id',$id);
		$this->db->delete('editorial_new');
		$this->session->userdata(array('editorial_deleting_data'=>'Success.! One Item Deleted'));
		redirect(base_url().'admin_editorial');
		exit();
	}
	// @param
	// delete data from messaging
	public function delete_message($id){
		$this->db->where('id',$id);
		$this->db->delete('messaging');
		redirect(base_url().'department_message');
		exit();
	}
	// @param
	// delete data from course
	// with flashdata
	public function delete_course($id){
		$this->db->where('id',$id);
		$this->db->delete('course');
		$this->session->set_flashdata(array('deletez'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-trash"></span> Success.! One Data Deleted .</div>'));
		redirect(base_url().'department_selected');
		exit();
	}
	// @param
	// delete data from admin_announcement
	// with flashdata
	public function delete_announcement($id){
		$this->db->where('id',$id);
		$this->db->delete('admin_announcement');
		$this->session->set_flashdata(array('announcement_delete_success'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Announcement Successfully Deleted </div>'));
		redirect(base_url().'department_announcement');
		exit();
	}
	// @param
	// delete data from department_topic
	// with flashdata
	public function delete_dept_status($id){
		$this->db->where('id',$id);
		$this->db->delete('department_topic');
		$this->session->set_flashdata(array('delete_success'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Status Successfully Deleted </div>'));
		redirect(base_url().'department_selected');
		exit();
	}

	// selecting data queries
	// @param
	// select data from messaging
	// DESC
	public function read_record(){
		$this->load->helper('text');
		$user_id = $this->session->userdata('login_id');
		$this->db->select('admin_messaging.id AS message_id, sender_id, receiver_id, image as picture, message as content, date, date_forward as latest_date');
		$this->db->from('admin_messaging');
		$this->db->where('sender_id',$user_id);
		$this->db->order_by('id','desc');
		$queryMessage = $this->db->get();
		return $queryMessage;
	}
	// @param
	// select data from user

	public function SelectReceiver(){
		
		$this->db->select('id, id_number, firstname, lastname, department_id');
		$this->db->from('user');
		$queryReceiver = $this->db->get();
		return $queryReceiver->result();
	}
	// @param
	// select data from news
	// DESC
	// ni gamit ku sa page ug num_rows
	public function news_selecting_data(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, admin_id, image, news_body, title, date');
		$this->db->from('news');
		$this->db->where('admin_id',$user_id);
		$this->db->order_by('id','desc');
		$queryNews = $this->db->get();
		return $queryNews; // ingun ani nga return ky nag num rows ko sa page
	}
	// @param
	// select data from sports
	// DESC
	public function sports_selecting_data(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, admin_id, image, sports_body, title, date');
		$this->db->from('sports');
		$this->db->where('admin_id',$user_id);
		$this->db->order_by('id','desc');
		$querySports = $this->db->get();
		return $querySports;
	}
	// @param
	// select data from entertainment_news
	// DESC
	public function entertainment_selecting_data(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, admin_id, title, entertainment, image, date');
		$this->db->from('entertainment_news');
		$this->db->where('admin_id',$user_id);
		$this->db->order_by('id','desc');
		$queryEntertainment = $this->db->get();
		return $queryEntertainment;
	}
	// @param
	// select data from editorial_new
	// DESC
	public function editorial_selecting_data(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, admin_id, title, editorial, image, date');
		$this->db->from('editorial_new');
		$this->db->where('admin_id',$user_id);
		$this->db->order_by('id','desc');
		$queryEditorial = $this->db->get();
		return $queryEditorial;
	}

	// @param
	// select data from messaging
	// DESC
	// LIMIT 1
	public function latest_date_message(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, sender_id, date as latest_date, date_forward');
		$this->db->from('admin_messaging');
		$this->db->where('sender_id',$user_id);
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$queryLatest = $this->db->get();
		return $queryLatest;
	}
	// @param
	// select data from messaging
	// DESC 
	// LIMIT 5
	// OFFSET 1
	public function privious_date_message(){
	
		$this->db->select('id, sender_id, date as previous_date');
		$this->db->from('admin_messaging');
		$this->db->order_by('id','desc');
		$this->db->limit(8,1);//ang 5 ky limit na dayun ang 1 ky offset na
		$queryOffsit = $this->db->get();
		return $queryOffsit->result();
	}
	// @param
	// select data from messaging
	// DESC
	// LIMIT 1
	public function last_forward_message(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, sender_id, date_forward as forwarded_date');
		$this->db->from('admin_messaging');
		$this->db->where('sender_id',$user_id);
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$queryForward = $this->db->get();
		return $queryForward;
	}
	// @param
	// select data from admin_personel
	
	public function EmailAddress(){
		$queryEmail = $this->db->get('admin_personel');
		return $queryEmail->result();
	}
	

	// @param
	// select data from admin_personel

	public function admin_status_image_profile(){
		
		$this->db->select('id, assigned_department, fname, lname, Mname, image, date');
		$this->db->from('admin_personel');
		$queryImagestatusPic = $this->db->get();
		return $queryImagestatusPic->result();
	}
	// @param
	// select data from department_topic
	// DESC
	// LIMIT 1
	public function admin_department_latest_status_date_logs(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, department_id, user_id, date as latest_date');
		$this->db->from('department_topic');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('id','desc');
		$this->db->limit(1);
		$queryStatusLatestdate = $this->db->get();
		return $queryStatusLatestdate;
	}
	// @param
	// select data from department_topic
	// DESC
	// LIMIT 10
	// OFFSET 1
	public function admin_department_old_status_date_logs(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, department_id, user_id, date as dateOldPost');
		$this->db->from('department_topic');
		$this->db->where('user_id',$user_id);
		$this->db->order_by('id','desc');
		$this->db->limit(5,1);//ang value nga 5 ky mao na ang limit while ang value nga 1 ky offset
		$queryOldtimeLog = $this->db->get();
		return $queryOldtimeLog->result();
	}
	// @param
	// select data from department_topic
	public function admin_department_get_like(){
		$queryGetLike = $this->db->get('department_topic');
		return $queryGetLike->result();
	}
	// @param
	// select data from admin_personel
	
	public function welcome_administrator_alert(){
		$queryAdmin = $this->db->get('admin_personel');
		return $queryAdmin->result();
	}
	// @param
	// select data from user
	public function select_message_receiver(){
		$queryReceiver = $this->db->get('user');
		return $queryReceiver->result();
	}
	// @param
	// select data from admin_annoucement
	// DESC
	public function show_published_announcement(){
		
		$this->db->select('id, admin_id,announcement, date, date_update');
		$this->db->from('admin_announcement');
		$this->db->order_by('id','desc');
		$queryannouncement = $this->db->get();
		return $queryannouncement->result();
	}
	// @param
	// select data from course
	// DESC
	public function depertment_view_course_added(){
		$this->load->helper('text');
		$this->db->select('*');
		$this->db->from('course');
		$this->db->order_by('id','desc');
		$query = $this->db->get();
		return $query->result();
	}
	// @param
	// select data from department_topic
	
	public function getAll_data(){
		$query = $this->db->get('department_topic');
		return $query->result();
	}
	// kani kay ang pag kuha nku sa super admin nga name ug ID para inig send nku sa message adtu niy apadung
	// super administrator query
	// and iyang Id number sa super admin ky always zero
	public function superAdminGettingInfo(){
		$superadminQuery = $this->db->query('SELECT sa_id, username FROM super_admin_info');
		return $superadminQuery->result();
	}



	// blocking a user
	// @param
	// checking person either block or not
	// if condition is in the if print user blocked
	// if contition is in the else statement block user
	// with flash data
	// block user insert into admin_block_list
	public function block_user($blocked_user_id,$admin_id,$reason_id,$date){

		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'blocked_id' => $blocked_user_id,
			'admin_id' => $admin_id,
			'reason_id'=>$reason_id,
			'date'=>$date,
			'status' => 1

			);
		$checkData = $this->db->get_where('admin_block_list',$data);
		if($checkData->num_rows() != 0){
		
		echo "blocked";
		
		}else{
		$this->session->set_flashdata(array('BlockSuccess'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> One User! Successfully Blocked</div>'));
		$this->db->insert('admin_block_list',$data);
		redirect(base_url().'administrator');
		exit();
		}
}
	// @param
	// insert data into department_topic
	// with flash data
	public function add_department_status($department,$user_id,$admin_id_number,$image,$department_topic_body,$date){

		 date_default_timezone_set('Asia/Manila');
         $date=date('h:i:s a m/d/y');
		 $data=array(
			'department_id'=>$department,
			'user_id'=>$user_id,
			'user_id_number'=>$admin_id_number,
			'image'=>$image,
			'department_topic'=>$department_topic_body,
			'date'=>$date
			);
		$this->db->insert('department_topic',$data);
		$this->session->set_flashdata(array('status_success'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Successfully shared status</div>'));
		redirect(base_url().'department_selected');
	}
	// @param
	// insert data into department_topic_comments
	public function admin_department_comment($admin_department_topic,$comments,$admin_id_number){
	$admin_id = $this->session->userdata('login_id');
	date_default_timezone_set('Asia/Manila');
    $date=date('h:i:s a m/d/y');
    $data = array(

	'department_topic_id' => $admin_department_topic,
	'commenter_id' => $admin_id,
	'comment' => $comments,
	'date' => $date,
	'admin_id_number' => $admin_id_number

	); 

   $this->db->insert('department_topic_comments',$data);


   }
   // @param
	// update comment to department_topic_comments
   public function edit_comment($comments,$department_topic_id,$comment_id){

   	$data = array(
   			'comment' => $comments,

   		);

   	$this->db->where('id',$comment_id);
   	$this->db->update('department_topic_comments',$data);
   }
	
   public function delete_comment($id){
   	$this->db->where('id',$id);
   	$this->db->delete('department_topic_comments');
   }

// messaging delete
   public function delete_action_message($message_id){
    $deleter_id = $this->session->userdata('login_id');
		$data = array(
			'message_id' => $message_id,
			'deleter_id' => $deleter_id
			);
		
		$this->db->insert('admin_delete_messages',$data);
		$this->delete_permanent_message($message_id);
	}
	public function delete_permanent_message($message_id){
			$query = "SELECT * FROM admin_delete_messages WHERE message_id ='$message_id'";
			$checkToPermanentDelete = $this->db->query($query);
			if($checkToPermanentDelete->num_rows() >= 2){
				$this->db->where('message_id',$message_id);
				$this->db->delete('admin_delete_messages');

				$this->db->where('id',$message_id);
				$this->db->delete('admin_messaging');
			}
	}
  // end

   // ======================change password ======================//
	// ge return nku ang iyang session login id para eh check either the user os logged In or not
   	// If false redirect to admin_login
 public function isLoggedIn(){
   	return $this->session->userdata('login_id') && $this->session->userdata('login_username') && $this->session->userdata('login_password') && $this->session->userdata('login');
 }
 	// @param
	// change password
	// for admin
	// for validation
public function member_page(){
	if($this->isLoggedIn()){
		return true; //return true;// if ang user ky ang Login_id niya nga session ky true then proceed to administrator the can access the change password page
	}else{
		redirect(base_url().'admin_login'); //eh return ni niya if ang user ky u try ug sulod ni agi ani http://localhost/scsit_social_site/change_password nya wa nka login... eh redirect jud niya
	}
}

//@param
// selecting password change for administrator
public function date_password_change(){
	$user_id = $this->session->userdata('login_id');
	$this->db->select('id, assigned_department, date_pass_change');
	$this->db->from('admin_personel');
	$this->db->where('id',$user_id);
	$passwordQuery = $this->db->get();
	return $passwordQuery;
}
// updating course mvc query
public function UpdateCourse(){
	$query = $this->db->get('course');
	return $query->result();
}
// @param
// gi tawag pa nku ang department para ang ma select nga message is equal to department id
// dayun nag left join ko sa admin messagin to super admin information nga ang id ky equal to zero
//  ky ang super admin ky usa ra
public function inbox(){
	$admin_id = $this->session->userdata('login_id'); // gi tawag pang session aron makit.an ang Id
	$this->db->where('id',$admin_id);
	$getDepartment = $this->db->get('admin_personel');
	foreach($getDepartment->result() as $departmentRow){
		$query = "SELECT username,message,admin_messaging.date as message_date,date_forward,admin_messaging.id as message_id,sender_id,receiver_id FROM admin_messaging LEFT JOIN super_admin_info ON super_admin_info.sa_id = admin_messaging.sender_id WHERE sender_id = 0 AND receiver_id = 
		'$departmentRow->assigned_department' ORDER BY admin_messaging.id DESC";
	 	$messageInbox = $this->db->query($query);
		return $messageInbox->result();
	}

	 
}
	// #param
	// joining table of admin personel and department

	public function departmentJoined(){
		$user_id = $this->session->userdata('login_id'); // gi tawag nku ang session para mkita ang id nga user_id
		$this->db->select('*');
		$this->db->from('admin_personel');
		$this->db->join('department','department.id = admin_personel.assigned_department','left');
		$this->db->where('admin_personel.id', $user_id);
		$queryResult = $this->db->get();
		return $queryResult->result();
		
	}
	// #param
	// para sa table join sa admin personel and department 
	// found in printer page
	public function joinedPrinterDepartmentname(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('department.id AS dept_id, department.*, admin_personel.id as admin_personel_id, admin_personel.*');
		$this->db->from('admin_personel');
		$this->db->join('department','department.id = admin_personel.assigned_department','left');
		$this->db->where('admin_personel.id',$user_id);
		$Printerblocked = $this->db->get();
		return $Printerblocked->result();
	}
	
	// @param
	// joined four tables in one query
	// ni gamit kug mga alias pra d ma conflict ang mga id rows in every department
	public function joinedDisplayblockedUser(){
		$user_id = $this->session->userdata('login_id');
		$id = $this->session->userdata('login_id');
		$this->db->select('user.id AS member_id, admin_block_list.id AS member_block_id, firstname, lastname, middlename, age, address, gender AS member_sex,  address, department_id, reason, admin_block_list.date AS blocked_date, admin_id, department_id');
		$this->db->from('admin_block_list');
		$this->db->join('user','user.id = admin_block_list.blocked_id','left');
		$this->db->join('block_reason','block_reason.id = admin_block_list.reason_id','left');
		$this->db->join('admin_personel','admin_personel.id = admin_id','left');
		$this->db->where('admin_id',$user_id);
		$this->db->order_by('member_id','desc');
		$blockedUserInformationQuery = $this->db->get();
		return $blockedUserInformationQuery->result();

	}

	// select sa messaging sa select nga receiver
	public function selectingJoining(){
		$dept_id = $this->session->userdata('login_id');
		$this->db->select('id, firstname, lastname, id_number AS IDnumber, middlename, department_id');
		$this->db->from('user');
		$this->db->where('department_id',$dept_id);
		$this->db->order_by('id','asc');
		$queryReceiverNi = $this->db->get();
		return $queryReceiverNi;

	}
	// @param
	// select data from admin_personel
	public function forDepartment_selected_post_status(){
		$queryDepartmentselected = $this->db->get('admin_personel');
		return $queryDepartmentselected->result();
	}
	// @param
	// joining ni dre para sa
	// adminpersonel Information
	// gi join natu ang admin_personel ug department ug nangitag user_id
	public function adminInfo(){
		$this->load->helper('text');
		$user_id = $this->session->userdata('login_id');
		$this->db->select('admin_personel.id AS personel_id, department.id AS Depart_id, fname, lname, Mname, admin_personel.image AS personel_image, department.image AS dept_picture, admin_id_number, department, username, email');
		$this->db->from('admin_personel');
		$this->db->join('department','department.id = admin_personel.assigned_department ','left');
		$this->db->where('admin_personel.id',$user_id);
		$queryAdminInfo = $this->db->get();
		return $queryAdminInfo->result();
	}
	// kani query ni sa navigation tab sa menu
	// nga nag dag variable nag $dept_id
	public function navTop(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('department.id AS dept_id, department.*, admin_personel.id as admin_personel_id, admin_personel.*');
		$this->db->from('admin_personel');
        $this->db->join('department','department.id = admin_personel.assigned_department','left');
        $this->db->where('admin_personel.id', $user_id);
        $queryTableJoin=$this->db->get();
        return $queryTableJoin->result();
                     
	}
	// @param
	// para ni sa navigation tab nga mu drop down
	// atung eh MVC arun nindot gayud
	public function navTopDrop(){
		$user_id = $this->session->userdata('login_id');
		$this->db->select('id, image, fname, lname');
		$this->db->from('admin_personel');
		$this->db->where('id',$user_id);
		$queryDrop = $this->db->get();
		return $queryDrop->result();
	}
	
// ===================== user news page queries ==========================//
function select_news(){

		$this->db->select('news.date as date_s,news.title as title, news.admin_id as admin_id, news.id as news_id, admin_personel.id as admin_personel_id, news.image as image, news.news_body as description');
		$this->db->from('news');
		$this->db->join('admin_personel','admin_personel.id = news.admin_id','left');
		$this->db->order_by('news.id','desc');
		$querySelectnews = $this->db->get();

		return $querySelectnews->result();
	}
	function select_sports(){

		$this->db->select('sports.date as date_s,sports.title as title, sports.admin_id as admin_id, sports.id as sports_id, admin_personel.id as admin_personel_id, sports.image as image, sports.sports_body as description');
		$this->db->from('sports');
		$this->db->join('admin_personel','admin_personel.id = sports.admin_id','left');
		$this->db->order_by('sports.id','desc');
		$Selectsports = $this->db->get();

		return $Selectsports->result();	
	}
	function select_entertainment(){

		$this->db->select('entertainment_news.date as date_s,entertainment_news.title as title, entertainment_news.admin_id as admin_id, entertainment_news.id as entertainment_id, admin_personel.id as admin_personel_id, entertainment_news.image as image, entertainment_news.entertainment as description');
		$this->db->from('entertainment_news');
		$this->db->join('admin_personel','admin_personel.id = entertainment_news.admin_id','left');
		$this->db->order_by('entertainment_news.id','desc');
		$Selectentertainment = $this->db->get();

		return $Selectentertainment->result();	
	}
	function select_editorial(){

		$this->db->select('editorial_new.date as date_s,editorial_new.title as title, editorial_new.admin_id as admin_id, editorial_new.id as editorial_id, admin_personel.id as admin_personel_id, editorial_new.image as image, editorial_new.editorial as description');
		$this->db->from('editorial_new');
		$this->db->join('admin_personel','admin_personel.id = editorial_new.admin_id','left');
		$this->db->order_by('editorial_new.id','desc');
		$Selecteditorial = $this->db->get();

		return $Selecteditorial->result();	
	}
	function select_announcement(){

		$this->db->order_by('admin_announcement.id','desc');
		$Selectannouncement = $this->db->get('admin_announcement');

		return $Selectannouncement;
	}
	/////////////////////////////////////////////////////////////////////////////////////////////////////
 }
 ?>