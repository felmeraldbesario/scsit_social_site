<!-- for advertisement only in index page
Program by: felmerald besario
 -->
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
 class Adver_model extends CI_Model{

 	// news adver
 	public function show_news_adver(){
 		$this->load->helper('text');
 		$this->db->select('id, image, news_body as newsContent, title, date');
 		$this->db->from('news');
 		$this->db->order_by('id','desc');
 		$queryAdvernews = $this->db->get();
 		return $queryAdvernews;
 	}
 	public function entertainment_result(){
 		$this->load->helper('text');
 		$this->db->select('id, image as picture, title, entertainment as content, date');
 		$this->db->from('entertainment_news');
 		$this->db->order_by('id','desc');
 		$queryEnter = $this->db->get();
 		return $queryEnter;
 	}
 	public function showEditorial_MVC(){
 		$this->load->helper('text');
 		$this->db->select('id, image as picture, title, editorial as content, date');
 		$this->db->from('editorial_new');
 		$this->db->order_by('id','desc');
 		$queryEditorial = $this->db->get();
 		return $queryEditorial;
 	}
 	public function showSports_MVC(){
 		$this->load->helper('text');
 		$this->db->select('id, image as picture, title, sports_body as content, date');
 		$this->db->from('sports');
 		$this->db->order_by('id','desc');
 		$querySports = $this->db->get();
 		return $querySports;
 	}
 	// limits
 	public function News_limit(){

 		$this->db->select('id, title, image as picture, date');
 		$this->db->from('news');
 		$this->db->order_by('id','desc');
 		$this->db->limit(1);
 		$post_news = $this->db->get();
 		return $post_news;
 	}
 	public function entertainment_limit(){
 		
 		$this->db->select('id, title, image as picture, date');
 		$this->db->from('entertainment_news');
 		$this->db->order_by('id','desc');
 		$this->db->limit(1);
 		$post_entertainment = $this->db->get();
 		return $post_entertainment;
 	}
 	public function sport_limit(){
 		
 		$this->db->select('id, title, image as picture, date');
 		$this->db->from('sports');
 		$this->db->order_by('id','desc');
 		$this->db->limit(1);
 		$post_sport = $this->db->get();
 		return $post_sport;
 	}
 	public function editorial_limit(){
 		
 		$this->db->select('id, image as picture, title, date');
 		$this->db->from('editorial_new');
 		$this->db->order_by('id','desc');
 		$this->db->limit(1);
 		$post_editorial = $this->db->get();
 		return $post_editorial;
 	}
 	public function getAnnouncement(){
 		
 		$this->db->select('id, announcement, date');
 		$this->db->from('admin_announcement');
 		$this->db->order_by('id','desc');
 		$queryAnnouncement = $this->db->get();
 		return $queryAnnouncement;
 	}
 	public function felcopyRightfooter(){
 		$queryCopy = $this->db->get('locator');
 		return $queryCopy->result();
 	}
 	public function readMore(){
 		$query = $this->db->get('news');
 		return $query->result();
 	}
 	public function clickmenews(){
 		$this->db->select('*');
 		$this->db->from('news');
 		$this->db->order_by('id','desc');
 		$this->db->limit(1);
 		$rowclick = $this->db->get();
 		return $rowclick;
 	}
 	public function sportsRead(){
 		$query = $this->db->get('sports');
 		return $query->result();
 	}
 	public function editorialRead(){
 		$query = $this->db->get('editorial_new');
 		return $query->result();
 	}
 	public function entertainementRead(){
 		$query = $this->db->get('entertainment_news');
 		return $query->result();
 	}

 	

 }