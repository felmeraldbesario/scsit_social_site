<?php 

class superadmin_model extends CI_Model{
	public function edp_connection(){

		$config['hostname'] = "localhost";
		$config['username'] = "root";
		$config['password'] = "";
		$config['database'] = "edp";
		$config['dbdriver'] = "mysql";
		$config['dbprefix'] = "";
		$config['pconnect'] = FALSE;
		$config['db_debug'] = TRUE;
		$config['cache_on'] = FALSE;
		$config['cachedir'] = "";
		$config['char_set'] = "utf8";
		$config['dbcollat'] = "utf8_general_ci";

		return $config;
	}

	public function process_login($username,$password){
		$data = array(
			'username' => $username,
			'password' => $password
			);

		$login_query = $this->db->get_where('super_admin_info',$data);
		if($login_query->num_rows() != 0){
			
			foreach($login_query->result() as $superadmin_info){

				$db_data = array(
					'superadmin_login_id' => $superadmin_info->sa_id,
					'login' => TRUE
					);
				$this->session->set_userdata($db_data);
				redirect(base_url().'superadmin');
			}
			
		}else{ 
				$message = array(
					'message' => "Invalid Username/Password"
					);
				$this->session->set_userdata($message);
				redirect(base_url().'superadmin_login');
		}
		
	}

	public function process_logout(){

		$session_data = array(
			'superadmin_login_id' => '',
			'login' => ''
			);
		$this->session->unset_userdata($session_data);
		$this->session->sess_destroy();
		redirect(base_url().'superadmin_login');

	}

	public function superadmin_login(){
		$admin_id = $this->session->userdata('login_id');
		if($admin_id != ""){
				redirect(base_url().'administrator');
		}else{
			return true;
		}
	}

	public function checkSuperadminInfo(){
		$superadmin_id = $this->session->userdata('superadmin_login_id');
		$admin_id = $this->session->userdata('login_id');

		if($superadmin_id != ""){
			$session_data = array(
				'sa_id' => $superadmin_id
				);
			$checkSuperadminInfo = $this->db->get_where('super_admin_info',$session_data);
			if($checkSuperadminInfo->num_rows() > 0){
				return true;
			}else{
				return false;
			}
		}else if($admin_id != ""){
				redirect(base_url().'administrator');
		}else{
				return false;
		}
	}
//===================para da manage department==========================
	public function superadmin_add_department($department_name,$image){
		$edp_database = $this->load->database($this->edp_connection(),TRUE);
		$data = array('department' => $department_name);
		
		$edp_info_query = $edp_database->get_where('department',$data);
		if($edp_info_query->num_rows() != 0){
		foreach($edp_info_query->result() as $edp_info){
			$data = array( 'department' => $department_name);
			$check_exist = $this->db->get_where('department',$data);
			if($check_exist->num_rows() == 0){
			$data = array('department' => $department_name, 'image' => $image);
			$this->db->insert('department',$data);
			$this->session->set_userdata(array('DEPARTMENT_ADD_SUCCESS' => '<div class="alert alert-success">Success</div>'));
			redirect(base_url().'superadmin_deptList');
			exit();
		}else{
			$this->session->set_userdata(array('DEPARTMENT_EXIST'=>'<div class="alert alert-danger">Department already exist</div>'));
			redirect(base_url().'superadmin_deptList');
			exit();
		}
		}
		}else{
			$this->session->set_userdata(array('DEPARTMENT_NAME_NOT_AVAILABLE'=>
				
				'<div class="alert alert-danger">Department not available...</div>'
				));
			redirect(base_url().'superadmin_deptList');
			exit();
		}

}
	public function superadmin_deptlist(){
		
		$query = $this->db->get('department');
		return $query->result();
	}
	public function superadmin_dept_personel(){

		
		$dept_personel_query = $this->db->get('admin_personel');

		return $dept_personel_query;
	}
	public function get_department($department_id){
		$this->db->where('id',$department_id);
		$department = $this->db->get('department');

		return $department->result();
	}
	public function superadmin_add_course($department_id,$course){
		$this->db->where('department_id',$department_id);
		$this->db->where('course',$course);
		$check_course_exist = $this->db->get('course');
		if($check_course_exist->num_rows() == 0){
			$data = array(
			'department_id' => $department_id,
			'course' => $course,
			'date_added' => date('m/d/y')
			);
		$this->db->insert('course',$data);
		$this->session->set_userdata(array('COURSE_SUCCESS' => '<div class="alert alert-success"> Course successfully added</div>'));
		}else{
			$this->session->set_userdata(array('COURSE_EXIST'=>'<div class="alert alert-danger">Course already exist</div>'));
		}
		
		redirect(base_url().'superadmin_deptList');
		exit();
	}
	public function superadmin_delete_course($course_id){
		$this->db->where('id',$course_id);
		$this->db->delete('course');
		$this->session->set_flashdata('update_course','<div class="alert alert-success">course was successfully DELETED</div>');
		redirect(base_url().'superadmin_deptList');
		exit();
	}

	public function superadmin_update_course($course,$course_id){
		$this->db->where('course',$course);
		$check_course_exist = $this->db->get('course');
		if($check_course_exist->num_rows() == 0){
			$data = array(
				'course' => $course
				);
			$this->db->where('id',$course_id);
			$this->db->update('course',$data);
			$this->session->set_flashdata('update_course','<div class="alert alert-success">course was successfully updated</div>');
			redirect(base_url().'superadmin_deptList');
			exit();
		}else{
			 $this->db->where('id',$course_id);
			$check_course = $this->db->get('course');
			 foreach($check_course->result() as $courseRow){
				if($courseRow->course == $course){
				
			$data = array(
				'course' => $course
				);
			$this->db->where('id',$course_id);
			$this->db->update('course',$data);
			$this->session->set_flashdata('update_course','<div class="alert alert-success">course was successfully updated</div>');
			redirect(base_url().'superadmin_deptList');
			exit();				
				}else{
			$this->session->set_flashdata('update_course_error','<div class="alert alert-danger">course was already exist</div>');
			redirect(base_url().'superadmin_deptList');
			exit();	
			 	}

			}
		}
	}
	public function update_department_name($department_id,$updated_department_name,$image){
		$edp_database = $this->load->database($this->edp_connection(),TRUE);
		$data = array('department' => $updated_department_name);
		
		$edp_info_query = $edp_database->get_where('department',$data);
		if($edp_info_query->num_rows() != 0){
		foreach($edp_info_query->result() as $edp_info){
			$data = array( 'department' => $updated_department_name);
			$check_exist = $this->db->get_where('department',$data);
			if($check_exist->num_rows() == 0){
				
					
				$data = array(
					'department' => $updated_department_name,
					'image' => $image
				);
			$this->db->where('id',$department_id);
			$this->db->update('department',$data);
			$this->session->set_userdata(array('SUCCESSFULLY_UPDATED'=>'<div class="alert alert-success">Department Name Successfully Updated</div>'));
			redirect(base_url().'superadmin_deptList');
			exit();
		}else{
			$this->db->where('id',$department_id);
			$check_exist_name = $this->db->get('department');
			foreach($check_exist_name->result() as $check_exist_row){
				if($check_exist_row->department == $updated_department_name){
									$data = array(
					'department' => $updated_department_name,
					'image' => $image
				);
			$this->db->where('id',$department_id);
			$this->db->update('department',$data);
			$this->session->set_userdata(array('SUCCESSFULLY_UPDATED'=>'<div class="alert alert-success">Department Name Successfully Updated</div>'));
			redirect(base_url().'superadmin_deptList');
			exit();
				}else{
			$this->session->set_userdata(array('DEPARTMENT_EXIST'=>'<div class="alert alert-danger">Department already exist</div>'));
			redirect(base_url().'superadmin_deptList');
			exit();
				}

			}
			
		}
		}
		}else{
			$this->session->set_userdata(array('DEPARTMENT_NAME_NOT_AVAILABLE'=>
				
				'<div class="alert alert-danger">Department not available...</div>'
				));
			redirect(base_url().'superadmin_deptList');
			exit();
		}



	}
	public function delete_department($department_id){
		$this->db->where('id',$department_id);
		$this->db->delete('department');
		$this->session->set_userdata(array('SUCCESSFULLY_DELETED'=>'<div class="alert alert-success">Department Name Successfully DELETED</div>'));
		redirect(base_url().'superadmin_deptList');
		exit();
	}

	public function show_course(){
		
		$getCourseQuery = $this->db->get('course');
		return $getCourseQuery->result();
	}
//===================end of manage department====================
//===================para da campus post========================
	public function superadmin_campus_post(){
		
		$query = "SELECT campus_post.id as post_id,firstname,lastname,user_id,image,campus_post,date FROM campus_post LEFT JOIN user ON campus_post.user_id = user.id ORDER BY campus_post.id DESC";
		
		$campusPostQuery = $this->db->query($query);
									
		return $campusPostQuery->result();
	}
	public function sa_campus_post($sa_campus_post,$image){
		
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'user_id' => 0,
			'user_id_number' => 0,
			'campus_post' => $sa_campus_post,
			'date' => $date,
			'image' => $image
			);
		$this->db->insert('campus_post',$data);
		redirect(base_url().'superadmin_campus');
		exit();
	}

	public function superadmin_last_post_date(){
		$post_date = $this->db->query('SELECT date FROM campus_post ORDER BY id DESC  LIMIT 1 ');
		return $post_date->result();
	}

	public function superadmin_previus_post_date(){
		$post_date = $this->db->query('SELECT date FROM campus_post ORDER BY id DESC  LIMIT 10 OFFSET 1');
		return $post_date->result();
	}

	public function getComments(){
		$campus_comments = $this->db->get('campus_post_comment');
		return $campus_comments->result();
	}


	public function sa_campus_comments($campus_comments,$campus_post_id){
		date_default_timezone_set("Asia/Manila");
		$date = date('h:m:s a m/d/y');

		$data = array(
			'campus_post_id' => $campus_post_id,
			'campus_commenter_id' => 0,
			'comment' => $campus_comments,
			'date' => $date
			);
		
		$this->db->insert('campus_post_comment',$data);
		
	}

	public function superadmin_department_post($id){
		$query = "SELECT admin_id_number,department_topic.id as department_topic_id,department_topic.department_id as dept_id,firstname,lastname,department_topic.user_id_number as user_number,department_topic.user_id as department_user_id,department_topic.image as department_topic_image,department_topic,department_topic.date as department_topic_date FROM department_topic 
		LEFT JOIN user ON department_topic.user_id = user.id
		LEFT JOIN admin_personel ON department_topic.user_id_number = admin_personel.admin_id_number 
			WHERE department_topic.department_id = '$id'
			ORDER BY department_topic.id DESC";
		
		$departmentPostQuery = $this->db->query($query);
									
		return $departmentPostQuery->result();
	}
	public function superadmin_department_last_post_date($id){
		$post_date = $this->db->query('SELECT date FROM department_topic WHERE department_id = "$id" ORDER BY id DESC  LIMIT 1 ');
		return $post_date->result();
	}

	public function superadmin_department_previus_post_date($id){
		$post_date = $this->db->query('SELECT date FROM department_topic WHERE department_id = "$id" ORDER BY id DESC  LIMIT 10 OFFSET 1');
		return $post_date->result();
	}

	public function sa_department_post($sa_department_post,$image,$department_id){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
			'user_id' => 0,
			'image' => $image,
			'user_id_number' => 0,
			'department_topic' => $sa_department_post,
			'date' => $date,
			'department_id' => $department_id
			);
		$this->db->insert('department_topic',$data);
		redirect(base_url()."superadmin_department?id=".$department_id);
		exit();
	}

	public function update_department_post($department_post,$post_id,$image,$department_id){
		if($image != "no image"){
		$data = array(
				'department_topic' => $department_post,
				'image' => $image
					);
		}else{
				$data = array(
				'department_topic' => $department_post
					);
		}
		$this->db->where('id',$post_id);
		$this->db->update('department_topic',$data);
		redirect(base_url()."superadmin_department?id=".$department_id);
		exit();
	}

	public function delete_department_post($id,$department_id){
		$this->db->where('id',$id);
		$this->db->delete('department_topic');

		$this->delete_dep($id);
		redirect(base_url().'superadmin_department?id='.$department_id);
		exit();
	}

	public function delete_dep($id){
		$this->db->where('department_topic_id',$id);
		$this->db->delete('department_topic_feedback');
	}

	public function dept_getComments(){
		$department_comments = $this->db->get('department_topic_comments');
		return $department_comments->result();
	}
	public function sa_department_comments($comment,$department_post_id){
		date_default_timezone_set("Asia/Manila");
		$date = date('h:m:s a m/d/y');

		$data = array(
			'department_topic_id' => $department_post_id,
			'commenter_id' => 0,
			'comment' => $comment,
			'date' => $date
			);
		
		$this->db->insert('department_topic_comments',$data);

		
		
	}
	public function department_comment_update($comment,$comment_id,$department_topic_id){
		
		$data = array('comment' => $comment);
		$this->db->where('department_topic_id',$department_topic_id);
		$this->db->where('id',$comment_id);
		$commentUpdate = $this->db->update('department_topic_comments',$data);
	}
	public function department_comment_update_modal($comment_id){
		$this->db->where('id',$comment_id);
		$commentInfo = $this->db->get('department_topic_comments');

		return $commentInfo->result();
	}

	public function department_comment_delete($comment_id){
		$this->db->where('id',$comment_id);
		$this->db->delete('department_topic_comments');
	}



	public function campus_comment_update($comment,$comment_id){
		
		$data = array('comment' => $comment);
		
		$this->db->where('id',$comment_id);
		$commentUpdate = $this->db->update('campus_post_comment',$data);
	}

	public function campus_comment_delete($comment_id){
		$this->db->where('id',$comment_id);
		$this->db->delete('campus_post_comment');
	}

	public function update_campus_post($campus_post,$post_id,$image){
		if($image != "no image"){
		$data = array(
				'campus_post' => $campus_post,
				'image' => $image
					);
		}else{
				$data = array(
				'campus_post' => $campus_post
					);
		}
		$this->db->where('id',$post_id);
		$this->db->update('campus_post',$data);
		redirect(base_url().'superadmin_campus');
		exit();
	}

	public function delete_campus_post($id){
		$this->db->where('id',$id);
		$this->db->delete('campus_post');

		$this->delete_campus($id);
		redirect(base_url().'superadmin_campus');
		exit();
	}

	public function delete_campus($id){
		$this->db->where('campus_post_id',$id);
		$this->db->delete('campus_post_comment');
	}
        public function modal_campus_comment($comment_id){
                $this->db->where('id',$comment_id);
                $getCommentQuery = $this->db->get('campus_post_comment');
                
                return $getCommentQuery->result();
        }

	//==================== para sa records =====================

	public function superadmin_records(){
		$query = "SELECT department.id as dept_id,user.id as user_id,admin_block_list.admin_id as admin_block_id,id_number,mobile_number,firstname,lastname,middlename,year,department as department_name, admin_block_list.date as blocked_date,blocked_id FROM user 
					LEFT JOIN department ON user.department_id = department.id
					LEFT JOIN admin_block_list ON user.id = admin_block_list.blocked_id";

		$records_query = $this->db->query($query);

		return $records_query->result();
	}

	public function superadmin_admin_records(){
		$query = "SELECT admin_id_number,department.id as dept_id,admin_personel.id as admin_id,fname,lname,Mname,username,department as department_name,email FROM admin_personel 
					LEFT JOIN department ON admin_personel.assigned_department = department.id";

		$records_query = $this->db->query($query);

		return $records_query->result();
	}
	public function admin_records($id){
		$query = "SELECT admin_id_number,department.id as dept_id,admin_personel.id as admin_id,fname,lname,mname,username,department as department_name,email,admin_personel.image as admin_image FROM admin_personel 
					LEFT JOIN department ON admin_personel.assigned_department = department.id
					WHERE admin_personel.id = '$id'";
		$records_query = $this->db->query($query);
		return $records_query->result();
	}

	public function count_records(){

		$countRecords = $this->db->count_all_results('user');
		return $countRecords;
	}

	public function superadmin_block($reason,$blocked_id){

		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array(
				'admin_id' => 0,
				'blocked_id' => $blocked_id,
				'date' => $date,
				'reason_id' => $reason
			);

		$this->db->insert('admin_block_list',$data);
		
		redirect(base_url().'superadmin_user_records');
		
		
	}

	public function superadmin_unblock($user_id,$department_id){
		$this->db->where('blocked_id',$user_id);

		$this->db->delete('admin_block_list');
		redirect(base_url()."superadmin_blocked?id=".$department_id);
		exit();
	}
//==================para sa user_blocked

	public function department_menu(){
		$query = $this->db->get('department');

		return $query->result();
	}
	
	public function blocked_user($id){

		$query = "SELECT user.id as user_id,admin_block_list.admin_id as admin_block_id,user_id_number,mobile_number,firstname,lastname,middlename,year, admin_block_list.date as blocked_date FROM admin_block_list
					LEFT JOIN user ON user.id = admin_block_list.blocked_id 
					LEFT JOIN admin_personel ON admin_personel.id = admin_id AND admin_id = '0'
					WHERE user.department_id = '$id' ";
		$blocked_user = $this->db->query($query);
		
		return $blocked_user->result();
	}

	public function department_blocked($id){

		$query = "SELECT admin_personel.id as admin_id,department,fname,lname FROM admin_personel 
				  LEFT JOIN department ON admin_personel.assigned_department = department.id
				  WHERE admin_personel.assigned_department = '$id'";
		
		$department_blocked = $this->db->query($query);

		return $department_blocked->result();
	}
	/// para sa profile pic
	public function get_superadmin_info(){
		$query = $this->db->get('super_admin_info');

		return $query->result();
	}
	public function update_profile($image){
		$data = array('image' => $image);

		$this->db->update('super_admin_info',$data);
		redirect(base_url().'superadmin');
		exit();
	}
	//change passsword
	public function change_password($current_password,$new_password){
		$get_superadmin_info = $this->db->get('super_admin_info');
		foreach($get_superadmin_info->result() as $SA_Row){
			if($current_password == $SA_Row->password){
				$data = array('password' => $new_password);
				$this->db->update('super_admin_info',$data);

				return true;
			}else{
				return false;
			}
		}
		
	}
	//change passsword
	public function change_username($current_username,$new_username){
		$get_superadmin_info = $this->db->get('super_admin_info');
		foreach($get_superadmin_info->result() as $SA_Row){
			if($current_username == $SA_Row->username){
				$data = array('username' => $new_username);
				$this->db->update('super_admin_info',$data);
				return true;
			}else{
				return false;
			}
		}
		
	}

	// para sa page
	public function superadmin_news(){
		$query = "SELECT news.id as news_id, admin_id,news.image as news_image,news_body,title,news.date as news_date,news.date_update as news_date_update,fname,lname,Mname
					FROM news LEFT JOIN admin_personel ON news.admin_id = admin_personel.id ORDER BY news.id DESC";

		$getNews = $this->db->query($query);
		return $getNews->result();
	}

	public function superadmin_sports(){
		$query = "SELECT sports.id as sports_id, admin_id,sports.image as sports_image,sports_body,title,sports.date as sports_date,sports.date_update as sports_date_update,fname,lname,Mname
					FROM sports LEFT JOIN admin_personel ON sports.admin_id = admin_personel.id ORDER BY sports.id DESC";

		$getSports = $this->db->query($query);
		return $getSports->result();
	}

	public function superadmin_entertainment(){
		$query = "SELECT entertainment_news.id as entertainment_id, admin_id,entertainment_news.image as entertainment_image,entertainment,title,entertainment_news.date as entertainment_date,entertainment_news.date_update as entertainment_date_update,fname,lname,Mname
					FROM entertainment_news LEFT JOIN admin_personel ON entertainment_news.admin_id = admin_personel.id ORDER BY entertainment_news.id DESC";

		$getEntertainment = $this->db->query($query);
		return $getEntertainment->result();
	}
	public function superadmin_editorial(){
		$query = "SELECT editorial_new.id as editorial_id, admin_id,editorial_new.image as editorial_image,editorial,title,editorial_new.date as editorial_date,editorial_new.date_update as editorial_date_update,fname,lname,Mname
					FROM editorial_new LEFT JOIN admin_personel ON editorial_new.admin_id = admin_personel.id ORDER BY editorial_new.id DESC";

		$getEditorial = $this->db->query($query);
		return $getEditorial->result();
	}

	public function insert_pages_post($image,$temp_id,$page_post,$title){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');

		if($temp_id == 1){
			$data = array(
				'admin_id' => 0,
				'image' => $image,
				'news_body' => $page_post,
				'title' => $title,
				'date' => $date
				);
			$this->db->insert('news',$data);
			redirect(base_url().'superadmin_news');
			exit();
		}else if($temp_id == 2){
			$data = array(
				'admin_id' => 0,
				'image' => $image,
				'sports_body' => $page_post,
				'title' => $title,
				'date' => $date
				);
			$this->db->insert('sports',$data);
			redirect(base_url().'superadmin_sports');
			exit();
		}else if($temp_id == 3){
			$data = array(
				'admin_id' => 0,
				'image' => $image,
				'entertainment' => $page_post,
				'title' => $title,
				'date' => $date
				);
			$this->db->insert('entertainment_news',$data);
			redirect(base_url().'superadmin_entertainment');
			exit();
		}else if($temp_id == 4){
			$data = array(
				'admin_id' => 0,
				'image' => $image,
				'editorial' => $page_post,
				'title' => $title,
				'date' => $date
				);
			$this->db->insert('editorial_new',$data);
			redirect(base_url().'superadmin_editorial');
			exit();
		}
	}
	public function getPostInfo($post_id,$temp_id){
		
		
		if($temp_id == 1){
			$query = "SELECT news_body as body,title,date,image,id FROM news WHERE id=$post_id";
			$postInfo = $this->db->query($query);
		}else if($temp_id == 2){
			$query = "SELECT sports_body as body,title,date,image,id FROM sports WHERE id=$post_id";
			$postInfo = $this->db->query($query);
		}else if($temp_id == 3){
			$query = "SELECT entertainment as body,title,date,image,id FROM entertainment_news WHERE id=$post_id";
			$postInfo = $this->db->query($query);		
		}else{
			$query = "SELECT editorial as body,title,date,image,id FROM editorial_new WHERE id=$post_id";
			$postInfo = $this->db->query($query);
		}

		return $postInfo->result();
				
	}
	public function update_pages_post($post_id,$image,$temp_id,$page_post,$title){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$this->db->where('id',$post_id);
		if($temp_id == 1){
			if($image == "no image"){
				$data = array(
				'admin_id' => 0,
				'news_body' => $page_post,
				'title' => $title,
				'date_update' => $date
				);
			}else{
				$data = array(
				'admin_id' => 0,
				'image' => $image,
				'news_body' => $page_post,
				'title' => $title,
				'date_update' => $date
				);
			}
			
			$this->db->update('news',$data);
			redirect(base_url().'superadmin_news');
			exit();
		}else if($temp_id == 2){
				if($image == "no image"){
				$data = array(
				'admin_id' => 0,
				'sports_body' => $page_post,
				'title' => $title,
				'date_update' => $date
				);
				}else{
				$data = array(
				'admin_id' => 0,
				'image' => $image,
				'sports_body' => $page_post,
				'title' => $title,
				'date_update' => $date
				);
				}

			$this->db->update('sports',$data);
			redirect(base_url().'superadmin_sports');
			exit();
		}else if($temp_id == 3){
					if($image == "no image"){
						$data = array(
						'admin_id' => 0,
						'entertainment' => $page_post,
						'title' => $title,
						'date_update' => $date
						);
					}else{
						$data = array(
						'admin_id' => 0,
						'image' => $image,
						'entertainment' => $page_post,
						'title' => $title,
						'date_update' => $date
				);					}
			$this->db->update('entertainment_news',$data);
			redirect(base_url().'superadmin_entertainment');
			exit();
		}else if($temp_id == 4){
					if($image == "no image"){
						$data = array(
						'admin_id' => 0,
						'editorial' => $page_post,
						'title' => $title,
						'date_update' => $date
							);
					}else{
						$data = array(
						'admin_id' => 0,
						'image' => $image,
						'editorial' => $page_post,
						'title' => $title,
						'date_update' => $date
							);
					}
			$this->db->update('editorial_new',$data);
			redirect(base_url().'superadmin_editorial');
			exit();
		}
	}
	public function delete_pages_post($post_id,$temp_id){
		$this->db->where('id',$post_id);
		if($temp_id == 1){
			$this->db->delete('news');
		}else if($temp_id == 2){
			$this->db->delete('sports');
		}else if($temp_id == 3){
			$this->db->delete('entertainment_news');
		}else if($temp_id == 4){
			$this->db->delete('editorial_new');
		}
	}

	// messages

	public function insert_new_message($department_id,$new_message){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');

		$data = array(
			'sender_id' => 0,
			'receiver_id' => $department_id,
			'message' => $new_message,
			'date' => $date 
			);

		$this->db->insert('admin_messaging',$data);
		$this->session->set_flashdata(array('messaging_alert' => 
			'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Message Successfully Send </div>'
			));
		redirect(base_url().'superadmin_message');
		exit();
	}

	public function messageInbox(){
		$query = "SELECT department,message,admin_messaging.date as message_date,date_forward,admin_messaging.id as message_id,sender_id,receiver_id FROM admin_messaging 
		LEFT JOIN department ON department.id = admin_messaging.sender_id
		WHERE admin_messaging.receiver_id=0

		ORDER BY admin_messaging.id DESC";
		$messageInbox = $this->db->query($query);

		return $messageInbox->result();
	}

	public function reply_info_message($message_id){
		
		$query = "SELECT department,message,admin_messaging.date as message_date,date_forward,admin_messaging.id as message_id,sender_id,receiver_id FROM admin_messaging LEFT JOIN department ON department.id = admin_messaging.sender_id
			WHERE admin_messaging.id = $message_id";
		$replyInfo = $this->db->query($query);

		return $replyInfo->result();
	}

	public function delete_action_message($message_id){

		$data = array(
			'message_id' => $message_id,
			'deleter_id' => 0
			);

		$this->db->insert('admin_delete_messages',$data);
		$this->delete_permanent_message($message_id);
	}
	public function delete_permanent_message($message_id){
			$query = "SELECT * FROM admin_delete_messages WHERE message_id ='$message_id'";
			$checkToPermanentDelete = $this->db->query($query);
			if($checkToPermanentDelete->num_rows() >= 2){
				$this->db->where('message_id',$message_id);
				$this->db->delete('admin_delete_messages');
			}
	}
	public function show_personal_info(){
		$superAdminInfo = $this->db->get('super_admin_info');
		return $superAdminInfo->result();
	}

// para sa reason of blocker

	public function get_reason(){
		$reason = $this->db->get('block_reason');
		return $reason->result();
	}
// para sa change personel info
	public function change_personel_info($firstname,$lastname,$middlename,$faculty_id,$email,$contact_number){
		$data = array(
			'firstname' => $firstname,
			'lastname' => $lastname,
			'middlename' => $middlename,
			'faculty_id' => $faculty_id,
			'email' => $email,
			'contact_number' => $contact_number
			);

		$this->db->where('sa_id',0);
		$this->db->update('super_admin_info',$data);

		$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Personal Information has been successfully updated </div>'));
		
	}

	public function get_scsit_info(){
		$scsit_information = $this->db->get('locator');
		return $scsit_information->result();
	}

	public function update_contact($contact_number,$email,$address){

		$data = array(
			'contact' => $contact_number,
			'locator' => $address,
			'information' => $email
			);
		$this->db->update('locator',$data);
		$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Contact  Info has been successfully updated </div>'));
		redirect(base_url().'superadmin_settings');
		exit();
	}
	public function add_reason($reason){
		$data = array(
			'reason' => $reason
			);
		$this->db->insert('block_reason',$data);
		$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Reason Successfully added </div>'));
		redirect(base_url().'superadmin_settings');
		exit();
	}

	public function update_reason($reason,$reason_id){
		$data = array(
			'reason' => $reason
			);
		$this->db->where('id',$reason_id);
		$this->db->update('block_reason',$data);
		$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Reason Successfully updated </div>'));
		redirect(base_url().'superadmin_settings');
		exit();
	}

	public function delete_reason($reason_id){
		$this->db->where('id',$reason_id);
		$this->db->delete('block_reason');

		$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Reason Successfully deleted </div>'));
		redirect(base_url().'superadmin_settings');
		exit();
	}

	public function deactivate_system($status){
		$data = array('status' => $status);
		$this->db->update('system_status',$data);

		if($status == 1){
			$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Successfully Deactivated system </div>'));
		}else{
			$this->session->set_flashdata(array('personal_info_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Successfully Activated system </div>'));
		}
		redirect(base_url().'superadmin_settings');
		exit();
	}
	public function get_system_status(){
		$this->db->where('status',1);
		$getStatus = $this->db->get('system_status');

		return $getStatus;
	}

	public function get_admin_logs(){
		$query = "SELECT admin_logs.id as logs_id,username,department,admin_personel.date as admin_date,admin_logs.date_login as logs_date_login,admin_logs.date_logout as logs_date_logout FROM admin_personel
					LEFT JOIN department ON admin_personel.assigned_department=department.id
					LEFT JOIN admin_logs ON admin_personel.id = admin_logs.admin_id";
		$admin_logsQuery = $this->db->query($query);

		return $admin_logsQuery->result();
	}
	public function delete_logs($logs_id){
		$this->db->where('id',$logs_id);
		$this->db->delete('admin_logs');

		$this->session->set_flashdata(array('logs_message'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Logs has been successfully deleted </div>'));

		redirect(base_url()."superadmin_logs");
		exit();
		
	}

	public function superadmin_announcement(){
		
		$query = "SELECT admin_announcement.id as announcement_id, admin_id,announcement,admin_announcement.date as announcement_date,admin_announcement.date_update as announcement_date_update,fname,lname,Mname
					FROM admin_announcement LEFT JOIN admin_personel ON admin_announcement.admin_id = admin_personel.id ORDER BY admin_announcement.id DESC";

		$getAnnouncement = $this->db->query($query);
		return $getAnnouncement->result();

	}

	public function add_announcement($announcement){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array('announcement' => $announcement,'date' => $date);

		$this->db->insert('admin_announcement',$data);
		$this->session->set_flashdata(array('admin_announcement'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Announcement has been successfully added</div>'));

		redirect(base_url()."superadmin_announcement");
		exit();
	}
	public function update_announcement($announcement,$announcement_id){
		date_default_timezone_set('Asia/Manila');
		$date = date('h:m:s a m/d/y');
		$data = array('announcement' => $announcement, 'date_update' => $date);
		$this->db->where('id',$announcement_id);
		$this->db->update('admin_announcement',$data);
		$this->session->set_flashdata(array('admin_announcement'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Announcement has been successfully UPDATED</div>'));

		redirect(base_url()."superadmin_announcement");
		exit();
	}

	public function delete_announcement($announcement_id){
		$this->db->where('id',$announcement_id);
		$this->db->delete('admin_announcement');
		$this->session->set_flashdata(array('admin_announcement'=>'<div class="alert alert-success alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> Announcement has been successfully DELETED</div>'));

		redirect(base_url()."superadmin_announcement");
		exit();
	}
	
}	





?>