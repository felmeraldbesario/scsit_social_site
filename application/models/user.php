<?php

/**
 * authour: Jermaine Maturan
 * Name: User
 * Date Created: December 24, 2015
 */
class user extends CI_Model {



	/**
     * Name: select_department
     * @param: null
     * return: void
     * purpose: destroying session
     */
	public function userlogout(){
		$id = $this->session->userdata('login_id');
		$status = array(
				'user_id' => $id,
				'status' => 0
			);
		$this->db->where('user_id', $id);
		$this->db->update('user_status', $status);
	    $sess_data = array(
	    		'login_id' =>'',
	    		'login_token' =>'',
	    		'login' => TRUE
	    	);
		$this->session->unset_userdata($data);
		$this->session->sess_destroy();
		redirect(base_url().'index');
	}

	/**
     * Name: sessionUser
     * @param: null
     * return: array
     * purpose: check session user
     */
	public function sessionUser(){
		$id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT id, birthdate, email, username, firstname, middlename, lastname, address, mobile_number, course, year, id_number, department_id FROM user WHERE id='".$id."'");
		$result = $query->row();

		$data = array(
				'firstname' => $result->firstname,
				'middlename' => $result->middlename,
				'id_number' => $result->id_number,
				'lastname' => $result->lastname,
				'address' => $result->address,
				'id_number' => $result->id_number,
				'mobile_number' => $result->mobile_number,
				'course' => $this->getCourse($result->course),
				'year' => $result->year,
				'email' => $result->email,
				'profile_pic' => $this->getProfilePic($id),
				'department' => $this->department($result->id),
				'birthdate' => $result->birthdate,
				'username' => $result->username,
				'department_image' => $this->departmentImg($result->department_id)
			);
		return $data;
	}

	/**
     * Name: departmentImg
     * @param: user_id
     * return: string
     * purpose: check session course
     */
	public function departmentImg($user_id){
		$query = $this->db->query("SELECT image FROM department WHERE id='".$user_id."'");
		$result = $query->row();
		return $result->image;
	}
	/**
     * Name: getCourse
     * @param: course_id
     * return: string
     * purpose: check session course
     */
	public function getCourse($course_id){
		$query = $this->db->query("SELECT course FROM course WHERE id='".$course_id."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			return $result->course;
		}
	}

	/**
     * Name: getProfilePic
     * @param: null
     * return: string
     * purpose: check session profile pic
     */
	public function getProfilePic($id){
		
		$query = $this->db->query("SELECT  image FROM user_profile_pic WHERE user_id='".$id."'");
		if($query->num_rows() == 0){
			return null;
		}else{
			$result = $query->row();
			return $result->image;
		}
		
	}
	/**
     * Name: getCourse
     * @param: null
     * return: string
     * purpose: check session course
     */
	public function departments(){
		$query = $this->db->query("SELECT id, department, image FROM department");
		$count = 0;
		foreach($query->result() as $row){
			$count++;
			$data[$count] = array(
					'id' => $row->id,
					'department' => $row->department,
					'image' => $row->image,
					'count' => $this->deptCount($row->id)
				);
		}
		return $data;
	}

	public function deptCount($dept_id){
		$this->db->where('department_id', $dept_id);
		return $this->db->count_all_results('user');
	}


	/**
     * Name: updatePic
     * @param: image
     * return: null
     * purpose: check session course
     */
	public function updatePic($image){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y");
     		
		$data = array(
				'image' => $image,
				'date' => $date_created
			);
		$id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT user_id FROM user_profile_pic WHERE user_id='".$id."'");
		if($query->num_rows() != 0){
			$this->db->where('user_id',  $id);
			$this->db->update('user_profile_pic', $data);
			redirect(base_url().'home');
		}else{
			$insertData = array(
				'user_id' => $id,
				'image' => $image,
				'date' => $date_created
			);
			$this->db->insert('user_profile_pic', $insertData);
			redirect(base_url().'home');
		}
	}

	/**
     * Name: getUsers
     * @param: image
     * return: null
     * purpose: check department members
     */
	public function getUsers($id){
		$user_id = $this->session->userdata('login_id');
		
		$query = $this->db->query("SELECT id, firstname, lastname, middlename, gender, age FROM user WHERE department_id='".$id."'");
		$count = 0;
		if($query->num_rows() != 0){
			foreach($query->result() as $row){
				$count++;
				$data[$count] = array(
					'id' => $row->id,
					'firstname' => $row->firstname,
					'lastname' => $row->lastname,
					'middlename' => $row->middlename,
					'gender' => $row->gender,
					'age' => $row->age,
					'profile_pic' => $this->getProfile($row->id),
					'friend' => $this->getFriend($row->id),
					'blocked' => $this->getBlock($row->id),
					'request' => $this->friendRequest($row->id)
				);
			}
			
			return $data;
		}
	}

	/**
     * Name: getProfile
     * @param: id
     * return: string
     * purpose: check user profile
     */
	public function getProfile($id){
		$query = $this->db->query("SELECT id, image FROM user_profile_pic WHERE user_id='".$id."'");
		if($query->num_rows != 0){
			$result = $query->row();
			if($result->image == ''){
				return '';
			}else{
				return $result->image;
			}
		}
	}

	/**
     * Name: getFriend
     * @param: id
     * return: int
     * purpose: check user friend
     */
	public function getFriend($user_id){
		$session_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT user_id, friends_id 
				FROM friend_list 
				WHERE user_id='".$session_id."' && friends_id='".$user_id."' 
				OR user_id='".$user_id."' && friends_id='".$session_id."'");
		if($query->num_rows() == 0){
			return 0;
		}else{
			return 1;
		}
	}

	/**
     * Name: getBlock
     * @param: id
     * return: in
     * purpose: check user if block
     */
	public function getBlock($user_id){
		$session_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT user_id, blocked_user_id 
				FROM user_block_list 
				WHERE user_id='".$session_id."' && blocked_user_id='".$user_id."'
				OR user_id='".$user_id."' && blocked_user_id='".$session_id."'");
		if($query->num_rows() == 0){
			return 0;
		}else{
			return 1;
		}
	}

	/**
     * Name: friendRequest
     * @param: user_id
     * return: int
     * purpose: check user if already have request
     */
	public function friendRequest($user_id){
		$session_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT sender_id, receiver_id 
				FROM add_friend 
				WHERE sender_id='".$session_id."' && receiver_id='".$user_id."'
				OR sender_id='".$user_id."' && receiver_id='".$session_id."'");
		if($query->num_rows() == 0){
			return 0;
		}else{
			return 1;
		}
	}

	/**
     * Name: friendRequestCount
     * @param: session_id
     * return: int
     * purpose: check user friend request
     */
	public function friendRequestCount($sess_id){
		$this->db->where('receiver_id', $sess_id);
		echo $this->db->count_all_results('add_friend');

	}

	/**
     * Name: sendFRequest
     * @param: user_id
     * return: int
     * purpose: send friend request
     */
	public function sendFRequest($user_id){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y");
		$sess_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT * FROM add_friend
				WHERE sender_id='".$sess_id."' && receiver_id='".$user_id."'
				OR sender_id='".$user_id."' && receiver_id='".$sess_id."'");
		if($query->num_rows() == 0){
			$data = array(
					'sender_id' => $sess_id,
					'receiver_id' => $user_id,
					'date' => $date_created
				);
			$this->db->insert('add_friend', $data);
		}
	}

	/**
     * Name: confirmFRequest
     * @param: user_id
     * return: int
     * purpose: display request info
     */
	public function confirmFRequest(){
		$sess_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT sender_id, receiver_id FROM add_friend WHERE receiver_id='".$sess_id."'");
		$count = 0;
		if($query->num_rows == 0){
			$data = '';
		}else{
			foreach($query->result() as $row){
				$count++;
				$data[$count] = array(
						'sender_id' => $row->sender_id,
						'sender_fname' => $this->getFirstName( $row->sender_id),
						'sender_lastname' => $this->getLastName($row->sender_id),
						'sender_age' => $this->getAge($row->sender_id),
						'sender_gender' => $this->getGender($row->sender_id),
						'sender_pic' => $this->getProfilePic($row->sender_id),
						'department'=> $this->department($row->sender_id)
						);
			}
			return $data;
		}
	}

	/**
     * Name: getFirstName
     * @param: sender_id
     * return: string
     * purpose: display request firstname
     */
	public function getFirstName($sender_id){
		$query = $this->db->query("SELECT firstname FROM user WHERE id='".$sender_id."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			return $result->firstname;
		}
	}	

	/**
     * Name: getLastName
     * @param:  sender_id
     * return: string
     * purpose: display request lastname
     */
	public function getLastName($sender_id){
		$query = $this->db->query("SELECT lastname FROM user WHERE id='".$sender_id."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			return $result->lastname;
		}
	}	


	/**
     * Name: getAge
     * @param:  sender_id
     * return: string
     * purpose: display request age
     */
	public function getAge($sender_id){
		$query = $this->db->query("SELECT age FROM user WHERE id='".$sender_id."'");
		if($query->num_rows() != 0){
		$result = $query->row();
		return $result->age;
		}
	}

	/**
     * Name: getGender
     * @param:  sender_id
     * return: string
     * purpose: display request age
     */
	public function getGender($sender_id){
		$query = $this->db->query("SELECT gender FROM user WHERE id='".$sender_id."'");
		if($query->num_rows() != 0){
		$result = $query->row();
		return $result->gender;
		}
	}

	/**
     * Name: getGender
     * @param:  sender_id
     * return: string
     * purpose: display request department
     */
	public function department($sender_id){
		$query = $this->db->query("SELECT department_id FROM user WHERE id='".$sender_id."'");
		$result = $query->row();
		$dept_id = $result->department_id;
		$deptQuery = $this->db->query("SELECT department FROM department WHERE id='".$dept_id."'");
		$deptResult = $deptQuery->row();
		return $deptResult->department;
	}

	/**
     * Name: getStatus
     * @param:  user_id
     * return: string
     * purpose: check if user is online or offline
     */
	public function getStatus($user_id){
		$query = $this->db->query("SELECT status FROM user_status WHERE user_id='".$user_id."'");
		$result = $query->row();
		return $result->status;
	}

	/**
     * Name: cfRequest
     * @param:  sender_id
     * return: null
     * purpose: confirm request
     */
	public function cfRequest($user_id){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y");
		$sess_id = $this->session->userdata('login_id');
		$data = array(
				'user_id' => $sess_id,
				'friends_id' => $user_id,
				'date' => $date_created
			);
		
		$this->db->insert('friend_list', $data);
		$query = $this->db->query("DELETE FROM add_friend 
			WHERE sender_id='".$sess_id."' && receiver_id='".$user_id."' 
			OR sender_id='".$user_id."' && receiver_id='".$sess_id."'");

	}


	/**
     * Name: checkFriendRelationship
     * @param: image
     * return: null
     * purpose: check department members
     */
	public function checkFriendRelationship(){
		$user_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT id, firstname, lastname, middlename, gender, age FROM user WHERE  id != '".$user_id."'");
		$count = 0;
		foreach($query->result() as $row){
			$count++;
			$data[$count] = array(
				'id' => $row->id,
				'firstname' => $row->firstname,
				'lastname' => $row->lastname,
				'middlename' => $row->middlename,
				'gender' => $row->gender,
				'age' => $row->age,
				'profile_pic' => $this->getProfile($row->id),
				'friend' => $this->getFriend($row->id),
				'blocked' => $this->getBlock($row->id),
				'request' => $this->friendRequest($row->id),
				'status' => $this->getStatus($row->id)
			);
		}

		return $data;
	}

	/**
     * Name: messageCount
     * @param: sess_id
     * return: int
     * purpose: check count of unread message/s
     */
	public function messageCount($sess_id){
		$this->db->where('receiver_id', $sess_id);
		$this->db->where('status', 0);
		echo $this->db->count_all_results('messaging');
	}

	/**
     * Name: campusPost
     * @param: status
     * return: null
     * purpose: insert status on db
     */
	public function campusPost($status, $image){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("F j,Y h:i:s A");
		$data = array(
				'user_id' => $this->session->userdata('login_id'),
				'user_id_number' =>$this->get_idNumber($this->session->userdata('login_id')),
				'image' => $image,
				'campus_post' => $status,
				'date' => $date_created
			);
		$this->db->insert('campus_post', $data);
		redirect(base_url().'home');
		
	}

	/**
     * Name: get_idNumber
     * @param: status
     * return: null
     * purpose: insert status on db
     */
	public function get_idNumber($id){
		$query = $this->db->query("SELECT id_number FROM user WHERE id='".$id."'");
		$result = $query->row();
		return $result->id_number;
	}

	/**
     * Name: getCampusPost
     * @param: status
     * return: String
     * purpose: retrieve campus_post sort by id
     */
	public function getCampusPost(){
		$query = $this->db->query("SELECT id, user_id, user_id_number, image, campus_post, campus_post.date FROM campus_post ORDER BY id DESC");
				$counter = 0;;
				if($query->num_rows() != 0){
					foreach($query->result() as $row){
						$data[$counter] = array(
								'cpost_id' => $row->id,
								'user_id' => $row->user_id,
								'user_id_number' => $row->user_id_number,
								'campus_post_image' => $this->getCampusPostImage($row->id),
								'status' => $this->getCampusPostStatus($row->id),
								'status_date' => $this->getCampusPostDate($row->id),
								'firstname' =>  $this->getFirstName($row->user_id),
								'lastname' => $this->getLastName($row->user_id),
								'friend' => $this->getFriend($row->user_id),
								'blocked' => $this->getBlock($row->user_id),
								'profile_pic' => $this->getProfile($row->user_id),
								'cpost_like' => $this->getCPostLikeCount($row->id),
								'cpost_commenters' => $this->getCpostCommenterCounts($row->id)
							);
						$counter++;
					}
					return $data;
				}
				
	}

	/**
     * Name: getCampusPostImage
     * @param: user_id
     * return: String
     * purpose: retrieve image From campus_post
     */
	public function getCampusPostImage($id){
		$query = $this->db->query("SELECT image FROM campus_post WHERE id='".$id."'");
		if($query->num_rows() == 0){
			return null;
		}else{
			$result = $query->row();
			return $result->image;
		}
	}


	/**
     * Name: getStatus
     * @param: user_id
     * return: String
     * purpose: retrieve status From campus_post
     */
	public function getCampusPostStatus($id){
		$query = $this->db->query("SELECT campus_post FROM campus_post WHERE id='".$id."'");
		if($query->num_rows() == 0){
			return null;
		}else{
			$result = $query->row();
			return $result->campus_post;
		}
	}


	/**
     * Name: getCampusPostDate
     * @param: user_id
     * return: String
     * purpose: retrieve date From campus_post
     */
	public function getCampusPostDate($id){
		$query = $this->db->query("SELECT campus_post.date FROM campus_post WHERE id='".$id."'");
		if($query->num_rows() == 0){
			return null;
		}else{
			$result = $query->row();
			return $result->date;
		}
	}

	/**
     * Name: delPost
     * @param: id
     * return: null
     * purpose: delete campus post using ID 
     */
	public function delPost($id){
		$this->db->where('id', $id);
		$this->db->delete('campus_post');	

		$this->db->where('topic_id', $id);
		$this->db->delete('campus_post_feedback');

		$this->db->where('campus_post_id', $id);
		$this->db->delete('campus_post_comment');
	}


	/**
     * Name: clikePost
     * @param: id
     * return: null
     * purpose: like campus post using ID 
     */
	public function clikePost($id){
		$user_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT * FROM campus_post_feedback 
				WHERE topic_id='".$id."' && student_id='".$user_id."'");
		if($query->num_rows() == 0){
			$data = array(
					'topic_id' => $id,
					'student_id' => $user_id
				);
			$this->db->insert('campus_post_feedback', $data);
			$this->db->where('topic_id', $id);
			echo $this->db->count_all_results('campus_post_feedback').' User';
		}else{
			$query = $this->db->query("DELETE FROM campus_post_feedback 
				WHERE topic_id='".$id."' && student_id='".$user_id."'");
			echo $this->db->count_all_results('campus_post_feedback').' User';
		}
	}

	/**
     * Name: getCPostLikeCount
     * @param: id
     * return: String
     * purpose: retrieve number of likes 
     */
	public function getCPostLikeCount($id){
		$this->db->where('topic_id', $id);
		return $this->db->count_all_results('campus_post_feedback');
	}

	/**
     * Name: getCpostCommenterCounts
     * @param: id
     * return: String
     * purpose: retrieve number of commenters 
     */
	public function getCpostCommenterCounts($id){
		$this->db->where('campus_post_id', $id);
		return $this->db->count_all_results('campus_post_comment');
	}

	/**
     * Name: commentCPost
     * @param: id, comment
     * return: string
     * purpose: insert comment
     */
	public function commentCPost($comment,$id){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y h:i:s A");
		$commenter_id = $this->session->userdata('login_id');
		if($comment != ''){
			$data = array(
					'campus_post_id' => $id,
					'campus_commenter_id' => $commenter_id,
					'comment' => $comment,
					'date' => $date_created
				);

			$selectQuery = $this->db->get_where('campus_post_comment', $data);
			if($selectQuery->num_rows() == 0){
				$this->db->insert('campus_post_comment', $data);
			}
		}
	}

	/**
     * Name: getAllCommentCPost
     * @param: id
     * return: string
     * purpose: retrieving all comments on the particular id
     */
	public function getAllCommentCPost($id){
		$count = 0;
		$query = $this->db->query("SELECT * FROM campus_post_comment WHERE campus_post_id='".$id."' ORDER BY id DESC LIMIT 5");
		if($query->num_rows() == 0){
			
		}else{
			foreach($query->result() as $row){
				$data[$count] = array(
						'cpost_comment_id' => $row->id,
						'cpost_id' => $row->campus_post_id,
						'cpost_commenter_id' => $row->campus_commenter_id,
						'cpost_firstname' => $this->getFirstName($row->campus_commenter_id),
						'cpost_lastname' => $this->getLastName($row->campus_commenter_id),
						'cpost_time' => $row->date,
						'cPostComment' => $row->comment,
						'cpost_commenter_profile' => $this->getProfilePic($row->campus_commenter_id)
					);
				$count++;
			}

			return $data;
		}
		
	}


	/**
     * Name: delComment
     * @param: id
     * return: null
     * purpose: delete comment of that particular id
     */
	public function delComment($id){
		$this->db->where('id', $id);
		$this->db->delete('campus_post_comment');
	}


	public function getCampusPostCommentsHome(){
		$count = 0;
		$query = $this->db->query("SELECT * FROM campus_post_comment ORDER BY id DESC LIMIT 5");
		if($query->num_rows() == 0){
			
		}else{
			foreach($query->result() as $row){
				$data[$count] = array(
						'cpost_comment_id' => $row->id,
						'cpost_id' => $row->campus_post_id,
						'cpost_commenter_id' => $row->campus_commenter_id,
						'cpost_firstname' => $this->getFirstName($row->campus_commenter_id),
						'cpost_lastname' => $this->getLastName($row->campus_commenter_id),
						'cpost_time' => $row->date,
						'cPostComment' => $row->comment,
						'cpost_commenter_profile' => $this->getProfilePic($row->campus_commenter_id)
					);
				$count++;
			}
			return $data;
		}
		
	}

	/**
     * Name: getCpostCommenterCountAjax
     * @param: id
     * return: String
     * purpose: retrieve number of commenters 
     */
	public function getCpostCommenterCountAjax($id){
		$this->db->where('campus_post_id', $id);
		echo $this->db->count_all_results('campus_post_comment');
	}

	/**
     * Name: cpost_viewAllComments
     * @param: id
     * return: String
     * purpose: retrieve all comments on a particular id
     */
	public function cpost_viewAllComments($id){
		$count = 0;
		$query = $this->db->query("SELECT * FROM campus_post_comment  WHERE campus_post_id='".$id."'ORDER BY id DESC");
		if($query->num_rows() == 0){
			
		}else{
			foreach($query->result() as $row){
				$data[$count] = array(
						'cpost_comment_id' => $row->id,
						'cpost_id' => $row->campus_post_id,
						'cpost_commenter_id' => $row->campus_commenter_id,
						'cpost_firstname' => $this->getFirstName($row->campus_commenter_id),
						'cpost_lastname' => $this->getLastName($row->campus_commenter_id),
						'cpost_time' => $row->date,
						'cPostComment' => $row->comment,
						'cpost_commenter_profile' => $this->getProfilePic($row->campus_commenter_id)
					);
				$count++;
			}
			return $data;
		}
	}

	/**
     * Name: getAllCPostCommenters
     * @param: id
     * return: String
     * purpose: retrieve all names of commenter of a particular id
     */
	public function getAllCPostCommenters($id){
		$query = $this->db->query("SELECT campus_commenter_id FROM campus_post_comment WHERE campus_post_id='".$id."' GROUP BY campus_commenter_id");
		$count = 0;
		if($query->num_rows() != 0){
			foreach($query->result() as $row){
				$data[$count] = array(
						'student_id' => $row->campus_commenter_id,
						'firstname' => $this->getFirstName($row->campus_commenter_id),
						'lastname' => $this->getLastName($row->campus_commenter_id)
					);
				$count++;
			}
			return $data;
		}
	}

	/**
     * Name: getDepartment
     * @param: id
     * return: String
     * purpose: retrieve department name from a particular id
     */
	public function getDepartment($id){
		$query = $this->db->query("SELECT department FROM department WHERE id='".$id."'");
		$result = $query->row();
		return $result->department;
	}

	/**
     * Name: jam_cpost_likers
     * @param: id
     * return: String
     * purpose: retrieve likers of the cpost of particular id
     */
	public function jam_cpost_likers($id){
		$query = $this->db->query("SELECT student_id FROM campus_post_feedback WHERE topic_id='".$id."'");
		if($query->num_rows() != 0){
			$count = 0;
			foreach($query->result() as $row){
				$data[$count] = array(
						'student_id' => $row->student_id,
						'firstname' => $this->getFirstName($row->student_id),
						'lastname' => $this->getLastName($row->student_id)
					);
				$count++;
			}
			return $data;
		}
	}	

	/**
     * Name: getAllPostPicture
     * @param: null
     * return: String
     * purpose: retrieve all post pictures of a sessioned user
     */
	public function getAllPostPicture($id){
		if(!$this->userBlockedChecker($id)){
			redirect(base_url().'home');
		}
		$query = $this->db->query("SELECT * FROM campus_post WHERE user_id='".$id."' && user_id_number!= 0");
		if($query->num_rows() != 0){
			return $query->result();
		}
	}

	/**
     * Name: jam_findProfile
     * @param: null
     * return: String
     * purpose: retrieve user data with a particular id
     */
	public function jam_findProfile($id){
		if(!$this->userBlockedChecker($id)){
			redirect(base_url().'home');
		}
		$query = $this->db->query("SELECT id, firstname, lastname FROM user WHERE id='".$id."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			$data = array(
					'firstname' => $result->firstname,
					'lastname' => $result->lastname,
					'profile_pic' => $this->getProfilePic($result->id)
				);

			return $data;
		}else{
			redirect(base_url().'home');
		}
	}

	/**
     * Name: userBlockedChecker
     * @param: null
     * return: boolean
     * purpose: check if user is blocked by sessioned user
     */
	public function userBlockedChecker($id){
		$session_id = $this->session->userdata('login_id');
		if($id == $session_id){
			return true;
		}else{
			$query = $this->db->query("SELECT user_id, blocked_user_id 
					FROM user_block_list
					WHERE  user_id='".$session_id."' && blocked_user_id='".$id."'
					OR user_id='".$id."' && blocked_user_id='".$session_id."'");
			if($query->num_rows() == 0){
				return true;
			}else{
				return false;
			}
		}
	}

	/**
     * Name: new_sms
     * @param: string
     * return: boolean
     * purpose: check user new sms
     */
	public function new_sms(){
		$session_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT * FROM messaging WHERE receiver_id='".$session_id."' && status = 0 ORDER BY id DESC");
		$count = 0;
		if($query->num_rows() != 0){
			foreach($query->result() as $row){
				$data[$count] = array(
						'sms_id' => $row->id,
						'sender_id' => $row->sender_id,
						'firstname' => $this->getFirstName($row->sender_id),
						'lastname' => $this->getLastName($row->sender_id),
						'message' => $row->message,
						'date' => $row->date,
						'blocked' => $this->getBlock($row->sender_id),
						'profile_pic'=> $this->getProfilePic($row->sender_id)
					);
				$count++;
			}
			return $data;
		}
	}

	/**
     * Name: show_sms
     * @param: id
     * return: string
     * purpose: retrieve all sms from a particular sender id
     */
	public function show_sms($id){
		$session_id = $this->session->userdata('login_id');
		$count = 0;
		
		$query = $this->db->query("SELECT * FROM messaging
				WHERE sender_id='".$id."' && receiver_id='".$session_id."'
				OR sender_id='".$session_id."' && receiver_id='".$id."'
				ORDER BY id DESC");
		if($query->num_rows()){
			foreach($query->result() as $row){
				$this->update_sms($row->id);
				$data[$count] = array(
							'sms_id' => $row->id,
							'sender_id' => $row->sender_id,
							'firstname' => $this->getFirstName($row->sender_id),
							'lastname' => $this->getLastName($row->sender_id),
							'message' => $row->message,
							'date' => $row->date,
							'blocked' => $this->getBlock($row->sender_id),
							'profile_pic'=> $this->getProfilePic($row->sender_id),
							'deleted' => $this->getDeletedSms($session_id, $row->id)
						);
					$count++;
			}

			return $data;	
		}
	}

	public function getDeletedSms($session_id, $id){
		$query = $this->db->query("SELECT * FROM deleted_messages
			WHERE message_id='".$id."' && deleter_id='".$session_id."'");
		if($query->num_rows() != 0){
			return 1;
		}
	}
	/**
     * Name: update_sms
     * @param: id
     * return: null
     * purpose: update unread messages to read messages 0 - 1
     */
	public function update_sms($id){
		$session_id = $this->session->userdata('login_id');
		$data = array(
				'status' => 1
			);
		$this->db->where('id', $id);
		$this->db->where('receiver_id', $session_id);
		$this->db->update('messaging', $data);
	}


	/**
     * Name: updateAccount
     * @param: firstname, middlename, lastname, address, mobile_number
     * return: null
     * purpose: update account
     */
	public function updateAccount($firstname, $middlename, $lastname, $address, $mobile_number, $birthdate){
		$id = $this->session->userdata('login_id');
		$data = array(
				'firstname' => $firstname,
				'lastname' => $lastname, 
				'middlename' => $middlename,
				'address' => $address,
				'mobile_number' => $mobile_number,
				'birthdate' => $birthdate
			);
	
		$this->db->where('id',  $id);
		$this->db->update('user', $data);
		redirect(base_url().'home');
	}

	/**
     * Name: chechValidPassword
     * @param:password
     * return: boolean
     * purpose: check if password match to the 
     */
	public function chechValidPassword($password){
		$id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT id, password FROM user
				WHERE id='".$id."' && password='".$password."'");
		if($query->num_rows() != 0){
			return true;
		}else{
			return false;
		}
	}

	/**
     * Name: updateSecurityAccount
     * @param:valid_pass, email, username
     * return: string
     * purpose: update useraccounts 
     */
	public function updateSecurityAccount($valid_pass, $email, $username){
		$data = array(
				'password' => $valid_pass,
				'email' => $email,
				'username' => $username
			);
		$id = $this->session->userdata('login_id');
		$this->db->where('id', $id);
		if($this->db->update('user', $data)){
			return "Successfully updated your account..";
		}else{
			return "Sorry..there was an error on updating your account..";
		}
	}

	/**
     * Name: editCPost
     * @param: id
     * return: String
     * purpose: retrieve campus post for editing in a specific id
     */
	public function editCPost($id){
		$query = $this->db->query("SELECT id, image, campus_post, campus_post.date FROM campus_post WHERE id='".$id."'");
		return $query->result();
	}

	/**
     * Name: updateCampusPost
     * @param: cpost_id, status_update, image
     * return: null
     * purpose: insert and delete old post
     */
	public function updateCampusPost($cpost_id, $status_update, $image){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("F j,Y h:i:s A");
     	if($image != ''){
			$data = array(
					'campus_post' => $status_update,
					'image' => $image,
					'date' => $date_created
				);
		}else{
			$data = array(
					'campus_post' => $status_update,
					'date' => $date_created
				);
		}
		$this->db->where('id', $cpost_id);
		$this->db->update('campus_post', $data);

		$query = $this->db->query("SELECT id FROM campus_post ORDER BY id DESC limit 1");
		$result = $query->row();
		
		$data1 = array(
				'id' => $result->id + 1
			);
		$data2 = array(
				'topic_id' => $result->id + 1
			);
		$data3 = array(
				'campus_post_id' => $result->id + 1
			);
		$this->db->where('id', $cpost_id);
		$this->db->update('campus_post', $data1);

		$this->db->where('topic_id', $cpost_id);
		$this->db->update('campus_post_feedback', $data2);

		$this->db->where('campus_post_id', $cpost_id);
		$this->db->update('campus_post_comment', $data3);
		redirect(base_url().'home');
	}

	/**
     * Name: jam_viewFullImage
     * @param: id
     * return: array
     * purpose: retrieve all information from a campus post of a specific id
     */
	public function jam_viewFullImage($id){
		$query = $this->db->query("SELECT * FROM campus_post WHERE id='".$id."'");
		$result = $query->row();
		$data = array(
				'id' => $result->id,
				'firstname' => $this->getFirstName($result->user_id),
				'lastname' => $this->getLastName($result->user_id),
				'profile_pic' => $this->getProfilePic($result->user_id),
				'image' => $result->image,
				'date' => $result->date,
				'cpost_user_id' => $result->user_id,
				'likes' => $this->getCpostLikesById($result->id),
				'commenters' => $this->getCpostCommenterCounts($result->id),
				'status' => $result->campus_post
			);
		return $data;
	}

	/**
     * Name: getCpostLikesById
     * @param: id
     * return: array
     * purpose: retrieve all likers of a campus post
     */
	public function getCpostLikesById($id){
		$this->db->where('topic_id', $id);
		return $this->db->count_all_results('campus_post_feedback');
	}

	/**
     * Name: jam_viewFullImageComment
     * @param: id
     * return: array
     * purpose: retrieve all comments of a campus post
     */
	public function jam_viewFullImageComment($id){
		$query = $this->db->query("SELECT  id, campus_post_id, campus_commenter_id, comment, campus_post_comment.date
				FROM campus_post_comment WHERE campus_post_id='".$id."'");
		$count = 0;
		if($query->num_rows() != 0){
			foreach($query->result() as $row){
				$count++;
				$data[$count] = array(
						'id' => $row->id,
						'firstname' => $this->getFirstName($row->campus_commenter_id),
						'lastname' => $this->getLastName($row->campus_commenter_id),
						'date' => $row->date,
						'commenter' => $row->campus_commenter_id,	
						'profile_pic' => $this->getProfilePic($row->campus_commenter_id),
						'comment' => $row->comment
					);

			}
			return $data;
		}
	}

 	/**
     * Name: commentFullImge
     * @param: id
     * return: array
     * purpose: inserting and retreiving of comments
     */
	public function commentFullImge($cpost_id, $comment){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y h:i:s A");
     	$id = $this->session->userdata('login_id');

     	$data = array(
     			'campus_post_id' => $cpost_id,
     			'comment' => $comment,
     			'campus_commenter_id' => $id,
     			'date' => $date_created
     		);
     	$this->db->insert('campus_post_comment', $data);
     	$count = 0;
     	$query = $this->db->query("SELECT * FROM campus_post_comment 
     		WHERE campus_post_id='".$cpost_id."'
     		ORDER BY id DESC");
     	foreach($query->result() as $row){
     		$count++;
     		$data1[$count] = array(
     				'id' => $row->id,
     				'firstname' => $this->getFirstName($row->campus_commenter_id),
     				'lastname' => $this->getLastName($row->campus_commenter_id),
     				'comment' => $row->comment,
     				'date' => $row->date,
     				'profile_pic' => $this->getProfilePic($row->campus_commenter_id),
     				'commenter' => $row->campus_commenter_id
     			); 
     	}
     	return $data1;
	}

	/**
     * Name: viewfullimagecommentDel
     * @param: id
     * return: null
     * purpose: del comment
     */
	public function viewfullimagecommentDel($id){
		$this->db->where('id', $id);
		$this->db->delete('campus_post_comment');
	}

	/**
     * Name: departmentPost
     * @param: status, image
     * return: null
     * purpose: insert to department_post	
     */
	public function departmentPost($status, $image){
		$id = $this->session->userdata('login_id');
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("F j,Y h:i:s A");
     	$id = $this->session->userdata('login_id');

		$query = $this->db->query("SELECT id, id_number, department_id FROM user
				WHERE id='".$id."'");
		$result = $query->row();
		$data = array(
				'department_id' => $result->department_id,
				'user_id' => $result->id,
				'user_id_number' => $result->id_number,
				'image' => $image,
				'department_topic' => $status,
				'date' => $date_created
			);
		$this->db->insert('department_topic', $data);
		redirect(base_url().'department');
	}


	public function retrieveDeptPost(){
		$id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT department_id, id_number FROM user
				WHERE id='".$id."'");
		$result = $query->row();
		$count = 0;
		$deptQuery = $this->db->query("SELECT * FROM department_topic 
				WHERE department_id='".$result->department_id."' ORDER BY id DESC");
		if($deptQuery->num_rows() != 0){
			foreach($deptQuery->result() as $row){
				$count++;
				$data[$count] = array(
						'id' => $row->id,
						'firstname' => $this->getFirstnameDeptPost($row->user_id, $row->user_id_number),
						'lastname' => $this->getLastnameDeptPost($row->user_id, $row->user_id_number),
						'date' => $row->date,
						'profile_pic' => $this->getProfilePic($row->user_id),
						'dept_pic' => $row->image,
						'id_number' => $row->user_id_number,
						'status' => $row->department_topic,
						'user_id' => $row->user_id,
						'dept_post_likeCount' => $this->getLikeCountDeptPost($row->id),
						'dept_post_commenterCount' => $this->getCommenterCountDeptPost($row->id),
					);
			}
			
			return $data;		
		}
	}


	public function getFirstnameDeptPost($id, $id_number){
		$query = $this->db->query("SELECT firstname FROM user 
				WHERE id='".$id."' && id_number='".$id_number."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			return $result->firstname;
		}else{
			$adminQuery = $this->db->query("SELECT fname FROM admin_personel 
				WHERE id='".$id."' && admin_id_number='".$id_number."'");
			if($adminQuery->num_rows() != 0){
				return "Department";
			}else{
				if($id_number == 0){
					return "Super";
				}else{
					return "";
				}
			}
		}
	}


	public function getLastnameDeptPost($id, $id_number){
		$query = $this->db->query("SELECT lastname FROM user 
				WHERE id='".$id."' && id_number='".$id_number."'");
		if($query->num_rows() != 0){
			$result = $query->row();
			return $result->lastname;
		}else{
			$adminQuery = $this->db->query("SELECT lname FROM admin_personel 
				WHERE id='".$id."' && admin_id_number='".$id_number."'");
			if($adminQuery->num_rows() != 0){
				return "Admin";
			}else{
				if($id_number == 0){
					return "Admin";
				}else{
					return "";
				}
			}
		}
	}

	public function getLikeCountDeptPost($id){
		$this->db->where('department_topic_id', $id);
		return $this->db->count_all_results('department_topic_feedback');
	}

	public function getCommenterCountDeptPost($id){
		$this->db->where('department_topic_id', $id);
		return $this->db->count_all_results('department_topic_comments');
	}

	public function dept_post($id, $comment){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y h:i:s A");
     	$commenter_id = $this->session->userdata('login_id');
     	$query = $this->db->query("SELECT department_id FROM user
     			 WHERE id='".$commenter_id."'");
     	$result = $query->row();
		$data = array(
				'department_topic_id' =>  $id,
				'department_id' => $result->department_id,
				'commenter_id' => $commenter_id,
				'comment' => $comment,
				'date' => $date_created	
			);
		$this->db->insert('department_topic_comments', $data);
	}

	public function deptCommentPost($id){
		$query = $this->db->query("SELECT id, department_topic_id, commenter_id, comment, department_topic_comments.date FROM department_topic_comments
				WHERE department_topic_id='".$id."'");
		$count = 0;
		if($query->num_rows() != 0){
			foreach($query->result() as $row){
				$count++;
				$data[$count] = array(
						'id' => $row->id,
						'firstname' => $this->getFirstName($row->commenter_id),
						'lastname' => $this->getLastName($row->commenter_id),
						'commenter_id' => $row->commenter_id,
						'comment' => $row->comment,
						'date' => $row->date,
						'profile_pic' => $this->getProfilePic($row->commenter_id)
					);
			}
			return $data;
		}
	}

	public function sendMessagetoUser($sms, $receiver_id){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y h:i:s A");
     	$sender_id = $this->session->userdata('login_id');
     	$data = array(
     			'sender_id' => $sender_id,
     			'receiver_id' => $receiver_id,
     			'message' => $sms,
     			'status' => 0,
     			'date' => $date_created
     		);

     	$this->db->insert('messaging', $data);
	}

	public function editComment($id){
		$query = $this->db->query("SELECT id, comment FROM campus_post_comment WHERE id='".$id."'");
		if($query->num_rows() != 0){
			return $query->result();
		}
	}

	public function editCampusPost($comment, $id){
		date_default_timezone_set('Asia/Manila');
     	$date_created = date("d-m-Y h:i:s A");

     	$data = array(
     			'comment' => $comment,
     			'date' => $date_created
     		);
     	$this->db->where('id', $id);
     	$this->db->update('campus_post_comment', $data);
     	redirect(base_url().'home');
	}

	public function jam_likeDeptPost($id){
		$liker = $this->session->userdata('login_id');
		$data = array(
				'department_topic_id' => $id,
				'user_id' => $liker
			);
		$query = $this->db->get('department_topic_feedback', $data);
		if($query->num_rows() == 0){
			$this->db->insert("department_topic_feedback", $data);
		}else{
			$this->db->query("DELETE FROM department_topic_feedback WHERE department_topic_id='".$id."' && user_id='".$liker."'");
		}
	}

	public function jam_deleteDeptPostComment($id){
		
		$this->db->where("id", $id);
		$this->db->delete("department_topic_comments");
	}

	public function jam_delDeptPost($id){
		$this->db->where("id", $id);
		$this->db->delete("department_topic");
	}

	public function delConversation($id){
		
		$session_id = $this->session->userdata('login_id');
		$query = $this->db->query("SELECT * FROM  messaging 
			WHERE sender_id='".$session_id."' && receiver_id='".$id."' 
			OR sender_id='".$id."' && receiver_id='".$session_id."'");
			if($query->num_rows() != 0){
				foreach($query->result() as $row){
				$query1 = $this->db->query("SELECT * FROM deleted_messages
					WHERE message_id='".$row->id."'");
					if($query1->num_rows() != 0){
						$this->db->where('id', $row->id);
						$this->db->delete('messaging');

						$this->db->where('message_id', $row->id);
						$this->db->delete('deleted_messages');
					}else{
						$data = array(
								'message_id' => $row->id,
								'deleter_id' => $session_id
							);
						$this->db->insert("deleted_messages", $data);
					}
				}	
			}
			redirect(base_url()."user_messaging?id=".$id);
	}
}