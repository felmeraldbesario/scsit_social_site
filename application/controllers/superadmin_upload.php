<?php

class superadmin_upload extends CI_Controller{

	function do_upload(){
		$config = array(

				'upload_path' => './upload/user',
				'allowed_types' => 'gif|jpg|png'
			);

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());
			return "no image";
		}
		else
		{
			$data =  $this->upload->data();
			$file_name = $data['file_name'];

			return $file_name;
		}
	}

	public function superadmin_campus_post(){
		//para ma clean
		$this->form_validation->set_rules('sa_campus_post','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$image = $this->do_upload();
		$sa_campus_post = $this->input->post('sa_campus_post');

		$this->superadmin_model->sa_campus_post($sa_campus_post,$image);
	}

	public function update_campus_post(){
		//para ma clean
		$this->form_validation->set_rules('campus_post','','xss_clean');
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$image = $this->do_upload();
		$campus_post = $this->input->post('campus_post');
		$post_id = $this->input->post('post_id');
		$this->superadmin_model->update_campus_post($campus_post,$post_id,$image);
	}

	public function superadmin_department_post(){
		//para ma clean
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->set_rules('sa_department_post','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$image = $this->do_upload();
		$department_id = $this->input->post('department_id');
		$sa_department_post = $this->input->post('sa_department_post');
		$this->superadmin_model->sa_department_post($sa_department_post,$image,$department_id);
	}

	public function update_department_post(){
		//para ma clean
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->set_rules('department_post','','xss_clean');
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$image = $this->do_upload();
		$department_id = $this->input->post('department_id');
		$department_post = $this->input->post('department_post');
		$post_id = $this->input->post('post_id');
		$this->superadmin_model->update_department_post($department_post,$post_id,$image,$department_id);
	}
	public function update_profile(){
		$image = $this->do_upload();

		$this->superadmin_model->update_profile($image);
	}
// para sa pges

	public function pages(){
		//para ma clean
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->set_rules('pages_post','','xss_clean');
		$this->form_validation->set_rules('title','','xss_clean');
		$this->form_validation->run();
		//end sa clean		
		$image = $this->do_upload();
		$temp_id = $this->input->post('temp_id');
		$page_post = $this->input->post('pages_post');
		$title = $this->input->post('title');
		$this->superadmin_model->insert_pages_post($image,$temp_id,$page_post,$title);
	}

	public function update_pages(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->set_rules('pages_post','','xss_clean');
		$this->form_validation->set_rules('title','','xss_clean');
		$this->form_validation->run();
		//end sa clean	
		$image = $this->do_upload();
		$post_id = $this->input->post('post_id');
		$temp_id = $this->input->post('temp_id');
		$page_post = $this->input->post('pages_post');
		$title = $this->input->post('title');
		$this->superadmin_model->update_pages_post($post_id,$image,$temp_id,$page_post,$title);
	}
	public function superadmin_add_department(){
		//para ma clean
		$this->form_validation->set_rules('department_name','','xss_clean');
		$this->form_validation->run();
		//end sa clean	
		$image = $this->do_upload();	
		$department_name = strtoupper($this->input->post('department_name'));
		$this->superadmin_model->superadmin_add_department($department_name,$image);
		
	}

	public function update_department_name(){
		//para ma clean
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->set_rules('updated_department_name','','xss_clean');
		$this->form_validation->run();
		//end sa clean	
		$image = $this->do_upload();
		$department_id = $this->input->post('department_id');
		$updated_department_name = strtoupper($this->input->post('updated_department_name'));
		$this->superadmin_model->update_department_name($department_id,$updated_department_name,$image);
	}

}