<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class upload extends CI_Controller{
	
    function __construct()
	{
		parent::__construct();
		$this->load->helper(array('form', 'url'));
		
	}
		
		// gi butang nku ni dre sa upload pra ma 
		// load dayun iyang page nig validate
	public function admin_editorial(){
		
		$rowEditorial['title'] = 'SCSIT | Admin Editorial Page';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$rowEditorial['editorial_data']=$this->department_admin_model->editorial_selecting_data();
		$this->load->view('admin/admin_header',$rowEditorial);
		$this->load->view('admin/admin_editorial',$rowEditorial+$data);
		$this->load->view('admin/admin_footer',$rowEditorial);
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function admin_sports(){
		
		$rowsports['title'] = 'SCSIT | Admin Sport Page';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$rowsports['sportsResult']=$this->department_admin_model->sports_selecting_data();
		$this->load->view('admin/admin_header',$rowsports);
		$this->load->view('admin/admin_sports',$rowsports+$data);
		$this->load->view('admin/adminModal/sportsModal',$rowsports);
		$this->load->view('admin/admin_footer',$rowsports);
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function admin_entertainment(){
		
		$rowEntertainment['title'] = 'SCSIT | Admin Entertainment Page';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$rowEntertainment['entertainment_data']= $this->department_admin_model->entertainment_selecting_data();
		$this->load->view('admin/admin_header',$rowEntertainment);
		$this->load->view('admin/admin_entertainment',$rowEntertainment+$data);
		$this->load->view('admin/admin_footer',$rowEntertainment);
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function admin_news(){
		
		$rowNews['title'] = 'SCSIT | Admin News Page';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$rowNews['getResult']=$this->department_admin_model->news_selecting_data();
		$this->load->view('admin/admin_header',$rowNews);
		$this->load->view('admin/admin_news',$rowNews+$data);
		$this->load->view('admin/adminModal/newsModals',$rowNews);
		$this->load->view('admin/admin_footer',$rowNews);
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function department_selected(){

		$DeptSelected['title'] = 'SCSIT | Admin Department';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$DeptSelected['departmentQuery'] = $this->department_admin_model->forDepartment_selected_post_status();
		$DeptSelected['StatusProfileAdminShow'] = $this->department_admin_model->admin_status_image_profile();
		$DeptSelected['status_time_logs_latest'] = $this->department_admin_model->admin_department_latest_status_date_logs();
		$DeptSelected['status_time_logs_old'] = $this->department_admin_model->admin_department_old_status_date_logs();
		$DeptSelected['GetLike'] =$this->department_admin_model->admin_department_get_like();
		$DeptSelected['viewCourse'] = $this->department_admin_model->depertment_view_course_added();
		$DeptSelected['getAll'] = $this->department_admin_model->getAll_data();
		$DeptSelected['updatedCourse'] =$this->department_admin_model->UpdateCourse();
		$this->load->view('admin/admin_header',$DeptSelected);
		$this->load->view('admin/department_selected',$DeptSelected+$data);
		$this->load->view('admin/admin_footer');
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function admin_signup(){
	$data['title'] = 'SCSIT | Admin Registration';
		$this->load->view('admin/admin_header',$data);
		$this->load->view('admin/navbarmenu');
		$this->load->view('admin/admin_signup',$data);
		$this->load->view('admin/copyright');
	}
	// gi butang nku ni dre sa upload pra ma 
	// load dayun iyang page nig validate
	public function department_message(){
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$value['record'] = $this->department_admin_model->read_record();
		$value['sent_latest_date'] = $this->department_admin_model->latest_date_message();
		$value['sent_previous_date'] = $this->department_admin_model->privious_date_message();
		$value['select_user'] = $this->department_admin_model->select_message_receiver();
		$value['last_forward_msg'] = $this->department_admin_model->last_forward_message();
		$value['inbox'] = $this->department_admin_model->inbox();
		// super admin message ni
		$value['superAdminInfo'] = $this->department_admin_model->superAdminGettingInfo();
		$value['title'] = 'SCSIT | Admin Messaging';
		$value['selectJoin'] = $this->department_admin_model->selectingJoining();

		$this->load->view('admin/admin_header' ,$value);
		$this->load->view('admin/department_message' ,$value+$data);
		$this->load->view('admin/admin_footer');
	}
	// @param
	// codes for uploding data

	function do_upload()
	{
		

		$config = array(

				'upload_path' => './upload/user/',  //container niya.. I mean ang sudlan nig uload sa image
				'allowed_types' => 'gif|jpg|png' //and iyang dawatung nga type or file sa image
			);

		$this->load->library('upload', $config); // pag tawag sa library aron ma load ang upload

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			
			return null;
			

			
		}
		else
		{
			$data =  $this->upload->data();
			$file_name = $data['file_name'];

			return $file_name;
		}
	}


	function do_upload_update()
	{
		
		
		$config = array(

				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
			);

		$this->load->library('upload', $config);

		if ( ! $this->upload->do_upload())
		{
			$error = array('error' => $this->upload->display_errors());

			
			return null;
			

			
		}
		else
		{
			$data =  $this->upload->data();
			$file_name = $data['file_name'];

			return $file_name;
		}
	}


	function profile_upload()
	{

		$image = $this->do_upload();

		$user_id = $this->input->post('user_id');

		$this->user_model->profile_upload($image,$user_id);

	}

// insert
	// @param
	// insert uploading sa registration
	// form validation
	public function admin_registration(){
		$image=$this->do_upload();
		if($image==null):
			$image="no image";
		endif;

		$data = array(

			array(
				'field'		=> 	'fname',
				'label'		=>	'Firsname',
				'rules'		=>	'required|xss_clean'
				),
			array(
				'field'		=>	'lname',
				'label'		=>	'Lastname',
				'rules'		=>	'required|xss_clean'
				),
			array(
				'field'		=>	'Mname',
				'label'		=>	'Middlename',
				'rules'		=>	'required|xss_clean'
				),
			array(
				'field'		=>	'username',
				'label'		=>	'username',
				'rules'		=>	'trim|is_unique[user.username]|required|min_length[6]|xss_clean'
				),
			array(
				'field'		=>	'password',
				'label'		=>	'Password',
				'rules'		=>	'trim|required|min_length[8]|matches[CPassword]|xss_clean'
				),
			array(
				'field'		=>	'CPassword',
				'label'		=>	'Confirm Password',
				'rules'		=>	'trim|required|xss_clean'
				),
			array(
				'field'		=>	'email',
				'label'		=>	'email',
				'rules'		=>	'required|valid_email|xss_clean'
				),
			array(
				'field'		=> 'assigned_department',
				'label'		=>	'Assigned Department',
				'rules'		=>	'required|xss_clean'
				),
			array(
				'field'		=>	'admin_id_number',
				'label'		=>	'ID Number',
				'rules'		=>	'required|xss_clean'
				)

			);
$this->form_validation->set_rules($data);
if($this->form_validation->run()==FALSE){
		$this->admin_signup();
	
}else{
	
 	$fname=$this->input->post('fname');
 	$lname=$this->input->post('lname');
 	$Mname=$this->input->post('Mname');
 	$username=$this->input->post('username');
 	$password=$this->input->post('password');
 	$email=$this->input->post('email');
 	$assigned_department=$this->input->post('assigned_department');
 	$admin_id_number=$this->input->post('admin_id_number');
 	$date=$this->input->post('date');
 	
 	$this->department_admin_model->admin_registration($fname,$lname,$Mname,$username,$password,$email,$assigned_department,$admin_id_number,$image,$date);
 }
}
	// @param
	// send message to user
	// form validation
 public function dept_message(){
 	$image=$this->do_upload();
 	if($image==null):
 		$image="no image";
 	endif;

 	$data = array(
 		array(
 			'field'	=>	'receiver_id',
 			'label'	=>	'Receiver Name',
 			'rules'	=>	'required|xss_clean'
 			),
 		array(
 			'field' =>	'message',
 			'label'	=>	'message',
 			'rules'	=>	'trim|required|xss_clean'
 			)
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){
 		$this->department_message();
 	}else{


 	$receiver_id=$this->input->post('receiver_id');
 	$sender_id=$this->input->post('sender_id');
 	$message=$this->input->post('message');
 	$date=$this->input->post('date');
 	$this->department_admin_model->dept_message($receiver_id,$sender_id,$message,$image,$date);
 }
}
	public function reply_message(){
			if($image==null):
 			$image="no image";
 			endif;
		$data = array(

				array(
	 			'field'	=>	'receiver_id',
	 			'label'	=>	'Receiver Name',
	 			'rules'	=>	'xss_clean'
 				),
	 		array(
	 			'field' =>	'message',
	 			'label'	=>	'message',
	 			'rules'	=>	'trim|xss_clean'
	 			)
			);
		$this->form_validation->set_rules($data);
		if($this->form_validation->run() == false){
			$this->department_message();
		}else{
			$receiver_id=$this->input->post('receiver_id');
		 	$sender_id=$this->input->post('sender_id');
		 	$message=$this->input->post('message');
		 	$date=$this->input->post('date');
		 	$this->department_admin_model->reply_message($receiver_id,$sender_id,$message,$image,$date);

		}
	}
	// @param
	// forwarding message to user
	// form validation
public function dept_message_forward(){
	$image = $this->do_upload(); if($image==null): $image = "no image"; endif;
	$data = array(
		array('field'=>'receiver_id','label'=>'receiver_id','rules'=>'xss_clean'),
		array('field'=>'message','label'=>'message','rules'=>'trim|xss_clean'));
	$this->form_validation->set_rules($data);
	if($this->form_validation->run() == FALSE){ $this->department_message();
	}else{
		$receiver_id=$this->input->post('receiver_id');
	 	$sender_id=$this->input->post('sender_id');
	 	$message=$this->input->post('message');
	 	$date_forward=$this->input->post('date_forward');
	 	$date = $this->input->post('date');
	 	$this->department_admin_model->dept_message_forward($receiver_id,$sender_id,$message,$image,$date_forward, $date);
	}

}
	// @param
	// add status to department or posting status to department
	// form validation

public function add_department_status(){
	$image=$this->do_upload();
 	if($image == null):
 	$image="no image";
 	endif;
 	$data = array(
 		array(
 			'field'=>'department',
 			'label'=>'department',
 			'rules'=>'xss_clean'
 			),
 		array(
 			'field'=>'user_id',
 			'label'=>'user_id',
 			'rules'=>'xss_clean'
 			),
 		array(
 			'field'=>'admin_id_number',
 			'label'=>'admin_id_number',
 			'rules'=>'xss_clean'
 			),
 		array(
 			'field'=>'department_topic_body',
 			'label'=>'department_topic_body',
 			'rules'=>'trim|xss_clean'
 			),
 		array(
 			'field'=>'date',
 			'label'=>'date',
 			'ruels'=>'trim|xss_clean'
 			)
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){

 		$this->department_selected();
 	}else{

 	$department=$this->input->post('department');
 	$user_id=$this->input->post('user_id');
 	$admin_id_number=$this->input->post('admin_id_number');
 	$department_topic_body=$this->input->post('department_topic_body');
 	$date=$this->input->post('date');
 	$this->department_admin_model->add_department_status($department,$user_id,$admin_id_number,$image,$department_topic_body,$date);
 }
    }

    // @param
    // insert news with image
    // form validation

 public function insert_news(){
 	$image=$this->do_upload();
 	if($image==null):
 		$image="no image";
 	endif;
 	$data = array (
 		array(
 			'field'	=>	'admin_id',
 			'label'	=>	'admin_id',
 			'rules'	=>	'xss_clean'
 			),
 		array('field'	=>	'title', 'label'	=>	'title', 'rules'	=>	'trim|xss_clean'),
 		array('field'	=>	'news_body', 'label'	=>	'news_body', 'rules'	=>	'trim|xss_clean'),
 		array('field'	=>	'date',	'label'	=>	'date', 'rules'	=>	'xss_clean')
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){
 		$this->admin_news();
 	}else{
 	$admin_id=$this->input->post('admin_id');
 	$title=$this->input->post('title');
 	$news_body=$this->input->post('news_body');
 	$date=$this->input->post('date');
 	$this->department_admin_model->insert_news($admin_id,$title,$news_body,$image,$date);
  }
 }
 	// param
 	// insert entertainment with image
 	// form validation
 public function insert_intertainment(){
 	$image=$this->do_upload();
 	if($image==null):
 		$image="no image";
 	endif;
 	$data = array (
 		array(
 			'field'	=>	'admin_id',
 			'label'	=>	'admin_id', 
 			'rules'	=>	'xss_clean'
 			),
 		array(
 			'field'	=>	'title',
 			'label'	=>	'title',	
 			'rules'	=>	'trim|xss_clean'
 			),
 		array('
 			field'	=>	'entertainment',
 			'label'	=>	'entertainment', 
 			'rules'	=>	'trim|xss_clean'
 			),
 		array(
 			'field'	=>	'date',	
 			'label'	=>	'date',
 			 'rules'	=>	'xss_clean'
 			 )
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){
 		$this->admin_entertainment();
 	}else{
 	$admin_id=$this->input->post('admin_id');
 	$title=$this->input->post('title');
 	$entertainment=$this->input->post('entertainment');
 	$date=$this->input->post('date');
 	$this->department_admin_model->insert_intertainment($admin_id,$title,$entertainment,$image,$date);
 }
}
	// @param
	// insert sports with image
	// form validation
 public function insert_sports(){
 	$image=$this->do_upload();
 	if($image==null):
 		$image="no image";
 	endif;
 	$data = array(
 		array(
 			'field'	=>	'admin_id',
 			'label'	=>	'admin_id',
 			'rules'	=>	'xss_clean'
 			),
 		array(
 			'field'	=>	'title',
 			'label'	=>	'title',	
 			'rules'	=>	'trim|xss_clean'
 			),
 		array(
 			'field'	=>	'sports_body',
 			'label'	=>	'sports_body', 
 			'rules'	=>	'trim|xss_clean'
 			),
 		array(
 			'field'	=>	'date',	
 			'label'	=>	'date', 	
 			'rules'	=>	'trim|xss_clean'
 			)
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){
 		$this->admin_sports();
 	}else{
 	$admin_id=$this->input->post('admin_id');
 	$title=$this->input->post('title');
 	$sports_body=$this->input->post('sports_body');
 	$date=$this->input->post('date');
 	$this->department_admin_model->insert_sports($admin_id,$title,$sports_body,$image,$date);
 }
}
	// @param
	// insert editorial with image
	// form validation
 public function insert_editorial(){
 	$image=$this->do_upload();
 	if($image==null):
 		$image="no image";
 	endif;
 	$data = array(
 		array(
 			'field'	=>	'admin_id',
 			'label'	=>	'admin_id',	
 			'rules'	=>	'xss_clean'
 			),
 		array(
 			'field'	=>	'title',	
 			'label'	=>	'title',	
 			'rules'	=>	'trim|xss_clean'
 			),
 		array(
 			'field'	=>	'editorial',
 			'label'	=>	'editorial', 
 			'rules'	=>	'trim|xss_clean'
 			),
 		array(
 			'field'	=>	'date',
 			'label'	=>	'date', 	
 			'rules'	=>	'xss_clean'
 			)
 		);
 	$this->form_validation->set_rules($data);
 	if($this->form_validation->run() == FALSE){
 		$this->admin_editorial();
 	}else{
 	$admin_id=$this->input->post('admin_id');
 	$title=$this->input->post('title');
 	$editorial=$this->input->post('editorial');
 	$date=$this->input->post('date');
 	$this->department_admin_model->insert_editorial($admin_id,$title,$editorial,$image,$date);
 }
}

 	// update
	// @param
	// update profile photo sa administration
 	public function Update_profile_dept_admin(){
 		$image=$this->do_upload();
 		if($image==null):
 			$image="no image";
 		endif;
 		$id=$this->input->post('id');
 		$date_profile_change=$this->input->post('date_profile_change');
 		$this->department_admin_model->Update_profile_dept_admin($id,$image,$date_profile_change);
 	}
 	
 	// @param
 	// updating data sa news
 	// form validation
 	public function fel_updating_news(){
 		$image=$this->do_upload();
 		if($image==null):
 			$image="no image";
 		endif;
 		$data = array(
 			array(
 				'field' => 'title',
 				'label' => 'title',
 				'rules' => 'xss_clean'
 				),
 			array(
 				'field'	=>	'news_body',
 				'label'	=> 'news_body',
 				'rules'	=>	'trim|xss_clean'
 				),
 			array(
 				'field'	=>	'date_update',
 				'label'	=>	'date_update',
 				'rules'	=>	'trim|xss_clean'
 				)
 			);
 		$this->form_validation->set_rules($data);
 		if($this->form_validation->run() == FALSE){
 			$this->admin_news();
 		}else{
 		$id=$this->input->post('id');
 		$title=$this->input->post('title');
 		$news_body=$this->input->post('news_body');
 		$date_update=$this->input->post('date_update');
 		$this->department_admin_model->fel_updating_news($id,$image,$title,$news_body,$date_update);
 	}
 	}
 	// @param
 	// updating data sa sports
 	// form validation
 	public function fel_updating_sports(){
 		$image=$this->do_upload();
 		if($image==null):
 			$image="no image";
 		endif;
 		$data = array(
 			array(
 				'field'=>'title',
 				'label'=>'title',
 				'rules'=>'xss_clean'
 				),
 			array(
 				'field'=>'sports_body',
 				'label'=>'sports_body',
 				'rules'=>'trim|xss_clean'
 				)
 			);
 		$this->form_validation->set_rules($data);
 		if($this->form_validation->run() == FALSE){
 			$this->admin_sports();
 		}else{
 		$id=$this->input->post('id');
 		$title=$this->input->post('title');
 		$sports_body=$this->input->post('sports_body');
 		$date_update=$this->input->post('date_update');
 		$this->department_admin_model->fel_updating_sports($id,$image,$title,$sports_body,$date_update);
 	}
 	}
 	// @param
 	// updating data sa entertainment
 	// form validation
 	public function fel_updating_entertainment(){
 		$image=$this->do_upload();
 		if($image==null):
 			$image="no image";
 		endif;
 		$data = array(
 			array(
 				'field' => 'title',
 				 'label' => 'title', 
 				 'rules' => 'xss_clean'
 				 ),
 			array(
 				'field' => 'entertainment', 
 				'label' => 'entertainment', 
 				'rules' => 'trim|xss_clean'
 				)
 			);
 		$this->form_validation->set_rules($data);
 		if($this->form_validation->run() == FALSE){
 			$this->admin_entertainment();
 		}else{
 		$id=$this->input->post('id');
 		$title=$this->input->post('title');
 		$entertainment=$this->input->post('entertainment');
 		$date_update=$this->input->post('date_update');
 		$this->department_admin_model->fel_updating_entertainment($id,$image,$title,$entertainment,$date_update);
 	}
 	}
 	// param
 	// updating data sa enditorial
 	// form validation
 	public function fel_updating_editorial(){
 		$image=$this->do_upload();
 		if($image==null):
 			$image="no image";
 		endif;
 		$data = array(
 			array(
 				'field' => 'title',
 				 'label' => 'title', 
 				 'rules' => 'xss_clean'),
 			array(
 				'field' => 'editorial',
 				 'label' => 'editorial',
 				  'rules' => 'trim|xss_clean'
 				  )
 			);
 		$this->form_validation->set_rules($data);
 		if($this->form_validation->run() == FALSE){
 			$this->admin_editorial();
 		}else{
 		$id=$this->input->post('id');
 		$title=$this->input->post('title');
 		$editorial=$this->input->post('editorial');
 		$date_update=$this->input->post('date_update');
 		$this->department_admin_model->fel_updating_editorial($id,$image,$title,$editorial,$date_update);
 	}
 }
 	// @param
 	// updating post sa department
 	// form validation
 	public function edit_departmentPost(){
 		$image = $this->do_upload();
 		if($image == null):
 			$image="no image";
 		 endif;
 		 $data = array(
 		 	array(
 		 		'field'	=> 'id',
 		 		'label'	=>	'id',
 		 		'rules'	=>	'xss_clean'
 		 		),
 		 	array(
 		 		'field'	=>	'department_topic_body',
 		 		'label'	=>	'department_topic_body',
 		 		'rules'	=>	'trim|xss_clean'
 		 		),
 		 	);
 		 $this->form_validation->set_rules($data);
 		 if($this->form_validation->run() == FALSE){
 		 	$this->department_selected();
 		 }else{

 		 	$id=$this->input->post('id');
			$department_topic_body=$this->input->post('department_topic_body');
			$this->department_admin_model->edit_departmentPost($id,$image,$department_topic_body);

 		 }
 	}

 	   // @param
    // inserting course to department selected
    // form validation
    public function add_course(){
    	$data = array(
    		array(
    			'field'	=>	'course',
    			'label'	=>	'Course',
    			'rules'	=>	'trim|min_length[4]|required|xss_clean'
    			),
    		array(
    			'field'	=>	'date_added',
    			'label'	=>	'date_added',
    			'rules'	=>	'xss_clean'
    			)
    		);
    	$this->form_validation->set_rules($data);
    	if($this->form_validation->run() == FALSE){
    		$this->department_selected();
    	}else{
    	$department_id=$this->input->post('department_id');
    	$course=$this->input->post('course');
    	$date_added=$this->input->post('date_added');
    	$this->department_admin_model->add_course($department_id,$course,$date_added);
    }
    
    }

 	



// ==============================user upload=========================================//
    function signUp_upload()
	{

		$data = array(
				
				array(
					'field' => 'id_number',
					'label' => 'ID Number',
					'rules' => 'required|xss_clean'
				),
				array(
					'field' => 'address',
					'label' => 'address',
					'rules' => 'required|xss_clean'
				),
				array(
					'field' => 'mobilenumber',
					'label' => 'Mobile Number',
					'rules' => 'required|numeric|xss_clean'
				),
				array(
					'field' => 'email',
					'label' => 'email',
					'rules' =>	'required|xss_clean'
				),
				array(
					'field' => 'user_answer',
					'label' => 'answer',
					'rules' => 'required|xss_clean'
				)
			);

		$config = array(
				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
			);
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			$year = $this->input->post('year');
			$id_number = $this->input->post('id_number');
			$address = $this->input->post('address');
			$mobilenumber = $this->input->post('mobilenumber');
			$email = $this->input->post('email');
			$course_id = $this->input->post('course_id');
			$answer = $this->input->post('answer');
			$user_answer = $this->input->post('user_answer');
			$image = null;
			$this->userModel->do_upload($year, $id_number, $address, $mobilenumber, $email, $course_id, $answer, $user_answer, $image);

		}else{
			
			$year = $this->input->post('year');
			$id_number = $this->input->post('id_number');
			$address = $this->input->post('address');
			$mobilenumber = $this->input->post('mobilenumber');
			$email = $this->input->post('email');
			$course_id = $this->input->post('course_id');
			$answer = $this->input->post('answer');
			$user_answer = $this->input->post('user_answer');
			$upload_data = $this->upload->data();
			$image = $upload_data['file_name'];
			$this->userModel->do_upload($year, $id_number, $address, $mobilenumber, $email, $course_id, $answer, $user_answer, $image);

		}
	}

	public function updatePic(){
		$config = array(
				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
		);
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			redirect(base_url().'home');
		}else{
			$upload_data = $this->upload->data();
			$image = $upload_data['file_name'];
			$this->user->updatePic($image);
		}
	}

	public function campusPost(){
		$config = array(
				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
			);
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			$invalid_char = array("<script>");
			$status = str_replace($invalid_char, "", $this->input->post('jam_campus_status'));
			$image = '';
			if($status != ''){
				$this->user->campusPost($status, $image);

			}else{
				redirect(base_url().'home');
			}
		}else{
			$upload_data = $this->upload->data();
			$image = $upload_data['file_name'];
			$status = $this->input->post('jam_campus_status');
			$this->user->campusPost($status, $image);
		}

	}

	public function updateCampusPost(){
		$image = "";
		$config = array(
				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
		);	
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			$cpost_id = $this->input->post('cpost_id');
			$status_update = $this->input->post('status_update');
			$this->user->updateCampusPost($cpost_id, $status_update, $image);
		}else{
			$upload_data = $this->upload->data();
			$image = $upload_data['file_name'];
			$cpost_id = $this->input->post('cpost_id');
			$status_update = $this->input->post('status_update');
			$this->user->updateCampusPost($cpost_id, $status_update, $image);
		}
	}

	public function departmentPost(){
		$invalid_char = array("<script>");
		$status = str_replace($invalid_char, "", $this->input->post('jam_department_status'));
		$image = "";
		$config = array(
				'upload_path' => './upload/user/',
				'allowed_types' => 'gif|jpg|png'
		);	
		$this->load->library('upload', $config);
		if(!$this->upload->do_upload()){
			$this->user->departmentPost($status, $image);

		}else{
			$upload_data = $this->upload->data();
			$image = $upload_data['file_name'];
//this->user->departmentPost($status, $image);
		}

	}










}
?>