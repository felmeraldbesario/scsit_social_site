
<!-- 
@Author: Felmerald Besario
@Date started Created: November 21, 2015
 -->
 
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class admin_controller Extends CI_Controller
{
	// @param
	// check user
	// if session true process to administrator homepage for admin
	// if session false redirect to admin login user can't login if their session is false
	public function administrator(){
		

		{
		if ($this->session->userdata('login_id') != '' && $this->session->userdata('login_username') != '' && $this->session->userdata('login_password') != '' && $this->session->userdata('login') == TRUE) {
			
		$data['title'] = 'SCSIT | Dashboard';
		$data['copyright'] ='&copy 2015 All Rights Reserved @ SCSIT Social Site Department Administrator | Salazar Colleges of Science and Institute of Technology';
		$data['welcome_alert'] = $this->department_admin_model->welcome_administrator_alert();
		$data['departmentJoined'] = $this->department_admin_model->departmentJoined();
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$this->load->view('admin/admin_header',$data);
		$this->load->view('admin/administrator',$data);
		$this->load->view('admin/admin_footer');
	}else{
		redirect(base_url().'admin_login');
	}

 }
		
	}
	// @param
	// for unblocking user blocked
	public function unblock_user_blocked_dept(){

		$dataUnblock['title'] ='SCSIT | Admin Unblock User';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$dataUnblock['joinedDisplayblockedUser'] = $this->department_admin_model->joinedDisplayblockedUser();

		$this->load->view('admin/admin_header' ,$dataUnblock);
		$this->load->view('admin/unblock_user_blocked_dept',$dataUnblock+$data);
		$this->load->view('admin/admin_footer');
	}
	// for unblock user
	// @param
	// for delete user blocked
	public function delete_unblock(){
    $blocked_id=$this->input->get('id');
 	$this->db->delete('admin_block_list', array('blocked_id' => $blocked_id));
 	$this->session->set_flashdata(array('unblockUserBlocked'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button><span class="glyphicon glyphicon-ok"></span> Success..! One User has been successfully unblock </div>'));
 	redirect(base_url().'unblock_user_blocked_dept');
 	exit();
	}
	// administrator login
	// with title
	public function admin_login(){

		$data['title'] = 'SCSIT | Admin Login';
		$data['copyni'] ="Salazar Colleges of Science and Institute of Technology | Department Administrator Login Panel";
		$this->load->view('admin/admin_header',$data);
		$this->load->view('admin/navbarmenu');
		$this->load->view('admin/admin_login');
	}
	// administrator sign up
	// with title
	public function admin_signup(){
		$data['title'] = 'SCSIT | Admin Registration';
		$this->load->view('admin/navbarmenu');
		$this->load->view('admin/admin_signup',$data);
		$this->load->view('admin/copyright');
	
	}
	// @param
	// for selecting announcement or events
	// inserting
	public function department_announcement(){
		$announce['title'] = "SCSIT | Admin Announcement";
		$announce['info'] = "Wrote the upcomming events";
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$announce['announcement_table'] = $this->department_admin_model->show_published_announcement();
		$this->load->view('admin/admin_header',$announce);
		$this->load->view('admin/department_announcement',$announce+$data);
		$this->load->view('admin/admin_footer');
	}
	// @param
	// for selecting user blocked information
	// print user blocked
	public function admin_printuser(){

		$printingUser['title'] = 'SCSIT | Admin Print User';
		$printingUser['emailAdmin'] =$this->department_admin_model->EmailAddress();
		$printingUser['joinedPrinterDepartmentname'] = $this->department_admin_model->joinedPrinterDepartmentname();
		$this->load->view('admin/admin_header' ,$printingUser);
		$this->load->view('admin/admin_printuser',$printingUser);
		$this->load->view('admin/admin_footer');
	}
	// param for changing administrator password
	// matching pasword
	public function change_password(){
		 date_default_timezone_set('Asia/Manila');
    	 $date_pass_change=date('h:i:s a m/d/y');
		 $this->load->model('department_admin_model'); // pwd naa pwd wala ky g.load naman ni nku sa autoload nga model.
		 $this->department_admin_model->member_page();

		 // mag form validation ko dre sa controller
		 // iyang eh pang print ang mga required nga field
		 $this->form_validation->set_rules('password','Current Password','required|xss_clean');
		 $this->form_validation->set_rules('new_pw','New Password','required|max_length[20]|min_length[6]|xss_clean');
		 $this->form_validation->set_rules('conf_pw','Confirm Password','required|matches[new_pw]|xss_clean');
		
		if($this->form_validation->run() == FALSE){

		$passRows['title'] = 'SCSIT | Admin Change Password';
		$data['adminInfo'] = $this->department_admin_model->adminInfo();
		$data['navTop'] = $this->department_admin_model->navTop();
		$data['navTopDrop'] = $this->department_admin_model->navTopDrop();
		$passRows['passwordchange']	= $this->department_admin_model->date_password_change(); // kani cya ky select ni sa date kung kanus.ah g.change
		$this->load->view('admin/admin_header',$passRows);
		$this->load->view('admin/vChangePassword',$passRows+$data);
		$this->load->view('admin/admin_footer',$passRows);

		}else{
			// set sa query to load admin session information
			$this->db->where('id',$this->session->userdata('login_id'));
			$sql = $this->db->get('admin_personel');
			// Take each result set as variable
			foreach($sql->result() as $my_info){
			$password = $my_info->password;
			$db_id = $my_info->id;
			}
			// double md5 check to see if  form password equal to password
			if(md5(md5($this->input->post('password')))==$password){

				// echo "alrights";
				// password happends here if matched or not
				$fixed_pw = md5(md5(mysql_real_escape_string($this->input->post('new_pw'))));
				// Update Password
				$data = array(
					'date_pass_change'=>$date_pass_change,//sa date nga password kung kanus.ah cya g.change ug password
					'password' => $fixed_pw // fixed_pw variable nga g. burabg bnku sa babaw
					);
				$this->db->where('id',$db_id);
				$this->db->update('admin_personel',$data);
				// using flash data as notification
				$this->session->set_flashdata(array('notification'=>'<div class="alert alert-info alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> <span class="glyphicon glyphicon-ok"></span> Password successfully change .! Login Again</div>'));
				redirect(base_url().'admin_login');
				exit();
			}else{
				
				// if not equal to password the flashdata incorrect will print
				$this->session->set_flashdata(array('incorrect'=>'<div class="alert alert-danger alert-dismissible" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button> <span class="glyphicon glyphicon-exclamation-sign"></span> Current Password Incorrect</div>'));
				redirect(base_url().'change_password');
				exit();
			}
		}

	}
	
	// after sign up
	// result success
	public function success(){
		
		$regSuccess['title'] = 'SCSIT | Registration Success!';

		$this->load->view('admin/admin_header',$regSuccess);
		$this->load->view('admin/navbarmenu');
		$this->load->view('admin/success');
		$this->load->view('admin/copyright');
		$this->load->view('admin/admin_footer');
	}
	// @param
	// insert announcement events to user
	public function announcement_insert(){
		$data = array(
			array(
				'field' => 'announcement',
				'label'	=>	'announcement',
				'rules'	=>	'required|min_length[16]|xss_clean'
				)
			);
		$this->form_validation->set_rules($data);
		if($this->form_validation->run() == FALSE){
			$this->department_announcement();
		}else{
			$admin_id = $this->input->post('admin_id');
			$announcement = $this->input->post('announcement');
			$date = $this->input->post('date');
			$this->department_admin_model->publish_announcement($admin_id,$announcement,$date);
		}
	}

	// queries here
// @param
	// login 
	// form validation
	public function login(){
		$data = array (
					array(
						'field' => 'username',
						'label' => 'Username',
						'rules' => 'trim|required|min_length[6]|xss_clean'
					),
					array(
						'field' => 'password',
						'label' => 'Password',
						'rules' => 'trim|required|min_length[6]|xss_clean'
					)
			);
	$this->form_validation->set_rules($data);
	if($this->form_validation->run() == FALSE){
		$this->admin_login();
	}else{
 	$username = $this->input->post('username');
 	$password = $this->input->post('password');
 	$this->department_admin_model->login($username,$password);
		}
	}
// @param
	// logout
	// destroy session
	public function logout(){
	$this->db->order_by('id','DESC');
	$this->db->limit(1);
	$get_logs = $this->db->get('admin_logs');
	foreach($get_logs->result() as $logs){
		$logs_id = $logs->id;
	}
	$user_id = $this->session->userdata('login_id');
	date_default_timezone_set('Asia/Manila');
	$date_logout = date('h:m:s a m/d/y');
	$data = array(
		'date_logout'=>$date_logout
		);
	$this->db->where('id',$logs_id);
	$this->db->where('admin_id',$user_id);
	$this->db->update('admin_logs',$data);
	
	$data = array(
			'login_id'=>'',
			'login_username'=>'',
			'login_password'=>'',
			'login'=>''
		);

	$this->session->unset_userdata($data);
	$this->session->sess_destroy();

	redirect(base_url().'admin_login');
    }
 
// deleting data
// @param
 // delete news events
    public function delete_news(){
    	$id=$this->input->get('id');
    	$this->department_admin_model->delete_news($id);
    }
    // @param
    // delete sports events
    public function delete_sports(){
    	$id=$this->input->get('id');
    	$this->department_admin_model->delete_sports($id);
    }
    // @param
    // delete entertainment events
    public function delete_entertainment(){
    	$id=$this->input->get('id');
    	$this->department_admin_model->delete_entertainment($id);
    }
    // @param
    // delete editorial events
    public function delete_enditorial(){
    	$id=$this->input->get('id');
    	$this->department_admin_model->delete_enditorial($id);
    }
    // @param
    // delete message 
    public function delete_message(){
    	$id = $this->input->get('id');
    	$this->department_admin_model->delete_message($id);
    }
    // @param
    // delete course added from department selected
    public function delete_course(){
    	$id = $this->input->get('id');
    	$this->department_admin_model->delete_course($id);
    }
    // @param
    // dlete announce inserted
    public function delete_announcement(){
    	$id=$this->input->get('id');
    	$this->department_admin_model->delete_announcement($id);
    }
    // @param
    // delete department selected status posted
    public function delete_dept_status(){
    	$id = $this->input->get('id');
    	$this->department_admin_model->delete_dept_status($id);
    }

    // updating file 
    // @param
    // update course added
    public function Updating_course(){
    	$id=$this->input->post('id');
    	$course=$this->input->post('course');
    	$date_update=$this->input->post('date_update');
    	$this->department_admin_model->Updating_course($id,$course,$date_update);
    }
    // @param
    // update announcement added
    public function update_announcement(){
    	$id = $this->input->post('id');
    	$announcement =$this->input->post('announcement');
    	$date_update = $this->input->post('date_update');
    	$this->department_admin_model->update_announcement($id,$announcement,$date_update);
    }
    // @param
    // update sa account
    public function account_update(){
    	$data = array(
    		array(
    			'field'=>'fname',
    			'label'=>'Firstname',
    			'rules'=>'xss_clean'
    			),
    		array(
    			'field'=>'fname',
    			'label'=>'Lastname',
    			'rules'=>'xss_clean'
    			),
    		array(
    			'field'=>'Mname',
    			'label'=>'Middle Name',
    			'rules'=>'xss_clean'
    			),
    		array(
    			'field'=>'email',
    			'label'=>'Email Address',
    			'rules'=>'xss_clean'
    			),
    		array(
    			'field'=>'admin_id_number',
    			'label'=>'ID Number',
    			'rules'=>'xss_clean'
    			)
    		);
    	$this->form_validation->set_rules($data);
    	if($this->form_validation->run() == false){
    	$this->administrator();
    	}else{
    		$id = $this->input->post('id');
    		$account_date_update = $this->input->post('account_date_update');
    		$fname =$this->input->post('fname');
    		$lname = $this->input->post('lname');
    		$Mname = $this->input->post('Mname');
    		$email = $this->input->post('email');
    		$this->department_admin_model->account_update($id,$account_date_update,$fname,$lname,$Mname,$email);
    	}
    }
    // blocking a user
    // @param
    // for blocking user to department
    // num rows
    // inserting
   public function block_user()
 {
 	$blocked_user_id = $this->input->post('blocked_user_id');
 	$admin_id = $this->input->post('admin_id');
 	$reason_id=$this->input->post('reason');
 	$date=$this->input->post('date');
 	$this->department_admin_model->block_user($blocked_user_id,$admin_id,$reason_id,$date);
 }
 // @param
 // select MVC
 // SElect user from department selected
 	public function select_user_receiver(){
 		$rowReceiver['queryRecord']=$this->department_admin_model->SelectReceiver();
 		$this->load->view('admin/admin_header');
		$this->load->view('admin/department_message',$rowReceiver);
		$this->load->view('admin/admin_footer');
 	}
 	// @param
 	// inserting comments
 	// temporary
 	// with ajax
 	// with form validation nga mu wagtang sa malicious data
 	public function admin_department_comment(){
 	
 		$data = array(
 			array(
 				'field'=>'department_topic_id',
 				'label'=>'department_topic_id',
 				'rules'=>'xss_clean'
 				),
 			array(
 				'field'=>'comments',
 				'label'=>'comments',
 				'rules'=>'xss_clean'
 				)
 			);
 		$this->form_validation->set_rules($data);
 		$this->form_validation->run();
 	$admin_id_number = $this->input->post('admin_id_number');
	$admin_department_topic = $this->input->post('department_topic_id');
	$comments = $this->input->post('comments');

	$this->department_admin_model->admin_department_comment($admin_department_topic,$comments,$admin_id_number);
	$this->load->view('admin/thirdParty/admin_temporary_comments');
	}

// @param
// edit comments
// temporary
// with ajax
	// gibutangan nkug form validation pra eh clean ang comment sa mga malicius data
public function edit_comment(){
	$data = array(
		array(
			'field'=>'comments',
			'label'=>'comments',
			'rules'=>'xss_clean'
			),
		array(
			'field'=>'department_topic_id',
			'label'=>'department_topic_id',
			'rules'=>'xss_clean'
			),
		array(
			'field'=>'comment_id',
			'label'=>'comment_id',
			'rules'=>'xss_clean'
			)
		);
	$this->form_validation->set_rules($data);
	$this->form_validation->run();
	$comments = $this->input->post('comments');
	$department_topic_id = $this->input->post('department_topic_id');
	$comment_id = $this->input->post('comment_id');

	$this->department_admin_model->edit_comment($comments,$department_topic_id,$comment_id);

	$this->load->view('admin/thirdParty/admin_temporary_comments');
}
// input comments
     
public function input_comment(){
	$this->load->view('admin/thirdParty/edit_comment');
}

// @param
// deleting comments
public function delete_comment(){
	$id = $this->input->post('id');
	$this->department_admin_model->delete_comment($id);
}

public function comment_load(){
	$id = $this->input->post('id');
	$data['id'] = $id;
	$this->load->view('admin/thirdParty/temp_count_comment',$data);
}
public function likers_load(){
	$id = $this->input->post('id');
	$data['id'] = $id;
	$this->load->view('admin/thirdParty/temp_count_likers',$data);
}
// =============delete messages hre=================//
// public function delete_message(){
// 	$this->load->view('admin/thirdParty/temp_delete_message');
// }
 public function delete_action_message(){

 		$message_id = $this->input->post('message_id');
 		$this->department_admin_model->delete_action_message($message_id);
 	}






}
?>