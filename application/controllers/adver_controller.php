
<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Adver_controller Extends CI_Controller
{
	// news adver
public function adver_news(){
	
	$newsRow['title'] = 'SCSIT | News Page';
	$newsRow['queryRecord']=$this->adver_model->show_news_adver();
	$this->load->view('advertise/adver_modal/adver_header',$newsRow);
	$this->load->view('advertise/adver_news',$newsRow);
	$this->load->view('advertise/adver_modal/adver_footer');
}
public function adver_entertainment(){
 	
	$entertainmentRow['title'] ='SCSIT | Entertainment Page';
	$entertainmentRow['queryEnter']=$this->adver_model->entertainment_result();
	$this->load->view('advertise/adver_modal/adver_header',$entertainmentRow);
	$this->load->view('advertise/adver_entertainment',$entertainmentRow);
	$this->load->view('advertise/adver_modal/adver_footer');
}
public function adver_editorial(){
	$RowEditorial['title'] = "SCSIT | Editorial Page";
	$RowEditorial['queryEditorial']=$this->adver_model->showEditorial_MVC();
	$this->load->view('advertise/adver_modal/adver_header', $RowEditorial);
	$this->load->view('advertise/adver_editorial', $RowEditorial);
	$this->load->view('advertise/adver_modal/adver_footer');
}
public function adver_sports(){

	$RowSport['title'] = "SCSIT | Sport Page";
	$RowSport['querySports']=$this->adver_model->showSports_MVC();
	$this->load->view('advertise/adver_modal/adver_header',$RowSport);
	$this->load->view('advertise/adver_sports',$RowSport);
	$this->load->view('advertise/adver_modal/adver_footer');

}


}