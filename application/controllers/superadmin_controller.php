<?php
class superadmin_controller Extends CI_Controller{
	

	public function superadmin_login(){
		
		if($this->superadmin_model->superadmin_login()):

		$data['error_message'] = $this->session->userdata('message');
		
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/includes/super_menulogin');
		$this->load->view('superAdmin/superadmin_login',$data);
		
		$this->session->unset_userdata(array('message' => ''));
		
		$this->load->view('superAdmin/includes/super_copyright');
		$this->load->view('superAdmin/includes/super_footer');
		
		endif;
	}
	// para sa login proccess
	public function login_superadmin(){
		//para ma clean
		$this->form_validation->set_rules('username','','xss_clean');
		$this->form_validation->set_rules('password','','xss_clean');
		$this->form_validation->run();
		//end sa clean

		$username = $this->input->post('username');
		$password = $this->input->post('password');

		$this->superadmin_model->process_login($username,$password);
	}
	
	public function logout_superadmin(){

		$this->superadmin_model->process_logout();
	}
	public function superadmin(){

		$this->load->view('superAdmin/includes/cache_control');
			if($this->superadmin_model->checkSuperadminInfo()):
				$data['query'] = $this->superadmin_model->superadmin_deptlist();
				$data['records_query'] = $this->superadmin_model->superadmin_admin_records();
				$data['countRecords'] = $this->superadmin_model->count_records();
				$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
				$this->load->view('superAdmin/includes/super_header');
				$this->load->view('superAdmin/superadmin',$data);
				$this->load->view('superAdmin/includes/super_footer',$data);
			else:
	
				redirect(base_url().'superadmin_login');
			endif;

		
	}
	public function superadmin_deptList(){
		$this->load->view('superadmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		$this->load->view('superAdmin/includes/super_header');
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getCourseQuery'] = $this->superadmin_model->show_course(); 
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		
			$data['dept_personel_query']= $this->superadmin_model->superadmin_dept_personel();
			$this->load->view('superAdmin/superadmin_deptList',$data);
			$this->load->view('superAdmin/includes/edit_departmentAdded_modal',$data);
			$this->load->view('superAdmin/includes/departmentCourse_modal');
			$this->load->view('superAdmin/includes/super_footer');
		
		else:
				redirect(base_url().'superadmin_login');
		endif;
		

	}
	public function superadmin_update_course(){
			$course = strtoupper($this->input->post('course'));
			$course_id = $this->input->post('course_id');

			$this->superadmin_model->superadmin_update_course($course,$course_id);
	}

	public function delete_department(){
		//para ma clean
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean	
		$department_id= $this->input->post('department_id');
		$this->superadmin_model->delete_department($department_id);
	}
	public function superadmin_delete_course(){
		$course_id = $this->input->post('course_id');

		$this->superadmin_model->superadmin_delete_course($course_id);
	}

	public function superadmin_add_course(){
		//para ma clean
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->set_rules('course','','xss_clean');
		$this->form_validation->run();
		//end sa clean		
		$department_id = $this->input->post('department_id');
		$course = strtoupper($this->input->post('course'));

		$this->superadmin_model->superadmin_add_course($department_id,$course);
	}

	//para sa post sa campus superaadmin
	public function superadmin_campus(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['last_post_date'] = $this->superadmin_model->superadmin_last_post_date();
		$data['campusPostQuery'] = $this->superadmin_model->superadmin_campus_post();
		$data['previus_post_date'] = $this->superadmin_model->superadmin_previus_post_date();
		$data['campus_comments'] = $this->superadmin_model->getComments();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_campus',$data);
		$this->load->view('superAdmin/includes/campus_modal_views',$data);
		$this->load->view('superAdmin/includes/campus_editOwnPost_modal');
		$this->load->view('superAdmin/includes/campus_editComment_modal',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	
	public function sa_campus_comments(){
		//para ma clean
		$this->form_validation->set_rules('campus_post_id','','xss_clean');
		$this->form_validation->set_rules('campus_comment','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$campus_post_id = $this->input->post('campus_post_id');
		$campus_comments = $this->input->post('campus_comment');
		$data['campus_comments'] = $this->superadmin_model->getComments();
		$data['campus_post_id'] =  $campus_post_id;
		
		$this->superadmin_model->sa_campus_comments($campus_comments,$campus_post_id);
		$this->load->view('superAdmin/thirdparty/temp_campus_comments',$data);
		$this->load->view('superAdmin/includes/campus_editComment_modal',$data);
	}
        public function modal_campus_comment(){
        //para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
            $comment_id = $this->input->post('comment_id');
            $data['getComment'] = $this->superadmin_model->modal_campus_comment($comment_id);
            $this->load->view('superAdmin/thirdparty/temp_edit_comment',$data);
        }

        public function modal_campus_delete(){
        //para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
        	$comment_id = $this->input->post('comment_id');
        
            $data['getComment'] = $this->superadmin_model->modal_campus_comment($comment_id);
        	$this->load->view('superAdmin/thirdparty/temp_delete_comment',$data);
        }
	public function campus_comment_update(){
		//para ma clean
		$this->form_validation->set_rules('comment','','xss_clean');
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->set_rules('campus_post_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment = $this->input->post('comment');
		$comment_id = $this->input->post('comment_id');
		$campus_post_id = $this->input->post('campus_post_id');
		$data['campus_comments'] = $this->superadmin_model->getComments();
		$data['campus_post_id'] =  $campus_post_id;
		$this->superadmin_model->campus_comment_update($comment,$comment_id);
		
		$this->load->view('superAdmin/thirdparty/temp_campus_comments',$data);
		$this->load->view('superAdmin/includes/campus_editComment_modal',$data);
	}

	public function campus_comment_delete(){
		//para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->set_rules('campus_post_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment_id = $this->input->post('comment_id');
		$campus_post_id = $this->input->post('campus_post_id');
		$data['campus_comments'] = $this->superadmin_model->getComments();
		$data['campus_post_id'] =  $campus_post_id;
		$this->superadmin_model->campus_comment_delete($comment_id);
		
		
		$this->load->view('superAdmin/thirdparty/temp_campus_comments',$data);
		$this->load->view('superAdmin/includes/campus_editComment_modal',$data);
	}

	public function delete_campus_post(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$post_id = $this->input->post('post_id');
		$this->superadmin_model->delete_campus_post($post_id);
	}

	// end sa post campus superadmin

	public function superadmin_department(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$department_id = $this->input->get('id');
		$data['department_id'] = $department_id;
		$data['last_post_date'] = $this->superadmin_model->superadmin_department_last_post_date($department_id);
		$data['previus_post_date'] = $this->superadmin_model->superadmin_department_previus_post_date($department_id);
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['departmentPostQuery'] = $this->superadmin_model->superadmin_department_post($department_id);
		$data['department_comments'] = $this->superadmin_model->dept_getComments();
		$this->load->view('superAdmin/includes/super_header');
		$data['department_query'] = $this->superadmin_model->get_department($department_id);
		$this->load->view('superAdmin/superadmin_department',$data);

		$this->load->view('superAdmin/includes/computer_modals',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}

	public function delete_department_post(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$post_id = $this->input->post('post_id');
		$department_id= $this->input->post('department_id');
		$this->superadmin_model->delete_department_post($post_id,$department_id);
	}

	public function sa_department_comments(){
		//para ma clean
		$this->form_validation->set_rules('department_post_id','','xss_clean');
		$this->form_validation->set_rules('department_topic','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$department_post_id = $this->input->post('department_post_id');
		$department_topic = $this->input->post('department_topic');
		
		$data['department_comments'] = $this->superadmin_model->dept_getComments();
		$data['department_post_id'] =  $department_post_id;
		
		$this->superadmin_model->sa_department_comments($department_topic,$department_post_id);
		
		$this->load->view('superAdmin/thirdparty/temp_department_comments',$data);
		$this->load->view('superAdmin/includes/department_modals',$data);
		
	}

	public function department_comment_update(){
		//para ma clean
		$this->form_validation->set_rules('comment','','xss_clean');
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->set_rules('department_topic_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment = $this->input->post('comment');
		$comment_id = $this->input->post('comment_id');
		$department_topic_id = $this->input->post('department_topic_id');
		$data['department_comments'] = $this->superadmin_model->dept_getComments();
		$data['department_post_id'] =  $department_topic_id;
		$this->superadmin_model->department_comment_update($comment,$comment_id,$department_topic_id);
		
		$this->load->view('superAdmin/thirdparty/temp_department_comments',$data);
		$this->load->view('superAdmin/includes/department_modals',$data);
	}

	public function department_comment_modal(){
		//para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment_id = $this->input->post('comment_id');
		$data['commentInfo'] = $this->superadmin_model->department_comment_update_modal($comment_id);
		$this->load->view('superAdmin/thirdparty/temp_dept_comment',$data);
	}

	public function department_delete_modal(){
		//para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment_id = $this->input->post('comment_id');
		$data['commentInfo'] = $this->superadmin_model->department_comment_update_modal($comment_id);
		$this->load->view('superAdmin/thirdparty/temp_dept_delete',$data);
	}
	public function department_comment_delete(){
		//para ma clean
		$this->form_validation->set_rules('comment_id','','xss_clean');
		$this->form_validation->set_rules('department_topic_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment_id = $this->input->post('comment_id');
		$department_topic_id = $this->input->post('department_topic_id');
		$data['department_comments'] = $this->superadmin_model->dept_getComments();
		$data['department_post_id'] =  $department_topic_id;
		$this->superadmin_model->department_comment_delete($comment_id);
		$this->load->view('superAdmin/thirdparty/temp_department_comments',$data);
		$this->load->view('superAdmin/includes/department_modals',$data);

	}
	

	///========para sa records===========//

	public function superadmin_block_user(){
		//para ma clean
		$this->form_validation->set_rules('reason','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$reason = $this->input->post('reason');
		$blocked_id = $this->input->post('blocked_id'); 
		

		$this->superadmin_model->superadmin_block($reason,$blocked_id);
	}

	public function superadmin_unblock_user(){
		//para ma clean
		$this->form_validation->set_rules('user_id','','xss_clean');
		$this->form_validation->set_rules('department_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$user_id = $this->input->post('user_id');
		$department_id = $this->input->post('department_id');
		$this->superadmin_model->superadmin_unblock($user_id,$department_id);
	}


	public function superadmin_blocked(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
	$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
	$id=$this->input->get('id');

	if($id == null){
	$data['blocked_user'] = $this->superadmin_model->blocked_user(1);
	$data['department_blocked'] = $this->superadmin_model->department_blocked(1);
	}else{
	$data['blocked_user'] = $this->superadmin_model->blocked_user($id);
	$data['department_blocked'] = $this->superadmin_model->department_blocked($id);
	}
		$data['records_query'] = $this->superadmin_model->superadmin_records();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['department_menu'] = $this->superadmin_model->department_menu();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_blocked',$data);
		$this->load->view('superAdmin/includes/bsit_block_modal');
		$this->load->view('superAdmin/includes/block_unblock_modal',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	
	public function superadmin_logs(){
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['admin_logs'] = $this->superadmin_model->get_admin_logs();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_logs',$data);
		$this->load->view('superAdmin/includes/super_footer');
	}
	
	public function superadmin_user_records(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['reason'] = $this->superadmin_model->get_reason();
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['records_query'] = $this->superadmin_model->superadmin_records();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_user_records',$data);
		$this->load->view('superAdmin/includes/print_modal');
		$this->load->view('superAdmin/includes/block_unblock_modal',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;

	}
	public function superadmin_print_records(){
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
			$id = $this->input->get('id');
		$data['admin_records'] = $this->superadmin_model->admin_records($id);
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_print_records',$data);
		$this->load->view('superAdmin/includes/super_footer');
		else:
				redirect(base_url().'superadmin_login');
		endif;

	}
	public function superadmin_message(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		//
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['messageInbox'] = $this->superadmin_model->messageInbox(); 
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_message',$data);
		$this->load->view('superAdmin/includes/message_modal',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	
	public function superadmin_news(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		//
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getNews'] = $this->superadmin_model->superadmin_news();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_news',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	public function superadmin_sports(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		//
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getSports'] = $this->superadmin_model->superadmin_sports();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_sports',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	public function superadmin_entertainment(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getEntertainment'] = $this->superadmin_model->superadmin_entertainment();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_entertainment',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
	public function superadmin_editorial(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		//
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getEditorial'] = $this->superadmin_model->superadmin_editorial();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_editorial',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}

	public function superadmin_announcement(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		//
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['getAnnouncement'] = $this->superadmin_model->superadmin_announcement();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_announcement',$data);
		$this->load->view('superAdmin/includes/super_footer');
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
// change password
	public function change_password(){
		//para ma clean
		$this->form_validation->set_rules('current_password','','xss_clean');
		$this->form_validation->set_rules('new_password','','xss_clean');
		$this->form_validation->run();
		//end sa clean

		$current_password = $this->input->post('current_password');
		$new_password = $this->input->post('new_password');
		$data['checkPassword'] = $this->superadmin_model->change_password($current_password,$new_password);

		$this->load->view('superAdmin/thirdparty/change_password_alert',$data);
	}
// change username
	public function change_username(){
		//para ma clean
		$this->form_validation->set_rules('current_username','','xss_clean');
		$this->form_validation->set_rules('new_username','','xss_clean');
		$this->form_validation->run();
		//end sa clean

		$current_username = $this->input->post('current_username');
		$new_username = $this->input->post('new_username');
		$data['checkUsername'] = $this->superadmin_model->change_username($current_username,$new_username);

		$this->load->view('superAdmin/thirdparty/change_username_alert',$data);
	}
// account settings

	public function account_settings(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['superadmin_info'] = $this->superadmin_model->show_personal_info();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/account_settings',$data);
		$this->load->view('superAdmin/includes/super_footer',$data);
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}
// para sa pages
	public function get_add_post_modal(){
		//para ma clean
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$data['temp_id'] = $this->input->post('temp_id');
		$this->load->view('superAdmin/thirdparty/temp_insert_post_pages',$data);
	}

	public function get_edit_post_modal(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$post_id = $this->input->post('post_id');
		$temp_id = $this->input->post('temp_id');
		$data['postInfo'] = $this->superadmin_model->getPostInfo($post_id,$temp_id);
		$data['temp_id'] = $temp_id;
		$data['post_id'] = $post_id;

		$this->load->view('superAdmin/thirdparty/temp_update_post_pages',$data);
	}

	public function get_delete_post_modal(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$data['temp_id'] = $this->input->post('temp_id');
		$data['post_id'] = $this->input->post('post_id');
		$this->load->view('superAdmin/thirdparty/temp_delete_post_pages',$data);
	}

	public function delete_pages_post(){
		//para ma clean
		$this->form_validation->set_rules('post_id','','xss_clean');
		$this->form_validation->set_rules('temp_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$post_id = $this->input->post('post_id');
		$temp_id = $this->input->post('temp_id');
		$this->superadmin_model->delete_pages_post($post_id,$temp_id);
	}

		public function send_new_message(){
		//para ma clean
		$this->form_validation->set_rules('send_to','','xss_clean');
		$this->form_validation->set_rules('new_message','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$department_id = $this->input->post('send_to');
		$new_message = $this->input->post('new_message');

		$this->superadmin_model->insert_new_message($department_id,$new_message);
	}
	public function reply_message_modal(){
		//para ma clean
		$this->form_validation->set_rules('message_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$message = $this->input->post('message_id');
		$data['replyInfo'] = $this->superadmin_model->reply_info_message($message);
		$this->load->view('superAdmin/thirdparty/reply_message.php',$data);
	}
	public function forward_message_modal(){
		//para ma clean
		$this->form_validation->set_rules('message_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$message = $this->input->post('message_id');
		$data['forwardInfo'] = $this->superadmin_model->reply_info_message($message);
		$this->load->view('superAdmin/thirdparty/temp_forward_message.php',$data);
	}
	public function delete_message_modal(){
		//para ma clean
		$this->form_validation->set_rules('message_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$message = $this->input->post('message_id');
		$data['forwardInfo'] = $this->superadmin_model->reply_info_message($message);
		$this->load->view('superAdmin/thirdparty/temp_delete_message.php',$data);
	}
	public function delete_action_message(){
		//para ma clean
		$this->form_validation->set_rules('message_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$comment_id = $this->input->post('message_id');
		$this->superadmin_model->delete_action_message($comment_id);
	}
	public function change_personel_info(){
		
		$firstname = $this->input->post('firstname');
		$lastname = $this->input->post('lastname');
		$middlename = $this->input->post('middlename');
		$faculty_id = $this->input->post('faculty_id');
		$email = $this->input->post('email');
		$contact_number = $this->input->post('contact_number');
		$this->superadmin_model->change_personel_info($firstname,$lastname,$middlename,$faculty_id,$email,$contact_number);

	}

	public function superadmin_settings(){
		// para sa session
		$this->load->view('superAdmin/includes/cache_control');
		if($this->superadmin_model->checkSuperadminInfo()):
		// 
		$data['superadminInfo'] = $this->superadmin_model->get_superadmin_info();
		$data['query'] = $this->superadmin_model->superadmin_deptlist();
		$data['scsit_information'] = $this->superadmin_model->get_scsit_info();
		$data['reason'] = $this->superadmin_model->get_reason();
		$data['system_status'] = $this->superadmin_model->get_system_status();
		$this->load->view('superAdmin/includes/super_header');
		$this->load->view('superAdmin/superadmin_settings',$data);
		$this->load->view('superAdmin/includes/super_footer',$data);
		//
		else:
				redirect(base_url().'superadmin_login');
		endif;
	}

	public function update_contact(){
		//para ma clean
		$this->form_validation->set_rules('contact_number','','xss_clean');
		$this->form_validation->set_rules('email','','xss_clean');
		$this->form_validation->set_rules('address','','xss_clean');
		$this->form_validation->run();
		//end sa clean

		$contact_number = $this->input->post('contact_number');
		$email = $this->input->post('email');
		$address = $this->input->post('address');

		$this->superadmin_model->update_contact($contact_number,$email,$address);
	}

	public function add_reason(){
		$this->form_validation->set_rules('reason','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$reason = $this->input->post('reason');
		$this->superadmin_model->add_reason($reason);
	}
	public function update_reason(){
		$this->form_validation->set_rules('reason','','xss_clean');
		$this->form_validation->set_rules('reason_id','','xss_clean');
		$this->form_validation->run();
		//end sa clean
		$reason = $this->input->post('reason');
		$reason_id = $this->input->post('reason_id');
		$this->superadmin_model->update_reason($reason,$reason_id);
	}
	public function delete_reason(){
		$reason_id = $this->input->post('reason_id');
		$this->superadmin_model->delete_reason($reason_id);
	}
	public function deactivate_system(){
		$status = $this->input->post('status');
		$this->superadmin_model->deactivate_system($status);

	}
	public function delete_logs(){
		$logs_id = $this->input->post('logs_id');
		$this->superadmin_model->delete_logs($logs_id);
	}
	public function add_announcement(){
		$announcement = $this->input->post('announcement');
		$this->superadmin_model->add_announcement($announcement);
	}
	public function update_announcement(){
		$announcement_id = $this->input->post('announcement_id');
		$announcement = $this->input->post('announcement');
		$this->superadmin_model->update_announcement($announcement,$announcement_id);
	}
	public function delete_announcement(){
		$announcement_id = $this->input->post('announcement_id');
		$this->superadmin_model->delete_announcement($announcement_id);
	}




}