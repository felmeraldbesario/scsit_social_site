<?php

class user_roycontroller extends CI_Controller {

	// *news functionalities*
	public function newspage(){
		//query news for admin
		$data['sessionUser'] = $this->user->sessionUser();
		$data['departments'] = $this->user->departments();
		$data['querySelectnews'] = $this->department_admin_model->select_news();
		$this->load->view('user/user_header');
		$this->load->view('user/newspage',$data);
		 $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/modals/editCampusPost');
            $this->load->view('user/user_footer');
	}
    // public function newspagemodaltwocomment(){
            
    //     $data['querySelectnews'] = $this->temporary_admin_model->select_news();
    //     $this->load->view('user/newspagemodaltwocomment',$data);
    // }
    // public function newspagemodalallcomment(){
            
    //     $data['querySelectnews'] = $this->temporary_admin_model->select_news();
    //     $this->load->view('user/newspagemodalallcomment',$data);
    // }
    // public function newspagemodalview_like_comment(){
            
    //     $data['querySelectnews'] = $this->temporary_admin_model->select_news();
    //     $this->load->view('user/newspagemodalview_like_comment',$data);
    // }
    // public function newspagemodal_readmore(){
            
    //     $data['querySelectnews'] = $this->temporary_admin_model->select_news();
    //     $this->load->view('user/newspagemodal_readmore',$data);
    // }
	public function callwhocomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwhocomment_modal($data);
		if(isset($result)){

			$data['Allcommentnews'] = $this->user_roymodel->callwhocomment_modal($data);
			$this->load->view('r_thirdparty/temporary_whocomment',$data);
		}
	}
	public function callwholikes(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwholikes_modal($data);
		if(isset($result)){

			$data['All_likenews'] = $this->user_roymodel->callwholikes_modal($data);
			$this->load->view('r_thirdparty/temporary_wholikes',$data);
		}	
	}
	public function postviews(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->countviews($data);
	}
	public function postlike(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);

		$this->user_roymodel->insert_like($data);
	}
	public function postcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');

		if($comment != '' && $comment != null){
			if(ctype_space($comment) == false){

				$data = array('topic_id'=>$topic_id,
 					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
			    $this->user_roymodel->insert_comment($data);
			}else{
				// if true white space or empty ang sulod sa $comment nga string... 
				echo "White spaces or blank comment is not allowed.";
			}
		}else{
			echo "Please fill-in the comment box.";
		}
		

	}
	public function post_countcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->countcomments($data);

	}
	public function post_uncountcomment(){

		//$id_comment = $this->input->post('id_comment');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$this->user_roymodel->uncountcomment($data);

	}
	public function editcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');
		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
		$result = $this->user_roymodel->update_comment($data);
		if($result){

			echo $comment;
		}else{

			echo "update failed";
		}
	}
	public function timeupdated_comments(){

		$id_comment = $this->input->post('id');
		if($id_comment){

			date_default_timezone_set('Asia/Manila');
			$date = date('F d, y h:ia');
			// echo '<span class="glyphicon glyphicon-time">'.$date.'</span>';
			echo '<h6 class="text-warning"style="margin-top:-5px;" id="updated_time'.$id_comment.'"><span class="glyphicon glyphicon-time"></span> ' .$date.'</h6>';
		}
	}
	public function deletecomment(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');

		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->delete_comment($data);
	}
	// end sa news functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// *sports functionalities*
	public function sports(){
		$data['sessionUser'] = $this->user->sessionUser();
		$data['departments'] = $this->user->departments();
		$data['Selectsports'] = $this->department_admin_model->select_sports();
		$this->load->view('user/user_header');
		$this->load->view('user/sports',$data);
		 $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/modals/editCampusPost');
            $this->load->view('user/user_footer');
	}
	public function sportsmodaltwocomment(){
            
        $data['Selectsports'] = $this->temporary_admin_model->select_sports();
        $this->load->view('user/sportsmodaltwocomment',$data);
    }
    // public function sportsmodalallcomment(){
            
    //     $data['Selectsports'] = $this->temporary_admin_model->select_sports();
    //     $this->load->view('user/sportsmodalallcomment',$data);
    // }
    // public function sportspagemodal_readmore(){
            
    //     $data['Selectsports'] = $this->temporary_admin_model->select_sports();
    //     $this->load->view('user/sportsmodal_readmore',$data);
    // }
    // public function sportsmodalview_like_comment(){
            
    //     $data['Selectsports'] = $this->temporary_admin_model->select_sports();
    //     $this->load->view('user/sportsmodalview_like_comment',$data);
    // }
	public function post_sportscomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');

		if($comment != '' && $comment != null){
			if(ctype_space($comment) == false){

				$data = array('topic_id'=>$topic_id,
 					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
			    $this->user_roymodel->insert_sportscomment($data);
			}else{
				// if true white space or empty ang sulod sa $comment nga string... 
				echo "White spaces or blank comment is not allowed.";
			}
		}else{
			echo "Please fill-in the comment box.";
		}
	}
	public function post_sportscountcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->sportscountcomments($data);		
	}
	public function post_sportseditcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');
		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
		$result = $this->user_roymodel->update_commentsports($data);
		if($result){

			echo $comment;
		}else{

			echo "update failed";
		}
	}
	public function timeupdated_postsportscomments(){

		$id_comment = $this->input->post('id');
		if($id_comment){

			date_default_timezone_set('Asia/Manila');
			$date = date('F d, Y h:ia');
			// echo '<span class="glyphicon glyphicon-time">'.$date.'</span>';
			echo '<h6 class="text-warning"style="margin-top:-5px;" id="updated_time_sports'.$id_comment.'"><span class="glyphicon glyphicon-time"></span> ' .$date.'</h6>';
		}
	}
	public function post_deletesportscomment(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');

		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->delete_commentsports($data);
	}
	public function post_uncountsportscomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$this->user_roymodel->uncountcommentsports($data);
	}
	public function post_likesports(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);

		$this->user_roymodel->insert_likesports($data);
	}
	public function callwholikes_sports(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwholikes_modalsports($data);
		if(isset($result)){

			$data['All_likesports'] = $this->user_roymodel->callwholikes_modalsports($data);
			$this->load->view('r_thirdparty/temporary_wholikes_sports',$data);
		}	
	}
    public function callwhocomment_sports(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwhocomment_modalsports($data);
		if(isset($result)){

			$data['Allcommentsports'] = $this->user_roymodel->callwhocomment_modalsports($data);
			$this->load->view('r_thirdparty/temporary_whocomment_sports',$data);
		}
	}
	public function postviews_sports(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->countviews_sports($data);
	}
	// end sa sports functionalities
	// *entertainment functionalities*
	public function entertainment(){
		$data['sessionUser'] = $this->user->sessionUser();
		$data['departments'] = $this->user->departments();
		$data['Selectentertainment'] = $this->department_admin_model->select_entertainment();
		$this->load->view('user/user_header');
		$this->load->view('user/entertainment',$data);
		 $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/modals/editCampusPost');
            $this->load->view('user/user_footer');
	}
	// public function entertainmentmodaltwocomment(){
            
 //        $data['Selectentertainment'] = $this->temporary_admin_model->select_entertainment();
 //        $this->load->view('user/entertainmentmodaltwocomment',$data);
 //    }
 //    public function sportsmodalallcomment(){
            
 //        $data['Selectsports'] = $this->temporary_admin_model->select_sports();
 //        $this->load->view('user/sportsmodalallcomment',$data);
 //    }
 //    public function sportspagemodal_readmore(){
            
 //        $data['Selectsports'] = $this->temporary_admin_model->select_sports();
 //        $this->load->view('user/sportsmodal_readmore',$data);
 //    }
    // public function entertainmentmodalview_like_comment(){
            
    //     $data['Selectentertainment'] = $this->temporary_admin_model->select_entertainment();
    //     $this->load->view('user/entertainmentmodalview_like_comment',$data);
    // }
	public function post_entertainmentcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');

		if($comment != '' && $comment != null){
			if(ctype_space($comment) == false){

				$data = array('topic_id'=>$topic_id,
 					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
			    $this->user_roymodel->insert_entertainmentcomment($data);
			}else{
				// if true white space or empty ang sulod sa $comment nga string... 
				echo "White spaces or blank comment is not allowed.";
			}
		}else{
			echo "Please fill-in the comment box.";
		}
	}
	public function post_entertainmentcountcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->entertainmentcountcomments($data);		
	}
	public function post_entertainmenteditcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');
		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
		$result = $this->user_roymodel->update_commententertainment($data);
		if($result){

			echo $comment;
		}else{

			echo "update failed";
		}
	}
	public function timeupdated_postentertainmentcomments(){

		$id_comment = $this->input->post('id');
		if($id_comment){

			date_default_timezone_set('Asia/Manila');
			$date = date('F d, Y h:ia');
			// echo '<span class="glyphicon glyphicon-time">'.$date.'</span>';
			echo '<h6 class="text-warning"style="margin-top:-5px;" id="updated_time_entertainment'.$id_comment.'"><span class="glyphicon glyphicon-time"></span> ' .$date.'</h6>';
		}
	}
	public function post_deleteentertainmentcomment(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');

		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->delete_commententertainment($data);
	}
	public function post_uncountentertainmentcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$this->user_roymodel->uncountcommententertainment($data);
	}
	public function post_likeentertainment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);

		$this->user_roymodel->insert_likeentertainment($data);
	}
	public function callwholikes_entertainment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwholikes_modalentertainment($data);
		if(isset($result)){

			$data['All_likeentertainment'] = $this->user_roymodel->callwholikes_modalentertainment($data);
			$this->load->view('r_thirdparty/temporary_wholikes_entertainment',$data);
		}	
	}
    public function callwhocomment_entertainment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwhocomment_modalentertainment($data);
		if(isset($result)){

			$data['Allcommententertainment'] = $this->user_roymodel->callwhocomment_modalentertainment($data);
			$this->load->view('r_thirdparty/temporary_whocomment_entertainment',$data);
		}
	}
	public function postviews_entertainment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->countviews_entertainment($data);
	}
	// end sa entertainment functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// *editorial functionalities*
	public function editorial(){
		$data['sessionUser'] = $this->user->sessionUser();
		$data['departments'] = $this->user->departments();
		$data['Selecteditorial'] = $this->department_admin_model->select_editorial();
		$this->load->view('user/user_header');
		$this->load->view('user/editorial',$data);
		$this->load->view('user/modals/homemodal');
        $this->load->view('user/user_wall_modal');
        $this->load->view('user/user_department_modal');
        $this->load->view('user/modals/editCampusPost');
        $this->load->view('user/user_footer');
	}
	// public function entertainmentmodaltwocomment(){
            
 //        $data['Selectentertainment'] = $this->temporary_admin_model->select_entertainment();
 //        $this->load->view('user/entertainmentmodaltwocomment',$data);
 //    }
 //    public function sportsmodalallcomment(){
            
 //        $data['Selectsports'] = $this->temporary_admin_model->select_sports();
 //        $this->load->view('user/sportsmodalallcomment',$data);
 //    }
 //    public function sportspagemodal_readmore(){
            
 //        $data['Selectsports'] = $this->temporary_admin_model->select_sports();
 //        $this->load->view('user/sportsmodal_readmore',$data);
 //    }
    // public function entertainmentmodalview_like_comment(){
            
    //     $data['Selectentertainment'] = $this->temporary_admin_model->select_entertainment();
    //     $this->load->view('user/entertainmentmodalview_like_comment',$data);
    // }
	public function post_editorialcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');

		if($comment != '' && $comment != null){ 
			if(ctype_space($comment) == false){

				$data = array('topic_id'=>$topic_id,
 					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
			    $this->user_roymodel->insert_editorialcomment($data);
			}else{
				// if true white space or empty ang sulod sa $comment nga string... 
				echo "White spaces or blank comment is not allowed.";
			}
		}else{
			echo "Please fill-in the comment box.";
		}
	}
	public function post_editorialcountcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->editorialcountcomments($data);		
	}
	public function post_editorialeditcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');
		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
		$result = $this->user_roymodel->update_commenteditorial($data);
		if($result){

			echo $comment;
		}else{

			echo "update failed";
		}
	}
	public function timeupdated_posteditorialcomments(){

		$id_comment = $this->input->post('id');
		if($id_comment){

			date_default_timezone_set('Asia/Manila');
			$date = date('F d, Y h:ia');
			// echo '<span class="glyphicon glyphicon-time">'.$date.'</span>';
			echo '<h6 class="text-warning"style="margin-top:-5px;" id="updated_time_editorial'.$id_comment.'"><span class="glyphicon glyphicon-time"></span> ' .$date.'</h6>';
		}
	}
	public function post_deleteeditorialcomment(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');

		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->delete_commenteditorial($data);
	}
	public function post_uncounteditorialcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$this->user_roymodel->uncountcommenteditorial($data);
	}
	public function post_likeeditorial(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);

		$this->user_roymodel->insert_likeeditorial($data);
	}
	public function callwholikes_editorial(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwholikes_modaleditorial($data);
		if(isset($result)){

			$data['All_likeeditorial'] = $this->user_roymodel->callwholikes_modaleditorial($data);
			$this->load->view('r_thirdparty/temporary_wholikes_editorial',$data);
		}	
	}
    public function callwhocomment_editorial(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwhocomment_modaleditorial($data);
		if(isset($result)){

			$data['Allcommenteditorial'] = $this->user_roymodel->callwhocomment_modaleditorial($data);
			$this->load->view('r_thirdparty/temporary_whocomment_editorial',$data);
		}
	}
	public function postviews_editorial(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->countviews_editorial($data);
	}
	// end sa editorial functionalities------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	// *announcement functionalities*
	public function announcement(){
		$data['sessionUser'] = $this->user->sessionUser();
		$data['departments'] = $this->user->departments();
		$data['Selectannouncement'] = $this->department_admin_model->select_announcement();
		$data['select_oneannouncement'] = $this->user_roymodel->select_oneannouncement();
		$data['select_allannouncement'] = $this->user_roymodel->select_allannouncement();
		$this->load->view('user/user_header' );
		$this->load->view('user/announcement',$data);
		$this->load->view('user/modals/announcement_modal');
		$this->load->view('user/user_footer');
	}
	public function post_announcement(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$comment = $this->input->post('comment');

		if($comment != '' && $comment != null){ 
			if(ctype_space($comment) == false){

				$data = array('topic_id'=>$topic_id,
 					  'user_id'=>$user_id,
					  'comment'=>$comment,
					  'date'=>$date);
			    $this->user_roymodel->insert_announcementcomment($data);
			}else{
				// if true white space or empty ang sulod sa $comment nga string... 
				echo "White spaces or blank comment is not allowed.";
			}
		}else{
			echo "Please fill-in the comment box.";
		}
	}
	public function post_announcementcountcomment(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$this->user_roymodel->announcementcountcomments($data);		
	}
	public function post_callwhocomment_announcement(){

		$topic_id = $this->input->post('topic_id');

		$result = $this->user_roymodel->callwhocomment_modalannouncement($topic_id);
		if(isset($result)){
			$data['select_announcement_modalcomment'] = $this->user_roymodel->callwhocomment_modalannouncement($topic_id);
			$this->load->view('r_thirdparty/temporary_whocomment_announcement',$data);
		}
	}
	public function post_likeannouncement(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);

		$this->user_roymodel->insert_likeannouncement($data);
	}
	public function callwholikes_announcement(){

		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->callwholikes_modalannouncement($data);
		if(isset($result)){

			$data['All_likeannouncement'] = $this->user_roymodel->callwholikes_modalannouncement($data);
			$this->load->view('r_thirdparty/temporary_wholikes_announcement',$data);
		}	
	}
	public function post_callmodal_update(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('id_comment'=>$id_comment,
			          'topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$result = $this->user_roymodel->callmodal_update($data);
		if(isset($result)){

			$data['select_announcement_comment'] = $this->user_roymodel->callmodal_update($data);
			$this->load->view('r_thirdparty/temporary_commentbody',$data);
		}
	}
	public function post_announcementeditcomment(){

		date_default_timezone_set('Asia/Manila');
		$date = date('F d, Y h:ia');
		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$comment = $this->input->post('comment');
		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'comment'=>$comment,
					  'date'=>$date);
		$result = $this->user_roymodel->update_commentannouncement($data);
		if($result){

			echo $comment;
		}else{

			echo "update failed";
		}
	}
	public function timeupdated_postannouncementcomments(){

		$id_comment = $this->input->post('id');
		if($id_comment){

			date_default_timezone_set('Asia/Manila');
			$date = date('F d, Y h:ia');

			echo $date; 
		}
	}
	public function post_callmodal_delete(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('id_comment'=>$id_comment,
			          'topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$result = $this->user_roymodel->callmodal_delete($data);
		if(isset($result)){

			$data['data_announcement_comment'] = $this->user_roymodel->callmodal_delete($data);
			$this->load->view('r_thirdparty/temporary_deletecommentbody',$data);
		}
	}
	public function post_deletecomment_announcement(){

		$id_comment = $this->input->post('id');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');

		$data = array('id_comment'=>$id_comment,
					  'topic_id'=>$topic_id,
					  'user_id'=>$user_id);
		$result = $this->user_roymodel->delete_commentannouncement($data);
	}
	public function post_uncountannouncementcomment(){

		//$id_comment = $this->input->post('id_comment');
		$topic_id = $this->input->post('topic_id');
		$user_id = $this->input->post('user_id');
		$data = array('topic_id'=>$topic_id,
			          'user_id'=>$user_id);
		$this->user_roymodel->announcementuncountcomments($data);

	}
	// end sa announcemnt functionalities
	// public function login(){

	// 	$username = $this->input->post('username');
	// 	$password = $this->input->post('password');
	// 	$data = array('username'=>$username,
	// 				  'password'=>$password);
	// 	$this->user_roymodel->login_auth($data);
	// }
	// public function log_out(){

	// 	$sessionData = array('user_id' => "",
	// 						 'username' => "",
	// 						 'firstname' => "",
	// 						 'lastname' => "",
	// 						 'logged_in' => FALSE);
	// 	$this->session->unset_userdata($sessionData);
	// 	// if(isset($result)){

	// 		redirect(base_url().'index');
	// 	// }
	// }
}
?>