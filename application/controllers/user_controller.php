<!-- 
@Author: Felmerald Besario
@Date Created: November 21, 2015 
modified: december 24, 2015

-->

<?php

class user_controller extends CI_Controller {

    // for designs
    function __construct(){
        parent::__construct();
        $this->load->library('Block');
    }
    
    
    public function index() {
        $data['news_limit'] = $this->adver_model->News_limit();
        $data['entertainment_limit'] = $this->adver_model->entertainment_limit();
        $data['sport_limit'] = $this->adver_model->sport_limit();
        $data['editorial_limit'] = $this->adver_model->editorial_limit();
        $data['announcement_event'] = $this->adver_model->getAnnouncement();
        $data['felmeraldCopy'] = $this->adver_model->felcopyRightfooter();
        $data['newsRead'] = $this->adver_model->readMore();
        $data['sportsRead'] = $this->adver_model->sportsRead();
        $data['editorialRead'] = $this->adver_model->editorialRead();
        $data['entertainementRead'] = $this->adver_model->entertainementRead();
        $data['clickmenews'] = $this->adver_model->clickmenews();
        $data['title'] = "Welcome to scsit social site";
        $data['newsAdverFooter'] ="&copy scsitsocialsite news advertisement";
        $data['dept_data'] = $this->userModel->select_department();
        $this->load->view('index',$data);
      
    }
    
    public function preSignUp(){
        $data = array(
            array(
                'field'=> 'department_id',
                'label'=> 'department_id',
                'rules'=> 'required|numeric|xss_clean'
            ),
            array(
                'field'=> 'firstname',
                'label'=> 'First name',
                'rules'=> 'required|xss_clean'
            ),
            array(
                'field'=> 'middlename',
                'label'=> 'Middle name',
                'rules'=> 'required|xss_clean'
            ),
            array(
                'field'=> 'lastname',
                'label'=> 'Last Name',
                'rules'=> 'required|xss_clean'
            ),
            array(
                'field'=> 'age',
                'label'=> 'age',
                'rules'=> 'required|xss_clean'
            ),
            array(
                'field'=> 'username',
                'label'=> 'username',
                'rules'=>'trim|required|min_length[6]|xss_clean'
            ),
            array(
                'field'=> 'password',
                'label'=> 'password',
                'rules'=> 'trim|required|min_length[6]|xss_clean'
            ),
            array(
                'field'=> 'cpassword',
                'label'=> 'confirm password area',
                'rules'=> 'trim|required|min_length[6]|xss_clean'
            )
        );
        $this->form_validation->set_message('alpha','This field must contain all letters..');
        $this->form_validation->set_message('numeric', 'This field must be all numbers..');
        $this->form_validation->set_rules($data);
        if($this->form_validation->run() == FALSE){
            $this->index();
        }else{
           $dept_id = $this->input->post('department_id');
           $firstname = $this->input->post('firstname');
           $middlename = $this->input->post('middlename');
           $lastname = $this->input->post('lastname');
           $bdate = $this->input->post('bdate');
           $age = $this->input->post('age');
           $gender = $this->input->post('gender');
           $username = $this->input->post('username');
           $password = $this->input->post('password');
           $cpassword = $this->input->post('cpassword');
           if($password != $cpassword){
               $this->index();
           }else{
              $this->userModel->preSignUp($dept_id, $firstname, $middlename, $lastname, $bdate, $age, $gender, $username, $password);
           }
        }
    }


    public function login(){
       $data = array(
           array(
                'field'=> 'login_username',
                'label'=> 'username',
                'rules'=>'trim|required|min_length[6]|xss_clean'
            ),
            array(
                'field'=> 'login_password',
                'label'=> 'password',
                'rules'=> 'trim|required|min_length[6]|xss_clean'
            ),
        );

       $this->form_validation->set_rules($data);
       if($this->form_validation->run() == FALSE){
            $this->auth();
       }else{
            $username = $this->input->post('login_username');
            $password = $this->input->post('login_password');
            $this->userModel->login($username, $password);
       }
    }

    public function home() {
        if($this->session->userdata('login_id') == '' && $this->session->userdata('login_token') == ''){
            redirect(base_url().'index');
        }else{ 
            $data['getCampusPostCommentsHome'] = $this->user->getCampusPostCommentsHome();
            $data['getCampusPost'] = $this->user->getCampusPost();
            $data['sessionUser'] = $this->user->sessionUser();
            $data['departments'] = $this->user->departments();
            $this->load->view('user/user_header');
            $this->load->view('user/home', $data);
            //$this->load->view('user/departmentUsers');
            $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/modals/editCampusPost');
            $this->load->view('user/user_footer');
        }
    }

    public function getUsers(){
       $id = $this->input->post('id');
       $data['getUsers'] = $this->user->getUsers($id);
       $this->load->view('thirdParty/users', $data);
    }

    public function friendRequestCount(){
        $sess_id = $this->input->post('sess_id');
        $this->user->friendRequestCount($sess_id);
    }

    public function confirmFRequest(){
        $data['sender_info'] = $this->user->confirmFRequest();
        $this->load->view('thirdParty/confirmFRequest', $data);
    }

    public function cfRequest(){
        $user_id = $this->input->post('user_id');
         $this->user->cfRequest($user_id);
       
    }

    public function sendFRequest(){
        $user_id = $this->input->post('user_id');
        $this->user->sendFRequest($user_id);
    }

    public function getFriends(){
        $data['checkFRelationship'] = $this->user->checkFriendRelationship();
        $this->load->view('thirdParty/friends', $data);
    }

    public function messageCount(){
        $sess_id = $this->input->post('sess_id');
        $this->user->messageCount($sess_id);
    }

    public function user_wall_modal() {
        $this->load->view('user/user_wall_modal');
    }

    public function delete_confirmation_modal() {
        $this->load->view('user/delete_confirmation_modal');
    }

    public function sidebar() {
        $this->load->view('user/sidebar');
    }

    public function nav_menu_top() {
        $this->load->view('user/nav_menu_top');
    }

    public function user_header() {
        $this->load->view('user/user_header');
    }

    public function edits_popup_modal() {
        $this->load->view('user/edits_popup_modal');
    }

    public function user_department_modal() {
        $this->load->view('user/user_department_modal');
    }

    public function user_messaging() {
         if($this->session->userdata('login_id') == '' && $this->session->userdata('login_token') == ''){
            redirect(base_url().'index');
         }else{
            $id = $this->input->get('id');
            if($id == ''){
                redirect(base_url().'home');
            }
            $data['show_sms'] = $this->user->show_sms($id);
            $data['sessionUser'] = $this->user->sessionUser();
            $data['departments'] = $this->user->departments();
            $this->load->view('user/user_header');
            $this->load->view('user/user_messaging', $data);
            $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/user_footer');
        }
    }

    public function profile() {
         if($this->session->userdata('login_id') == '' && $this->session->userdata('login_token') == ''){
            redirect(base_url().'index');
         }else{
            $id = $this->input->get('id');
            if($id == ''){
                redirect(base_url().'home');
            }
            $data['jam_findProfile'] = $this->user->jam_findProfile($id);
            $data['getAllPostPicture'] = $this->user->getAllPostPicture($id);
            $data['sessionUser'] = $this->user->sessionUser();
            $data['departments'] = $this->user->departments();
            $this->load->view('user/user_header');
            $this->load->view('user/profile', $data);
            $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/modals/viewFullImage');
            $this->load->view('user/modals/editCampusPost');
            $this->load->view('user/modals/editCPostComment');
            $this->load->view('user/user_footer');
        }
    }

    public function department() {
        if($this->session->userdata('login_id') == '' && $this->session->userdata('login_token') == ''){
            redirect(base_url().'index');
         }else{
            $data['sessionUser'] = $this->user->sessionUser();
            $data['departments'] = $this->user->departments();
            $data['retrieveDeptPost'] = $this->user->retrieveDeptPost();            
            $this->load->view('user/user_header');
            $this->load->view('user/department', $data);
            $this->load->view('user/modals/department_modals');
            $this->load->view('user/modals/department_post_modals');
            $this->load->view('user/modals/department_comment_modals');
            $this->load->view('user/modals/homemodal');
            $this->load->view('user/user_wall_modal');
            $this->load->view('user/user_department_modal');
            $this->load->view('user/user_footer');
        }
    }

    public function verification(){
        $this->block->checkSession();
        $this->load->view('verification');
        $this->load->view('footer');
    }

    public function verify(){
       $data = array(
            array(
                'field' => 'verification',
                'label' => 'verification',
                'rules' => 'trim|required|xss_clean'
            )
        );
       $this->form_validation->set_rules($data);
       if($this->form_validation->run() == FALSE){
            $this->verification();
       }else{
         $verificationCode = $this->input->post('verification');
         $this->userModel->verify($verificationCode);
       }
    }
    public function header() {
        $this->load->view('header');
    }

    public function footer() {
        $this->load->view('footer');
    }

    public function auth() {
        $this->load->view('auth');
    }

    public function signup() {
        $this->block->checkSession();
        $data['course'] = $this->userModel->select_course();
        $data['question'] = $this->userModel->question();
        $this->load->view('signup', $data);
        $this->load->view('checkanswerModal');
        $this->load->view('footer');
    }

    public function checkanswerModal() {
        $this->load->view('checkanswerModal');
    }

  

    public function newspageModal() {
        $this->load->view('user/newspageModal');
    }

    public function newspageEditCommentModal() {
        $this->load->view('user/newspageEditCommentModal');
    }

    public function newspageDeleteConfirmationModal() {
        $this->load->view('user/newspageDeleteConfirmationModal');
    }

    public function departmentEditandDeleteModal() {
        $this->load->view('user/departmentEditandDeleteModal');
    }

    public function debate() {
        $this->load->view('user/user_header');
        $this->load->view('user/debate');
        $this->load->view('user/modals/debate_modals');
        $this->load->view('user/user_footer');
    }

    public function left_sidebar() {
        $this->load->view('user/left_sidebar');
    }

    public function search_result_modal() {
        $this->load->view('user/modals/search_result_modal');
    }

    public function search_result_friendlist() {
        $this->load->view('user/modals/search_result_friendlist');
    }

    public function search_result_blocklist() {
        $this->load->view('user/modals/search_result_blocklist');
    }

    public function search_result_unfollow() {
        $this->load->view('user/modals/search_result_unfollow');
    }

    public function change_pass_modal() {
        $this->load->view('user/modals/change_pass_modal');
    }

    public function userlogout(){
        $this->user->userlogout();
    }

    public function campus_post(){
       $jam_campus_status = $this->input->post('jam_campus_status');
       $this->user->campus_post($jam);
    }

    public function delPost(){
        $id = $this->input->post('id');
        $this->user->delPost($id);
    }

    public function clikePost(){
        $id = $this->input->post('id');
        $this->user->clikePost($id);
    }

    public function commentCPost(){
       $invalid_characters = array("<script>");
       $comment = str_replace($invalid_characters, "", $this->input->post('comment'));
       $id = $this->input->post('id');
       $this->user->commentCPost($comment,$id);
       $data['getAllCommentCPost'] = $this->user->getAllCommentCPost($id);
       $this->load->view('thirdParty/cpost_comment', $data);
    }
  
    public function delComment(){
        $id = $this->input->post('id');
        $this->user->delComment($id);
    }

    public function getCampusCommentCount(){
        $id = $this->input->post('id');
        $this->user->getCpostCommenterCountAjax($id);
    }

    public function cpost_viewAllComments(){
       $id = $this->input->post('id');
       $data['getAllCommentCPost'] = $this->user->cpost_viewAllComments($id);
       $this->load->view('thirdParty/cpost_comment', $data);
    }

    public function getAllCPostCommenters(){
        $id = $this->input->post('id');
        $data['getAllCPostCommenters'] = $this->user->getAllCPostCommenters($id);
        $this->load->view('thirdParty/cpost_commenters', $data);
    }

    public function getDepartment(){
        $id = $this->input->post('id');
        echo ucwords(strtolower($this->user->getDepartment($id)));
    }

    public function jam_cpost_likers(){
        $id = $this->input->post('id');
        $data['jam_cpost_likers'] = $this->user->jam_cpost_likers($id);
       $this->load->view('thirdParty/cpost_likers', $data);
    }

    public function new_sms(){
        $data['new_sms'] = $this->user->new_sms();
        $this->load->view('thirdParty/new_sms', $data);
    }

    public function updateAccount(){
        $data = array(
                array(
                    'field' => 'firstname',
                    'label' => 'firstname',
                    'rules' => 'xss_clean'
                ),
                array(
                     'field' => 'middlename',
                     'label' => 'middlename',
                     'rules' => 'xss_clean'   
                ),
                array(
                    'field' => 'lastname',
                    'label' => 'lastname',
                    'rules' => 'xss_clean'
                ),
                array(
                    'field' => 'address',
                    'label' => 'address',
                    'rules' => 'xss_clean'
                ),
                array(
                    'field' => 'mobile_number',
                    'label' => 'mobile_number',
                    'rules' => 'xss_clean'
                ),
            );
        $this->form_validation->set_rules($data);
        if($this->form_validation->run() == FALSE){
            $this->home();
        }else{
            $firstname = $this->input->post('firstname');
            $middlename = $this->input->post('middlename');
            $lastname = $this->input->post('lastname');
            $address = $this->input->post('address');
            $mobile_number = $this->input->post('mobile_number');
            $birthdate = $this->input->post('birthdate');
            $this->user->updateAccount($firstname, $middlename, $lastname, $address, $mobile_number, $birthdate);
        }
    }

    public function updateSecurityAccount(){
        $invalid_characters = array("$", "%", "#", "<", ">", "|", "?", "/", "\\", " ", ";", ",");
        $password = str_replace($invalid_characters, "", trim($this->input->post('oldpass')));
        $newPassword = $this->input->post('newPassword');
        $email = str_replace($invalid_characters, "", trim($this->input->post('email')));
        $username = str_replace($invalid_characters, "", trim($this->input->post('username')));
        $charArray = str_split($newPassword);
        $valid_pass = "";
        $invalid = "";
        $first = 0;
        $second = 0;
        $third = 0;
        $fourth = 0;
        $fifth = 0;
        $sixth = 0;
        $seventh = 0;
        $eight = 0;
        $nine = 0;
        $tenth = 0;
        $eleventh = 0;
        $twelve = 0;
        foreach($charArray as $row){
            
            switch ($row) {
                case '$':
                   $first++;
                    if($first > 1){
                         break;
                    }else{
                         $invalid = $invalid." \"$\"";
                         break;
                     }

                 case '%':
                    $second++;
                    if($second > 1){
                         break;
                    }else{
                         $invalid = $invalid." \"%\"";
                         break;
                     }

                case '#':
                    $third++;
                    if($third > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"#\"";
                        break;
                    }

                case '<':
                    $fourth++;
                    if($fourth > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"<\"";
                        break;
                    }

                 case '>':
                    $fifth++;
                    if($fifth > 1){
                         break;
                    }else{
                        $invalid = $invalid." \">\"";
                        break;
                    }

                case '|':
                    $sixth++;
                    if($sixth > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"|\"";
                        break;
                    }

                case '?':
                    $seventh++;
                    if($seventh > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"?\"";
                        break;
                    }
                case '/':
                    $eight++;
                    if($eight > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"/\"";
                        break;
                    }
                case '\\':
                    $nine++;
                    if($nine > 1){
                         break;
                    }else{
                        $invalid = $invalid." \"\\\"";
                        break;
                    }
                case ' ':
                    $tenth++;
                    if($tenth > 1){
                         break;
                    }else{
                        $invalid = $invalid." \" space \"";
                        break;
                    }
                case ';':
                    $eleventh++;
                    if($eleventh > 1){
                         break;
                    }else{
                        $invalid = $invalid." \";\"";
                        break;
                    }

                 case ',':
                    $twelve++;
                    if($twelve > 1){
                         break;
                    }else{
                        $invalid = $invalid." \",\"";
                        break;
                    }

                default:
                    $valid_pass = $valid_pass.$row;
                    break;
            }
        }
        if($newPassword == $valid_pass){
            if(strlen($valid_pass) < 6){
                echo "password must be of 6 characters..";
            }else{
               if($this->user->chechValidPassword(md5(md5($password)))){
                    echo $this->user->updateSecurityAccount(md5(md5($valid_pass)), $email, $username);
               }else{
                    echo "incorrect old password...";
               }
            }

        }else{
            echo $invalid." is invalid character/s..";
        }
    }

    public function editCPost(){
        $id = $this->input->post('id');
        $data['editCpost'] = $this->user->editCPost($id);
        $this->load->view('thirdParty/editCpost', $data);
    }


    public function jam_viewFullImage(){
        $id = $this->input->post('id');
        $data['sessionUser'] = $this->user->sessionUser();
        $data['viewFullImage'] = $this->user->jam_viewFullImage($id);
        $data['viewFullImageComment'] = $this->user->jam_viewFullImageComment($id);
        $this->load->view('thirdParty/cpostViewFull', $data);
    }

    public function commentFullImge(){
        $invalid_characters = array("<script>");
        $cpost_id = $this->input->post('id');
        $comment = str_replace($invalid_characters, '', $this->input->post('comment'));
        if($comment != "" ){
            $data['commentFullImge'] = $this->user->commentFullImge($cpost_id, $comment);
            $this->load->view('thirdParty/viewFullImageComments', $data);
         }      

    }

    public function viewfullimagecommentDel(){
        $id = $this->input->post('id');
        $this->user->viewfullimagecommentDel($id);
    }

    public function dept_post(){
         $invalid_characters = array("<script>");
         $comment = str_replace($invalid_characters, "", $this->input->post('comment'));
         $id = $this->input->post('id');
         $this->user->dept_post($id, $comment);
         $data['deptCommentPost'] = $this->user->deptCommentPost($id);
         $this->load->view('thirdParty/department_post', $data);
    }

    public function jam_viewDeptComment(){
        $id = $this->input->post('id');
        $data['deptCommentPost'] = $this->user->deptCommentPost($id);
        $this->load->view('thirdParty/department_post', $data);
    }

    public function jam_delDeptPost(){
        $id = $this->input->post('id');
        $this->user->jam_delDeptPost($id);
    }

    public function readMessage(){
       $id = $this->input->post("id");
       $data['show_sms'] = $this->user->show_sms($id);
       $this->load->view("thirdParty/messaging", $data);
    }

    public function sendMessagetoUser(){
        $invalid_characters = array("<script>");
        $sms = str_replace($invalid_characters, "", $this->input->post('sms'));
        $receiver_id = $this->input->post('receiver_id');
        $this->user->sendMessagetoUser($sms, $receiver_id);
    }

    public function editComment(){
        $id = $this->input->post('id');
        $data['editComment'] = $this->user->editComment($id);
        $this->load->view('thirdParty/editCPostComment', $data);
    }

    public function editCommentFromCpost(){
        $invalid_characters = array("<script>");
        $comment = str_replace($invalid_characters, "", $this->input->post('comment'));
        $id = $this->input->post('id');

        $this->user->editCampusPost($comment, $id);
    }

    public function jam_likeDeptPost(){
        $id = $this->input->post("id");
        $this->user->jam_likeDeptPost($id);
    }

    public function jam_deleteDeptPostComment(){
        $id = $this->input->post('id');
        $this->user->jam_deleteDeptPostComment($id);
    }

    public function delConversation(){
        $id = $this->input->get('id');
        $this->user->delConversation($id);
    }

    }
?>