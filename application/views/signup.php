<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="content-type" content="text/html; charset=UTF-8">
        <meta charset="utf-8">
        <title>SCSIT SOCIAL SITE</title>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="css/styles.css" rel="stylesheet">
        <link rel="shortcut icon" href="img/logo.png">
    </head>
    <body style="background-image: url(img/Map.jpg);background-repeat: no-repeat;background-size: 100% 100%;height:100%;background-attachment: fixed;">
    <input type="hidden" id="pre_username" value="<?php echo $this->session->userdata('pre_username');?>"/>
        <!-- navbar -->
        <nav class="navbar navbar-default" role="navigation"style="background:#336699">
            <div class="navbar-header"> 
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
                    <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <img src="<?= base_url() . 'img/logo.png' ?>" class="img-responsive hover" alt="logo"style="height:30px;margin-left:10px;margin-top:7px;"/> </div>
            <div class="collapse navbar-collapse " id="example-navbar-collapse">
            </div> 
        </nav>
        <!-- end -->


        <!-- body -->
        <div class="container-fluid">
        <form action="<?php echo base_url().'upload/signUp_upload'?>" method="post" enctype="multipart/form-data">

            <div class="col-sm-3">
            </div>

            <div class="col-sm-6">

                <div class="well">
                    <label class="text-success"><?= ucwords(strtolower($this->session->userdata('pre_firstname').' '.$this->session->userdata('pre_middlename').' '.$this->session->userdata('pre_lastname')));?></label><br/>
                    <label class='text-warning'><?php echo ucfirst($this->session->flashdata('exist')); ?></label>
            
                    <label class="text-muted"><span class="glyphicon glyphicon-picture"></span> Please select your profile photo</label>
                   

                   
                   <!-- uploading file -->
                    <div class="form-group">
                        <input type="file" name="userfile" id="profilepic_id"/>
                    </div>
                    <label for="profilepic_id">
                        <img src="<?= base_url() . 'img/default_profile.jpg' ?>" style="height:200px; width: 200px;" class="img-responsive hover" id="profilepic_img"/>
                    </label>
                     <!-- end uploading file ---!>



                    <!-- for department selected and year -->
                    <ul class="list-group">
                        <li class="list-group-item">

                            <p class="label label-success">
                                Department Selected: Computer Department</p>

                            <div class="form-group"style="margin-top:5px;">
                                <label class="text-muted">Year:</label>
                                <label class="checkbox-inline">
                                    <input type="radio" name="year" id="optionsRadios3" value="1" checked /> 1 
                                </label> 
                                <label class="checkbox-inline">
                                    <input type="radio" name="year" id="optionsRadios4" value="2" /> 2 
                                </label> 
                                <label class="checkbox-inline">
                                    <input type="radio" name="year" id="optionsRadios5" value="3" /> 3
                                </label> 
                                <label class="checkbox-inline">
                                    <input type="radio" name="year" id="optionsRadios6" value="4" /> 4
                                </label> 
                                <label class="checkbox-inline">
                                    <input type="radio" name="year" id="optionsRadios7" value="5" /> 5
                                </label> 
                            </div>
                            <div class="form-group">
                                <input type="text" name="id_number" id="jamuserid_number"class="form-control" required placeholder="Enter your ID Number"/>
                            </div>
                            <div class="form-group">
                                <a href="#felcheckanswer" data-toggle="modal" class="btn btn-primary btn-xs pull-right" id="checkValid_id"><span class="glyphicon glyphicon-refresh"></span> Check ID Number</a>
                            </div>
                        </li>
                    </ul>
                    <!-- end -->
                    <div class="form-group">
                        <input type="text" name="address" class="form-control" placeholder="Current Address" required />
                    </div>
                    <div class="form-group">
                        <input type="number" name="mobilenumber" class="form-control" placeholder="Mobile Number" required />
                    </div>
                    
                    <div class="form-group">
                        <input type="email" name="email" class="form-control" placeholder="Email Address" required />
                    </div>

                    <label class="text-muted">Select Course:</label>
                <div class="form-group">
               
                     <select type="text" name="course_id" class="form-control" >
                        <?php foreach ($course as $row): ?>
                         <option value="<?= $row->id; ?>"><?php echo strtoupper($row->course); ?></option>
                        <?php endforeach; ?>
                    </select>
                </div>
                    <!-- robot -->
                    <ul class="list-group">
                        <li class="list-group-item">
                            <label class="label label-primary">Prove that your are not a robot!</label>
                            <!-- for build in question and answer -->
                            <br/><br/>
                            <p style="margin-top:5px;"><strong>Question:</strong> <?php echo $question['question'];?></p>
                            <small class="text-warning"><?php echo $this->session->flashdata('wrong');?></small>
                            <div class="form-group">
                                <input type="hidden" name="answer" value="<?php echo $question['answer'];?>"/>
                                <input type="text" name="user_answer" class="form-control"  required placeholder="Enter your valid answer here!">
                            </div>

                            <!-- accordion panel slide -->
                            <div class="panel-group"id="accordion">
                                <div class="form-group">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#felcheckanswerrobot"class="collapsed pull-right btn btn-primary btn-xs"style="margin-top:3px;"><span class="glyphicon glyphicon-refresh"></span> Check Answer</a>
                                </div>

                                <div id="felcheckanswerrobot" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="panel-body">
                                        <label class="text-success"><span class="glyphicon glyphicon-check"></span> Valid</label>
                                        <br>
                                        <label class="text-danger"><span class="glyphicon glyphicon-warning-sign"></span> Invalid</label>
                                    </div>
                                </div>


                            </div>
                            <!--/accordion panel slide -->
                            <!-- PLEASE READ
                                    
                                    Hi guys, check answer if you are not a robot if true the answers valid will print
                                    if false the Invalid answer will print.
                            
                            -->
                        </li>

                    </ul>
                    <br>
                    <input type="submit"class="btn btn-sm btn-primary"value="Submit">
                    
                </div>

            </div>

            <div class="col-sm-3">
            </div>
        </div>
        <!-- end -->
        <br>





