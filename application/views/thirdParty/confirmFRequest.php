  <?php if($sender_info != ''):?>
    <?php foreach($sender_info as $sender):?>       
       <div class="media">
      <a href="#"class="pull-left">
      <?php if($sender['sender_pic'] == ''):?>
        <img src="<?= base_url();?>img/default_profile.jpg"class="media-object"alt="user profile"style="height:30px;width:30px;">
      <?php else: ?>
        <img src="<?php echo base_url().'upload/user/'.$sender['sender_pic'];?>"class="media-object"alt="user profile"style="height:30px;width:30px;">
      <?php endif;?>
      </a>
      <div class="media-body">
      <h5 class="media-heading" ><span class="glyphicon glyphicon-flag"></span> <strong><?php echo ucwords($sender['sender_fname'].' '.$sender['sender_lastname']);?></strong>Wants to be your friend
      <button class="btn btn-primary btn-xs"style="margin-left:10px;" onclick="jam_cfRequest(<?php echo $sender['sender_id'];?>)" ><span class="glyphicon glyphicon-plus"></span> Confirm</button></h5>
        <h6 id="buttonConfirm<?php echo $sender['sender_id'];?>"></h6>
      <h6 class="text-warning list-inline"><span class="glyphicon glyphicon-heart"></span><?php echo ucfirst($sender['sender_gender']);?> <span class="glyphicon glyphicon-time"style="margin-left:5px;"></span><?php echo $sender['sender_age'];?> years old <span class="glyphicon glyphicon-map-marker"style="margin-left:5px">
        
      </span> <?php echo ucwords(strtolower($sender['department']));?></h6>

      
      </div>
      </div>
    <?php endforeach; ?>
  <?php else: ?>
      <h3>No Request</h3>
  <?php endif;?>