<?php 
	if($commentFullImge != ''){
		foreach($commentFullImge as $fullComment):
?>
<div id="view_full_comment_div<?php echo $fullComment['id'];?>">
<div class="media">
					<!-- SA MEDIA HEADER -->
					<a href="#" class="pull-left">
					 				
					<?php if($fullComment['profile_pic'] == ''): ?>
						<img src="<?= base_url();?>img/default_profile.jpg" class="media-object" style="height:35px; width:30px; margin-top:10px;">
					<?php else: ?>
						<img src="<?php echo base_url().'upload/user/'.$fullComment['profile_pic'];?>" class="media-object" style="height:35px; width:30px; margin-top:10px;">
					<?php endif; ?>				
									<!-- sa dropdown nga edit ug delete -->
								<?php if($fullComment['commenter'] == $this->session->userdata('login_id')): ?>	
									<div class=" dropdown pull-right">                        
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"  title="Click to manage"><i class="glyphicon glyphicon-cog"></i></a>
                                      <ul class="dropdown-menu">
                                      <li><a href=""><span class="text-success glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Comment?"> Update</span></a></li> 
                                     <li><a href="#"><span class="text-danger glyphicon glyphicon-trash" onclick="viewfullimagecommentDel(<?php echo $fullComment['id'];?>)" data-toggle="tooltip" title="Delete Comment?"> Delete</span></a></li>    
                                      </ul>
                                      </div>
                                <?php endif; ?>
                                      <!-- end sa dropdown nga edit ug delete -->
					</a>
					<!-- END HEADER -->
					<div id="jam_vfComments">
					<div class="media-body">
					<h5 style="word-break:keep-all;"><strong><?php echo ucwords(strtolower($fullComment['firstname'].' '.$fullComment['lastname']));?></strong></h5>
					<p style="font-size:10px;word-break:keep-all;margin-top:-5px;"><span class="glyphicon glyphicon-time"></span> <?php echo $fullComment['date'];?></p>
					<h6 class="text-muted" style="word-break:keep-all;">
						<?php echo nl2br($fullComment['comment']);?>
					</h6>
					</div>
					</div>
			</div>
			</div>
<?php 
		endforeach;
	}
?>
