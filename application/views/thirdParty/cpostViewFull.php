<div class="modal-body">
				<div class="panel-body">

					<div class="col-sm-7 col-md-7 col-lg-7">


					<div class="media">
						<a href="#" class="pull-left">
						<?php if($viewFullImage['profile_pic'] == ''):?>
							<img src="<?= base_url();?>img/default_profile.jpg" class="media-object" style="height:50px; width:50px; margin-top:10px;">
						<?php else: ?>
							<img src="<?php echo base_url().'upload/user/'.$viewFullImage['profile_pic'];?>" class="media-object" style="height:50px; width:50px; margin-top:10px;">
						<?php endif; ?>
						


						</a>
						<div class="media-body">
						<h5 style="word-break:keep-all;"><?php echo ucwords($viewFullImage['firstname'].' '.$viewFullImage['lastname']);?></h5>
						<h6 style="word-break:keep-all;"><span class="glyphicon glyphicon-time"></span> <?php echo $viewFullImage['date'];?></h6>
						</div>
					</div>

					<br>

						<img src="<?php echo base_url().'upload/user/'.$viewFullImage['image'];?>" class="img-responsive img-thumbnail" style="height:300px;width:100%;" id="jam_target_cpost_image">
						<h5>
						<a href="#felcount_user_like" onclick="jam_cpost_likers(<?php echo $viewFullImage['id'];?>)" data-toggle="modal" data-dismiss="modal"><span class="text-primary glyphicon glyphicon-thumbs-up" data-toggle="tooltip" title="like post?"></span> <?php echo $viewFullImage['likes'];?> Like</a>
						<a href="#fel_count_comment_name"  onclick="getAllCPostCommenters(<?php echo $viewFullImage['id'];?>)" data-toggle="modal" data-dismiss="modal" class="text-primary"><span class="text-success glyphicon glyphicon-comment" data-toggle="tooltip" title="Comment Are"></span>  <?php echo $viewFullImage['commenters'];?> Comment</a>
						<!-- sa dropdown nga edit ug delete -->
						<?php if($viewFullImage['cpost_user_id'] == $this->session->userdata('login_id')): ?>	
									<div class=" dropdown pull-right">                        
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"  title="Click to manage"><i class="glyphicon glyphicon-cog"></i></a>
                                      <ul class="dropdown-menu">
                                      <li>
                                       <a href="#edit_cpost"data-toggle="modal">
                                      <span class="glyphicon glyphicon-edit"></span> 
                                       <p class="text-success"style="margin-left:20px;margin-top:-20px;" onclick="editCPost(<?php echo $viewFullImage['id'];?>);">Edit</p></a>
                                      <a href="#" onclick="delPostFullImage(<?php echo $viewFullImage['id'];?>);"><span class="text-danger glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete Post?">Delete</span></a>
                                      </li>   
                                      </ul>
                                      </div>
                       <?php endif; ?>
                                      <!-- end sa dropdown nga edit ug delete -->
						</h5>
						<h6 style="word-break:keep-all; margin-top:5px;"><?php echo nl2br($viewFullImage['status']);?></h6>
					</div>

			<div class="col-sm-5 col-md-4 col-lg-5">
			<!-- media nga g.seleckan ug comment -->
			<div id="jam_vfComments">
			
			<?php 
				if($viewFullImageComment != ''):
					foreach($viewFullImageComment as $fullComment):
			?>
			<div id="view_full_comment_div<?php echo $fullComment['id'];?>">
			<div class="media">
					<!-- SA MEDIA HEADER -->
					<a href="#" class="pull-left">
					 				
					<?php if($fullComment['profile_pic'] == ''): ?>
						<img src="<?= base_url();?>img/default_profile.jpg" class="media-object" style="height:35px; width:30px; margin-top:10px;">
					<?php else: ?>
						<img src="<?php echo base_url().'upload/user/'.$fullComment['profile_pic'];?>" class="media-object" style="height:35px; width:30px; margin-top:10px;">
					<?php endif; ?>				
									<!-- sa dropdown nga edit ug delete -->
								<?php if($fullComment['commenter'] == $this->session->userdata('login_id')): ?>	
									<div class=" dropdown pull-right">                        
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown"  title="Click to manage"><i class="glyphicon glyphicon-cog"></i></a>
                                      <ul class="dropdown-menu">
                                      <li><a href="#editCpostComment" data-toggle="modal" onclick="editComment(<?php echo $fullComment['id'];?>)"><span class="text-success glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit Comment?"></span> Update</a></li> 
                                      <li><a href="#"><span class="text-danger glyphicon glyphicon-trash" onclick="viewfullimagecommentDel(<?php echo $fullComment['id'];?>)" data-toggle="tooltip" title="Delete Comment?"> Delete</span></a></li>   
                                      </ul>
                                      </div>
                                <?php endif; ?>
                                      <!-- end sa dropdown nga edit ug delete -->
					</a>
					<!-- END HEADER -->
					<div id="jam_vfComments">
					<div class="media-body">
					<h5 style="word-break:keep-all;"><strong><?php echo ucwords(strtolower($fullComment['firstname'].' '.$fullComment['lastname']));?></strong></h5>
					<p style="font-size:10px;word-break:keep-all;margin-top:-5px;"><span class="glyphicon glyphicon-time"></span> <?php echo $fullComment['date'];?></p>
					<h6 class="text-muted" style="word-break:keep-all;">
						<?php echo nl2br($fullComment['comment']);?>
					</h6>
					</div>
					</div>
			</div>
			</div>
			<?php 
				endforeach;

				endif; 
			?>
			</div>
			<!-- end sa select comment -->

			<!-- media sa butangan ug comment -->
				<div class="media">
						<!-- SA MEDIA HEADER -->
					<a href="#" class="pull-left">
					<?php if($sessionUser['profile_pic'] == ''): ?>
						<img src="<?= base_url();?>img/default_profile.jpg" class="media-object" style="height:35px; width:30px;">
					<?php else: ?>
						<img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>" class="media-object" style="height:35px; width:30px;">
					<?php endif; ?>
					</a>
					<!-- END HEADER -->
					<div class="media-body">
					<div class="col-sm-9">
					<input type="text" class="form-control" style="border-radius:0px;" id="jam_cpfullImage<?php echo $viewFullImage['id'];?>">
					</div>
					<div class="col-sm-3">
					<button type="submit" onclick="commentFullImge(<?php echo $viewFullImage['id'];?>)" class="btn btn-primary form-control" style="border-radius:0px;margin-top:2px;"><span class="glyphicon glyphicon-ok"></span></button>
					</div>
					</div>

				</div>
			<!-- end sa media nga butangan ug comment -->



			</div>
			</div>
			<div class="modal-footer"></div>
		</div>