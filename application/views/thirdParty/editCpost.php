<?php if($editCpost != ''): ?>
	<?php foreach($editCpost as $row):?>
		<div class="modal-header"><label><?php echo $row->date;?></label></div>
		<div class="modal-body">
		<div class="panel-body">
		<form action="<?php echo base_url().'upload/updateCampusPost'?>" method="post" enctype="multipart/form-data">
		<div class="col-sm-8 col-md-8 col-lg-8">
		<input type="file" name="userfile" id="jam_updateCPost_image">
		<input type="hidden" name="cpost_id" value="<?php echo $row->id; ?>">
		<?php if($row->image == ''):?>
			<label for="jam_updateCPost_image">
				<img src="<?=  base_url();?>img/default_profile.jpg" class="img-responsive img-thumbnail" style="height:300px;width:100%;" id="jam_target_cpost_image">
			</label>
		<?php else: ?>
			<label for="jam_updateCPost_image">
				<img src="<?php echo base_url().'upload/user/'.$row->image; ?>" class="img-responsive img-thumbnail" style="height:300px;width:100%;" id="jam_target_cpost_image">
			</label>
		<?php endif; ?>
		</div>
		<div class="col-sm-4 col-md-4 col-lg-4">
		<div class="panel panel-default" style="border-radius:0px;">
		<div class="panel-body">
		<textarea cols="5" rows="5" class="form-control" name="status_update" style="border-radius:0px;border:1px solid gray;"><?php echo nl2br($row->campus_post);?></textarea>

		</div>
		</div>
		<div class="panel panel-default" style="border-radius:0px;">
		<div class="panel-body">
		<center>
		<button class="btn btn-default" data-dismiss="modal" style="border-radius:1px;margin:auto;">Cancel</button>
		<button type="submit" class="btn btn-primary" style="border-radius:1px;margin:auto;"><span class="glyphicon glyphicon-ok"></span> Update</button>
		</center>
		</div>
		</div>
		</div>
		</div>
		</form>
		</div>
		<div class="modal-footer">
			
		</div>
	<?php endforeach;?>
<?php endif; ?>