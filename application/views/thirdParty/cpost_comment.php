<?php if($getAllCommentCPost != ''):?>
  <?php foreach($getAllCommentCPost as $cpostRow):?>
   <div id="cpost_comment_id<?php echo $cpostRow['cpost_comment_id'];?>">
         <div class="col-sm-1">
          </div>
                                        
        <ul class="media-list">
                <li class="media">
                <!-- for profile -->
              
                <div class="media-left">
                <?php if($cpostRow['cpost_commenter_profile'] == ''):?>
                 <img src="<?php echo base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                <?php elseif($cpostRow['cpost_commenter_profile'] != ''):?>
                   <img src="<?php echo base_url().'upload/user/'.$cpostRow['cpost_commenter_profile'];?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                <?php endif;?>
                </div>
                <!-- end for profile -->
                <!-- for body -->
                <div class="media-body">
                <h5 class="media-heading"><strong><?php echo ucwords($cpostRow['cpost_firstname'].' '.$cpostRow['cpost_lastname']);?></strong>

                <!-- for dropdown ni -->
                <?php if($cpostRow['cpost_commenter_id'] == $this->session->userdata('login_id')):?>
                  <li class=" dropdown pull-right">
                  <a href="#" class="dropdown-toggle"  title="Manage your comment" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#feleditcampusComment" data-toggle="modal" onclick="editComment(<?php echo $cpostRow['cpost_comment_id'];?>)">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#FeldeletecampusComment"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                      <p class="text-danger"style="margin-left:20px;margin-top:-20px;" onclick="jam_delComment(<?php echo $cpostRow['cpost_comment_id'];?>, <?php echo $cpostRow['cpost_id'];?>)">Delete</p></a>
                    </li>    
                  </ul>
                  </li>
              <?php endif; ?>
                <!-- end sa dropdown dre hehe --> 

                </h5>
                <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <?php echo $cpostRow['cpost_time'];?></h6>

                <p><em><?php echo nl2br($cpostRow['cPostComment']);?></em></p>
                <hr>

                </div>

                <!-- end for body -->

                </li>
         
          </ul>
      </div>
  <?php endforeach; ?>
    <!-- for View all comments -->
 <div class="panel-group" id="accordion">
  <ul class="list-inline">
  <li class="pull-right">
<a data-toggle="collapse" id="c_viewAllComments<?php echo $cpostRow['cpost_id'];?>" onclick="cpost_viewAllComments(<?php echo $cpostRow['cpost_id'];?>)" data-parent="#accordion" href="#collapseViewComment" class="collapsed " style="text-decoration:none">
      View Comment<span class="glyphicon glyphicon-comment"></span>
  </a>
   </li>
   <br>


      </ul>
</div>
<?php endif; ?>