
<?php include'header.php';?>
<div class="container-fluid">
<div class="row">


<div class="col-sm-8 col-md-8 col-lg-8">

<!-- design sa code -->
<div class="row">

<div class="col-sm-12 col-md-12 col-lg-12">
<div class="panel-body" style="margin-top: 40px;">
<div class="col-sm-4 col-md-4 col-lg-4">
<img src="<?php echo base_url();?>img/index_like.png" class="img-responsive felmeraldRotationimagelike" style="margin-top: -100px; padding: 10px 14px;">
</div>
<div class="col-sm-8 col-md-8 col-lg-8">
<h3 class="text-primary" style="word-break: keep-all;font-family: Facebook Letter Faces">
Stay tuned for updates about what's going on and news about school events.
</h3>
</div>
</div><!-- /panel body -->

<div id="test">
<div id="flip" style="margin-top: 40px;border:0px;">

<h5 >Announcement: 
<span class="badge"  ><?php echo $this->db->count_all('admin_announcement');?></span>
</h5>


<a href="#" class="pull-right text-muted btn btn-default" style="margin-top: -30px;">
	<span class="glyphicon glyphicon-open"></span> View
</a>
</div>
</div>
<div id="panel" style="border:0px;">
<?php if($announcement_event->num_rows() != 0){ 

	foreach($announcement_event->result() as $data):
		echo '<h5 style="word-break:keep-all;">'.nl2br(ucfirst($data->announcement.'<br><i class="pull-right text-danger" style="font-size:10px;"><strong>Date published: </strong>'.$data->date.'</i>')).'</h5><hr/>';
 	endforeach;

 }else{
 	echo "<h3 class='text-muted'style='font-weight:bold;'>No announcement published</h3>";
 }
 ?>	
</div><!-- end sa id=panel -->

<!-- 
para sa panel nga naay news
@param
@query flow
#news,#sports,#entertainment,#editorial
every news category will display news only one
query flow example
SELECT * FROM news_category ORDER BY id DESC LIMIT 1

for design sa cap-bot naa sa http://localhost/scsit_social_site/css/chrisraldforever.css
design on hover by css 3
 -->
<div class="panel panel-default" style="margin-top: 10px;">
<div class="panel-heading">
	<ol class="breadcrumb">
	    <li class="text-primary">NEWS PAGES</li>
		<li><a href="<?php echo base_url(); ?>adver_news"><span class="badge"><?= $this->db->count_all('news');?></span> News</a></li>
		<li><a href="<?php echo base_url(); ?>adver_sports"><span class="badge"><?= $this->db->count_all('sports');?></span> Sports</a></li>
		<li><a href="<?php echo base_url(); ?>adver_editorial"><span class="badge"><?= $this->db->count_all('editorial_new');?></span> Editorial</a></li>
		<li><a href="<?php echo base_url(); ?>adver_entertainment"><span class="badge"><?= $this->db->count_all('entertainment_news');?></span> Entertainment</a></li>
	</ol>
	<h4>Latest news</h4>
</div>
<div class="panel-body">

<div class="row">
<!-- for new page -->
<div class="col-sm-3 col-md-3 col-lg-3">


	<?php if($news_limit->num_rows() !=0){?>

		<?php foreach($news_limit->result() as $select_department):?>

			<?php if($select_department->picture !="no image"){ ?>
<figure class="cap-bot">
<img src="<?= base_url();?>upload/user/<?= $select_department->picture; ?>" class="img-responsive" style="height:150px;width: 150px;">
<figcaption style="word-break:keep-all;">

<h6>"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#newsRead<?= $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>

</figcaption>
</figure>
<?php }else{ ?>
<figure class="cap-bot">
<img src="<?= base_url();?>img/adver/news.png" class="img-responsive">
<figcaption style="word-break:keep-all;">

<h6 style="word-break:keep-all;">"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#newsRead<?php echo $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>

</figcaption>
</figure>
<?php } ?>

		<?php endforeach; ?>

	<?php }else{?>

	
		<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h6 class="text-danger" style="word-break: keep-all;"><span class="glyphicon glyphicon-pushpin"></span> No news published</h6>
			</div>
		</div>
	

	<?php } ?>


</div>
<!-- end sa news page -->
<!-- sports property -->
<div class="col-sm-3 col-md-3 col-lg-3">

<?php if($sport_limit->num_rows() !=0){?>

	<?php foreach($sport_limit->result() as $select_department): ?>


		<?php if($select_department->picture !="no image"){ ?>
<figure class="cap-bot">
<img src="<?= base_url();?>upload/user/<?= $select_department->picture; ?>" class="img-responsive" style="height:150px;width: 150px">
<figcaption style="word-break:keep-all;">

<h6 style="word-break:keep-all;">"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#sportsRead<?php echo $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>


</figcaption>
</figure>
<?php }else{ ?>

<figure class="cap-bot">
<img src="<?= base_url();?>img/adver/sport.png" class="img-responsive">
<figcaption style="word-break:keep-all;">

<h6>"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#sportsRead<?php echo $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>

</figcaption>
</figure>
<?php } ?>


	<?php endforeach; ?>

<?php }else{ ?>

	<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h6 class="text-danger" style="word-break: keep-all;"><span class="glyphicon glyphicon-pushpin"></span> No sports news published</h6>
			</div>
		</div>

<?php } ?>



</div>
<!-- end of sports property -->
<!-- editorial property -->
<div class="col-sm-3 col-md-3 col-lg-3">


<?php if($editorial_limit->num_rows() !=0){?>

	<?php foreach($editorial_limit->result() as $select_department ):?>
		<?php if($select_department->picture !="no image"){ ?>
<figure class="cap-bot">
<img src="<?= base_url();?>upload/user/<?= $select_department->picture; ?>" class="img-responsive" style="height:150px;width: 150px">
<figcaption style="word-break:keep-all;">

<h6 style="word-break:keep-all;">"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#editorialRead<?php echo $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>


</figcaption>
</figure>
<?php }else{ ?>

<figure class="cap-bot">
<img src="<?= base_url();?>img/adver/editorial.png" class="img-responsive">
<figcaption style="word-break:keep-all;">

<h6>"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#editorialRead<?php echo $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>



</figcaption>
</figure>
<?php } ?>
	<?php endforeach; ?>

<?php }else{ ?>

	<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h6 class="text-danger" style="word-break: keep-all;"><span class="glyphicon glyphicon-pushpin"></span> No Editorial news published</h6>
			</div>
		</div>

<?php } ?>
</div>
<!-- end of editorial property -->
<!-- entertainment news Property -->
<div class="col-sm-3 col-md-3 col-lg-3">

<?php if($entertainment_limit->num_rows() !=0){ ?>
	
	<?php foreach($entertainment_limit->result() as $select_department):?>

		<?php if($select_department->picture !="no image"){ ?>
<figure class="cap-bot">
<img src="<?= base_url();?>upload/user/<?= $select_department->picture; ?>" class="img-responsive" style="height:150px;width: 150px">
<figcaption style="word-break:keep-all;">

<h6 style="word-break:keep-all;">"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#enter<?= $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>


</figcaption>
</figure>
<?php }else{ ?>

<figure class="cap-bot">
<img src="<?= base_url();?>img/adver/entertainment.png" class="img-responsive">
<figcaption style="word-break:keep-all;">

<h6>"<?= ucwords(ucfirst(strtolower($select_department->title)));?>"</h6>
<h6><span class="glyphicon glyphicon-time"></span> <?= $select_department->date;?></h6>
<center><a href="#enter<?= $select_department->id;?>" data-toggle="modal" class="btn btn-primary"><span class="glyphicon glyphicon-paperclip"></span> Read</a></center>



</figcaption>
</figure>
<?php } ?>


	<?php endforeach; ?>

<?php }else{ ?>
<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h6 class="text-danger" style="word-break: keep-all;"><span class="glyphicon glyphicon-pushpin"></span> No Entertainment news published</h6>
			</div>
		</div>
<?php } ?>



</div>
<!-- entertainment news end -->

</div><!-- /end row -->


</div>
<div class="panel-footer">
</div>
</div>


</div><!-- ens sa col-12 -->



</div>
<!-- /end designing  -->


</div><!-- end sa design -->




<div class="col-sm-4 col-md-4 col-lg-4">
<h3 class="text-primary" style="font-family: Facebook Letter Faces">SIGN UP</h3>

<!-- start ug form dre -->

  <form action="<?= base_url() . 'user_controller/preSignUp' ?>" method="post">
        
                <h5 class="text-danger">
                    <?php echo $this->session->flashdata('loginError');?>
                    <?php echo $this->session->flashdata('alreadyExistUser');?>
                </h5>
                
                <small class="text-danger"><?= form_error('department_id'); ?></small>
                <label class="text-muted">Select Department:</label>
                <div class="form-group">
                     <select type="text" name="department_id" class="form-control" required="">
                     <?php if($dept_data != ''):?>
                        <?php foreach ($dept_data as $select_department): ?>
                            <option value="<?= $select_department->id ?>"><?= $select_department->department ?></option>
                        <?php endforeach; ?>
                      <?php endif; ?>
                    </select>
                </div>
                <div class="form-group">
                    <small class="text-danger"><?= form_error('firstname'); ?></small>
                    <input type="text" name="firstname"class=" form-control"required="" placeholder="Firstname"/>
                </div>
                 <div class="form-group">
                    <small class="text-danger"><?= form_error('middlename'); ?></small>
                    <input type="text" name="middlename"class=" form-control"required="" placeholder="Middlename"/>
                </div>
                <div class="form-group">
                    <small class="text-danger"><?= form_error('lastname'); ?></small>
                    <input type="text" name="lastname" class="form-control" required="" placeholder="Lastname"/>
                </div>
                <div class="form-group">
                    <label class="text-muted">Birthdate:</label>
                    <input type="date" name="bdate" class="form-control" required="" placeholder="Birthdate" id="datepicker"/>
                </div>
                <div class="form-group">
                    <input type="number" name="age" class="form-control" required="" placeholder="Age"/>
                </div>
                <div class="form-group">
                    <label class="text-muted">Gender:</label>
                    <label class="checkbox-inline">
                        <input type="radio" name="gender" id="optionsRadios3" value="male" checked required=""/> Male </label> 
                    <label class="checkbox-inline">
                        <input type="radio" name="gender" id="optionsRadios4" value="female" required=""/> Female </label>
                </div>
                <div class="form-group">
                    <small class="text-danger">
						<?php echo form_error('username');?>
						<?php echo $this->session->flashdata('usernameExist');?>
					</small>
                    <input type="text" name="username" class="form-control" required="" placeholder="Username"/>
                </div>
                <div class="form-group">
                    <small class="text-danger"><?= form_error('password'); ?></small>
                    <input type="password" name="password" id="password" class="form-control"required=""placeholder="Password"/>
                </div>
                <div class="form-group">
                    <small class="text-danger"><?= form_error('cpassword'); ?></small>
                    <input type="password" name="cpassword" id="cpassword" class="form-control" required="" placeholder="Confirm password"/>
                </div>
                <small id="passwordVal" class="text-danger" style="font-weight:bold;">
                    
                </small>
                <br/>
                <input type="submit" class="btn btn-primary btn-sm pull-right" id="submit" value="Submit"/>
                <br>




  


    </form>


<!-- end sa form dre -->


</div><!-- end sa col-sm-4 -->








</div>
</div>
<div class="container">
<center>
<?php foreach($felmeraldCopy as $select_department): ?>
<h5><span class="glyphicon glyphicon-phone-alt"></span> <?= $select_department->contact;?>
 |	<span class="glyphicon glyphicon-map-marker"></span> <?= $select_department->locator;?>
 |	<span class="glyphicon glyphicon-envelope"></span> <?= $select_department->information;?>
</h5> 
<?php endforeach; ?>
<h6 class="text-muted">
	&copy 2015 - <?= date("Y")?> Salazar Colleges of Science and Institute of Technology | Social Site
</h6>
<h6 class="text-muted">
	211. N. Bacalso., Avenue Cebu City
</h6>
</center>
</div>

<?php include'advertise/adver_modal/news_advertisement_modals.php';?>
<?php include'advertise/adver_modal/enterZoom.php';?>

<?php include'footer.php';?>


