                    <div class="navbar navbar-blue navbar-static-top">  
                    <div class="navbar-header">
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
          				     <span class="icon-bar"></span>
          				      <span class="icon-bar"></span>
                      </button>
                      <img src="<?= base_url();?>img/logo.png" class="img-responsive hover" alt="logo" style="height:30px;margin-left:10px;margin-top:7px;">
                  	</div>
                  	<nav class="collapse navbar-collapse" role="navigation">
                    
                    <ul class="nav navbar-nav">
                      <li class="active">
                        <a href="<?= base_url();?>administrator" class="active"><i class="glyphicon glyphicon-home"></i> Home</a>
                      </li>
                      
                      <li>
                      <a href="<?= base_url();?>department_selected"><span class="glyphicon glyphicon-map-marker"></span>
                      <?php 
                      foreach($navTop as $data):
                      $dept_id = $data->dept_id;
                      ?>
                      <?= ucwords(strtolower($data->department)); ?> Department
                      <?php endforeach; ?>
                       </a>
                      </li>
                      <li>
                        <a href="<?= base_url();?>department_message" role="button"><i class="glyphicon glyphicon-envelope"></i> Message</a>
                      </li>
                      <li>
                      <a href="<?=  base_url();?>department_announcement" role="button">
                      <span class="glyphicon glyphicon-pencil"></span>
                      Make an announcement
                      </a>
                      </li>
                                            
                    </ul>


                    <ul class="nav navbar-nav navbar-right"style="margin-right:20px;">
                      <li class="dropdown ">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                        <ul class="dropdown-menu bodycolor">
               <?php foreach($navTopDrop as $data):?>
                      <?php if($data->image !="no image"){?>
                <center>
                  <a href="#view_profile<?= $data->id;?>"data-toggle="modal">
                      <img src="<?= base_url();?>upload/user/<?php echo $data->image;?>"class="hover img-responsive img-circle img-thumbnail popup"  data-toggle="tooltip" title="Tap to View Profile">
                  </a> 
                </center> 
                      <?php }else{ ?>
                  <center>
                  <a href="#view_profile<?= $data->id;?>"data-toggle="modal">
                      <img src="<?= base_url();?>img/default_profile.jpg"class="hover img-responsive img-circle img-thumbnail popup"  data-toggle="tooltip" title="Tap to View Profile">
                  </a>
                </center>
                      <?php }?>

           
                <center>
                  <h5 class="text-primary">What's Up 
                    <strong>
                      <?= ucfirst(strtolower($data->fname)).' '.ucfirst(strtolower($data->lname)); ?> 
                    </strong>
                  </h5>
                </center>
                <?php endforeach; ?>

                          <hr>
                          <li><a href="<?= base_url();?>logout"style="color:black"><span class="glyphicon glyphicon-off"></span> Logout</a>
                              <a href="<?= base_url();?>change_password" style="color:black"><span class="glyphicon glyphicon-lock"></span> Change Password</a>
                          </li>
            
                        </ul>
                      </li>
                    </ul>
                  	</nav>
                </div>
