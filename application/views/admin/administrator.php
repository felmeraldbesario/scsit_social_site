
<?php include 'admin_session.php'; ?>

<body>
    <div class="wrapper">
        <div class="box">
            <div class="row row-offcanvas row-offcanvas-left">


                <?php include'admin_sidebar.php'; ?>
                <!-- /end sa sidebar -->

                <!-- main right col -->
                <div class="column col-sm-10 col-xs-11" id="main">

                    <!-- top nav -->
                    <?php include'admin_nav_menu_top.php'; ?>

                    <!-- /top nav -->

                    <div class="padding">

                        <div class="full col-sm-9">


                            <!-- content -->                      
                            <div class="row">



                                <div class="col-md-9">
                                    <?php include 'alerts/login_welcome_alert.php'; ?>

                                    <div class="panel panel-default panel-shadow">
                                        <div class="panel-body">
                                            <div class="folded">
                                                <h2 style="background-color: #337ab7;">Log's Submitted</h2>
                                            </div>
                                            <hr>
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped">
                                                    <thead>
                                                        <tr>
                                                            <th>News Published</th><th>Sports Published</th><th>Entertainment Published</th><th>Editorial Published</th><th>Announcement Published</th><th>Course Added</th><th>Department Post</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td><?php $this->db->where('admin_id', $user_id);
                                    echo sprintf('%04.0d', $this->db->count_all_results('news'));
                                    ?></td>
                                                            <td><?php $this->db->where('admin_id', $user_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('sports'));
                                    ?></td>
                                                            <td><?php $this->db->where('admin_id', $user_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('entertainment_news'));
                                    ?></td>
                                                            <td><?php $this->db->where('admin_id', $user_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('editorial_new'));
                                    ?></td>
                                                            <td><?php $this->db->where('admin_id', $user_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('admin_announcement'));
                                    ?></td>
                                                            <td><?php $this->db->where('department_id', $dept_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('course'));
                                    ?></td>
                                                            <td><?php $this->db->where('user_id', $user_id);
                                                                echo sprintf('%04.0d', $this->db->count_all_results('department_topic'));
                                    ?></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>


                                            <?= $this->session->flashdata('BlockSuccess'); ?>
                                    <!-- Advanced Tables dre-->
                                    <div class="panel panel-default panel-shadow">
                                        <div class="panel-heading"style="background:#cccccc;">

                                            <?php
                                            
                                            foreach ($departmentJoined as $data):
                                                ?>
                                                <div class="folded">
                                                    <h2 style="background-color: #337ab7;"><?= strtoupper($data->department); ?> DEPARTMENT</h2>
                                                </div>

                                            <?php endforeach; ?>


                                        </div>
                                        <div class="panel-body">
                                            <div class="table-responsive">
                                                <table class="table table-hover table-bordered table-striped" id="dataTables-example">
                                                    <thead>
                                                        <tr>
                                                            <th>ID Number</th>
                                                            <th>Fullname</th>
                                                            <th>Department</th>
                                                            <th>Course</th>
                                                            <th>Year</th>
                                                            <th>Block</th>
                                                            <th>Location</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <?php
                                                        $this->db->where('department_id', $dept_id);
                                                        $queryDepartmentList = $this->db->get('user');
                                                        foreach ($queryDepartmentList->result() as $rowDeptList) {

                                                            $this->db->where('id', $dept_id);
                                                            $queryDeptname = $this->db->get('department');
                                                            foreach ($queryDeptname->result() as $rowDeptname) {
                                                                ?>

                                                                <tr>
                                                                    <td><?= sprintf('%08d', $rowDeptList->id_number); ?></td>
                                                                    <td><?= ucwords($rowDeptList->firstname . ' ' . $rowDeptList->lastname); ?></td>
                                                                    <td><?= $rowDeptname->department; ?></td>
                                                                    <td><?= ucfirst($rowDeptList->course); ?></td>
                                                                    <td><?= $rowDeptList->year; ?></td>
                                                                    <td>
        <?php
        $this->db->where('status', 1);
        $this->db->where('blocked_id', $rowDeptList->id);
        $block_listQuery = $this->db->get('admin_block_list');
        if ($block_listQuery->num_rows != 0) {
            ?>
                                                                            <span class="glyphicon glyphicon-ban-circle disabled" data-toggle="tooltip" title="BLOCKED"></span>

        <?php } else { ?>

                                                                            <a href="#fel_block_user_modal<?= $rowDeptList->id; ?>" data-toggle="modal" class="text-danger"><span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="Block this user"></span></a>
        <?php } ?>
                                                                    </td>
                                                                    <td>



                                                                        <a href="#fel_user_location<?= $rowDeptList->id; ?>" data-toggle="modal" class="text-success"><span class="glyphicon glyphicon-map-marker" data-toggle="tooltip" title="User's Location"></span></a>
                                                                    </td>
                                                                </tr>
    <?php }
} ?>


                                                    </tbody>
                                                </table>
                                            </div>

                                        </div>
                                    </div>
                                    <!--End Advanced Tables hehehe -->
                                </div>

                                <div class="col-md-3">

                                    <div class="panel panel-danger panel-shadow">

                                        <div class="panel-body">

                                            <div class="folded">
                                                <h2 style="background-color:#AF0000">Blocked Users</h2>
                                            </div>
                                            <h1 class="text-center text-danger">


<?php
$this->db->where('admin_id', $user_id);
echo $this->db->count_all_results('admin_block_list');
?>

                                            </h1>

                                        </div>
                                        <div class="panel-footer"style="background-color:#AF0000">
                                            <center>
                                                <a href="<?= base_url(); ?>unblock_user_blocked_dept" class="button button-red"><span class="glyphicon glyphicon-folder-open"></span> View & Print Blocked User</a>
                                            </center>
                                        </div>
                                    </div>



                                </div>


                            </div><!--/row-->

                            <!-- footer dre hahaha -->
                            <hr>

                            <?php include'copyright.php';?>
                            <br>
<?php include 'admin_locator.php'; ?>

                        </div>


                        <!-- end sa footer -->



                    </div><!-- /col-9 -->

                </div><!-- /padding -->

            </div>
            <!-- /main -->


        </div>

    </div>





<?php $queryPrint = $this->db->get('user');
foreach ($queryPrint->result() as $rowDeptList): ?>

        <div id="fel_user_location<?= $rowDeptList->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">

                    <div class="modal-header">

                        <img src="<?= base_url(); ?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
                        <img src="<?= base_url(); ?>img/logo.png" class="img-responsive logo-height">
                        <h5 class="header_margin"><strong>Salazar Colleges of Science and Institute of Technology</strong></h5>
                        <h6 class="scsit_margin"><strong>Social Site Department User Record</strong></h6>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="panel-body">
                                <p style="word-break: keep-all;"><span class="text-primary glyphicon glyphicon-map-marker"></span> <?= ucwords($rowDeptList->address); ?></p>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <p class="text-right" style="word-break: keep-all;">&copy <?= $title; ?> | User Address</p>
                    </div>

                </div>
            </div>
        </div>
<?php endforeach; ?>


<?php include'adminModal/block_modal.php'; ?>
