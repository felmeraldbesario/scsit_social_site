<?php include 'admin_session.php';?>


<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
              
                <!-- /top nav -->
              
                <div class="padding">

                    <div class="full col-sm-9">

                      
                        <!-- content -->                      
                        <div class="row">
                        
                         
                          
 <div class="col-sm-2 col-md-2 col-lg-2"></div>
 <div class="col-sm-8 col-md-8 col-lg-8">
 <div class="panel panel-default panel-shadow">
 <div class="panel-body">
 <div class="folded">
 <h2 style="background-color:#337ab7;">Change Password</h2>
 </div>
<hr>
<?= $this->session->flashdata('incorrect');?>
<?= $this->session->flashdata('notification');?>



<?php if($passwordchange->num_rows() !=0){ ?>
    <?php foreach($passwordchange->result() as $passRows):?>

<!-- query -->
<h5 class="text-muted">
<span class="glyphicon glyphicon-lock"></span> Last password changed on: 
            <small class="text-warning">
            
            <?php if($passRows->date_pass_change !=null){ ?>
                        <?= $passRows->date_pass_change;?>
                    <?php }else{ ?>
                        You haven't changed your password once.!
            <?php } ?>
            </small>
</h5>
<!-- end query -->
    <?php endforeach;?>
<?php } ?>


<?= form_open(base_url().'index.php/change_password');?>
<div class="form-group">
<input type="password" name="password" class="form-control input-lg" placeholder="Current Password" style="border-radius: 0px;">
<small class="text-danger"><?= form_error("password");?></small>
</div>
<div class="form-group">
<input type="password" name="new_pw" class="form-control input-lg" placeholder="New Password" style="border-radius: 0px;">
<small class="text-danger"><?= form_error("new_pw");?></small>
</div>
<div class="form-group">
<input type="password" name="conf_pw" class="form-control input-lg" placeholder="Confirm Password" style="border-radius: 0px;">
<small class="text-danger"><?= form_error("conf_pw");?></small>
</div>

<div class="form-group">
<input type="submit" name="submit" value="Update Password" class="btn btn-primary pull-right">
</div>
<?= form_close();?>

 </div>
 </div>
 </div>


      


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>

<p>&copy 2015 All Rights Reserved @ SCSIT Social Site | Salazar Colleges of Science and Institute of Technology</p>
<br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>

<script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>


