<?php include 'admin_session.php';?>

<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
              
                <!-- /top nav -->
              
                <div class="padding">

                    <div class="full col-sm-9">

                      
                        <!-- content -->                      
                      	<div class="row">
                        
                         
                          
 <div class="col-md-9">
<?= $this->session->flashdata('unblockUserBlocked'); ?>

<div class="well panel-shadow">
<div class="folded">
<h2 style="background-color: #337ab7;">Unblock Blocked User's</h2>
</div>
<div class="table-responsive res">
<table class="table table-hover table-bordered" id="dataTables-example">

<thead>
  <tr>
    <th>Full Name</th>
    <th>Address</th>
    <th>Age</th>
    <th>Gender</th>
    <th>Status</th>
    <th>Blocked Reason</th>
    <th>Date Blocked</th>
    <th>Unblock & Print</th>
  </tr>
</thead>
<tbody>

<?php foreach($joinedDisplayblockedUser as $dataUnblock){?>
  <tr>
    
    <td><?= ucwords(strtolower($dataUnblock->firstname.' '.$dataUnblock->middlename.' '.$dataUnblock->lastname))?></td>
    <td><?= ucwords(strtolower($dataUnblock->address));?></td>
    <td><?= ucwords(strtolower($dataUnblock->age));?></td>
    <td><?= ucwords(strtolower($dataUnblock->member_sex));?></td>
    <td>Blocked</td>
    <td><?= ucwords(strtolower($dataUnblock->reason));?></td>
    <td><?= $dataUnblock->blocked_date; ?></td>
    <td><a href="#confirm_unblock<?php echo $dataUnblock->member_id;?>" data-toggle="modal" class="btn btn-danger btn-xs"><span class="glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="Do you want to Unblock this User?"></span></a>
      <a href="admin_printuser?id=<?= $dataUnblock->member_id;?>"class="text-primary" data-toggle="modal"><span class="glyphicon glyphicon-print" data-toggle="tooltip" title="Print blocked user record"></span></a>
    </td>
    
  </tr>
<?php } ?>
</tbody>
  
</table>

</div>
</div>
                   
  </div>

  		<div class="col-md-3">

      <div class="panel panel-danger panel-shadow">
      
      <div class="panel-body">
        
        <div class="folded">
          <h2 style="background-color:#AF0000">Blocked Users</h2>
        </div>
        <h1 class="text-center text-danger"><?php $this->db->where('admin_id',$user_id); echo $this->db->count_all_results('admin_block_list'); ?></h1>
        
      </div>
     
      </div>



  		</div>


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>

<p>&copy 2015 All Rights Reserved @ SCSIT Social Site | Salazar Colleges of Science and Institute of Technology</p>
<br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>



<?php include'adminModal/unblock_modal.php';?>