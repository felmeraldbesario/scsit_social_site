

<?php 

header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
header("Pragma: no-cache");
$user_id = $this->session->userdata('login_id');
$username = $this->session->userdata('login_username');
$password = $this->session->userdata('login_password');
$sessionData = array(
	'id'=>$user_id,
	'username'=>$username,
	'password'=>$password
);

$checkUserSession =$this->db->get_where('admin_personel',$sessionData);
if($checkUserSession->num_rows() == 0){

	$checkAdmin = $this->db->get_where('user',$sessionData);
	if($checkAdmin->num_rows() != 0){
		 redirect(base_url().'home');
		 
	}else{
		
		 $data = array(
            'login_id' => '',
            'login_username' => '',
            'login_password' => '',
            'login' => ''
        );
        $this->session->unset_userdata($data);
        // $this->session->sess_destroy();
        
	}
}else{
	redirect(base_url().'administrator');
}

?>

<div class="container-fluid">
<div class="marginTop"></div>

<div class="col-sm-3">
</div>
<div class="col-sm-6">

<div class="well panel-shadow">



<blockquote style="border-left:6px solid #286090;background:#fcf8e3;">
<img src="<?=  base_url();?>img/admin_sidebar/secur.png" class="login_security img-responsive">
<h4 class="admin_login_security" style="margin-top:-35px; word-break: break-all;"><strong>Administrator Login</strong></h4>
</blockquote>
<?= $this->session->flashdata('notification');?>



<!-- login panel -->
<div class="inner-addon left-addon marginlabeltop">
<?php echo form_open(base_url().'login');?>

<small class="text-danger"><?=form_error('username');?></small>
<p>
<i class="glyphicon glyphicon-user"></i>
<input type="text"name="username"class="form-control input-lg border"placeholder=" Username" >
<span style="display:none;color:red;">Valid username can access only</span>
</p>
</div>
<div class="inner-addon left-addon">
<small class="text-danger"><?=form_error('password');?></small>
<p>
<i class="glyphicon glyphicon-lock"></i>
<input type="password"name="password"class="form-control input-lg border"placeholder=" Password" >
<span style="display:none;color:red;">Valid password can access only</span> 
<!-- 
#kaning small ky butangan sa counter nga mu identity sa user kung pila ilang character nga g. type
#javascript location rald_js/count_character.js
 -->
</p>
</div>

                     


<!-- =======================Error Login========================= -->
                     <h6 class="text-danger">
                     <?php echo $this->session->userdata('loginError');?>
                     </h6>
                     <?php $this->session->unset_userdata(array('loginError'=>''));?>
<!-- =======================Error Login========================= -->





<div class="form-group">
<button type="submit" class="btn btn-primary">
login </button>
</div>
<div class="form-group">
<a href="<?= base_url();?>admin_signup" class="button button-gray button-full">
<span class="glyphicon glyphicon-registration-mark pull-left" style="margin-left:15px;"></span>
<strong class="text-muted pull-left word-break" style="margin-left:10px;font-size:12px;">Register as Administrator to your Department</strong> 
</a>
</div>


<!-- end -->
<?php echo form_close();?>

</div>
</div>
<div class="col-sm-3">
</div>



</div>

<!-- end -->
<br><br><br>

<br><br>
<br><br>
<div class="container">
	<center>
		<h5 class="text-muted"> &copy 2015 - <?= date("Y")?> <?= $copyni; ?></h5>
	</center>
</div>


<script src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
<?php  include 'javascript/tooltip_popover.php';?>
<?php include 'javascript/login_focusin.php';?>

</body>
</html>