<!-- ////////////////////////////////////sidebar -->
            <!-- sidebar -->
            <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar"style="background:#cccccc;">
              
              <!-- kani sa toggle sidebar okay -->
                <ul class="nav">
                <a href="#" data-toggle="offcanvas" class="visible-xs text-center">
                <span style="background:grey"class="glyphicon glyphicon-chevron-right"></span></a>
              </ul>
              <!-- end for toggle side bar -->

              	<!-- profile pics here -->


                
                <?php foreach($adminInfo as $data){?>
                
                <?php if($data->personel_image !="no image"){?>
                
                <a href="#updateProfile<?=$data->personel_id?>"data-toggle="modal">
                <img src="<?= base_url();?>upload/user/<?php echo $data->personel_image;?>"class="img-responsive hover profilestyle" data-toggle="tooltip" title="Tap to Change" data-placement="right">
                </a>  
             
                <?php }else{ ?>
                <a href="#updateProfile<?=$data->personel_id?>"data-toggle="modal">
                <img src="<?= base_url();?>img/default_profile.jpg"class="img-responsive hover profilestyle" data-toggle="tooltip" title="Tap to Change" data-placement="right">
                </a>
                <?php } ?>

             
                <!-- end of profile pics -->

                <?php include 'alerts/profile_update_alert.php'; ?>
                <!-- list sa user details here, name, idnumber, location, department -->
                <br>
             

                <ul class="nav hidden-xs" id="lg-menu">
            
                   <img src="<?= base_url();?>img/admin_sidebar/admin.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Fullname"> <h6 class="h6sidebarmargin">
                   <?php 
                   $completename = character_limiter(ucwords(strtolower($data->lname.', '.$data->fname.' '.$data->Mname.'.')),30);
                   echo $completename;
                   ?>
                   </h6>

                   <img src="<?= base_url();?>img/admin_sidebar/username.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Username"> <h6 class="h6sidebarmargin"><?php echo $data->username; ?></h6>

                    <img src="<?= base_url();?>img/admin_sidebar/id.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="ID #"> <h6 class="h6sidebarmargin"><?php echo sprintf('%08.0d',$data->admin_id_number);?></h6>

                    <img src="<?= base_url();?>img/admin_sidebar/email.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Email Address"> <h6 class="h6sidebarmargin">
                    <?php 
                    $email = character_limiter($data->email,30);
                    echo $email;
                    ?>
                   
                    </h6>

                    <img src="<?= base_url();?>img/circle_icon/location.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Department"> <h6 class="h6sidebarmargin"><?php echo ucfirst(strtolower($data->department)); ?></h6>
                      
                      <?php } ?>
<!-- =========end sa foreach dre para sa admin personel info============= -->



                    <hr>
      
                    <img src="<?=  base_url();?>img/admin_sidebar/clocks.gif" class="time_set_value img-responsive img-circle" data-toggle="tooltip" title="Philippine Time">  <h6 class="h6sidebarmargin"> CLOCK</h6>
                    <!-- 
                      #javascript running time
                      #path/location
                      #administrator.js
                     -->
                    <span id="date_time" class="h6sidebarmarginone"></span>
                    <script type="text/javascript">window.onload = date_time('date_time');</script>
                </ul>

                <hr>
                <ul class="nav hidden-xs" id="xs-menu">
                <p class="Pcolor">News Page</p>
                    <img src="<?= base_url();?>img/admin/news.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Manage News?"> <h6 class="h6sidebarmargin"><a href="<?= base_url(); ?>admin_news" style="color:black">News</a></h6>
                    <img src="<?= base_url();?>img/admin/sports.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Manage Sports?"> <h6 class="h6sidebarmargin"><a href="<?= base_url(); ?>admin_sports" style="color:black">Sports</a></h6>
                    <img src="<?= base_url();?>img/admin/entertainment.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Manage Entertainment?"> <h6 class="h6sidebarmargin"><a href="<?= base_url(); ?>admin_entertainment" style="color:black">Entertainment</a></h6>
                    <img src="<?= base_url();?>img/admin/editorial.png"class="img-responsive hover newsheight" data-toggle="tooltip" title="Manage Editorial?"> <h6 class="h6sidebarmargin"><a href="<?= base_url(); ?>admin_editorial" style="color:black">Editorial</a></h6>

                </ul>
                

              
            </div>
            <!-- ////////////////////////////////////sidebar -->

            