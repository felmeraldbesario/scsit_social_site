<?php include 'admin_session.php';?>
<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                        
                         
                          
 <div class="col-md-9">
   <?= $this->session->flashdata('reply_success');?>
 <?php include 'alerts/message_alert.php'; ?>

<div class="panel panel-default panel-shadow">
<div class="panel-body">
<?php echo form_open_multipart(base_url().'dept_message'); ?>
  <input type="hidden" name="sender_id" value="<?php echo $user_id;?>">
  
<div class="col-sm-4">

<div class="form-group">
<input type="file" name="userfile" class="filestyle" data-buttonName="btn-primary" id="profilepic_id">
</div>
<label for="profilepic_id">
  <img class="img-responsive" id="profilepic_img" style="width:100%; height: 150px;">
</label>

</div>
<div class="col-sm-8">
  


	<div class="form-group">
 <small class="text-danger"><?=form_error('receiver_id');?></small>
 <p><span style="display:none;color:red;">Please Select Receiver ..</span>
	<select class="form-control" name="receiver_id">
		<option></option>
      <?php if($selectJoin->num_rows() !=0){?>
          <?php foreach($selectJoin->result() as $user_department_name): ?>
              <option value="<?= $user_department_name->id; ?>">
                  <?= sprintf("%08.0d",$user_department_name->IDnumber).' : '.ucwords(strtolower($user_department_name->lastname.', '.$user_department_name->firstname.' '.$user_department_name->middlename.'.')); ?>
              </option>
              <?php endforeach; ?>
      <?php }else{?>
    <option class="text-danger">No user in your department</option>
       <?php } ?>

    <?php foreach($superAdminInfo as $value){ ?>
    <!-- super admin Information -->
    <option>Super Admin - <?= ucfirst(strtolower($value->username)); ?></option>
    <!-- end super admin -->
    <?php } ?>

	</select>
  </p>



	</div>
	<div class="form-group">
  <small class="text-danger"><?php  echo form_error('message');?></small>
  <p><span style="display:none;color:red;">What's on your mind ..!</span>
	<textarea cols="5"rows="5" name="message" class="form-control" placeholder="Type your text message here.."></textarea>
	</p>
  </div>
  <div class="form-group">
<input type="submit" value="SEND" class="pull-right btn btn-primary">
  </div>

  </div>



  </div>
  </div>
   <?php echo form_close(); ?>

<?php echo $this->session->flashdata('forward_success');?>
<!-- ======================= Message Log's================= -->
<div class="well panel-shadow">

<div class="panel panel-default panel-shadow">
  <div class="panel-body">
      <h4 class="text-primary">
        <span class="text-danger glyphicon glyphicon-pushpin"></span>
          Message Log's
      </h4>
  </div>
</div>

<div class="table-responsive">
<table class="table table-hover table-bordered panel-shadow">
  <thead>
    <tr>
      <th>Date</th><th>Forward</th><th>Message Sent</th><th>Image</th><th>Option</th>
    </tr>
  </thead>
  <tbody>

<?php if($record->num_rows() !=0){ ?>
    
    <?php foreach($record->result() as $message_sent_item):?>
      <?php if($this->admin_library->check_delete_message($message_sent_item->message_id)):?>
      <tr id="message_row<?=$message_sent_item->message_id?>">
        <td><h6 class="text-muted"><?= $message_sent_item->date; ?></h6></td>
          <td>
          <!-- ============ sa forward date ni ================ -->
          <?php if($message_sent_item->latest_date != null){?>
                  <h6 class="text-muted"><?= $message_sent_item->latest_date;  ?></h6>
          <?php }else{?>
              <h6 class="text-danger">
                  <span class="glyphicon glyphicon-exclamation-sign"></span> 
                  Not yet forwarded
              </h6>
          <?php } ?>
          <!-- ============end sa forward date =============== -->
          </td>
            <td>
                <h6>
                <!-- ========= character limiter
                    #if ang character/string/letter kung malapas ug 100 ky iyang putlon
                 -->
                <?php 
                  $content = character_limiter(nl2br(ucfirst($message_sent_item->content)),80);
                  echo $content;
                ?>
                 

                </h6>
            </td>
                   <td>
                  <!-- =====for image condition========== -->
                   <?php if($message_sent_item->picture !="no image"){ ?>
                          <img src="<?= base_url();?>upload/user/<?php echo $message_sent_item->picture;?>" class="img-responsive img_set_height">
                   <?php }else{ ?>
                      <h6 class="text-danger"> 
                        <span class="glyphicon glyphicon-exclamation-sign"></span>
                      No Image Attached
                      </h6>
                   <?php } ?>
                   <!-- ========end sa image condition=========== -->
                    
                  </td>
        <td>
          
          <a href="#del_message<?= $message_sent_item->message_id; ?>"  data-toggle="modal">
              <span class="glyphicon glyphicon-trash text-danger" data-toggle="tooltip" title="Remove"></span>
          </a>
              <a href="#forward_message<?= $message_sent_item->message_id; ?>" data-toggle="modal">
                  <span class="glyphicon glyphicon-envelope text-success" data-toggle="tooltip" title="Forward"></span>
                </a>

        </td>
      </tr>

    <?php endif; ?>
    <?php endforeach; ?>

  <?php }else{ ?>

   <div class="panel panel-default panel-shadow">
    <div class="panel-body">
      <h4 class="text-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> No Message Sent</h4>
    </div>
   </div>
<?php } ?>


  </tbody>
</table>
</div>
</div>

<!-- ======================= Message Log's================= -->

                </div>

  		<div class="col-md-3">
  		<!-- for Inbox -->
      <div class="panel panel-default panel-shadow">
      <div class="panel-body">
      <div class="folded">
      <h2 style="background-color: #337ab7;"><span class="glyphicon glyphicon-envelope"></span> Inbox</h2>
      </div>
       <hr>
       <center>
       <a href="#inbox" data-toggle="modal" class="btn btn-primary "><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" title="View All Messages"></span> View</a>
       </center>
      </div>
      </div>
      <!-- end inbox -->


      <div class="panel panel-default panel-shadow">
      <div class="panel-body">
      <div class="folded">
          <h2 style="background-color:#AF0000"><span class="glyphicon glyphicon-time"></span> Time Sent</h2>
      </div>
      <hr>
       <h5 class="text-muted" style="font-weight:bold;"><span class="glyphicon glyphicon-time"></span> Last Sent</h5>
      <div class="table-responsive">
      <table class="table table-hover">
      <tbody>


     <?php if($sent_latest_date->num_rows() !=0){?>
          <?php foreach($sent_latest_date->result() as $data):?>
            <tr>
          <td class="text-primary"><?php echo $data->latest_date;?></td>
        </tr>
        <?php endforeach; ?>
     <?php }else{ ?>
     <tr>
       <td class="text-danger">
         <span class="glyphicon glyphicon-exclamation-sign"></span> You havent posted
       </td>
     </tr>
     <?php } ?>

      </tbody>
      </table>
      </div>

      <?php if($last_forward_msg->num_rows() !=0){ ?>

      <h5 class="text-muted" style="font-weight:bold;"><span class="glyphicon glyphicon-time"></span> Last Message Forward</h5>
      <?php foreach($last_forward_msg->result() as $value):?>
      <div class="table-responsive">
      <table class="table table-hover">
      <tbody>
      
        <tr>
          <td>
            <?php if($value->forwarded_date !=null){ ?>
              <?php echo $value->forwarded_date;?>
            <?php }else{ ?>
              <h6 class="text-danger">
                <span class="glyphicon glyphicon-exclamation-sign"></span>
                  New message is not yet forwarded
              </h6>
            <?php } ?>
          </td>
        </tr>
     
      </tbody>
      </table>
      </div>
    <?php endforeach;?>
      <?php }else{ ?>
        <div class="table-responsive">
            <table class="table table-hover">
                <tbody>
          
                    <tr>
                          <td class="text-danger">
                            <span class="glyphicon glyphicon-exclamation-sign"></span>
                            No Result for Forwarded
                          </td>
                    </tr>
         
                </tbody>
            </table>
      </div>
      <?php } ?>
       

       <?php foreach($sent_previous_date as $value):  if($value->sender_id == $user_id): ?>
      <h5 class="text-muted" style="font-weight:bold;"><span class="glyphicon glyphicon-time"></span> Previous Sent Date</h5>
      <div class="table-responsive">
      <table class="table table-hover">
        <tbody>
        
          <tr>
            <td><?php echo $value->previous_date;?></td>
          </tr>
       
        </tbody>
      </table>
      </div>
       <?php endif; endforeach; ?>
   

      </div>
      </div>

  		</div>


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
<?php include'copyright.php';?>
 <br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>

<?php include'adminModal/messaging_modal.php';?>
<?php include'adminModal/inbox_modal.php';?>
 <script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>

    


