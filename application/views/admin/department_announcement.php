<?php include 'admin_session.php';?>

<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                    
                        <!-- content -->                      
                        <div class="row">

                        <div class="col-sm-9 col-md-9 col-lg-9">
                        <small class="text-danger"><?= $this->session->flashdata('announcement_published_success');?></small>
                        <div class="panel panel-default panel-shadow">
                        <div class="panel-body">
                        <div class="folded">
                             <h2 style="background-color: #337ab7;"> <span class="glyphicon glyphicon-pencil"></span> <?= $info;?></h2>

                        </div>
                        <hr>
                        <?php echo form_open(base_url().'announcement_insert');?>
                        <input type="hidden" name="admin_id" value="<?php echo $user_id;?>">
                        

                        <div class="form-group">
                        <small class="text-danger"><?php echo form_error('announcement');?></small>
                        <textarea name="announcement" rows="5" cols="5" class="form-control" placeholder="Write the announcement here..!"></textarea>
                        </div>
                        <div class="form-group">
                        <button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-share"></span> Post</button>
                        </div>
                        <?php echo form_close();?>

                        </div>
                        </div>
                        <?= $this->session->flashdata('announce_update_success');?>
                        <?= $this->session->flashdata('announcement_delete_success');?>
                        <div class="panel panel-default panel-shadow">
                        <div class="panel-body">
                        <h5 style="font-weight: bold;"><span class="text-primary glyphicon glyphicon-pushpin"></span> Published Announcement</h5>
                        <hr>
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered table-striped" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>Publish On</th><th>Announcement Update On</th><th>Announcement Posted</th><th>Option</th>
                            </tr>
                        </thead>
                        <tbody>
                        <?php foreach($announcement_table as $announce): if($announce->admin_id == $user_id):?>
                            <tr>
                                <td><?= $announce->date; ?></td>
                                        <td>
                                                <?php if($announce->date_update !=null){?>
                                                    <?= $announce->date_update; ?>
                                                <?php }else{ ?>
                                                    <h6 class="text-danger">
                                                        <span class="glyphicon glyphicon-exclamation-sign"></span> 
                                                            Not Yet Forwarded 
                                                    </h6>
                                                <?php } ?>
                                        </td>
                                <td><?= nl2br(ucfirst($announce->announcement));?></td>
                                <td>
                                    <!-- for edit -->
                                    <a href="#announcement_edit<?= $announce->id;?>" data-toggle="modal"><span class="glyphicon glyphicon-pencil text-success" data-toggle="tooltip" title="EDIT"></span></a>
                                    <!-- /edit -->
                                    <!-- for delete -->
                                    <a href="#announcement_trash<?= $announce->id;?>" data-toggle="modal"><span class="text-danger glyphicon glyphicon-trash" data-toggle="tooltip" title="REMOVE"></span></a>
                                    <!-- end delete -->
                                </td>
                            </tr>
                <?php endif; endforeach; ?>
                        </tbody>
                            
                        </table>
                        </div>

                        </div>
                        </div>

                        </div>
                        <!-- / column 9 -->
                       
                         
                          
 


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
<?php include'copyright.php';?>
 <br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>


    <!-- modal -->
    <?php include'adminModal/announcement_modal.php';?>
    <script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>
   

  





