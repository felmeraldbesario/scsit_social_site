<!-- 
#Developer Information
#Felmerald Besario Front end developer for user , administrator and super administrator
and Back end programmer in this administrator
 -->
<!DOCTYPE html>
<html lang="en">
	<head>
	
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $title;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?= base_url();?>css/bootstrap.css" rel="stylesheet">
		<link href="<?= base_url();?>css/styles.css" rel="stylesheet">
		<!-- for data tables style -->
		<link href="<?= base_url();?>css/dataTables.bootstrap.css"ref="stylesheet">
		<!-- custom style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/custom.css">
		<!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/font-awesome.css">
		<link rel="shortcut icon" href="<?= base_url();?>img/logo.png">
		<!-- felmerald style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald_buttons.css">
		<!-- felmerald style for on hover images css3 -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/chrisraldforever.css" media="screen">
		<script src="<?= base_url();?>rald_js/administrator.js"></script>
		
	</head>

