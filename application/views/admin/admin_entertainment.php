<?php include 'admin_session.php';?>

<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                        <div class="col-sm-2">
                        <div class="panel panel-default panel-shadow">
                        <div class="panel-body">
                        <a href="#feladdnews"data-toggle="modal">
                        <img src="<?=base_url();?>img/admin_footer/add.png" class="img-responsive add_item_button hover" data-toggle="tooltip" title="Add Entertainment?">
                        </a>
                        </div>
                        </div>
                        </div>


                        <div class="col-sm-10">
                        <?php include 'alerts/entertainment_alert.php';?>

                        <div class="panel panel-default">
                        <div class="panel-body">
                 


<?php if($entertainment_data->num_rows() !=0){?>
    
    <?php foreach($entertainment_data->result() as $rowEntertainment):?>

        <div class="col-sm-6">
                        <div class="panel panel-default default_hyt_new panel-shadow">
                        <div class="panel-body">

            
                        <?php
                        if($rowEntertainment->image !="no image"){
                         ?>
                        
                        <figure class="cap-bot">
                        <img src="<?= base_url();?>upload/user/<?= $rowEntertainment->image; ?>" class="img-responsive hyt" >
                        <figcaption class="newsimages">
                        <h4 class="word-keep"><?= ucwords(strtolower($rowEntertainment->title)); ?></h4>
                        <h5><?= $rowEntertainment->date;?></h5>
                        <!-- button links -->
                        <a href="#felnewsreadmore<?= $rowEntertainment->id; ?>" data-toggle="modal" class="button button-blue button-xs">
                        <span class="glyphicon glyphicon-folder-open" data-toggle="tooltip" title="Tap to view"></span>
                        </a>
                        <a href="#fel_update_news<?=  $rowEntertainment->id;?>" data-toggle="modal" class="button button-xs">
                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Tap to edit"></span>
                        </a>
                        <a href="#fel_remove_news<?=  $rowEntertainment->id;?>" data-toggle="modal" class="button button-red button-xs" >
                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Tap to delete"></span>
                        </a>
                        <!-- end button links -->
                        </figcaption>
                        </figure>


                        <?php }else{ ?>

                        <figure class="cap-bot">
                        <img src="<?= base_url();?>img/def_pic.png" class="img-responsive hyt">
                        <figcaption class="newsimages">
                        <h4 class="word-keep"><?= ucwords(strtolower($rowEntertainment->title)); ?></h4>
                        <h5><?= $rowEntertainment->date;?></h5>

                        <!-- button links -->
                        <a href="#felnewsreadmore<?= $rowEntertainment->id; ?>" data-toggle="modal" class="button button-blue button-xs">
                        <span class="glyphicon glyphicon-folder-open" data-toggle="tooltip" title="Tap to view"></span>
                        </a>
                        <a href="#fel_update_news<?=  $rowEntertainment->id;?>" data-toggle="modal" class="button button-xs">
                        <span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Tap to edit"></span>
                        </a>
                        <a href="#fel_remove_news<?=  $rowEntertainment->id;?>" data-toggle="modal" class="button button-red button-xs" >
                        <span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Tap to delete"></span>
                        </a>
                        <!-- end button links -->
                        </figcaption>
                        </figure>

                        <?php } ?>
                        <div class="panel-footer">
                          <div class="folded">
                        <h2 style="background-color: #5bc0de;"><span class="glyphicon glyphicon-time"></span> <?= $rowEntertainment->date;?></h2>
                        </div>
                        </div>

                        </div>
                        </div>
                        </div>

    <?php endforeach; ?>

<?php }else{ ?>

         <div class="col-sm-12">
                            <div class="panel panel-default panel-shadow">
                                <div class="panel-body">
                                    <h4 class="text-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> You havent posted!!</h4>
                                        <a href="#feladdnews" data-toggle="modal" class="btn btn-primary">
                                       <span class="glyphicon glyphicon-plus"></span> Click me to Add
                                        </a>
                                </div>
                            </div>
                        </div>

<?php } ?>





                       </div>
                        </div>

                        </div>
                         
                          
 


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
 <?php include'copyright.php';?>
 <br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>



<?php include 'adminModal/entertainment_modal.php'; ?>
<script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>









