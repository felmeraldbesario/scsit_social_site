<?php 

header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
header("Pragma: no-cache");
$user_id = $this->session->userdata('login_id');
$username = $this->session->userdata('login_username');
$password = $this->session->userdata('login_password');
$sessionData = array(
	'id'=>$user_id,
	'username'=>$username,
	'password'=>$password
);

$checkUserSession =$this->db->get_where('admin_personel',$sessionData);
if($checkUserSession->num_rows() == 0){

	$checkAdmin = $this->db->get_where('user',$sessionData);
	if($checkAdmin->num_rows() != 0){
		 redirect(base_url().'home');
		 
	}else{
		
		 $data = array(
            'login_id' => '',
            'login_username' => '',
            'login_password' => '',
            'login' => ''
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        
	}
}else{
	redirect(base_url().'administrator');
}

?>

<div class="container">

<div class="col-sm-2 col-md-2 col-lg-2"></div>

<div class="col-sm-8 col-md-8 col-lg-8">
<div class="panel panel-default">
<div class="panel-body">




<hr>
     <h2 class="text-center"><strong>REGISTER SUCCESS!!!</strong></h2>

        <div class="row">
        <center>
        <?= $this->session->flashdata('reg_success');?>
         <p>Thank You for registering you can <a href="<?= base_url();?>admin_login">Login Now.!</a></p> 
         </center>
         </div>
         <hr>  
         <div class="center-align">
         	<a href="<?= base_url();?>admin_login"><span class="btn btn-primary">Login</span></a>
         	<a href="<?= base_url();?>admin_signup"><span class="btn btn-primary">Create new Account</span></a>
         </div>


</div>
</div>
</div>




</div>