<?php 
header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
header("Pragma: no-cache");
$user_id = $this->session->userdata('login_id');
$username = $this->session->userdata('login_username');
$password = $this->session->userdata('login_password');
$sessionData = array(
  'id'=>$user_id,
  'username'=>$username,
  'password'=>$password
);
$checkAdmin = $this->db->get_where('admin_personel',$sessionData);
if($checkAdmin->num_rows() == 0){

  $checkUserSession =$this->db->get_where('user',$sessionData);
  if($checkUserSession->num_rows() != 0){
     redirect(base_url().'home');
     
  }else{
    
     $data = array(
            'login_id' => '',
            'login_username' => '',
            'login_password' => '',
            'login' => ''
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        redirect(base_url() . 'admin_login');
  }
}

?>