 <!-- for added alert -->
 <?php if ($this->session->userdata('sports_added')!='') { ?>
                      
  <div class="alert alert-info alert-dismissible" role="alert" style="margin-top:2px;">
   <span class="glyphicon glyphicon-ok"></span>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
    <?php echo $this->session->userdata('sports_added');?>
   </div>
 <?php } ?>

<!-- end added alert -->

<!-- for update file event -->
<?php if($this->session->userdata('sports_updated_file_default') !=''){ ?>
<div class="alert alert-success alert-dismissible" role="alert" >
<span class="glyphicon glyphicon-edit"></span>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
<?= $this->session->userdata('sports_updated_file_default');?>
</div>
<?php } ?>
<!-- end for update file -->

<!-- for deleting data -->
<?php 
if($this->session->userdata('sports_deleting') !=''){?>
<div class="alert alert-danger alert-dismissible" role="alert">
<span class="glyphicon glyphicon-trash"></span>
<button type="button" class="close" data-dismiss="alert" aria-label="Close">
	<span aria-hidden="true">&times;</span>
</button>
<?= $this->session->userdata('sports_deleting'); ?>
</div>
<?php } ?>
<!-- end of delete -->
<?php 
$this->session->unset_userdata (

	array(
		'sports_added'=>'',
		'sports_updated_file_default'=>'',
		'sports_deleting'=>''
		)

	);
?>