<!-- =====================Notification if successfully send message================================ -->
                      <?php if ($this->session->userdata('costumer_message_sent')!='') { ?>
                     <div class="alert alert-info alert-dismissible" role="alert" style="margin-top:10px;">
                     <span class="glyphicon glyphicon-ok"></span>
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                     <?php echo $this->session->userdata('costumer_message_sent');?>
                     </div>
                     <?php } ?>
                     <?php $this->session->unset_userdata(array('costumer_message_sent'=>''));?>
 <!-- =====================Notification if successfully send message================================ -->