                    <!-- =======================session delete========================= -->
                      <?php if ($this->session->userdata('news_deleting')!='') { ?>
                      
                     <div class="alert alert-danger alert-dismissible" role="alert" style="margin-top:2px;">
                     <span class="glyphicon glyphicon-trash"></span>
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                     <?php echo $this->session->userdata('news_deleting');?>
                    </div>
                    <?php } ?>
                    <?php $this->session->unset_userdata(array('news_deleting'=>''));?>
                     <!-- =======================session delete========================= -->

                     <!-- =======================session edit========================= -->
                     <?php if ($this->session->userdata('news_updating_event')!='') { ?>
                      
                     <div class="alert alert-warning alert-dismissible" role="alert" style="margin-top:2px;">
                     <span class="glyphicon glyphicon-pencil"></span>
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                     <?php echo $this->session->userdata('news_updating_event');?>
                    </div>
                    <?php } ?>
                    <?php $this->session->unset_userdata(array('news_updating_event'=>''));?>
                     <!-- =======================session edit========================= -->


                     <!-- =======================session Insert========================= -->
                     <?php if ($this->session->userdata('news_inserting')!='') { ?>

                     <div class="alert alert-success alert-dismissible" role="alert" style="margin-top:2px;">
                     <span class="glyphicon glyphicon-ok"></span>
                     <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                      <span aria-hidden="true">&times;</span></button>
                     <?php echo $this->session->userdata('news_inserting');?>
                    </div>
                    <?php } ?>
                    <?php $this->session->unset_userdata(array('news_inserting'=>''));?>
                     <!-- =======================session Insert========================= -->