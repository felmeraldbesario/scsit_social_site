<?php if($this->session->userdata('course_success_added') !=''){ ?>
<div class="alert alert-info alert-dismissible" role="alert">
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
   <span class="glyphicon glyphicon-ok"></span>
   <?= $this->session->userdata('course_success_added');?>
   </div>
<?php } ?>
 

<?php $this->session->unset_userdata(
	array(
		'course_success_added'=>'',
				)
	);?>