
 <?php if ($this->session->userdata('entertainment_inserting_data')!='') { ?>
                      
  <div class="alert alert-info alert-dismissible" role="alert" style="margin-top:2px;">
   <span class="glyphicon glyphicon-ok"></span>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
    <?= $this->session->userdata('entertainment_inserting_data');?>
   </div>
 <?php } ?>

 <?php if($this->session->userdata('entertainment_update') !=''){?>
 <div class="alert alert-success alert-dismissible" role="alert" style="margin-top:2px;">
   <span class="glyphicon glyphicon-pencil"></span>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
   <?= $this->session->userdata('entertainment_update'); ?>
   </div>
 <?php } ?>
<?php if($this->session->userdata('entertainment_deleting_data') !=''){ ?>
<div class="alert alert-danger alert-dismissible" role="alert" style="margin-top:2px;">
   <span class="glyphicon glyphicon-trash"></span>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
   <?= $this->session->userdata('entertainment_deleting_data');?>
   </div>
<?php }?>

<?php $this->session->unset_userdata(
array(
	'entertainment_inserting_data'=>'',
	'entertainment_update'=>'',
	'entertainment_deleting_data'=>''
	));?>