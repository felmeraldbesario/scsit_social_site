<?php if ($this->session->userdata('editorial_added_data')!='') { ?>
                      
  <div class="alert alert-info alert-dismissible" role="alert">
   <span class="glyphicon glyphicon-ok"></span>
   <button type="button" class="close" data-dismiss="alert" aria-label="Close">
   <span aria-hidden="true">&times;</span></button>
    <?= $this->session->userdata('editorial_added_data');?>
   </div>
 <?php } ?>
 <?php if($this->session->userdata('Updating_editorial_event') !=''){ ?>
 <div class="alert alert-success alert-dismissible" role="alert">
 <span class="glyphicon glyphicon-pencil"></span>
 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
 	<span aria-hidden="true">&times;</span>
 </button>
 <?= $this->session->userdata('Updating_editorial_event');?>
 </div>
 <?php } ?>
 <?php if($this->session->userdata('editorial_deleting_data') !=''){?>
 <div class="alert alert-danger alert-dismissible" role="alert">
 <span class="glyphicon glyphicon-trash"></span>
 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
 	<span aria-hidden="true">&times;</span>
 </button>
 <?= $this->session->userdata('editorial_deleting_data');?>
 </div>
 <?php } ?>

<?php $this->session->unset_userdata(

	array(
		'editorial_added_data'=>'',
		'Updating_editorial_event'=>'',
		'editorial_deleting_data'=>''
		)
	); ?>