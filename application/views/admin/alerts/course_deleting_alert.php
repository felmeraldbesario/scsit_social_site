<?php if($this->session->userdata('kurso_eraser') !=''){?>
 <div class="alert alert-danger alert-dismissible" role="alert">
 <span class="glyphicon glyphicon-trash"></span>
 <button type="button" class="close" data-dismiss="alert" aria-label="Close">
 	<span aria-hidden="true">&times;</span>
 </button>
 <?= $this->session->userdata('kurso_eraser');?>
 </div>
 <?php } ?>
 <?php $this->session->unset_userdata(
	array(
		'kurso_eraser'=>''
		)
	);?>