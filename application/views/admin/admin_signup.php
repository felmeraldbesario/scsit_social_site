

<?php 

header("Cache-Control: no-store, no-cache, must-revalidate, no-transform, max-age=0, post-check=0, pre-check=0");
header("Pragma: no-cache");
$user_id = $this->session->userdata('login_id');
$username = $this->session->userdata('login_username');
$password = $this->session->userdata('login_password');
$sessionData = array(
	'id'=>$user_id,
	'username'=>$username,
	'password'=>$password
);

$checkUserSession =$this->db->get_where('admin_personel',$sessionData);
if($checkUserSession->num_rows() == 0){

	$checkAdmin = $this->db->get_where('user',$sessionData);
	if($checkAdmin->num_rows() != 0){
		 redirect(base_url().'home');
		 
	}else{
		
		 $data = array(
            'login_id' => '',
            'login_username' => '',
            'login_password' => '',
            'login' => ''
        );
        $this->session->unset_userdata($data);
        $this->session->sess_destroy();
        
	}
}else{
	redirect(base_url().'administrator');
}

?>


<!DOCTYPE html>
<html lang="en">
	<head>
	
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $title;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?= base_url();?>css/bootstrap.css" rel="stylesheet">
		<link href="<?= base_url();?>css/styles.css" rel="stylesheet">
		<!-- for data tables style -->
		<link href="<?= base_url();?>css/dataTables.bootstrap.css"ref="stylesheet">
		<!-- custom style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/custom.css">
		<!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/font-awesome.css">
		<link rel="shortcut icon" href="<?= base_url();?>img/logo.png">
		<!-- felmerald style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald_buttons.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/sign_up.css">
		
	</head>

<body>



<div class="container-fluid">
<div class="marginTop"></div>
<div class="col-sm-2"></div>

<div class="col-sm-8">
<div class="panel panel-default panel-shadow">
<div class="panel-heading"><span class="glyphicon glyphicon-registration-mark"></span> Registration

<a href="<?= base_url();?>admin_login" class="btn btn-primary pull-right marginButtonNegative">Login</a>
</div>
<div class="panel-body">


<!-- <?php echo form_open_multipart(base_url().'admin_registration');?> -->

<form action="<?php echo base_url().'upload/admin_registration'?>" method="post" enctype="multipart/form-data" onreset="Reset_message()">

<?php
date_default_timezone_set('Asia/Manila');
$date=date('h:i:s a m/d/y');
?>
<input type="hidden" name="date" value="<?php echo $date;?>">


<div class="col-sm-6">
<div class="inner-addon left-addon">
<label for="fname"></label><span class="text-danger"><?php  echo form_error('fname');?></span>
<p>
<i class="text-muted glyphicon glyphicon-user"></i>
<input type="text" class="form-control border" name="fname"  placeholder=" Firstname"  value="<?php echo set_value('fname');?>">
<span style="display:none;color:red;"> Your Firstname Must Be Required</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="lname"></label><span class="text-danger"><?php  echo form_error('lname');?></span>
<p>
<i class="text-muted glyphicon glyphicon-user"></i>
<input type="text" class="form-control border" name="lname" placeholder=" Lastname"  value="<?php echo set_value('lname');?>">
<span style="display:none;color:red;">Your Family name Must Be Required</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="Mname"></label><span class="text-danger"><?php  echo form_error('Mname');?></span>
<p>
<i class="text-muted glyphicon glyphicon-user"></i>
<input type="text" class="form-control border" name="Mname" placeholder=" Middlename" r value="<?php echo set_value('Mname');?>">
<span style="display:none;color:red;">Your Middle name Must Be Required</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="assigned_department"></label>
<span class="text-danger"><?php echo form_error('assigned_department');?></span>

<label class="text-muted">Select Department</label>
<p>
<span style="display:none;color:red;">Kindly Select One Department You Want to Administer</span>
<select class="form-control border"  name="assigned_department" value="<?php echo set_value('assigned_department');?>">
	<option></option>
	<?php 
		$queryDepartment=$this->db->get('department');
			foreach($queryDepartment->result() as $rowDepartment){
				$this->db->where('assigned_department',$rowDepartment->id);
					$querycourse=$this->db->get('admin_personel');
						if($querycourse->num_rows()==0){ ?>
	<option value="<?php  echo $rowDepartment->id?>"><?php echo strtoupper($rowDepartment->department);?></option>
	<?php } 
	}?>
</select>


</p>
</div>

</div>

<div class="col-sm-6">
<div class="inner-addon left-addon">
<label for="email"></label><span class="text-danger"><?php  echo form_error('email');?></span>
<p>
<i class="text-muted glyphicon glyphicon-envelope"></i>
<input type="email" class="form-control border" name="email" placeholder=" Email Address"  value="<?php echo set_value('email');?>">
<span style="display:none;color:red;">Email Address Must Be Valid and Required</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="admin_id_number"></label><span class="text-danger"><?php  echo form_error('admin_id_number');?></span>
<p>
<i class="text-muted glyphicon glyphicon-earphone"></i>
<input type="number" class="form-control border" name="admin_id_number" placeholder=" ID Number" value="<?php echo set_value('admin_id_number');?>">
<span style="display:none;color:red;">School ID Number Must be required</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="username"></label><span class="text-danger"><?php  echo form_error('username');?></span>
<p>
<i class="text-muted glyphicon glyphicon-user"></i>
<input type="text" class="form-control border" name="username" placeholder=" Username" value="<?php echo set_value('username');?>">
<span style="display:none;color:red;">Create username</span>
</p>
</div>
<div class="inner-addon left-addon">
<label for="password"></label><span class="text-danger"><?php  echo form_error('password');?></span>
<p>
<i class="text-muted glyphicon glyphicon-lock"></i>
<input type="password" class="form-control border" name="password" placeholder=" Create Password" value="<?php echo set_value('password');?>">
<span style="display:none;color:red;">Create Strong Password</span>
</p>
</div>
<div class="inner-addon left-addon">
<i class="text-muted glyphicon glyphicon-lock"></i>
<label for="CPassword"></label><span class="text-danger"><?php  echo form_error('CPassword');?></span>
<p>
<input type="password" class="form-control border" name="CPassword" placeholder=" Re-type password" value="<?php echo set_value('CPassword');?>">
<span style="display:none;color:red;">Confirm Your Password</span>
</p>
</div>

<!-- ====================Uploading Ajax================ -->

<div class="form-group"><h5 class="text-muted pull-left"><span class="glyphicon glyphicon-picture text-success"></span> Create Profile Photo</h5></div>
<br><br>
<center>

 <label for="profilepic_id">
	<img class="img-responsive" id="profilepic_img" />
</label>
</center>
<div class="form-group">
<input type="file" name="userfile" class="filestyle border" data-buttonName="btn-primary" id="profilepic_id">
</div>


<!-- ====================Uploading Ajax================ -->
<br>
<div class="form-group">
<button class="button button-black " type="reset">
<span class="glyphicon glyphicon-erase"data-toggle="tooltip" title="REMOVE ALL TEXT"></span>
</button>
<input type="submit"value="Sign Up"class="btn btn-primary pull-right">
</div>
</div>


</form>
         


</div>
</div>
</div>

</div>



        <script src="<?php echo base_url();?>js/jquery-1.10.2.js"></script>
		<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>rald_js/load_image.js"></script>
		<script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>
		<?php include 'javascript/image_style.php';?>
		<?php include 'javascript/login_focusin.php';?>
		<?php  include 'javascript/tooltip_popover.php';?>
	
</body>
</html>

