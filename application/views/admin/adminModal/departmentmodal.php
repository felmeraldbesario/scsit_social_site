
<!-- ///////////////////// -->

<?php $queryUpdateCourse=$this->db->get('course');
foreach($queryUpdateCourse->result() as $rowCourse): ?>
<!-- update course -->
<div id="felcourseEdit<?= $rowCourse->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

  <div class="modal-header">
   <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
   <br><br>
   <?php $this->db->where('id',$rowCourse->id); $queryCourseUp=$this->db->get('course'); foreach($queryCourseUp->result() as $rowOne){?>
    <?= form_open(base_url().'Updating_course'); ?>
    <input type="hidden" name="id" value="<?php echo $rowOne->id;?>">
         

     <div class="form-group">
     <p>
      <i class="glyphicon glyphicon-pencil"></i> Update Course Name
    <br><br>
     <input type="text" name="course" class="form-control" value="<?= $rowOne->course; ?>">
     <span style="display:none;color:red;">Update this course <?= strtoupper($rowOne->course);?></span>

     </p>
     </div>
     <div class="form-group">
     <button type="submit" class="button button-blue pull-right"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="UPDATE"></span></button>
     </div>

     <br><br>
     <?php } ?>
   <?php echo form_close(); ?>



  </div>

  </div>
  </div>
  </div>

 <?php endforeach; ?>
 
<?php foreach($viewCourse as $DeptSelected){ if($DeptSelected->department_id == $dept_id){?>
 <div id="delete_course<?= $DeptSelected->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>         
      
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <p class="text-danger"><span class="glyphicon glyphicon-trash"></span> Are you sure you want to delete this course ?</p>
      </div>
      </div>
      <div class="modal-footer">
      <a href="#final_decide<?= $DeptSelected->id; ?>" data-dismiss="modal" data-toggle="modal" class="btn btn-danger"><span class="glyphicon glyphicon-ok"></span> Delete</a>
      </div>
     
  </div>
  </div>
</div>
<?php } }?>
<!-- final decide -->
<?php foreach($viewCourse as $DeptSelected){ if($DeptSelected->department_id == $dept_id){?>
<div id="final_decide<?= $DeptSelected->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">

<div class="modal-header">
</div>
<div class="modal-body">
<div class="panel-body">
<h5 class="text-danger">Final Attemp, Are you sure you want to delete this course?</h5>
<?php $this->db->where('id',$DeptSelected->id); $query = $this->db->get('course'); foreach($query->result() as $rowFinal){ ?>
<h5 class="text-danger"><strong class="text-muted">Course name:</strong> <?= strtoupper($rowFinal->course); ?></h5>
<?php } ?>
<p class="text-danger">Press red button to confirm</p>
<p> Press blue button to exit</p>
<center>
<div class="form-group">
<button class="btn btn-primary" data-dismiss="modal">EXIT</button>
<a href="delete_course?id=<?= $DeptSelected->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-ok"></span> Delete</a>
</div>
</center>

</div>
</div>
<div class="modal-footer">
</div>
</div>
</div>
</div>
<?php } }?>