<!-- modal -->

  <div id="feladdnews" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>

          <?php echo form_open_multipart(base_url().'insert_intertainment');?>
          <input type="hidden" name="admin_id" value="<?php echo $user_id;?>">
        

         <div class="form-group">
         <?php echo form_error('title');?>
         <label class="text-muted">Title</label>
         <input type="text" name="title" class="form-control" required placeholder="Title..">
         </div>

         <!--for ajax uploading image-->
         <div class="form-group">
         <label class="text-muted">News Image</label>
         <input type="file" name="userfile" class="filestyle" data-buttonName="btn-warning" id="profilepic_id">
         </div>
          
        <label for="profilepic_id">
        <img src="<?= base_url();?>img/def_pic.png" class="img-responsive news_img_default pull-left" id="profilepic_img"/>
        </label>
<!-- end for ajax uploading image-->

         <div class="form-group">
         <?php echo form_error('entertainment');?>
         <label class="text-muted">Description</label>
         <textarea cols="5" rows="5" name="entertainment" class="form-control" required="" Placeholder="Add description here"></textarea>
         </div>

        <div class="form-group">
            <button type="reset" class="button button-black"><span class="glyphicon glyphicon-erase" data-toggle="tooltip" title="RESET"></span></button>
         <button type="submit" class="pull-right button button-blue"><span class="glyphicon glyphicon-plus" data-toggle="tooltip" title="ADD"></span></button>
         </div>


         <?php echo form_close();?>
        <br><br>
      </div>
     
  </div>
  </div>
</div>

<?php
$queryReadnews=$this->db->get('entertainment_news');
foreach($queryReadnews->result() as $rowEntertainment){
 ?>

<div id="felnewsreadmore<?php echo $rowEntertainment->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
        <?php
        $this->db->where('id',$rowEntertainment->id);
        $queryResult=$this->db->get('entertainment_news');
        foreach($queryResult->result() as $rowResults){
         ?>
        
        <h4 class="word-keep"><span class="glyphicon glyphicon-tag"></span> <?php echo nl2br(strtoupper($rowResults->title));?></h4>
        <h6><span class="glyphicon glyphicon-time"></span> <?php echo $rowResults->date;?></h6>
        <br>
        <br>

        <p class="word-keep">
        <?php echo nl2br(ucfirst($rowResults->entertainment));?>
          
        </p>
        <?php } ?>

         

         

      </div>
     
  </div>
  </div>
</div>

<?php } ?>



<?php
$queryUpdate=$this->db->get('entertainment_news');
foreach($queryUpdate->result() as $rowsports){
 ?>
<div id="fel_update_news<?php echo $rowEntertainment->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br><br>
         <div class="alert alert-success" role="alert">Update File</div>
         
         <?php
         $this->db->where('id',$rowEntertainment->id);
         $queryEdit=$this->db->get('entertainment_news');
         foreach($queryEdit->result() as $rowedit){
          ?>

          <?php echo form_open_multipart(base_url().'fel_updating_entertainment');?>
          <input type="hidden" name="id" value="<?php echo $rowedit->id;?>">

         


          <div class="form-group">
          <label class="text-muted"> Title</label>
          <input type="text" name="title" placeholder="Title" class="form-control" value="<?php echo $rowedit->title; ?>">
          </div>
          <div class="form-group">
          <?php 
          if($rowedit->image !="no image"){
          ?>
          <img src="<?= base_url();?>upload/user/<?php echo $rowedit->image; ?>" class="img-responsive update_hyt">
          <?php }else{ ?>
          <img src="<?= base_url();?>img/def_pic.png" class="img-responsive update_hyt">
          <?php } ?>

          <label class="text-muted">Change Image</label>
          <input type="file" name="userfile">
          </div>
          <div class="form-group">
          <label class="text-muted">Description</label>
          <textarea cols="3" rows="3" name="entertainment" class="form-control"><?php echo $rowedit->entertainment;?></textarea>
          </div>

          <div class="form-group">
             <button type="submit" class="button button-blue pull-right"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="UPDATE"></span></button>
             <br>
          </div>
        <?php }echo form_close(); ?>



      </div>
     
  </div>
  </div>
</div>
<?php } ?>


<?php 
$queryEntertainment = $this->db->get('entertainment_news');
foreach($queryEntertainment->result() as $rowEntertainment){
?>

<div id="fel_remove_news<?php echo $rowEntertainment->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href="delete_entertainment?id=<?php echo $rowEntertainment->id;?>"class="button button-red button-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="REMOVE"></span></a>&nbsp 
         <button data-dismiss="modal"class="button button-black button-xs pull-right"><span class="glyphicon glyphicon-off" data-toggle="tooltip" title="DISMISS"></span></button>
      </div>
     
  </div>
  </div>
</div>

<?php } ?>
