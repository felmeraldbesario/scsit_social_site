<?php foreach($announcement_table as $announce):?>
<div id="announcement_edit<?= $announce->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>         
         <hr>
        

         <?php $this->db->where('id',$announce->id); $queryAnnounce = $this->db->get('admin_announcement');foreach($queryAnnounce->result() as $rowannouncement): ?>

          <?php echo form_open(base_url().'update_announcement');?>
          <input type="hidden" name="id" value="<?php echo $rowannouncement->id;?>">

         <div class="form-group">
         <p><span class="glyphicon glyphicon-edit"></span> Update Announcement Posted</p>
         <textarea name="announcement" cols="5" rows="5" class="form-control"><?php echo $rowannouncement->announcement; ?></textarea>
         </div>
         <div class="form-group">
         <button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Update</button>
         </div>
          <?php echo form_close();?>
       <?php endforeach; ?>
      
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>

<?php foreach($announcement_table as $announce):?>
<div id="announcement_trash<?= $announce->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>         
      
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <p class="text-danger"><span class="glyphicon glyphicon-trash"></span> Are you sure you want to delete this announcement ?</p>
      </div>
      </div>
      <div class="modal-footer">
      <a href="delete_announcement?id=<?= $announce->id;?>" class="btn btn-danger"><span class="glyphicon glyphicon-ok"></span> Delete</a>
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>