<?php foreach($GetLike as $DeptSelected): ?>
<div id="department_like<?= $DeptSelected->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
      <h6 class="text-muted" style="font-weight: bold;word-break: keep-all;">
        <span class="text-primary glyphicon glyphicon-thumbs-up"></span>
        USER WHO LIKE THIS STATUS
      </h6>
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;margin-top: -30px;">  
      </div>
      <div class="modal-body">
      <div class="container" style="margin-top: 10px;">
                        
                         
                          <?php 
                   
                          $this->db->where('department_topic_id',$DeptSelected->id);
                          $query=$this->db->get('department_topic_feedback');
                          foreach($query->result() as $rows){

                          $this->db->where('id',$rows->user_id);
                          $query=$this->db->get('user');
                          foreach($query->result() as  $row1){

                          ?>
                          
                                  <p style="word-break: keep-all;" class="pull-left"><button class="btn btn-default " style="width: 23.6%;">
       <span class="glyphicon glyphicon-user"></span>
      &nbsp; &nbsp; &nbsp;<?= ucwords(strtolower($row1->lastname.', '.$row1->firstname.' '.$row1->middlename));?> 
      <span class="text-primary glyphicon glyphicon-thumbs-up" ></span></button></p>
                               
                                 <?php } } ?>
 
      </div>
      </div>
      <div class="modal-footer">
      <h6 class="text-muted">
        &copy SCSIT SOCIAL SITE  Department
      </h6>
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>

<!-- ================new comment here======================== -->
 <?php foreach($this->admin_library->Selecting_department_profile_status($user_id) as $DeptSelected){?>
<div id="newComment<?= $DeptSelected->department_topic_id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="modal-header">
        <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
      </div>
          <div class="modal-body">

            <div class="panel-body">
                    <div class="row">

                 <div class="media" id="show<?= $DeptSelected->department_topic_id; ?>" >

                 <?php 
                     foreach($this->admin_library->showComment($DeptSelected->department_topic_id) as $commentRow){

                 ?>  <span id="delete_comment<?= $commentRow->comment_id; ?>">
                                        
                      
                      <?php if($commentRow->commenter_id == "0"){?>
                      <a href="#" class="pull-left">
                      <?php if($this->admin_library->get_superadmin_image() != false){?>
                          <img href="#" class="media-object" src="<?= base_url();?>upload/superadmin/campus_post/<?=$this->admin_library->get_superadmin_image()?>" style="height: 40px; width: 40px;" alt="d">
                      <?php }else{
                        ?>
                          <img href="#" class="media-object" src="<?= base_url();?>img/logo.png" style="height: 40px; width: 40px;" alt="d">
                        <?php
                        } ?>
                      </a>
                              <div class="media-body">
                                <h5 class="media-heading">

                                  <div class="pull-right">
                                    <a href="#" class="text-danger" onclick="return delete_comment(<?=$commentRow->comment_id?>,<?=$DeptSelected->department_topic_id?>);">
                                      <span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Remove"></span>
                                    </a>
                                        <a href="#" onclick="return input_comment(<?=$commentRow->comment_id?>);" class="text-success">
                                        <span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit"></span>
                                        </a>

                                  </div>

                                Super Admin
                                <h6 class="text-danger">
                                    <span class="glyphicon glyphicon-time"></span> <?=$commentRow->department_date?>
                                  </h6>


                                </h5>
                                
                                  <h6 id="fel_edit_comment<?=$commentRow->comment_id?>" style="word-break: keep-all;"><?=nl2br($commentRow->comment)?></h6>
                                  

                              </div>
                                <?php }else if($commentRow->department_admin_id_number != ""){
                                 
                                  ?>
                      <a href="#" class="pull-left">
                      <?php if($this->admin_library->get_admin_image($commentRow->department_admin_id_number) != false){?>
                           <img href="#" class="media-object" src="<?= base_url();?>upload/user/<?=$commentRow->admin_image?>" style="height: 40px; width: 40px;" alt="d">
                           <?php }else{
                            ?>
                            <img href="#" class="media-object" src="<?= base_url();?>img/logo.png" style="height: 40px; width: 40px;" alt="d">
                            <?php
                            } ?>
                      </a>
                              <div class="media-body">
                                <h5 class="media-heading">

                                 <div class="pull-right">
                                    <a href="#" class="text-danger" onclick="return delete_comment(<?=$commentRow->comment_id?>,<?=$DeptSelected->department_topic_id?>);">
                                      <span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Remove"></span>
                                    </a>
                                        <a href="#" onclick="return input_comment(<?=$commentRow->comment_id?>);" class="text-success">
                                        <span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit"></span>
                                        </a>

                                  </div>

                                Admin
                                <h6 class="text-danger">
                                    <span class="glyphicon glyphicon-time"></span> <?=$commentRow->department_date?>
                                  </h6>


                                </h5>
                                
                                  <h6 id="fel_edit_comment<?=$commentRow->comment_id?>" style="word-break: keep-all;"><?=nl2br($commentRow->comment)?></h6>
                                  

                              </div>
                                  <?php }else{
                                    ?>
                      <a href="#" class="pull-left">
                                   <?php if($this->admin_library->get_user_image($commentRow->commenter_id) != false){?>
                           <img href="#" class="media-object" src="<?= base_url();?>upload/user/<?=$this->admin_library->get_user_image($commentRow->commenter_id)?>" style="height: 40px; width: 40px;" alt="d">
                           <?php }else{
                            ?>
                            <img href="#" class="media-object" src="<?= base_url();?>img/logo.png" style="height: 40px; width: 40px;" alt="d">
                            <?php
                            } ?>
                      </a>
                              <div class="media-body">
                                <h5 class="media-heading">

                                  <div class="pull-right">
                                    <a href="#" class="text-danger" onclick="return delete_comment(<?=$commentRow->comment_id?>,<?=$DeptSelected->department_topic_id?>);">
                                      <span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Remove"></span>
                                    </a>
                                        <a href="#" onclick="return input_comment(<?=$commentRow->comment_id?>);" class="text-success">
                                        <span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="Edit"></span>
                                        </a>

                                  </div>

                                <?= ucfirst(strtolower($commentRow->firstname))." ".ucfirst(strtolower($commentRow->lastname))?>
                                <h6 class="text-danger">
                                    <span class="glyphicon glyphicon-time"></span> <?=$commentRow->department_date?>
                                  </h6>


                                </h5>
                                
                                  <h6 id="fel_edit_comment<?=$commentRow->comment_id?>" style="word-break: keep-all;"><?=nl2br($commentRow->comment)?></h6>
                                  

                              </div>
                                    <?php
                                    } ?>

                                 </span>

                 
 <?php 
                     }
                   ?>
                 </div>
                 

                  <div class="row">
                    <div class="col-sm-1"></div>
                      <div class="col-sm-10">
                        <input type="text" id="com<?php echo $DeptSelected->department_topic_id;?>" class="form-control" style="border-radius: 0px;" placeholder="write your comment here">
                      </div>
                        <center>
                          <?php 
                    $this->db->where('id',$user_id);
                    $adminUser = $this->db->get('admin_personel');
                    foreach($adminUser->result() as $adminRow){
                      $admin_id_number = $adminRow->admin_id_number;
                    }
                    ?>
                          <button type="submit" onclick="return admin_department_comment(<?=$DeptSelected->department_topic_id;?>,<?=$admin_id_number?>);" class="btn btn-primary" style="margin-top: 2px;"><span class="glyphicon glyphicon-ok"></span></button>
                        </center>
                  </div>



                </div>

            </div>

       
            <div class="panel-footer"></div>
    </div>
    </div>
  </div>
</div>
<?php } ?>
<!-- ================end===================================== -->
   
