<?php $queryviewProfile=$this->db->get('admin_personel'); foreach($queryviewProfile->result() as $rowProfile): ?>
<div id="view_profile<?= $rowProfile->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">

  <div class="modal-content">
 
      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      
    <div class="modal-body">
     <div class="panel-body">
      <div class="panel panel-default panel-shadow">
      <div class="panel-body">
         <!-- folded -->
      <div class="folded">
      <h2 style="background-color: #337ab7;">Administrator Information</h2>
      </div>
      <!-- end folder -->
      <div class="col-sm-4">
      <?php $this->db->where('id',$rowProfile->id); $queryviewResult=$this->db->get('admin_personel'); 
      foreach($queryviewResult->result() as $rowviewResult): ?>

      <?php if($rowviewResult->image !="no image"){ ?>
      <figure class="cap-bot">
      <img src="<?= base_url();?>upload/user/<?= $rowviewResult->image; ?>" class="img-responsive img-thumbnail img-shadow" height="150" width="150" >
      <figcaption class="word-keep">
      <center>
      <h5 class="word-keep"><?= ucwords(strtoupper($rowviewResult->fname.' '.$rowviewResult->lname));?></h5>
      </center>
      </figcaption>
      </figure>
      <?php }else {?>
      <figure class="cap-bot">
      <img src="<?= base_url();?>img/default_profile.jpg" class="img-responsive img-thumbnail" height="150" width="150" >
      <figcaption class="word-keep">
      <center>
      <h5 class="word-keep"><?= ucwords(strtoupper($rowviewResult->fname.' '.$rowviewResult->lname));?></h5>
      </center>
      </figcaption>
      </figure>
      <?php } ?>

    <?php endforeach; ?>
    </div>
    <div class="col-sm-8">
    <blockquote>
      <h6 class="word-keep"><span class="glyphicon glyphicon-time"></span> <strong>LAST PROFILE UPDATE:</strong> 
      <br>
      <?php if($rowviewResult->date_profile_change !=null){ ?>
      <?= $rowviewResult->date_profile_change; ?>
      <?php }else{ ?>
        <span class="text-danger glyphicon glyphicon-exclamation-sign"></span>
        Profile not yet update
      <?php } ?>
      </h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-lock"></span> <strong>LAST PASSWORD CHANGE:</strong>
      <br>
       
       <?php if($rowviewResult->date_pass_change !=null){?>
        <?= $rowviewResult->date_pass_change; ?>
       <?php }else{?>
          <span class="text-danger glyphicon glyphicon-exclamation-sign"></span>
            Password not yet changed
       <?php } ?>
       </h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-user"></span> <strong>LAST ACCOUNT UPDATE:</strong>
      <br>
       
       <?php if($rowviewResult->account_date_update !=null){ ?>
          <?= $rowviewResult->account_date_update; ?>
       <?php }else{ ?>
          <span class="text-danger glyphicon glyphicon-exclamation-sign"></span>
            Account not yet update
       <?php } ?>
       </h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-user"></span> <strong>NAME:</strong> <?= ucwords(strtolower($rowviewResult->lname.', '.$rowviewResult->fname.' '.$rowviewResult->Mname)); ?></h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-lock"></span> <strong>USERNAME: </strong> <?= $rowviewResult->username;?></h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-envelope"></span> <strong>EMAIL ADDRESS: </strong> <?= $rowviewResult->email;?></h6>
      <h6 class="word-keep"><span class="glyphicon glyphicon-phone"></span> <strong>ID NUMBER: </strong> 
      <?= sprintf('%08.0d',$rowviewResult->admin_id_number);?></h6>
    </blockquote>
    <a href="#update_admin_info<?= $rowviewResult->id; ?>" data-dismiss="modal" data-toggle="modal" class="pull-right btn btn-primary"><span class="glyphicon glyphicon-pencil" data-toggle="tooltip" title="tap to update"></span> Update Information</a>
    <a href="<?= base_url();?>change_password" class="button button-blue button-full"><span class="glyphicon glyphicon-lock"></span> Change Password</a>

    </div>
    </div>
      </div>
      </div>
      </div>
      <div class="modal-footer"></div>
     
  </div>
  </div>
</div>
<?php endforeach; ?>

<!-- update information -->
<?php $queryviewProfile=$this->db->get('admin_personel'); foreach($queryviewProfile->result() as $rowProfile){?>
<div id="update_admin_info<?= $rowProfile->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-sm">
<div class="modal-content">
<div class="modal-header">
 <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
</div>
<div class="modal-body">
<div class="panel-body">

<?php echo form_open_multipart(base_url().'account_update');?>
<input type="hidden" name="id" value="<?php echo $rowProfile->id;?>">

<?php $this->db->where('id',$rowProfile->id); $query = $this->db->get('admin_personel'); foreach($query->result() as $result){?>

<div class="form-group">
<label>Firstname</label>
<input type="text" name="fname" class="form-control" style="border-radius: 0px;" required value="<?= $result->fname;?>">
</div>
<div class="form-group">
<label>Lastname</label>
<input type="text" name="lname" class="form-control" style="border-radius: 0px;" required value="<?= $result->lname;?>">
</div>
<div class="form-group">
<label>Middle name</label>
<input type="text" name="Mname" class="form-control" style="border-radius: 0px;" required value="<?= $result->Mname;?>">
</div>
<div class="form-group">
<label>Email Address</label>
<input type="email" name="email" class="form-control" style="border-radius: 0px;" required value="<?= $result->email;?>">
</div>
<div class="form-group">
<label>ID Number</label>
<input type="number" name="admin_id_number" class="form-control" style="border-radius: 0px;" required value="<?= $result->admin_id_number; ?>">
</div>


<?php } ?>
</div>
</div>
<div class="modal-footer">
<button type="submit" class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Update</button>
</div>
<?php echo form_close();?>
</div>
</div>
</div>
<?php }?>