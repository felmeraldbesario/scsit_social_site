<!-- ======================modal=================== -->
<?php $query= $this->db->get('admin_messaging'); foreach($query->result() as $value){?>
  <div id="del_message<?=$value->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href="#"onclick="return delete_action_message(<?=$value->id?>)" data-dismiss="modal"class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
         </div>
     
     
  </div>
  </div>
  </div>
  <?php } ?>

<?php $query= $this->db->get('admin_messaging'); foreach($query->result() as $value):?>
  <div id="forward_message<?= $value->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

      </div>
      <div class="modal-body">
      <div class="panel-body">
      <h5 style="font-weight: bold;" class="text-muted"> <span class="text-success glyphicon glyphicon-envelope"></span> Forward Message</h5>
<!-- here -->
<?php echo form_open_multipart(base_url().'dept_message_forward'); ?>
  <input type="hidden" name="sender_id" value="<?php echo $user_id;?>">
  

      <div class="form-group">
      <p>Please Select Receiver</p>
     <select class="form-control" name="receiver_id" required="">
    <option></option>
     <?php $this->db->where('department_id',$dept_id); $queryDepartmentname=$this->db->get('user'); foreach($queryDepartmentname->result() as $rowDeptname): ?>
     <option value="<?php echo $rowDeptname->id;?>">
     <?php echo sprintf('%08d',$rowDeptname->id_number).' '.ucwords(ucfirst($rowDeptname->firstname.' '.$rowDeptname->lastname));?>
     </option>
     <?php endforeach; ?>

     <?php $query = $this->db->get('super_admin_info'); foreach($query->result() as $rowsInfo){?>
      <option>Super Admin - <?=$rowsInfo->username;?></option>
     <?php } ?>

     </select>
      </div>


      <?php 
      $this->db->where('id',$value->id);
      $queryForward = $this->db->get('admin_messaging');
      foreach($queryForward->result() as $value1):
       ?>

   
     <?php if($value1->image != "no image"):?>
     <img src="<?= base_url(); ?>upload/user/<?php echo $value1->image; ?>" class="img-responsive img-thumbnail" style="height: 100px;width: 30%">
     </label>
     <?php endif;?>
   
         <div class="form-group">
         <label class="text-muted">Change Image</label>
         <input type="file" name="userfile" class="filestyle" data-buttonName="btn-warning">
         </div>

      <div class="form-group">
      <p><span class="text-warning glyphicon glyphicon-pencil"></span> Edit Message</p>
      <textarea class="form-control" rows="5" cols="5" name="message" required style="border: 1px solid #f2f2f2;"><?php echo $value1->message;?></textarea>
      </div>
      <div class="form-group">
      <button class="btn btn-primary pull-right" type="submit"><span class="glyphicon glyphicon-send"></span> Send</button>
      </div>
     <?php endforeach;?>
<!-- end here -->
     <?php echo form_close();?>

      </div>
      </div>
      <div class="modal-footer">
      </div>
     
  </div>
  </div>
  </div>


<?php endforeach;?>