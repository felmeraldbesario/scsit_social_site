
<div id="feladdnews" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>

          <?php echo form_open_multipart(base_url().'insert_news');?>
          <input type="hidden" name="admin_id" value="<?php echo $user_id;?>">
         

         <div class="inner-addon left-addon">
         <?php echo form_error('title');?>
         <label class="text-muted">Title</label>
         <p>
         <input type="text" name="title" class="form-control" required placeholder="Title.." >
         <span style="display:none;color:red;">Put Title Correctly</span>
         </p>
         </div>
<!--for ajax uploading image-->
         <div class="form-group">
         <label class="text-muted">News Image</label>
         <input type="file" name="userfile" class="filestyle" data-buttonName="btn-warning" id="profilepic_id">
         </div>
          
        <label for="profilepic_id">
        <img src="<?= base_url();?>img/def_pic.png" class="img-responsive news_img_default pull-left" id="profilepic_img"/>
        </label>
<!-- end for ajax uploading image-->

         <div class="inner-addon left-addon">
         <?php echo form_error('news_body');?>
         <label class="text-muted">Description</label>
         <p>

         <textarea cols="5" rows="5" name="news_body" class="form-control" required="" Placeholder="Add description here"></textarea>
          <span style="display:none;color:red;">Put Description Correctly</span>
        </p>
         </div>

        <div class="form-group">
         <button class="button button-black " type="reset"><span class="glyphicon glyphicon-erase" data-toggle="tooltip" title="RESET"></span></button>
         <button type="submit" value="Add" class="pull-right button button-blue"><span class="glyphicon glyphicon-plus" data-toggle="tooltip" title="ADD"></span></button>
         </div>


         <?php echo form_close();?>
        <br><br>
      </div>
     
  </div>
  </div>
</div>

<?php
$queryReadnews=$this->db->get('news');
foreach($queryReadnews->result() as $rowNews){
 ?>

<div id="felnewsreadmore<?php echo $rowNews->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
        <?php
        $this->db->where('id',$rowNews->id);
        $queryResult=$this->db->get('news');
        foreach($queryResult->result() as $rowResults){
         ?>
        
        <h4 class="word-keep"><span class="glyphicon glyphicon-tag"></span> <?php echo nl2br(strtoupper($rowResults->title));?></h4>
        <h6><span class="glyphicon glyphicon-time"></span> <?php echo $rowResults->date;?></h6>
        <br>
        <br>

        <p class="word-keep">
        <?php echo nl2br(ucfirst($rowResults->news_body));?>
          
        </p>
        <?php } ?>

         

         

      </div>
     
  </div>
  </div>
</div>

<?php } ?>

<?php
$queryUpdate=$this->db->get('news');
foreach($queryUpdate->result() as $rowNews){
 ?>
<div id="fel_update_news<?php echo $rowNews->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br><br>
         <div class="alert alert-success" role="alert">Update File</div>
         
         <?php
         $this->db->where('id',$rowNews->id);
         $queryEdit=$this->db->get('news');
         foreach($queryEdit->result() as $rowedit){
          ?>

          <?php echo form_open_multipart(base_url().'fel_updating_news');?>
          <input type="hidden" name="id" value="<?php echo $rowedit->id;?>">

        

          <div class="form-group">
          <label class="text-muted"> Title</label>
          <input type="text" name="title" placeholder="Title" class="form-control" value="<?php echo $rowedit->title; ?>" id="dontPressenter"onkeypress="dontPressenterha(event);">
          </div>
          <div class="form-group">
          <?php 
          if($rowedit->image !="no image"){
          ?>
          <img src="<?= base_url();?>upload/user/<?php echo $rowedit->image; ?>" class="img-responsive update_hyt">
          <?php }else{ ?>
          <img src="<?= base_url();?>img/def_pic.png" class="img-responsive update_hyt">
          <?php } ?>

          <label class="text-muted">Change Image</label>
          <input type="file" name="userfile">
          </div>
          <div class="form-group">
          <label class="text-muted">Description</label>
          <textarea cols="5" rows="5" name="news_body" class="form-control"><?php echo $rowedit->news_body;?></textarea>
          </div>

          <div class="form-group">
             <button type="submit" class="button button-blue pull-right"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="UPDATE"></span></button>
             <br>
          </div>
        <?php }echo form_close(); ?>



      </div>
     
  </div>
  </div>
</div>
<?php } ?>

<?php 
$queryNews=$this->db->get('news');
foreach($queryNews->result() as $rowNews){
?>

<div id="fel_remove_news<?= $rowNews->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

  <div class="modal-header">

         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href="delete_news?id=<?= $rowNews->id; ?>"class="button button-red button-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="REMOVE" ></span></a>&nbsp 
         <button data-dismiss="modal"class="button button-black button-xs pull-right"><span class="glyphicon glyphicon-off" data-toggle="tooltip" title="DISMISS"></span></button>
      </div>
     
  </div>
  </div>
</div>

<?php } ?>


