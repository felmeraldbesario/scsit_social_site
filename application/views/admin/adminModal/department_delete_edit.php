<?php foreach($getAll as $DeptSelected): ?>
<div id="delete_dept_status<?= $DeptSelected->id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>         
      </div>
      <div class="panel-body">
      <p class="text-danger">Are you sure you want to delete ?</p>
      </div>
      <div class="panel-footer">
      <a href="delete_dept_status?id=<?= $DeptSelected->id; ?>" class="btn btn-danger"><span class="glyphicon glyphicon-ok"></span></a>
      </div>
     
  </div>
  </div>
</div>
<?php endforeach; ?>
<!-- edit -->
<?php foreach($getAll as $DeptSelected):?>
<div id="edit_dept_status<?= $DeptSelected->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>         
      </div>
      <div class="panel-body">
      <?php 
      $this->db->where('id',$DeptSelected->id);
      $query = $this->db->get('department_topic');
      foreach($query->result() as $rowResult):
      ?>
    <?php echo form_open_multipart(base_url().'edit_departmentPost');?>
    <input type="hidden" name="id" value="<?php echo $rowResult->id;?>">

      <div class="col-sm-12 col-md-12 col-lg-12">
      <p>Current Image Uploaded</p>
      <?php  if($rowResult->image !="no image"){ ?>
      <img src="<?= base_url(); ?>upload/user/<?= $rowResult->image;?>" class="img-responsive img-thumbnail" style="width: 100%; height: 180px;">
      <?php }else{ ?>
      <h4 class="text-muted"><span class="glyphicon glyphicon-picture"></span> No image attached</h4>
      <?php }?>

      <label class="sr-only" for="inputfile">Change Photo?</label>
      <input type="file" name="userfile">
      </div>
      
      <div class="form-group">

      <label style="margin-top: 10px;"><span class="text-primary glyphicon glyphicon-pencil"></span> Status</label>
      <textarea class="form-control" name="department_topic_body" cols="4" rows="4"><?= $rowResult->department_topic; ?></textarea>
      </div>
      <button type="submit"  class="btn btn-primary"><span class="glyphicon glyphicon-ok"></span> Update</button>
    <?php echo form_close();?>
    <?php endforeach; ?>

      </div>
      <div class="panel-footer">
     
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>