<div id="admin_contact" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      <div class="modal-body">
      <div class="panel-body">

      <?php $queryContact = $this->db->query('SELECT id, contact FROM locator ORDER BY id DESC');
      foreach($queryContact->result() as $rowContact): ?>
      <p style="word-break: keep-all;"><?= ucwords(strtolower($rowContact->contact)); ?></p>
      <?php endforeach; ?>

      </div>
      </div>  
      <div class="modal-footer">
      <h6 class="text-muted">
      &copy 2015 - <?= date("Y")?> SCSIT SOCIAL SITE ADMINISTRATOR
      </h6>
      </div>
     
  </div>
  </div>
</div>

<!-- locator -->
<div id="admin_locator" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
    
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <?php $queryLocator = $this->db->query('SELECT id, locator FROM locator ORDER BY id ASC');
      foreach($queryLocator->result() as $rowLocator): ?>
      <p style="word-break: keep-all;"><?= ucwords(strtolower($rowLocator->locator)); ?></p>
       <?php endforeach; ?>
      </div>
      </div>
      <div class="modal-footer">
      <h6 class="text-muted">
      &copy 2015 - <?= date("Y")?> SCSIT SOCIAL SITE ADMINISTRATOR
      </h6>
      </div>
     
  </div>
  </div>
</div>

<!-- information -->
<div id="admin_information" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
    
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <?php $queryInformation = $this->db->query('SELECT id, information FROM locator ORDER BY id DESC');
      foreach($queryInformation->result() as $rowInfo): ?>
      <p style="word-break: keep-all;"><?= ucwords(strtolower($rowInfo->information)); ?></p>
    <?php endforeach; ?>
      </div>
      </div>
      <div class="modal-footer">
      <h6 class="text-muted">
      &copy 2015 - <?= date("Y")?> SCSIT SOCIAL SITE ADMINISTRATOR
      </h6>
      </div>
     
  </div>
  </div>
</div>