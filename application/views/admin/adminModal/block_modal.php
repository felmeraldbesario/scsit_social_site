
<?php 
$userQuery = $this->db->get('user'); foreach($userQuery->result() as $rowDeptList):
?>
 <div class="modal fade" id="fel_block_user_modal<?= $rowDeptList->id;?>" tab-index="-1" role="dialog">
            <div class="modal-dialog modal-sm">
                <div class="modal-content">
                <div class="modal-header">
                 <img src="img/circle_icon/close.png"class="close"style="height:20px;"data-dismiss="modal">
                 <br>
                </div>
                    <div class="modal-body">
<?php echo form_open(base_url().'block_user');?>
<div class="panel-body">
<img src="img/circle_icon/warning.png"class="img-responsive"alt="warning"style="height:25px;">
<h5 class="text-danger"style="margin-left:40px;margin-top:-20px;">
Are you sure to block this user?
</h5>
<?php 
$this->db->where('id',$rowDeptList->id);
$query =   $this->db->get('user');
foreach($query->result() as $rows):
    $id=$rowDeptList->id;
?>
Name: <?= ucwords(strtolower($rows->firstname.' '.$rows->middlename.' '.$rows->lastname)); ?><br>
Age: <?= $rows->age; ?><br>
Gender: <?= ucwords($rows->gender); ?><br>
Address: <?= ucwords(strtolower($rows->address));?>
<?php endforeach; ?>
<hr>
<label class="text-muted">Select Reason</label>
<select class="form-control"name="reason"required="">
 <option></option>
<?php $query=$this->db->get('block_reason'); foreach($query->result() as $rows3): ?>
  <option value="<?php echo $rows3->id;?>"><?php echo ucfirst(strtolower($rows3->reason));?></option>
  <?php endforeach; ?>
</select>

                            

                    </div>
                    <div class="modal-footer">

<input type="hidden"name="blocked_user_id"value="<?php echo $id;?>">
<input type="hidden"name="admin_id"value="<?php echo $user_id;?>">
<button type="submit" class="button button-red pull-left"><span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="BLOCK"></span></button>
<?php echo form_close();?>
<a href="#user" class="button button-black"data-toggle="modal"data-dismiss="modal">
<span class="glyphicon glyphicon-off" data-toggle="tooltip" title="DISMISS"></span></a>
</div>
                    </div>
                </div>
            </div>
        </div>
<!-- end ni -->
<?php endforeach;
?>
