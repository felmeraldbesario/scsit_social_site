<div id="inbox" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
<div class="modal-dialog modal-lg">
<div class="modal-content">
<div class="modal-header">
<span class="text-danger glyphicon glyphicon-envelope"></span> Super administrator Conversation
 <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>

</div>
<div class="modal-body">
<div class="panel-body">
<!-- table -->
<div class="table-responsive">
<table class="table table-hover table-bordered">
<thead>
	<tr>
		<th><span class="glyphicon glyphicon-user"></span> Super Admin</th>
		<th><span class="glyphicon glyphicon-time"></span> Date Sent</th>
		<th><span class="glyphicon glyphicon-envelope"></span> Message</th>
		<th><span class="glyphicon glyphicon-exclamation-sign"></span></th>
	</tr>
</thead>
<tbody>
<?php foreach($inbox as $value){  ?>
	<tr>
		<td>Super Admin - <?=ucfirst($value->username);?></td>
		<td><?= $value->message_date;?></td>
		<td><?= nl2br(ucfirst($value->message)); ?></td>
		<td>
		<a href="#replyMessage<?= $value->sender_id; ?>" data-dismiss="modal" data-toggle="modal">
		<span class="text-success glyphicon glyphicon-comment" data-toggle="tooltip" title="Press to Reply"></span>
		</a>

		</td>
	</tr>
	<?php  } ?>	
</tbody>
	
</table>
</div>

</div>
</div>
<div class="modal-footer">
</div>
</div>
</div>
</div>

<!-- reply message sa super administrator -->
<?php foreach($inbox as $value){  ?>
<div id="replyMessage<?= $value->sender_id; ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
			<span class="glyphicon glyphicon-comment"></span> Reply Message
				<img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         		<br>
				</div>
				<div class="modal-body">
					<div class="panel-body">
						<div class="panel panel-default panel-shadow">
							<div class="panel-body">

									<?php echo form_open_multipart(base_url().'reply_message'); ?>
  									<input type="hidden" name="sender_id" value="<?php echo $user_id;?>">

  							<div class="form-group">
  								<select class="form-control" disabled name="receiver_id">
  									<?php foreach($superAdminInfo as $value){ ?>
								    <!-- super admin Information -->
								    <option>Super Admin - <?= ucfirst(strtolower($value->username)); ?></option>
								    <!-- end super admin -->
								    <?php } ?>
  								</select>
  							</div>

								<div class="form-group">
									<textarea cols="5" rows="5" name="message" class="form-control" required style="border:1px solid #f2f2f2;" placeholder="Write reply message here...."></textarea>
								</div>
								
									<div class="form-group">
										<button type="submit" class="btn btn-primary pull-right"><span class="glyphicon glyphicon-ok"></span> Send</button>
									</div>

									<?php echo form_close(); ?>
							</div>
						</div>
					</div>

				</div>
					<div class="modal-footer">
					</div>
			
		</div>
		
	</div>
</div>
	<?php  } ?>	