<!-- update profile pic -->
<?php
$queryUpdateProfile=$this->db->get('admin_personel');
foreach($queryUpdateProfile->result() as $rowProfile):
?>
<div id="updateProfile<?=$rowProfile->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
 Update Profile
      </div>
      <div class="modal-body">
      <div class="panel-body">
          <?php echo form_open_multipart(base_url().'Update_profile_dept_admin');?>
          <input type="hidden" name="id" value="<?php echo $rowProfile->id;?>">
            
            

           <div class="col-sm-12">
           <!-- <input type="file" name="userfile" id="profilepic_id">
 -->
           <!-- <label for="profilepic_id">
              <img class="img-responsive" id="profilepic_img" style="height: 180px;width: 100%;">
            </label>
            <div class="form-group">
            <input type="file" name="userfile" class="filestyle" data-buttonName="btn-primary" id="profilepic_id">
            </div> -->
            <div class="col-sm-6 col-md-6 col-lg-6">
            <div class="well panel-shadow" style="border-radius: 0px;">
            <!-- folded -->
              <div class="folded">
              <h2 style="background-color: #337ab7;"><span class="glyphicon glyphicon-picture"></span> Current Image</h2>
              </div>
              <!-- end folded -->

            
            <?php $this->db->where('id',$rowProfile->id); $query = $this->db->get('admin_personel'); foreach($query->result() as $row): ?>
            <img src="<?= base_url(); ?>upload/user/<?= $row->image;?>" class="img-responsive img-thumbnail" alt="Profile" style="height: 150px; width: 100%;">
             <?php endforeach;?>
             </div>
             </div>

             <div class="col-sm-6 col-md-6 col-lg-6">
             <div class="well panel-shadow" style="border-radius: 0px;">
             <div class="folded">
               <h2 style="background-color: #337ab7;"><span class="glyphicon glyphicon-upload"></span> Update Photo?</h2>
             </div>
             <br>
             <input type='file' name="userfile" accept='image/*' onchange='openFile(event)'>
             <img id='output' style="height: 90px;width: 100%;margin-top: 5px; border: 0px;">
             </div>
           </div>
           </div>
    

          </div>
      </div>
      <div class="modal-footer">
          <div>
           <button type="submit"value="Update"class="btn btn-primary">
           <span class="glyphicon glyphicon-ok" data-toggle="tooltip" title="PRESS TO UPDATE"></span></button>
          <?php echo form_close();?>
      </div>  
      </div>
  </div>
  </div>
</div>
<?php endforeach; ?>
<!-- end here -->
