<script type="text/javascript">
	
	function Mysignupvalidation(){

		if(document.signupValidation.fname.value==""){
			alert("Please provide your firstname");
			document.signupValidation.fname.focus();
			return false;
		}
		if(document.signupValidation.lname.value==""){
			alert("Please provide your lastname");
			document.signupValidation.lname.focus();
			return false;
		}
		if(document.signupValidation.Mname.value==""){
			alert("Please provide your Middle name");
			document.signupValidation.Mname.focus();
			return false;
		}
		if(document.signupValidation.assigned_department.value=="-1"){
			alert("Please select your department");
			document.signupValidation.assigned_department.focus();
			return false;
		}
		// for @ email validation
		var emailID = document.signupValidation.email.value;
		atpos=emailID.indexOf("@");
		dotpos=emailID.indexOf(".");
		if(atpos < 1 || (dotpos-atpos<2))
		{
			alert("Please provide your email address");
			document.signupValidation.email.focus();
			return false;
		}
		if(document.signupValidation.admin_id_number.value==""){
			alert("Please provide your ID number");
			document.signupValidation.admin_id_number.focus();
			return false;

		}
		if(document.signupValidation.username.value==""){
			alert("Please provide your username");
			document.signupValidation.username.focus();
			return false;
		}
		if(document.signupValidation.password.value==""){
			alert("Please provide your password");
			document.signupValidation.password.focus();
			return false;
		}

		return(true);
	}

</script>
