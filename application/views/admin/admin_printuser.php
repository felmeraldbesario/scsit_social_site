<?php include 'admin_session.php';?>

<body>

<div class="container">

<div class="row print-row-margin">


<div class="col-sm-2 col-md-2 col-lg-2"></div>

<div class="col-sm-8 col-md-8 col-lg-8">

<div class="panel-body" style="margin-top: 50px;">


<div class="col-sm-2 col-md-2 col-lg-2">
<img src="<?= base_url();?>img/logo.png" class="img-resposive img-logo-size" alt="Logo" style="margin-top: 10px;">
</div>
<!-- PRINT STARTS HERE -->
<div class="col-sm-8 col-md-8 col-lg-8">
<h4 class="text-muted text-center">
	Salazar Colleges of Science and Institute of Technology
</h4>
<h5 class="text-muted text-center">
211 N. Bacalso., Avenue Cebu City
</h5>
<h5 class="text-primary text-center">
                      <?php foreach($joinedPrinterDepartmentname as $printingUser){?>
                        Social Site <?php echo ucfirst(strtolower($printingUser->department)); ?> Department User's Record
                      <?php } ?>
 
</h5>



</div>

<div class="col-sm-12">

<br>



<?php 
  $id = $this->input->get('id'); 
  foreach($this->admin_library->joinedblockedname($id) as $printingUser){?>

<p style="word-break:keep-all; text-indent: 50px;margin-top:45px;">
<strong>
<?= ucwords(ucfirst(strtolower($printingUser->firstname.' '.$printingUser->middlename.' '.$printingUser->lastname))); ?></strong>, <?= $printingUser->age; ?> years of age and a <?= ucfirst($printingUser->member_sex);?> member of Salazar Colleges of Science and Institute of Technology, Social Site. We're Sorry, you are blocked on <strong> <?= $printingUser->blocked_date; ?></strong> of <strong><?= $printingUser->reason; ?></strong>
</p>

<p style="word-break:keep-all; text-indent: 50px;margin-top:5px;">
For the assistance please do contact the Department administrator personel in this email address <strong><?= $printingUser->adminEmailAddress;?></strong>
</p>

  <?php } ?>





<h6 class="text-danger pull-right" style="margin-top:50px;"><button type="submit" class="btn btn-default" onclick="javascript:window.print();"><span class="glyphicon glyphicon-print"></span> Printed On : 
<span id="date_time"></span>
<script type="text/javascript">window.onload = date_time('date_time');</script>
</button></h6>

<h6 class="text-muted" style="margin-top: 70px;"><?php include'copyright.php';?>
 <br></h6>

<!-- PRINT ENDS HERE -->



  

  


</div>








</div>

</div>









</div>

</div>
</body>