<?php include 'admin_session.php';?>


<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'admin_sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'admin_nav_menu_top.php';?>
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                      

                        <div class="col-sm-5">
                        <div class="panel panel-default panelStyle panel-shadow">
                        <div class="panel-body">
                        <div class="folded">
                             <h2 style="background-color: #337ab7;"><span class="glyphicon glyphicon-plus text-info"></span> Add Course</h2>
                        </div>
                        <br>
                       
                        <hr class="hrmargin">
                         <?php include'alerts/course_added_alert.php';?>
                         <?= $this->session->flashdata('deletez'); ?>
                         <?= $this->session->flashdata('Updatez_course');?>

                         <?php echo form_open(base_url().'add_course');?>

                         <input type="hidden" name="department_id" value="<?php echo $dept_id;?>">

                        <div class="form-group">
                      
                        <p>
                        <input type="text" name="course" class="form-control" placeholder="Type New Department Course" id="keypress" onkeypress="triggers(event);">
                         <span style="display:none;color:red;">Add Department Course</span>
                        </p>
                        <small class="text-danger"><?= form_error('course');?></small>
                        </div>
                        <div class="form-group">
                        <button type="submit" class="button button-blue"><span class="glyphicon glyphicon-plus" data-toggle="tooltip" title="ADD COURSE"></span></button>
                        </div>
                        
                        <?php echo form_close();?>


                        <div class="table-responsive">
                        <table class="table table-hover">
                        <thead>
                          <tr>
                            <th>Date Added</th>
                            <th>Course</th>
                            <th>Action</th>
                          </tr>
                        </thead>
                        <tbody>
  <?php foreach($viewCourse as $DeptSelected): 
  if($DeptSelected->department_id == $dept_id):
    ?>
                          <tr>
                        
                            <td>
                            <?= $DeptSelected->date_added;?>
                            </td>
                            <td>
                          
                            <?php 
                            $course = character_limiter(strtoupper($DeptSelected->course),8);
                            echo $course;
                            ?>
                            </td>
                            <td>
                              <a href="#felcourseEdit<?= $DeptSelected->id; ?>" data-toggle="modal" class="text-primary"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="Update this?"></span></a>&nbsp
                              <a href="#delete_course<?= $DeptSelected->id;?>" data-toggle="modal" class="text-danger"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="Delete this ?"></span></a>
                            </td>

                          </tr>
 <?php endif; endforeach; ?>
                          
                        </tbody>
                          
                        </table>
                        </div>
                        </div>
                        </div>

                          <div class="panel panel-default panel-shadow">
                            <div class="panel-body">
                              <h5 class="text-muted"><span class="glyphicon glyphicon-time"></span> Department Log's</h5>
                                <hr>
                                   
                                <!-- sa latest time ni -->
                                      <?php

                                      if($status_time_logs_latest->num_rows() !=0){
                                        foreach($status_time_logs_latest->result() as $data):
                                            echo '<h6 class="text-primary" style="word-break: keep-all;"><span class="glyphicon glyphicon-time"></span> Latest Date Posted: '.$data->latest_date.'</h6>';
                                          endforeach;
                                      }else{
                                        echo '<h5 class="text-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> You havent Posted </h5>';
                                      }

                                       ?>
                              <!-- end sa latest time -->
                                      

                                      <?php 
                                       
                                        foreach($status_time_logs_old as $DeptSelected){
                                        
                                        ?>
                                       <h6 class="text-muted" style="word-break: keep-all;"><span class="glyphicon glyphicon-time"></span> Previous Date Posted: <?= $DeptSelected->dateOldPost; ?></h6>
                                       <?php } ?>
                                
                            </div>
                          </div>

                        


                        </div>

                        <div class="col-sm-7">
                        <div class="well panel-shadow">


                        <?php echo form_open_multipart(base_url().'add_department_status');?>

                        <?php foreach($departmentQuery as $DeptSelected):
                        if($DeptSelected->id == $user_id):
                        ?>
                
                        <input type="hidden" name="department" value="<?php echo $DeptSelected->assigned_department;?>">
                        <input type="hidden" name="admin_id_number" value="<?php echo $DeptSelected->admin_id_number;?>">

                        <?php 
                        endif;
                        endforeach;?>

                        <input type="hidden" name="user_id" value="<?php echo $user_id;?>">

                        <div class="form-group">
                        <textarea class="form-control" name="department_topic_body" id="department_topic_body" row="3" col="3" required placeholder="What's on your mind?" required=""></textarea>
                        </div>

                        <div class="form-group">
                        <input type="file" name="userfile">
                        </div>

                        <div class="form-group">
                        <!-- and mga button nga type submit ky g.butangan nkug anti spamming -->
                        <button type="submit" class="btn btn-primary" id="btn_tweet"><span class="glyphicon glyphicon-share-alt"></span> Share</button>
                        </div>
                        
                        <?php echo form_close();?>


                        </div>
                        <?= $this->session->flashdata('walayImage');?>
                        <?= $this->session->flashdata('delete_success');?>
                        <?= $this->session->flashdata('status_success');?>

                        <?php foreach($this->admin_library->Selecting_department_profile_status($dept_id) as $DeptSelected){?>
                        <!-- ====== start here====== -->
                        <div class="panel panel-default panel-shadow" id="ajaxAutoRefresh">
                        <div class="panel-body">

                              

                <ul class="list-inline">
                                  <li>
                                  <?php if($this->admin_department_post->department_post_admin($DeptSelected->department_user_id,$DeptSelected->admin_id_number)){
                                    if($this->admin_department_post->department_admin_image($DeptSelected->admin_id_number)){
                                    ?>
                                 <img src="<?php base_url();?>upload/user/<?=$this->admin_department_post->department_admin_image($DeptSelected->admin_id_number)?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                    <?php }else{
                                      ?>
                                 <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                      <?php
                                      } ?>
                                 </li>
                                 <li>
                                 <h5><strong>Admin</strong></h5>
                                 <?php }else if($DeptSelected->department_user_id != "0" ){
                                  if($this->admin_department_post->get_user_image($DeptSelected->department_user_id)){
                                  ?>
                                  <img src="<?php base_url();?>upload/user/<?=$this->admin_department_post->get_user_image($DeptSelected->department_user_id)?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php } ?>
                                 </li>
                                 <li>
                                 <h5><strong> <?=ucfirst(strtolower($DeptSelected->firstname))." ".ucfirst(strtolower($DeptSelected->lastname))?></strong></h5>
                                 
                                  <?php
                                  }else{
                                  if($this->admin_department_post->get_superadmin_image() != "" || $this->admin_department_post->get_superadmin_image() != "no image"){
                                  ?>
                                  <img src="<?php base_url();?>upload/user/<?=$this->admin_department_post->get_superadmin_image()?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php } ?>
                                  </li>
                                 <li>
                                 <h5><strong>Super Admin</strong></h5>
                                   
                                    <?php
                                    } ?>
                                 <h6 class="text-warning"> <span class="glyphicon glyphicon-time"></span> <?=$DeptSelected->department_topic_date?></h6>
                                 </li>
  
                                 </ul>
  
                      
                        <!-- dropdown -->
                         <div class="dropdown pull-right" style="margin-top: -50px;">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                                <ul class="dropdown-menu bodycolor">
                                <div class="panel-body">
                                <a href="#delete_dept_status<?= $DeptSelected->department_topic_id; ?>" data-toggle="modal" class="text-danger"><span class="glyphicon glyphicon-trash" data-toggle="tooltip" title="TRASH"></span></a>
                                <br>
                                <a href="#edit_dept_status<?= $DeptSelected->department_topic_id;?>" data-toggle="modal" class="text-primary"><span class="glyphicon glyphicon-edit" data-toggle="tooltip" title="UPDATE"></span></a>
                                </div>
                                </ul>
                         </div>
                      <!-- dropdown -->
                 
                        <hr>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <?php if($DeptSelected->department_topic_image !="no image"):?>
                          <center>
                        <img src="<?= base_url();?>upload/user/<?= $DeptSelected->department_topic_image; ?>" style="width: 100%; height: 150px;">
                        </center>
                        <?php endif; ?>
                        <p style="word-break: keep-all;margin-top: 5px;">
                          <?= nl2br(ucfirst(strtolower($DeptSelected->department_topic)));?>
                        </p>
                        
                        </div>

                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <br>
                        <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                        <tbody>
                          <tr>
                            <td>
                            <span id="likers_load<?= $DeptSelected->department_topic_id; ?>">
                            <a href="#department_like<?= $DeptSelected->department_topic_id;?>" data-toggle="modal" class="btn btn-default" onclick="return likers_load(<?= $DeptSelected->department_topic_id; ?>)'">
                            <span class="text-primary glyphicon glyphicon-thumbs-up" data-toggle="tooltip" title="View Person Who Liked"></span> 
                            
                            <?php 
                             $this->db->where('department_topic_id',$DeptSelected->department_topic_id);
                              echo $this->db->count_all_results('department_topic_feedback');
                            ?>
                      
                            </a>
                            </span>
                            <!-- new ni dre ha -->
                            <span id="comment_load<?= $DeptSelected->department_topic_id ?>">
                            <a href="#newComment<?= $DeptSelected->department_topic_id; ?>" data-toggle="modal" class="btn btn-default" onclick="return comment_load(<?= $DeptSelected->department_topic_id ?>);">
                            <span class="text-success glyphicon glyphicon-comment"></span> 
                            <?php 
                           $this->db->where('department_topic_id',$DeptSelected->department_topic_id);
                              echo $this->db->count_all_results('department_topic_comments');
                                 ?>
                            </a>
                             </span>
                           <!-- end sa new ni dre -->


                            </td>
                          </tr>
                        </tbody>
                          
                        </table>
                        </div>
                        </div>

                        </div>
                        </div>

                        <!-- ==============end here========= -->
                        <?php 
                      }
                    
                        ?>




                       

                        </div>


                        
                       
                         




                       </div><!--/row-->

                      
                         <!-- footer dre hahaha -->
                         <hr>
<?php include'copyright.php';?>
 <br>
<?php include 'admin_locator.php';?>

</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>








    <?php include 'adminModal/departmentmodal.php';?>
    <?php include 'adminModal/department_like_comment.php';?>
    <?php include 'adminModal/department_delete_edit.php';?>
    <script src="<?= base_url();?>rald_js/admin_department_reply.js"></script>
    <script src="<?=  base_url();?>rald_js/felAjaxload.js"></script>
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
 <!--  <script src="<?= base_url();?>rald_js/refresh.js"></script> -->
    <script src="<?php echo base_url();?>rald_js/admin_kill_enter.js"></script>
    







