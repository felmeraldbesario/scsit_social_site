
	<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                        <div class="col-sm-12">
                        <div class="breadcrumb">
                        <li><a href="<?php base_url();?>newspage">News</a></li>
                        <li><a href="<?php base_url();?>sports">Sports</a></li>
                        <li><a href="<?php base_url();?>entertainment">Entertainment</a></li>
                        <li class="active">Editorial</li>
                     
                        </div>
                          </div>
                        <?php $user_id = $this->session->userdata('login_id'); ?>
                          <!-- main col right ryt ni -->
                          <?php
                          $SelectEditorial = $this->db->get('editorial_new');
                          $CountEditorialTopic = $SelectEditorial->num_rows();
                          // check if naay ni exist nga topic kani nga page
                          if($CountEditorialTopic != 0){ 
                          foreach ($Selecteditorial as $Roweditorial) {
                          ?>
                          <span id="editorial_content<?php echo $Roweditorial->editorial_id; ?>">
                          <?php
                          //kaning num_of_comment_func para ni sa ajax aron ma identify nato
                          //kung unsa nga content iyang e refresh..
                          $this->db->where('topic_id',$Roweditorial->editorial_id);
                          $selectcomments = $this->db->get('editorial_comment');
                          $num_of_comments = $selectcomments->num_rows();
                          ?>
                          <div class="col-sm-6">
                               <div class="well">
                               <div class="row">

                               <div class="col-sm-8">
                               <label class="text-muted">
                               <span class="glyphicon glyphicon-tag"></span><?php echo $Roweditorial->title; ?></label>

                               <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <?php echo $Roweditorial->date_s; ?></h6>
                               <a href="#readmore_editorial<?php echo $Roweditorial->editorial_id; ?>"data-toggle="modal"class="btn btn-sm btn-primary" onclick="count_viewsfunc_editorial(<?php echo $Roweditorial->editorial_id; ?>)"><span class="glyphicon glyphicon-plus-sign"></span> Read more...</a>
                               
                               </div>

                               <div class="col-sm-4">
                               <input type="hidden" id="topic_id" value="<?php echo $Roweditorial->editorial_id; ?>">
                               <input type="hidden" id="user_id" value="<?php echo $user_id; ?>">
                               <?php
                               //if naay profile pic ang admin atung e output
                               if($Roweditorial->image != null){
                               ?>
                               <img src="<?php echo base_url().'upload/user/'.$Roweditorial->image;?>"class="img-responsive"style="height:150px;width:200px;">
                               <?php
                               //if wala kay atung e output ang default profile pic
                               }else{
                               ?>
                               <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"style="height:150px;width:200px;">
                               <?php
                               }
                               ?>
                               </div>

                               <!-- for comment queries -->
                               <!-- 
                                # Note
                                Atleast 2 ka comment ang mo display ang sunod ky sa toggle slide na
                                -->
                              <!-- for like and comments functions-->
                              <div class="table responsive">
                              <br>
                                    <table class="table table-hover table-bordered">
                                      <tr>
                                        <td>
                                          
                                           <a href="#" data-toggle="tooltip" title="Like post?" data-placement="left"><img src="<?php base_url();?>img/circle_icon/like.png"class="img-responsive hover"alt="like"style="height:20px;width:20px" data-toggle="popover" onclick="like_func_editorial(<?php echo $Roweditorial->editorial_id; ?>)"></a><a href="#view_likes_editorial<?php echo $Roweditorial->editorial_id; ?>" data-toggle="modal" onclick="view_modallikes_editorial(<?php echo $Roweditorial->editorial_id; ?>)"><h6 class="text-primary"style="margin-left:25px;margin-top:-15px;" id="result_editorial<?php echo $Roweditorial->editorial_id; ?>"><?php $this->db->where('topic_id',$Roweditorial->editorial_id); echo $this->db->count_all_results('editorial_feedback'); ?> User</h6></a>
                                        </td>
                                        <td>
                                          <a href="#" data-toggle="tooltip" title="User who comment"><span class="glyphicon glyphicon-comment"></span></a><a href="#view_comments_editorial<?php echo $Roweditorial->editorial_id; ?>" data-toggle="modal" class="text-primary" onclick="view_modalcomment_editorial(<?php echo $Roweditorial->editorial_id; ?>)" id="result_countcomments_editorial<?php echo $Roweditorial->editorial_id; ?>"> <?php $this->db->where('topic_id',$Roweditorial->editorial_id); echo $this->db->count_all_results('editorial_modalcomment'); ?> User</a>
                                        </td>
                                        <td id="result_countviews_editorial<?php echo $Roweditorial->editorial_id; ?>">
                                        Views:<?php $this->db->where('topic_id',$Roweditorial->editorial_id); echo $this->db->count_all_results('editorial_views'); ?>
                                        </td>
                                      </tr>
                                    </table>
                                </div>
                                <!--end sa like and comments functions-->    
                                   
                               <!-- comment body -->
                               <div id="temporary_wholebody_editorial<?php echo $Roweditorial->editorial_id; ?>">
                               <span id="twocommentbody_editorial<?php echo $Roweditorial->editorial_id; ?>">
                               <?php
                               $this->db->where('topic_id',$Roweditorial->editorial_id);
                               $Commenteditorial = $this->db->get('editorial_comment');
                               //if true naay ge return nga row atung e output
                               //sa user ang comments og view comments
                               if($Commenteditorial->num_rows() > 0){

                               //kani nga function kai para sa two comments only ra ang mo output
                               $topic_id = $Roweditorial->editorial_id;
                               $selectTwocommenteditorial = $this->db->query("SELECT editorial_comment.id as id, editorial_comment.topic_id as topic_id, editorial_comment.comments as comment, editorial_comment.date as date_s, user.firstname as firstname, user.lastname as lastname, user.id as id_user, user_profile_pic.image as image FROM editorial_comment LEFT JOIN user ON user.id = editorial_comment.user_id LEFT JOIN user_profile_pic ON user_profile_pic.user_id = editorial_comment.user_id WHERE editorial_comment.topic_id = '$topic_id' ORDER BY editorial_comment.id ASC LIMIT 2 ");
                               foreach ($selectTwocommenteditorial->result() as $Rowtwocommenteditorial) {
                                 
                               
                               ?>
                               <!-- ang id nga attribute para ni sa delete comment using ajax aron ma hide natu ang message
                               ineg homan og delete-->
                               <div class="media" id="comment_body_editorial<?php echo $Rowtwocommenteditorial->id; ?>">
                               <a href="#"class="pull-left">
                               <?php
                               //if true naa nay profile pic ang user atung e output
                               if($Rowtwocommenteditorial->image != null){
                                ?>
                                <img src="<?php echo base_url().'upload/user/'.$Rowtwocommenteditorial->image;?>"class="media-object"style="height:30px;width:30px;">
                                <?php
                               //if wala atung e output ang default profile pic
                               }else{
                                ?>
                                <img src="<?php base_url();?>img/default_profile.jpg"class="media-object"style="height:30px;width:30px;">
                                <?php
                               }
                               ?>
                               </a>
                               <div class="media-body">
                               <div class="media-heading">
                               <label class="text-muted"><span class="glyphicon glyphicon-tag"></span> <?php echo ucfirst(strtolower($Rowtwocommenteditorial->firstname)).' '.ucfirst(strtolower($Rowtwocommenteditorial->lastname)); ?></label>

                               
                                <!-- for dropdown ni para sa settings nga mo trigger for delete and update comment-->
               <?php
               //if true ang comment kay sa user nga ni log in
               //authorize sya nga mo delete og update sa iyang comments
               //para ni sa Rowtwocommentsnews
               if($Rowtwocommenteditorial->id_user == $user_id){
               ?>                 
               <div class=" dropdown pull-right">
                        <a href="#editEditorialcomment<?php echo $Rowtwocommenteditorial->id; ?>"data-toggle="modal">
                          <span class="text-success glyphicon glyphicon-edit"></span> 
                        </a>
                        
                      <a href="#deleteEditorialcomment<?php echo $Rowtwocommenteditorial->id; ?>"data-toggle="modal"><span class="text-danger glyphicon glyphicon-trash"></span> 
                      </a>
               </div>
               <?php
               }
               ?>
              <!-- end sa dropdown dre hehe -->

                               <h6 class="text-warning"style="margin-top:-5px;" id="updated_time_editorial<?php echo $Rowtwocommenteditorial->id; ?>"><span class="glyphicon glyphicon-time"></span> <?php echo $Rowtwocommenteditorial->date_s; ?></h6>
                               </div>
                               <!-- <br> -->
                               <!-- atung ge butangan og attribute nga id aron ma trace sa ajax kung asa 
                               ge update nga comment sa user-->
                               <h5 id="result_editedcomment_editorial<?php echo $Rowtwocommenteditorial->id; ?>"><?php echo $Rowtwocommenteditorial->comment; ?></h5>

                               </div>
                               </div>
                               <?php
                               }//end sa Rowtwocommentsports loop
                               ?>
                               </span>
                               <!-- end sa twocommentbody id -->
                               <!--end sa comment body-->
                               <!--end sa twocomments only-->

                               <!-- all comments ni dri dapit -->

                               <!--view comments function sa jquery-->
                               <?php
                               // if true $Commentnews greater than equal 3
                               // e output nato sa view comments ang 
                               // mga comments nga belong sa view comments 
                               if($Commenteditorial->num_rows() >= 3){
                               //$selectTwocommentnews = $this->db->query("SELECT news_comment.id as id, news_comment.topic_id as topic_id, news_comment.comments as comment, news_comment.date as date_s, user.firstname as firstname, user.lastname as lastname, user.id as id_user, user_profile_pic.image as image FROM news_comment LEFT JOIN user ON user.id = news_comment.user_id LEFT JOIN user_profile_pic ON user_profile_pic.user_id = news_comment.user_id WHERE news_comment.topic_id = '$topic_id' ORDER BY news_comment.id ASC LIMIT 2 ");
                               ?>
                              <div class="col-sm-12">
                               <div class="panel-group" id="accordion">
                               <ul class="list-inline">
                                    <li class="pull-right">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseViewCommenteditorial<?php echo $Roweditorial->editorial_id; ?>" class="collapsed " style="text-decoration:none">
                                        View Comments <span class="glyphicon glyphicon-comment"></span>
                                    </a>
                                    </li>

                                    <div id="collapseViewCommenteditorial<?php echo $Roweditorial->editorial_id; ?>" class="panel-collapse collapse" style="height: 0px;">
                                    <div class="col-sm-1"></div>
                                    <div class="col-sm-11">
                                    <br>
                                      <!--asd-->  
                               <span id="allcommentbody_editorial<?php echo $Roweditorial->editorial_id; ?>">
                               <?php
                               //query for all comments
                               //ang $countall_rows nga variable kay para sa limit aron ma trace nato kung pila
                               //ka rows atung e output  
                               $editorial_comment_rows = $this->db->get('editorial_comment');
                               $countall_rows = $editorial_comment_rows->num_rows();
                               $selectAllcommenteditorial = $this->db->query("SELECT editorial_comment.id as id, editorial_comment.topic_id as topic_id, editorial_comment.comments as comment, editorial_comment.date as date_s, user.firstname as firstname, user.lastname as lastname, user.id as id_user, user_profile_pic.image as image FROM editorial_comment LEFT JOIN user ON user.id = editorial_comment.user_id LEFT JOIN user_profile_pic ON user_profile_pic.user_id = editorial_comment.user_id WHERE editorial_comment.topic_id = '$topic_id' ORDER BY editorial_comment.id ASC LIMIT $countall_rows OFFSET 2");
                               foreach ($selectAllcommenteditorial->result() as $Rowallcommenteditorial) {
                                 
                               
                               ?>
                               <!-- atung ge butangan og id nga attribute para ineg delete sa user
                               ma trace sa ajax kung kinsa iyang e hide nga commend body ineg homan
                               og delete -->                             
                                <div class="media" id="comment_body_editorial<?php echo $Rowallcommenteditorial->id; ?>">
                               <a href="#"class="pull-left">
                               <?php
                               //if true naa nay profile pic ang user atung e output
                               if($Rowallcommenteditorial->image != null){
                               ?>
                               <img src="<?php echo base_url().'upload/user/'.$Rowallcommenteditorial->image;?>"class="media-object"style="height:30px;width:30px;">
                               <?php
                               //if wala kay atung e output ang default profile pic
                               }else{
                               ?>
                               <img src="<?php base_url();?>img/default_profile.jpg"class="media-object"style="height:30px;width:30px;">
                               <?php
                               }
                               ?>
                               </a>
                               <div class="media-body">
                               <div class="media-heading">
                               <label class="text-muted"><span class="glyphicon glyphicon-tag"></span> <?php echo ucfirst(strtolower($Rowallcommenteditorial->firstname)).' '.ucfirst(strtolower($Rowallcommenteditorial->lastname)); ?></label>

                               <!-- for dropdown ni para sa settings nga mo trigger for delete and update comment-->
               <?php
               //if true ang comment kay sa user nga ni log in
               //authorize sya nga mo delete og update sa comments
               //para ni sa Rowallcommentnews
               if($Rowallcommenteditorial->id_user == $user_id){
               ?>                 
               <div class=" dropdown pull-right">
                        <a href="#editEditorialcomment<?php echo $Rowallcommenteditorial->id; ?>"data-toggle="modal">
                          <span class="text-success glyphicon glyphicon-edit"></span> 
                        </a>
                        
                      <a href="#deleteEditorialcomment<?php echo $Rowallcommenteditorial->id; ?>"data-toggle="modal"><span class="text-danger glyphicon glyphicon-trash"></span> 
                      </a>
               </div>
               <?php
               }//end sa settings nga mo delete og mo update
               ?>
              <!-- end sa dropdown dre hehe -->
                               <h6 class="text-warning"style="margin-top:-5px;" id="updated_time_editorial<?php echo $Rowallcommenteditorial->id; ?>"><span class="glyphicon glyphicon-time"></span> <?php echo $Rowallcommenteditorial->date_s; ?></h6>
                               </div>
                               <!-- atung ge butangan og attribute nga id aron ineg update nato
                               using ajax kay ma trace nato kung asa ang ge update nga comment sa user-->
                               <h5 id="result_editedcomment_editorial<?php echo $Rowallcommenteditorial->id; ?>"><?php echo $Rowallcommenteditorial->comment; ?></h5>

                               </div>
                               </div>
                               <?php
          
                               }//end sa all comments nga loop
                               ?>
                                    </span>
                                    </div>
                                    </div>
                                    </ul>
                               </div>
                               </div>
                               <!-- end sa view comments function-->
                               <?php
                                }//end sa if conidtion nga mo output sa view all comments
                                //if mo lapas sa 3 or equal atung e output ang view comments nga function 
                               }//end sa if condition nga mo output og comments
                                //og view comments if true naay ge return nga rows
                               else{
                                //if wala pa ni display nga comments kay atung
                                //gaan og space sa babaw sa comment box aron 
                                //user friendly
                                echo '<br/><br/><br/>';
                               }
                               ?>
                               <!--asd-->
                              
                               </div>
                               <!-- end sa allcommentbody id -->
                               <!--</div>-->
                               <!-- end sa whole body ni -->
                                      <span id="invalid_comment_editorial<?php echo $Roweditorial->editorial_id; ?>"></span>
                                      <div class="input-group" id="comment_box">
                                      <!--atung ge butangan og news_id nga value ang attribute nga id aron ma trace nato kung asa nga topic ni comment ang user-->
                                      <input type="text" class="form-control" onkeypress="pressed_commentfunc_editorial(<?php echo $Roweditorial->editorial_id.','.$num_of_comments; ?>,event)" id="comment_editorial<?php echo $Roweditorial->editorial_id; ?>" placeholder="Add a comment..">
                                      <!-- count_commentfunc para sa who comment nga function-->
                                       <div class="input-group-btn">
                                       <button class="btn btn-primary" onclick="comment_func_editorial(<?php echo $Roweditorial->editorial_id.','.$num_of_comments; ?>)"><span class="glyphicon glyphicon-share" ></span></button>
                                       </div>
                                       </div>
                                       <!-- end for comment -->
                               </div>
                               </div>
                              </div>
                              </span>
                                 <!-- end sa well nga div tag -->
                         <?php
                         //foreach end for news query sa admin ni
                          }
                         //end sa if statement nga $CountEditorial 
                         }else{

                            echo "<h1 class='text-muted'>No Editorial Publish.<h1>";
                            echo "<br><br><br><br><br><br><br>";
                         }
                         ?>
                          
                          <!-- end sa col-ms-6 div tag -->
 <!-- main col left ni dre --> 
                         

                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
<p>&copy 2015 All Rights Reserved @ SCSIT Social Site | Salazar Colleges of Science and Institute of Technology</p>
<br>
<div class="col-sm-2">
<a href="#scistcontactus"data-toggle="modal"><span class="glyphicon glyphicon-phone"></span> SCSIT Contact</a>
</div>
<div class="col-sm-2">
<a href="#scistlocation"data-toggle="modal"><span class="glyphicon glyphicon-globe"></span> SCSIT Location</a>
</div>
<div class="col-sm-2">
<a href="#scistaboutus"data-toggle="modal"><span class="glyphicon glyphicon-question-sign"></span> SCSIT About Us</a>
</div>
</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
</div>





<!-- includes for all modals -->
<span id="modals">
<?php include'editorialmodaltwocomment.php';?>
<?php include'editorialmodalallcomment.php';?>
<?php include'editorialmodalview_like_comment.php';?>
<?php include'editorialmodal_readmore.php';?>
<?php include'user_wall_modal.php';?>
<?php include'delete_confirmation_modal.php';?>
<?php include'edits_popup_modal.php';?>
<?php include'user_department_modal.php';?>
</span>
	<!-- script references for bootstrap orayt haha -->
		<script src="<?= base_url();?>js/jquery.min.js"></script>
		<script src="<?= base_url();?>js/bootstrap.min.js"></script>
		<script src="<?= base_url();?>js/scripts.js"></script>
     <!-- tooltip -->
     <script type="text/javascript">
      $(function(){$("[data-toggle='tooltip']").tooltip();});

      </script>
<!-- popover -->
      <script type="text/javascript">

      $ (function(){$("[data-toggle='popover']").popover();});
      </script>
	</body>
</html>