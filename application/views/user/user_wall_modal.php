
<div id="inbox" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
          <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
			<span class="glyphicon glyphicon-envelope"></span> Inbox
      </div>
      <div class="modal-body">
        <div id="jam_inbox">

        </div>
      </div>
     <div class="modal-footer">
     </div>

  </div>
  </div>
</div>



<!-- update profile pic -->
<div id="updateProfile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <form action="<?php echo base_url().'upload/updatePic'?>" method="post" enctype="multipart/form-data">
  <div class="modal-content">
      <div class="modal-header">
   
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
      
 Update Profile
      </div>
      <div class="modal-body">
      <div class="panel-body">
          
          <!-- current profile photo -->
          <div class="col-sm-6">
          <label class="text-muted"><span class="glyphicon glyphicon-picture"></span> Current Profile Photo</label>
           <?php if($sessionUser['profile_pic'] == ''):?>
            <label for="changeProf">
            <img src="<?= base_url();?>img/default_profile.jpg"class="media-object"alt="profile"style="height:150px;width:150px;" id="changeProfile">
            </label>
           <?php else: ?>
            <label for="changeProf">
             <img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>"class="media-object"alt="profile"style="height:150px;width:150px;" id="changeProfile">
          </label>
           <?php endif; ?>
           </div>
            <div class="col-sm-6">
           <p class="text-muted">Do you want to change? <em class="text-warning">Please select a new photo</em> </p>
           <div class="form-group">
           <input type="file" name="userfile" id="changeProf">
           </div>
           </div>

          </div>
      </div>
      <div class="modal-footer">
          <div>
           <input type="submit"value="Update"class="btn btn-primary btn-sm pull-right" value="update" />
          
      </div>  
      </div>
  </div>
  </div>
  </form>
</div>
<!-- end here -->



<div id="fel_profile_confirm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Profile has been successfully change!
        <!-- end -->
        <br><br>
        <label>New Profile Photo</label>
        <br>
        <img src="<?= base_url();?>img/default_profile.jpg"class="media-object"alt="profile"style="height:80px;width:80px;">



      </div>
     
  </div>
  </div>
</div>

<!-- friend request -->
<div id="felfriendrequest" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
<br>
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <div id="jam_confirmFRquest">
        
      </div>

          </div>
      </div>
      <div class="modal-footer">
         
      </div>
  </div>
  </div>
</div>
<!-- end -->

<!-- friend request confirmation -->
<div id="fel_friend_request_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> 
        <strong class="text-danger">Firstname Lastname</strong> has been successfully confirm!
        <!-- end -->



      </div>
     
  </div>
  </div>
</div>
<!-- end -->




<!-- for blocked list -->
<div id="felBlockedUserList"class="modal fade"tabindex="-1"role="dialog"aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
 <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
<br>

<!-- table -->
<div class="table-responsive">
<table class="table table-hover">
<thead>
  <tr>
  <th>User ID</th>
  <th>Firstname</th>
  <th>Lastname</th>
  <th>Reason</th>
  </tr>
</thead>
<tbody>
  <tr>
  <td>212-3854</td>
  <td>Felmerald</td>
  <td>Besario</td>
  <td>Hacker</td>
  </tr>
</tbody>
</table>
</div>
<!-- end -->

</div>
</div>
</div>
</div>
<!-- end -->



<div id="felsearchresult"class="modal fade"tabindex="-1"role="dialog"aria-hidden="true">
<div class="modal-dialog">
<div class="modal-content">
<div class="modal-header">
 <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
<br>
<center><img src="<?= base_url();?>img/default_profile.jpg"class="img-responsive"style="height:200px;width:300px;">

 <label class="text-muted" style="margin-top:10px;">
                               <span class="glyphicon glyphicon-tag"></span> Title of the news</label>
                               <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> November 18, 2015 12:06pm</h6>
                               </center>


</div>
</div>
</div>
</div>