<!-- edit comment-->

<div id="felDepartmentEditComment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      <div class="modal-body">
      <div class="panel-body">
      <label class="text-muted">Comment</label>
      <input type="text"name=""class="form-control">
     

      </div>
      </div>

      <div class="modal-footer">
      <input type="submit"value="Update"class="btn btn-primary btn-sm pull-right">
      </div>
     
  </div>
  </div>
</div>

<!-- edit user department post -->

<div id="felEditUserDepartmentPost" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      <div class="modal-body">
      <div class="panel-body">

     <div class="form-group">
     <label class="text-muted">Change Photo?</label>
     <input type="file">
    
    </div>
     <textarea type="text"class="form-control" autofocus=""col="2"row="3"></textarea>

      </div>
      </div>

      <div class="modal-footer">
      <input type="submit"value="Update"class="btn btn-primary btn-sm pull-right">
      </div>
     
  </div>
  </div>
</div>


<!-- delete comment confirmation -->

<div id="<?= base_url();?>FelDepartmentDeleteComment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href=""class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
      </div>
     
  </div>
  </div>
</div>

<!-- delete user department post -->

<div id="<?= base_url();?>FelDeleteUserDepartmentPost" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href=""class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
      </div>
     
  </div>
  </div>
</div>