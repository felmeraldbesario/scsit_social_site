
<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->                      
                      	<div class="row">
                          
                        
                          <!-- main col right ryt ni -->
                          <div class="col-sm-7">
                               
                              <div class="well" style="height:320px;"> 
                                  <ul class="nav nav-tabs">
                                  <li class="active">
                                    <a href="#updateStatus" data-toggle="tab">
                                    <img src="<?php echo base_url();?>img/circle_icon/add_status.png"class="hover img responsive"alt="Update Status"style="height:25px;" > Update Status</a>
                                    </li>
                                    <li>
                                    <form action="<?php echo base_url(); ?>upload/departmentPost" method="post" enctype="multipart/form-data">
                                    <label for="jam_campus_userfile">
                                       <p style="margin-top:10px;"><img src="<?php echo base_url();?>img/circle_icon/camera.png"   class="hover img responsive"alt="Update Status"style="height:25px;"> Add Image</p>
                                    </label>
                                    <input type="file" id="jam_campus_userfile" name="userfile">
                                    </li>
                                    </ul>
                                    <div class="tab-content">
                                    <div class="tab-pane fade active in" id="updateStatus">
                                      
                                     <form class="form-horizontal" role="form">
                                        <div class="form-group" style="padding:14px;">
                                     <div class="col-sm-2">
                                     <!-- default profike must be here -->
                                     <CENTER>
                                     <?php if($sessionUser['profile_pic'] == ''):?> 

                                      <img src="<?php echo base_url();?>img/default_profile.jpg"class="img-responsive"alt="User Profile"style="height:50px;width:50px;">
                                     <?php else: ?>
                                    <img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>"class="img-responsive"alt="User Profile"style="height:50px;width:50px;">
                                 
                                     <?php endif;?>
                                     </CENTER>

                                     <!-- end -->
                                     </div>
                                      <div class="col-sm-10">  
                                       <img src="<?php echo base_url().'img/default_profile.jpg';?>" style="width:100px;height:100px;border:3px solid #3B5999;" class="img-responsive" id="jam_campus_img">
                                    <br>
                                          <textarea class="form-control" name="jam_department_status" placeholder="What's on your mind?"></textarea>
                                      </div>
                                    </div>
                                 
                                   <input id="jam_campus_post"class="btn btn-primary btn-sm pull-right" type="submit" value="post" style="margin-top:10px;">
                  
                                  </form>
                                    </div>
                                    </div>
 
                                  </ul>
                                  </div>
                        <?php 
                          if($retrieveDeptPost != 0):
                            foreach($retrieveDeptPost as $row):
                        ?>
                        <div id="jam_dept_post<?php echo $row['id'];?>">
                          
                               <div class="panel panel-default">
                                 <div class="panel-heading">

                                 <!-- profile sa nag post okay -->
                                <ul class="list-inline">
                                  <li>
                                  <?php if(strtolower($row['firstname']) == 'department' && strtolower($row['lastname']) == 'admin'):?>
                                    <img src="<?php echo base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php elseif($row['id_number'] == 0):?>
                                    <img src="<?php echo base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">  
                                  <?php elseif($row['profile_pic'] == ''):?>
                                 <img src="<?php echo base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php else: ?>
                                      <a href="profile?id=<?php echo $row['user_id'];?>"><img src="<?php echo base_url().'upload/user/'.$row['profile_pic']; ?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php endif; ?>
                                 </li>
                                 <li>
                                 <h5><strong> <?php echo ucwords(strtolower($row['firstname'].' '.$row['lastname']));?></strong></h5>
                                 <h6 class="text-warning"> <span class="glyphicon glyphicon-time"></span> <?php echo $row['date'];?></h6></a>
                                 </li>
                          <!-- for dropdown ni -->
                          <?php 
                            if($row['user_id'] == $this->session->userdata('login_id') && $row['id_number'] == $sessionUser['id_number']):
                          ?>
                          <li class=" dropdown pull-right">
                                                        
                          <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                            <ul class="dropdown-menu">
                            
                             <li><a href="#"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                             <p class="text-danger"style="margin-left:20px;margin-top:-20px;" onclick="jam_delDeptPost(<?php echo $row['id'];?>)">Delete</p></a>
                             </li>    
                            </ul>
                            </li>
                          <?php endif; ?>
                            <!-- end sa dropdown dre hehe -->
                                </ul>
                               
                                 <!-- end -->


                                 </div>
                                  <div class="panel-body">
                                    
                                    <!-- coditional if true naay image select ni ha if false ang select only text ra -->
                                     <?php if($row['dept_pic'] != ''):?>
                                      <img src="<?php echo base_url().'upload/user/'.$row['dept_pic']; ?>"class="img-responsive"alt="img"style="width:740px;height:380px;">
                                    <?php endif; ?>
                                    <!-- end -->
                                    
                                    <!-- if naa or wala -->
                                    <p style="margin-top:2px;"> <?php echo nl2br($row['status']);?></p>
                                    
                                     <div class="table responsive">
                                    <table class="table table-hover table-bordered">
                                      <tr>
                                        <td>
                                          
                                           <a href="#" data-toggle="tooltip" title="Like post?" data-placement="left" ><img src="<?php base_url();?>img/circle_icon/like.png"class="img-responsive hover"alt="like"style="height:20px;width:20px" data-toggle="popover" title="You" onclick="jam_likeDeptPost(<?php echo $row['id'];?>)"></a><a href="#" data-toggle="modal"><h6 id="jam_count" class="text-primary"style="margin-left:25px;margin-top:-15px;" ><?php echo $row['dept_post_likeCount'];?> User</h6></a>
                                        </td>
                                        
                                        <td>
                                          <a href="#" data-toggle="tooltip" title="User who comment"><span class="glyphicon glyphicon-comment"></span></a><a href="#" data-toggle="modal" class="text-primary"> <?php echo $row['dept_post_commenterCount'];?> Comment</a>
                                        </td>
                                      </tr>
                                    </table>
                                    </div>
                                    <!-- end -->
                                    <hr>
                                    <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                    <div id="jam_deptPostCommentdiv<?php echo $row['id'];?>">

                                    </div>

                                    <!-- end sa comment -->
                                    <!-- /////////////////////////end sa comment nga media -->

                                    <!-- for View all comments -->
                                    <div class="panel-group" id="accordion">
                                    <ul class="list-inline">
                                    <li class="pull-right">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseViewComment" class="collapsed " style="text-decoration:none" id="jam_viewAllDeptComment<?php echo $row['id'];?>" onclick="jam_viewDeptComment(<?php echo $row['id'];?>)">
                                        View Comments <span class="glyphicon glyphicon-comment"></span>
                                    </a>
                                     </li>
                                     <br>
                                     <!-- id ni sa view comment -->
                                     <div id="collapseViewComment" class="panel-collapse collapse" style="height: 0px;">
                                    
                                        <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                    <div class="col-sm-2">
                                    </div>
                                   

                                     </div>

                                    </ul>
                                    </div>
                                    <!-- end -->


                                   
                                    <div class="col-sm-2">
                                      <?php if($sessionUser['profile_pic'] != ''):?>
                                         <img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>"class="img-responsive"alt="user profile"style="height:auto;width:40px;">
                                      <?php elseif($sessionUser['profile_pic'] == ''): ?>
                                        <img src="<?php echo base_url().'img/default_profile.jpg';?>"class="img-responsive"alt="user profile"style="height:auto;width:40px;">
                                      <?php endif; ?>
                                     </div>
                                      <div class="input-group">
                                     
                                      <input type="text" class="form-control" id="jam_deptCommentPost<?php echo $row['id'];?>" placeholder="Add a comment..">
                                       <div class="input-group-btn">
                                       <button class="btn btn-primary" onclick="jam_dept_post(<?php echo $row['id'];?>)"><i class="glyphicon glyphicon-share"></i></button>
                                       </div>
                                       </div>

                                    

                                    
                                  </div>
                               </div>
                        </div>
                        <?php 
                          endforeach;
                          endif;
                        ?>       
                          </div>

 <!-- main col left ni dre --> 
                        <?php include'left_sidebar.php';?>


                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
<p>&copy 2015 All Rights Reserved @ SCSIT Social Site | Salazar Colleges of Science and Institute of Technology</p>
<br>
<div class="col-sm-2">
<a href="#scistcontactus"data-toggle="modal"><span class="glyphicon glyphicon-phone"></span> SCSIT Contact</a>
</div>
<div class="col-sm-2">
<a href="#scistlocation"data-toggle="modal"><span class="glyphicon glyphicon-globe"></span> SCSIT Location</a>
</div>
<div class="col-sm-2">
<a href="#scistaboutus"data-toggle="modal"><span class="glyphicon glyphicon-question-sign"></span> SCSIT About Us</a>
</div>
</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
</div>





