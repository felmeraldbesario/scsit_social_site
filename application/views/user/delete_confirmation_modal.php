
<!-- confirmation modal alert -->
<div id="delete_Message_fromInbox" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href="#fel_message_delete_confirmation" data-toggle="modal" data-dismiss="modal" class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
      </div>
     
  </div>
  </div>
</div>


<!-- message confirmation delete -->

<div id="fel_message_delete_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="danger glyphicon glyphicon-trash"></span> 
        <strong class="text-danger">Successfully Deleted</strong> 
        <!-- end -->



      </div>
     
  </div>
  </div>
</div>








<!-- for delete campus post status alert -->
<div id="Feldeletecampusstatus" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href=""class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
      </div>
     
  </div>
  </div>
</div>

<!-- for campus comment delete alert -->
<div id="FeldeletecampusComment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <img src="<?= base_url();?>img/circle_icon/warning.png"class="img-responsive pull-left"alt="warning"style="height:25px;width:25px;">
         <h5 class="text-danger"style="margin-left:10px;margin-top:5px;"> &nbsp Are you sure you want to delete?</h5>
         <a href=""class="btn btn-danger btn-xs pull-right"style="margin-left:5px;"><span class="glyphicon glyphicon-trash"></span> Yes</a>&nbsp 
         <button data-dismiss="modal"class="btn btn-info btn-xs pull-right"> No</button>
      </div>
     
  </div>
  </div>
</div>
