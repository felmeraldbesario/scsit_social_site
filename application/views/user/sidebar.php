<!-- ////////////////////////////////////sidebar -->
            <!-- sidebar -->
             <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar"style="background:#cccccc;">
              
              <!-- kani sa toggle sidebar okay -->
                <ul class="nav">
                <a href="#" data-toggle="offcanvas" class="visible-xs text-center"><span style="background:grey"class="glyphicon glyphicon-chevron-right"></span></a>
              </ul>
              <!-- end for toggle side bar -->
           <input type="hidden" id="jam_sessId" value="<?php echo $this->session->userdata('login_id');?>"/>
        
              	<!-- profile pics here -->
                <?php if($sessionUser['profile_pic'] == ''):?> 
                    <a href="#updateProfile"data-toggle="modal"><img src="<?php base_url();?>img/default_profile.jpg"style="height:30px;width:30px;margin-top:-10px;"class="img-responsive hover"></a>
               <?php else: ?>
                    <a href="#updateProfile"data-toggle="modal"><img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>"style="height:30px;width:30px;margin-top:-10px;"class="img-responsive hover"></a>
                <?php endif;?>
                <!-- end of profile pics -->

                <!-- list sa user details here, name, idnumber, location, department -->
                <br>
               
                  
              
                <ul class="nav hidden-xs" id="lg-menu">

                   <img src="<?php echo base_url();?>img/circle_icon/user.png"class="img-responsive hover" data-toggle="tooltip" title="Fullname" style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><?php echo ucwords($sessionUser['firstname'].' '.$sessionUser['lastname']);?> </h6>
                   <img src="<?php echo base_url();?>img/circle_icon/location.png"class="img-responsive hover" data-toggle="tooltip" title="Address"style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><?php echo ucwords($sessionUser['address']);?></h6>

                    <img src="<?php echo base_url();?>img/circle_icon/telephone.png"class="img-responsive hover" data-toggle="tooltip" title="ID #"style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><?php echo $sessionUser['id_number'];?></h6>

                    <img src="<?php echo base_url();?>img/circle_icon/telephone.png"class="img-responsive hover" data-toggle="tooltip" title="Mobile #"style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><?php echo sprintf("%011.0f", $sessionUser['mobile_number']);?></h6>

                    <img src="<?php echo base_url();?>img/circle_icon/add_status.png"class="img-responsive hover" data-toggle="tooltip" title=" Course"style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><?php echo $sessionUser['course'].' '.$sessionUser['year'];?></h6>

                </ul>

                <hr>
              
               
                <ul class="nav hidden-xs" id="xs-menu">
                <p style="color:grey">Departments</p>
                  <?php foreach($departments as $department):?>
                    <!-- myBootbox.js -->
                    <!-- department_modal -->
                   <a href="#jam_departmentUsers" onclick="getUsers(<?php echo $department['id'];?>)" data-toggle="modal"><img src="<?php echo base_url().'upload/user/'.$department['image'];?>" class="img-responsive hover"style="height:20px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"><span class="badge" data-toggle="tooltip" title="New user registered in this department are"><?php if($department['count'] != 0){ echo $department['count'];}?></span><?php echo ucwords(strtolower($department['department']));?></h6></a>
                  <?php endforeach;?>  
                  
                    <p style="color:grey">News</p>
                    <a href="<?php echo base_url();?>newspage"><img src="<?php echo base_url();?>img/circle_icon/news.png"class="img-responsive hover"style="height:20px;margin-top:-15px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"> SCSIT News Page</h6></a>

                     <p style="color:grey">Announcement</p>
                    <a href="<?php echo base_url();?>announcement"><img src="<?php echo base_url();?>img/circle_icon/news.png"class="img-responsive hover"style="height:20px;margin-top:-15px;"> <h6 style="margin-left:30px;margin-top:-16px;color:black"> Announcement</h6></a>

                </ul>
                  
            </div>
            <!-- ////////////////////////////////////sidebar -->

            