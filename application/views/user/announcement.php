
	<body>
<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'sidebar.php';?>
            <!-- /end sa sidebar -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav -->
                <?php include'nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                      
                        <!-- content -->
                        <?php $user_id = $this->session->userdata('login_id'); ?>
                        <input type="hidden" id="user_id" value="<?php echo $user_id; ?>">                      
                      	<div class="row">
                        <div class="col-sm-1 col-md-1 col-lg-1"></div>
                        <div class="col-sm-10 col-md-10 col-lg-10">
                        <!-- sa announcement body ni -->
                        <?php if($Selectannouncement->num_rows() == 0){ ?>
                        <br>
                        <br>
                        <br>
                        <br>
                        <div class="panel panel-default" style="border-radius: 0px;box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);">
                        <div class="panel-body">
                        <h1>No Announcement Publish</h1>
                        </div>
                        </div>
                        <?php

                         }else{

                        ?>
                        <?php
                        // query para sa admin_announcement
                        foreach($Selectannouncement->result() as $Rowannouncement){
                        $this->db->where('topic_id',$Rowannouncement->id);
                        $Commentannouncement = $this->db->get('announcement_comment');
                        ?>
                        <span id="announcement_content<?php echo $Rowannouncement->id; ?>">
                        <div class="panel panel-default" style="border-radius: 0px;box-shadow: 0 2px 2px 0 rgba(0, 0, 0, 0.2), 0 3px 10px 0 rgba(0, 0, 0, 0.19);">
                        <div class="panel-body">
                        <?php
                        $num_of_comments = $Commentannouncement->num_rows();
                        ?>
                        <!--ribbon class-->
                        <!--start na-->
                        <div class="well" style="border-radius: 0px;">

                            <div class="row">

                            <div class="folded">
                            <h2 style="background-color: #337ab7;"><span class="
                             glyphicon glyphicon-bullhorn"></span> Announcement / Events</h2>
                            </div>
                            <!-- end ribbon class -->
                             <hr>
                            <!-- sa body or content ni -->
                             <h5 style="text-indent: 10px; word-break: keep-all;">
                            <span class="text-primary glyphicon glyphicon-bullhorn"></span>  
                            <?php echo $Rowannouncement->announcement; ?>
                             </h5>
                             <!-- end sa content -->
                             <!-- sa date ni dre -->
                             <h6 class="pull-right"><span class="text-primary glyphicon glyphicon-time"></span>
                             <em><strong>Published on:</strong><?php echo $Rowannouncement->date; ?></em>
                             </h6>
                             <!-- end sa date -->
                             <br>
                             <hr>
                             <h5>
                             <!-- for like ni -->
                             <a href="#" onclick="like_func_announcement(<?php echo $Rowannouncement->id; ?>)"><span class="glyphicon glyphicon-thumbs-up text-primary" data-toggle="tooltip"></span></a><a href="#announcement_like" data-toggle="modal" onclick="view_modallikes_announcement(<?php echo $Rowannouncement->id; ?>)"><span id="result_announcement<?php echo $Rowannouncement->id; ?>"> <?php 
                             $this->db->where('topic_id',$Rowannouncement->id); 
                             echo $this->db->count_all_results('announcement_feedback');
                             ?> user</span></a>
                             <!-- end sa like -->                            
                             |
                             <!-- for comment ni -->
                             <a href="#announcement_commented" data-toggle="modal" onclick="view_modalcomment_announcement(<?php echo $Rowannouncement->id; ?>)"><span class="glyphicon glyphicon-comment text-success" data-toggle="tooltip"></span> <span id="result_countcomments_announcement<?php echo $Rowannouncement->id; ?>"> <?php 
                             $this->db->where('topic_id',$Rowannouncement->id); 
                             echo $this->db->count_all_results('announcement_modalcomment'); 
                             ?> user</span></a>
                             <!-- end sa commment -->
                             </h5>


                             </div>
                         </div>
                         <!-- comment dre -->
                         <div class="col-sm-1 col-md-1 col-lg-1"></div>
                         <div class="col-sm-10 col-md-10 col-lg-10">
                         <?php
                         //para ni sa comments atung ge check if naay ge return nga value
                         if($Commentannouncement->num_rows() == 0){
                         ?>
                             <br>
                             <br>
                             <br> 
                         <?php

                         }else{
                         
                        // kani nga query para sa limit one comment
                        foreach ($select_oneannouncement as $Row_onecomment) {
                            // if true same silag topic id
                            if($Row_onecomment['topic_id'] == $Rowannouncement->id){
                                //e check nato if naay ni comment limit only one
                                if($Commentannouncement->num_rows() > 0){
                         ?>
                         <div class="row" id="onecommentbody_announcement<?php echo $Rowannouncement->id; ?>"><!--para sa comments ni-->
                           <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <?php
                                    if($Row_onecomment['image'] == null){
                                    ?>
                                        <div class="media-left">

                                        <img src="<?php base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        </div>
                                    <?php
                                    }else{
                                    ?>
                                        <div class="media-left">

                                        <img src="<?php echo 'upload/user/'.$Row_onecomment['image'];?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong><?php echo ucfirst(strtolower($Row_onecomment['firstname'])).' '.ucfirst(strtolower($Row_onecomment['lastname'])); ?></strong>

                  <!-- for dropdown ni -->
               <?php if($Row_onecomment['user_id'] == $user_id){ ?>   
               <li class=" dropdown pull-right">
                                
                  <a href="#" class="dropdown-toggle"  title="Manage your comment" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#edit_comment"data-toggle="modal" onclick="call_modalupdate_announcement(<?php echo $Rowannouncement->id.','.$Row_onecomment['id']; ?>)">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#delete_comment"data-toggle="modal" onclick="call_modaldelete_announcement(<?php echo $Rowannouncement->id.','.$Row_onecomment['id']; ?>)"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
               <?php } ?>
              <!-- end sa dropdown dre hehe --> 

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <span id="updated_time_announcement<?php echo $Row_onecomment['id']; ?>"><?php echo $Row_onecomment['date']; ?></span></h6>

                                    <h6 style="word-break: keep-all;"><em id="result_editedcomment_announcement<?php echo $Row_onecomment['id']; ?>"><?php echo $Row_onecomment['comment']; ?></em></h6>
                                    <hr>

                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                         </div>
                         <!-- end comment -->
                         <?php        
                              }// if statement e check nato if naay ni comment limit one
                            }//end sa if statement nga e match if same silag topic id 
                         }//end sa foreach nga limit one comment
                         if($Commentannouncement->num_rows() >= 2){
                                
                         ?>

                          <!-- for View all comments -->
                                    <div class="panel-group" id="accordion">
                                    <ul class="list-inline">
                                    <li class="pull-right">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseViewComment<?php echo $Rowannouncement->id; ?>" class="collapsed " style="text-decoration:none">
                                        View Comments <span class="glyphicon glyphicon-comment"></span>
                                    </a>
                                     </li>
                                     <br>
                                     <!-- id ni sa view comment -->
                                     <div id="collapseViewComment<?php echo $Rowannouncement->id; ?>" class="panel-collapse collapse" style="height: 0px;">
                                        <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                    <?php
                                    if($select_allannouncement != 0){
                                        foreach ($select_allannouncement as $Row_allcomment) {
                                        // e check nato if match ba sla sa topic id dayon e output nato
                                            if($Row_allcomment['topic_id'] == $Rowannouncement->id){
                                    ?>
                                    <span id="allcommentbody_announcement<?php echo $Rowannouncement->id; ?>">
                                    <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <?php
                                    if($Row_allcomment['image'] == null){
                                    ?>
                                        <div class="media-left">

                                        <img src="<?php base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        </div>
                                    <?php
                                    }else{
                                    ?>
                                        <div class="media-left">

                                        <img src="<?php echo 'upload/user/'.$Row_allcomment['image'];?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        </div>
                                    <?php
                                    }
                                    ?>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong><?php echo ucfirst(strtolower($Row_allcomment['firstname']).' '.ucfirst(strtolower($Row_allcomment['lastname']))); ?></strong>

                 
                  <!-- for dropdown ni -->
               <?php if($Row_allcomment['user_id'] == $user_id){ ?>   
               <li class=" dropdown pull-right">
                                
                  <a href="#" class="dropdown-toggle"  title="Manage your comment" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#edit_comment"data-toggle="modal" onclick="call_modalupdate_announcement(<?php echo $Rowannouncement->id.','.$Row_allcomment['id']; ?>)">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#delete_comment"data-toggle="modal" onclick="call_modaldelete_announcement(<?php echo $Rowannouncement->id.','.$Row_allcomment['id']; ?>)"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
               <?php } ?>
              <!-- end sa dropdown dre hehe -->     

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <span id="updated_time_announcement<?php echo $Row_allcomment['id']; ?>"><?php echo $Row_allcomment['date']; ?></span></h6>

                                    <h6 style="word-break: keep-all;"><em id="result_editedcomment_announcement<?php echo $Row_allcomment['id']; ?>"><?php echo $Row_allcomment['comment']; ?></em></h6>
                                    <hr>
 
                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                                    </span>
                                    <!-- end sa comment -->
                                    <!-- /////////////////////////end sa comment nga media -->
                                    <?php
                                              }// end sa if statement nga mo match sa topic id
                                        }//end sa foreach nga $select_allannouncementcomment
                                     }   
                                     ?>
                                     </div><!--view all comments end-->
                                    </ul>
                                    </div><!--end para sa comments ni-->
                                    <?php

                                    //end sa if statement nga e check if greater than equal 2
                                    }
                                    ?>
                         <!--end sa if statement nga e check if naay value ang variable nga $select_announcement-->
                         <?php
                         }
                         ?>
                                    <!-- end -->
                                    <span id="invalid_comment_announcement<?php echo $Rowannouncement->id; ?>"></span>
                                    <div class="input-group" id="comment_box">
                                      <!--atung ge butangan og news_id nga value ang attribute nga id aron ma trace nato kung asa nga topic ni comment ang user-->
                                    <input type="text" class="form-control" onkeypress="pressed_commentfunc_announcement(<?php echo $Rowannouncement->id.','.$num_of_comments; ?>,event)" id="comment_announcement<?php echo $Rowannouncement->id; ?>" placeholder="Add a comment..">
                                      <!-- count_commentfunc para sa who comment nga function-->
                                    <div class="input-group-btn">
                                    <button class="btn btn-primary" onclick="commentfunc_announcement(<?php echo $Rowannouncement->id.','.$num_of_comments; ?>)"><span class="glyphicon glyphicon-share" ></span></button>
                                    </div>
                                    </div>

                        </div><!--end sa start-->
                        </div><!--end sa panel-body-->
                        </div><!--panel default-->
                        </span>
                        <!--end sa foreach nga $Selectannouncement-->
                        <?php } ?>
                        <!--end sa condition nga if walay ge return nga value ang $Selectannouncement variable-->
                        <?php } ?>





                        </div> <!-- end sa column 10 -->
                          




                       </div><!--/row-->
                      
                         <!-- footer dre hahaha -->
                         <hr>
<p>&copy 2015 All Rights Reserved @ SCSIT Social Site | Salazar Colleges of Science and Institute of Technology</p>
<br>
<div class="col-sm-2">
<a href="#scistcontactus"data-toggle="modal"><span class="glyphicon glyphicon-phone"></span> SCSIT Contact</a>
</div>
<div class="col-sm-2">
<a href="#scistlocation"data-toggle="modal"><span class="glyphicon glyphicon-globe"></span> SCSIT Location</a>
</div>
<div class="col-sm-2">
<a href="#scistaboutus"data-toggle="modal"><span class="glyphicon glyphicon-question-sign"></span> SCSIT About Us</a>
</div>
</div>

       
                   <!-- end sa footer -->

                        
                      
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
</div>



<!-- includes for all modals -->
