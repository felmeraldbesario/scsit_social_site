<!-- fel edit for campus post status -->
<div id="feleditCamposStatus" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      <div class="modal-body">
      <div class="panel-body">

     <div class="form-group">
     <label class="text-muted">Change Photo?</label>
     <input type="file">
    
    </div>
     <textarea type="text"class="form-control" autofocus=""col="2"row="3"></textarea>

      </div>
      </div>

      <div class="modal-footer">
      <input type="submit"value="Update"class="btn btn-primary btn-sm pull-right">
      </div>
     
  </div>
  </div>
</div>

<!-- edit campus comment -->
<!-- fel edit for campus post status -->
<div id="feleditcampusComment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
      </div>
      <div class="modal-body">
      <div class="panel-body">
<label class="text-muted">Comment</label>
     <input type="text"name=""class="form-control">
     

      </div>
      </div>

      <div class="modal-footer">
      <input type="submit"value="Update"class="btn btn-primary btn-sm pull-right">
      </div>
     
  </div>
  </div>
</div>

<!-- for change cover photo sa profile -->
<div id="felupdatecoverphoto" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
         <br>
         <div class="form-group">
         <label class="text-muted"><span class="glyphicon glyphicon-picture"></span> Select new Image</label>
         <input type="file">
         </div>
      </div>
     
  </div>
  </div>
</div>

<!-- view image -->
<div id="felviewImage" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
         <!-- result of viewing images -->
        <img src="<?= base_url();?>img/default_profile.jpg"class="img-responsive"alt="View Image"style="margin-top:20px;">
      </div>
     
  </div>
  </div>
</div>


