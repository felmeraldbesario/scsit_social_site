<div id="feluser_changePassword" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
  <form action="" method="post">
      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;" id="jam_dismissUpAccount">
        <br>

        <!-- body -->
        <span class="text-success glyphicon glyphicon-edit"></span> Change Account
        <br>
        <br>

        <div class="form-group">
          <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-envelope"></i></span>
         <input type="email" class="form-control" id="jam_emailupdate" value="<?php echo $sessionUser['email'];?>" aria-describedby="basic-addon1">
         </div>
         </div>

         <div class="form-group">
          <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-user"></i></span>
         <input type="text" class="form-control" id="jam_usernameupdate" value="<?php echo $sessionUser['username'];?>" aria-describedby="basic-addon1">
         </div>
         </div>
          
          <div class="form-group">
          <div class="input-group">
          <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
         <input type="password" class="form-control" placeholder="Old Password" id="jam_oldPassUpdate" aria-describedby="basic-addon1">
         </div>
         </div>
         <?php 
          $invalid_characters = array("$", "%", "#", "<", ">", "|", "?", "/", "\\", " ", ";", ",");
         ?>
         <label class="text-warning">invalid characters 
         <?php 
            $char = "";
            $count = 0;
            foreach($invalid_characters as $row){
               $count++;
              if($count == 1){
                $char = $char."\"".$row."\"";
              }else{
                $char = $char.", \"".$row."\"";
              }
            }
             
            echo $char;
         ?>
         </label>
         <div class="form-group">
         <div class="input-group">
         <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
         <input type="password" class="form-control" placeholder="New Password" id="jam_newPassUpdate" aria-describedby="basic-addon1">
         </div>
         </div>

          <div class="form-group">
         <div class="input-group">
         <span class="input-group-addon" id="basic-addon1"><i class="glyphicon glyphicon-lock"></i></span>
         <input type="password" class="form-control" placeholder="Retype Password" id="jam_retypePassUpdate" aria-describedby="basic-addon1">
         </div>
         </div>
         <label id="jam_changeAccount" class="text-warning"></label>
         <br>
         <br>
         <input type="submit" value="Update" id="jam_updateAccount">
        <!-- end -->


      </div>
     </form>
  </div>
  </div>
</div>