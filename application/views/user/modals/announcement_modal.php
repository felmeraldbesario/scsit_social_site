<!-- count all likes -->
<div id="announcement_like" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/like.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Like</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody id="body_who_likes_announcement">
          <!-- ajax here -->
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- count all and display of the name who commented -->
<div id="announcement_commented" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Commented</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody id="view_modalcomment">
          <!-- ajax here -->
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- edi comment -->
<div id="edit_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Update Comment</h5>
        <!-- end -->
        <!-- table -->
        <span id="result_updating_comment">
          <!-- ajax here -->
        </span>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- for delete comment -->
<div id="delete_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog modal-sm">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/warning.png" class="img-responsive news-image-height">
        <h5  class="like-margin text-danger">Are you sure you want to delete?</h5>
        <!-- end -->
        <!-- table -->
        <span id="result_deleting_comment">
          
        </span>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>