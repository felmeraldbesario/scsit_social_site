<!-- own status edit -->
<div id="fel_department_user_edits" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <SPAN class="glyphicon glyphicon-edit"></SPAN> Update Status
        <!-- end -->
        <hr>
        <div class="col-sm-4">
        <span class="glyphicon glyphicon-picture"></span> Current Image
        <img src="<?= base_url();?>img/sliderImages/8.png" class="img-responsive department_current_size">

        <div class="form-group">
        <label class="text-muted">Change Image</label>
        <input type="file">
        </div>
        </div>

        <div class="col-sm-8">

        <div class="form-group">
        <input type="text" class="form-control">
        </div>
         <div class="form-group">
         <textarea class="form-control" type="text" cols="3" rows="3"></textarea>
        </div>
          <div class="form-group">
         <a href="#fel_department_post_edit_confirm" data-toggle="modal" data-dismiss="modal" class="btn btn-sm btn-primary pull-right" type="submit"><span class="glyphicon glyphicon-refresh"></span></a>
        </div>



        </div>



        <br><br><br><br><br><br><br><br><br><br><br><br>
      </div>
     
  </div>
  </div>
</div>

<!-- confirmation -->

<div id="fel_department_post_edit_confirm" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Successfully Update!
        <!-- end -->
        
      </div>
     
  </div>
  </div>
</div>

<!-- delete -->

<div id="fel_department_user_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete?
        <!-- end -->
        <div class="form-group">
        <br>
        <a href="#fel_dept_del_con" data-toggle="modal" data-dismiss="modal" type="submit"  class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-ok"></span></a>
        </div>
        
      </div>
     
  </div>
  </div>
</div>

<!-- delete confirmation -->
<div id="fel_dept_del_con" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-danger glyphicon glyphicon-trash"></span> Successfully Deleted!
        <!-- end -->
        
      </div>
     
  </div>
  </div>
</div>