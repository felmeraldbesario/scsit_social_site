<div id="felcount_news_like" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/like.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Like</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- comment -->

<div id="fel_count_news_name" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Comment</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>
