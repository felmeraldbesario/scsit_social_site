<!-- edit -->
<div id="fel_dept_edit_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <SPAN class="glyphicon glyphicon-edit"></SPAN> Update Comment
        <hr>
        <div class="form-group">
        <textarea cols="3" rows="3" class="form-control"></textarea>
        </div>
        <br>
        <a href="#fel_edit_conf" data-toggle="modal" data-dismiss="modal" class="btn btn-sm btn-primary" type="submit"><span class="glyphicon glyphicon-refresh"></span></a>
        <!-- end -->
        
      </div>
     
  </div>
  </div>
</div>

<!-- confirmation -->

<div id="fel_edit_conf" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Successfully Update!
        <!-- end -->
        
      </div>
     
  </div>
  </div>
</div>

<!-- delete comment -->
<div id="fel_dept_del_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete?
        <!-- end -->
        <div class="form-group">
        <br>
        <a href="#fel_dept_del_confirmation" data-toggle="modal" data-dismiss="modal" type="submit"  class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-ok"></span></a>
        </div>
        
      </div>
     
  </div>
  </div>
</div>

<div id="fel_dept_del_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-danger glyphicon glyphicon-trash"></span> Successfully Deleted!
        <!-- end -->
        
      </div>
     
  </div>
  </div>
</div>
