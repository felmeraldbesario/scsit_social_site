 <!-- search result -->
<div id="fel_search_result" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <div class="col-sm-3">
        <img src="<?= base_url();?>img/default_profile.jpg" class="img-responsive img-search-size" data-toggle="tooltip" title="User Profile">
        <!-- end -->
        </div>
        <div class="col-sm-9">
        
        <span class="glyphicon glyphicon-user" data-toggle="tooltip" title="User name"></span> Felmerald Calago Besario
        <br>
        <img src="<?= base_url();?>img/circle_icon/location.png" class="img-responsive img_location_size" data-toggle="tooltip" title="Department">
        <h6 class="department_margin_fix"><strong>Computer Department</strong></h6>
       
        <img src="<?= base_url();?>img/circle_icon/location.png" class="img-responsive img_location_size" data-toggle="tooltip" title="Address">
        <h6 class="department_margin_fix"><strong>Address of the User</strong></h6>

        <span class="text-danger  glyphicon glyphicon-time" data-toggle="tooltip" title="Birthhdate"></span> Birthdate
      <br>
        <span class="text-danger  glyphicon glyphicon-time time_join_margin" data-toggle="tooltip" title="Date Joined"></span> Date Joined in SCSIT SOCIAL SITE

        <hr>

        <img src="<?= base_url();?>img/circle_icon/contact.png" class="img-responsive img_location_size" data-toggle="tooltip" title="Friends List">
        <h6 class="department_margin_fix"><strong><a href="#fel_search_count_friends" data-toggle="modal" data-dismiss="modal">100 Friends</a></strong></h6>

        <span class="text-danger glyphicon glyphicon-ban-circle" data-toggle="tooltip" title="User Block"></span> 
        <a href="#fel_search_view_block" data-toggle="modal" data-dismiss="modal">1 Friend Blocked</a>

        <!-- button dropdown -->
        <div class="dropdown pull-right dropdown_margin_fix">
        <button class="btn btn-primary dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
        <span class="glyphicon glyphicon-ban-circle"></span>
        <span class="caret"></span>
        </button>
        <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
        <li><a href="#fel_result_modal_unfollow" data-toggle="modal" data-dismiss="modal" class="text-warning">Unfollow</a></li>
        <li><a href="#fel_result_modal_block" data-toggle="modal" data-dismiss="modal" class="text-danger">Block</a></li>
        </ul>
        </div>
        <!-- end -->

        </div>




        <div class="modal_margin_fix">
        </div>
      </div>
     
  </div>
  </div>
</div>
<!-- end -->


<div id="fel_result_modal_unfollow" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/unblock.png" class="img-responsive user_height">
        <h5 class="like-margin">Unfollow Friend</h5>
        <!-- end -->
        <!-- table -->

        <img src="<?= base_url();?>img/default_profile.jpg" class="img-responsive img-size-total">
        <br>
        <span class="glyphicon glyphicon-user"></span>  Firstname Lastname
        <br><br>
        <a href="#fel_con_unfollow" data-toggle="modal" data-dismiss="modal" type="submit" value="Unfollow" class="btn btn-primary btn-sm">Unfollow</a>


       
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<div id="fel_con_unfollow" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        
        <span class="text-danger glyphicon glyphicon-question-sign"></span> Are you sure you want to unfollow firstname middlename lastname?
       <br>
       <input type="submit" value="YES" class="btn btn-sm btn-danger">
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- block -->
<div id="fel_result_modal_block" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        
        <span class="text-danger glyphicon glyphicon-question-sign"></span> Are you sure you want to block firstname middlename lastname?
        <br>
        <a href="#fel_block_reason" data-toggle="modal" data-dismiss="modal" type="submit" value="YES" class="btn btn-sm btn-danger">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- reason -->

<div id="fel_block_reason" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        
        <div class="form-group">
        <label>Select Reason</label>
        <select class="form-control" required>
            <option>Select Reason</option>
            <option>Hacker</option>
            <option>Spam</option>
        </select>
        </div>
        <br>
        <input type="submit" value="YES" class="btn btn-sm btn-primary">
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

