 <div class="col-sm-5">
                         
                         <!-- for updates sa profile pics, updates personal information -->
                         <div class="panel panel-default">
                         <div class="panel-body">

                         <img src="<?php echo base_url();?>img/circle_icon/add_status.png"class="img-responsive"alt="Update Profile Pic"style="height:25px;"> <h5 style="margin-left:35px;margin-top:-20px;"><strong> <?php echo ucwords(strtolower($sessionUser['firstname'].' '.$sessionUser['lastname']));?>'s Wall</strong></h5>
                         <br>
                         <!-- for list sa group -->
                         <ul class="list-group">
                         <li class="list-group-item">
                       <a href="#updateProfile"data-toggle="modal"class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-camera"></span></a>
                       <?php if($sessionUser['profile_pic'] == ''):?>
                          <img src="<?php echo base_url();?>img/default_profile.jpg"class="img-responsive"alt="User Profile"style="height:30px;width:30px;">
                        <?php else: ?>
                          <img src="<?php echo base_url().'upload/user/'.$sessionUser['profile_pic'];?>"class="img-responsive"alt="User Profile"style="height:30px;width:30px;">
                       <?php endif;?>
                       <p class="pull-left"style="margin-left:50px;margin-top:-25px">Current Profile Photo</p>
                       </li>
                       
                        <li class="list-group-item">
                        <a href="#FeluserDepartmentModal"data-toggle="modal"class="btn btn-primary btn-xs pull-right"><span class="glyphicon glyphicon-plus"></span></a>
                       <img src="<?php echo base_url().'img/department/'.$sessionUser['department_image'];?>"class="img-responsive"alt="User Profile"style="height:30px;width:30px;">
                       <p class="pull-left"style="margin-left:50px;margin-top:-25px"> <?php echo ucwords(strtolower($sessionUser['department']));?> Department</p>

                        </li>
                      </ul>
                      <!-- end -->
                         

                         </div>
                         </div>
                         <!-- end -->

                         <!-- for groups, department groups, social groups -->
                        
                         <!-- end -->

                         <!-- friends list online and offline -->
                         <div class="panel panel-default">
                         <div class="panel-heading">
                         <i class="glyphicon glyphicon-th-list"></i> <strong>Friend's</strong><strong class="pull-right">Total:<u class="text-muted">100</u></strong>
                         </div>
                         <div class="panel-body"style="height:300px;overflow-y:scroll">
                          <div class="col-sm-12">
                          <div id="jam_friendsList"></div>
                        </div>
                         </div>
                         <div class="panel-footer">

                          

                         </div>
                         </div>
                         <!-- end -->
                        
                           
                          </div>


                          <!-- modal -->

                          <div id="fel_updatePersonalData" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
                         <div class="modal-dialog">
                          <div class="modal-content">
                          <form action="<?php echo base_url().'user_controller/updateAccount'?>" method="post">
                          <div class="modal-header">
                            <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
                            <br>

        <!-- body -->
                                                 <div class="form-group">
                         <label>Firstname</label>
                         <input type="text" name="firstname" value="<?php echo $sessionUser['firstname'];?>" class="form-control">
                         </div>
                         <div class="form-group">
                         <label>Middlename</label>
                         <input type="text" name="middlename" value="<?php echo $sessionUser['middlename'];?>" class="form-control">
                         </div>
                         <div class="form-group">
                         <label>Lastname</label>
                         <input type="text" name="lastname" value="<?php echo $sessionUser['lastname'];?>" class="form-control">
                         </div>
                         <div class="form-group">
                         <label>Address</label>
                         <input type="text" name="address" value="<?php echo $sessionUser['address'];?>" class="form-control">
                         </div>
                         <div class="form-group">
                         <label>Mobile number</label>
                         <input type="text" name="mobile_number" value="<?php echo sprintf("%011.0f", $sessionUser['mobile_number']);?>" class="form-control">
                         </div>
                         <div class="form-group">
                         <label>Birthdate</label>
                         <input type="date" name="birthdate" value="<?php echo $sessionUser['birthdate'];?>" class="form-control">
                         </div>
                        <br>
                        <input type="submit" value="Update" class="btn btn-sm btn-primary">
        <!-- end -->


                       </div>
     
                      </div>
                      </form>
                      </div>
                      </div>