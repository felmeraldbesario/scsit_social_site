<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SCSIT SOCIAL SITE</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php base_url();?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php base_url();?>css/styles.css" rel="stylesheet">
		
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald.css">
		<link rel="shortcut icon" href="<?php base_url();?>img/logo.png">
		<script src="roy_js/jquery.js"></script>
		<script src="roy_js/jquery2.js"></script>
		<script src="roy_js/myfunc.js"></script>
		<!-- new design sa style nga ribbon -->
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ribbon_fel.css">
	</head>


	