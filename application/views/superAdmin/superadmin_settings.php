<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
              	
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->                      
                      	<div class="row">


                        <div class="col-sm-12 col-md-12 col-lg-12">

                        <div class="panel panel-default">
                        <div class="panel-body">
                            <!-- para sa ribbon -->
      <div class="header_fold sa_page">
     <h2 style="background-color: #337ab7;">
                    SCSIT Social Site Configuration
      </h2>
   </div>
   <!-- end para sa ribbon -->
   <br><br>
   <?=$this->session->flashdata('personal_info_message')?>
                    
                    
                    <?php foreach($scsit_information as $scsitRow){?>
                    <label class="text-success">Contact us</label>
                    <hr style="margin-top:0px">
                    <div class="row">
                        <div class="col-sm-10">
                        <?=nl2br($scsitRow->contact)?><br>
                        <?=nl2br($scsitRow->information)?><br>
                        <?=nl2br($scsitRow->locator)?>
                     </div>
                       <?php } ?>
                        <div class="col-sm-2">
                            <!-- for update button--><a href="#update_contact" data-toggle="modal"class="text-danger pull-right">Update</a><br>

                        </div>
                    </div>
                   
                     <br>
                    <br>
                    <label class="text-success">Activator</label>
                    <br><small>(deactivate or activate user page if examination)</small>
                    <hr style="margin-top:0px">
                    <div class="row">
                        <div class="col-sm-12">
                        <?php if($system_status->num_rows() == 0):?>
                            <a href="#diactivate_system"data-toggle="modal"class="btn btn-success"><span class="glyphicon glyphicon-ok"></span> Activate User Page</a>
                        <?php else: ?>
                            <a href="#activate_system"data-toggle="modal"class="btn btn-danger"><span class="glyphicon glyphicon-exclamation-sign"></span> Deactivate User Page</a>
                        <?php endif;?>
                     </div>
                        
                    </div>
                    <br>
                   <label class="text-success">Reason</label>
                    <br><small>(Manage Reasons for blocking user)</small>
                    <hr style="margin-top:0px">
                    <div class="row">
                        <div class="col-sm-12">
                        <div class="panel-body">
                        <ul>
                        <?php foreach($reason as $reasonRow):?>
                            
                            <div class="col-sm-1"><a href="#edit_reason<?=$reasonRow->id?>" data-toggle="modal"><span class="glyphicon glyphicon-edit text-success"></span></a> <a href="#delete_reason<?=$reasonRow->id?>" data-toggle="modal"><span class="glyphicon glyphicon-trash text-danger" data-toggle="modal"></span></a></div><li><?=$reasonRow->reason?></li>
                           
                        <?php endforeach;?>
                        </ul>
                         </div>
                            <a href="#manage_reason" data-toggle="modal"class="btn btn-primary"><span class="glyphicon glyphicon-plus"></span> reason</a>
                     </div>
                        
                    </div>
                    <br>
                   

                        </div>
                        </div>

                        </div>
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
    </div>

<!-- para sa update_contact -->
<div id="update_contact" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Update Contact Info
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
<?php foreach($scsit_information as $scsitRow):?>
    <?=form_open(base_url().'superadmin_controller/update_contact')?>
     <label>Contact number:</label>
     <textarea name="contact_number" class="form-control"><?=$scsitRow->contact?></textarea>
     <br>
     <label>Email:</label>
     <textarea name="email" required class="form-control"><?=$scsitRow->information?></textarea>
     <br>
     <label>Address:</label>
     <textarea name="address" required class="form-control"><?=$scsitRow->locator?></textarea>
<?php endforeach;?>
     <br>
     <input type="submit" name="submit" value="submit" class="btn btn-primary">
      </div>  
      <?=form_close()?>

     
  </div>
  </div>
</div>

<!-- manage reason -->
<div id="manage_reason" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Manage Reason
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
    <?=form_open(base_url().'superadmin_controller/add_reason')?>
     <label>Reason:</label>
     <textarea name="reason" class="form-control"></textarea>
    <br>
     <input type="submit" name="submit" value="submit" class="btn btn-primary">
      </div>  
      <?=form_close()?>


  </div>
  </div>
</div>

<!-- edit reason -->
<?php foreach($reason as $reasonRow){?>
<div id="edit_reason<?=$reasonRow->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Manage Reason
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
    <?=form_open(base_url().'superadmin_controller/update_reason')?>
     <label>Reason:</label>
     <textarea name="reason" class="form-control"><?=nl2br($reasonRow->reason)?></textarea>
    <br>
    <input type="hidden" name="reason_id" value="<?=$reasonRow->id?>">
     <input type="submit" name="submit" value="submit" class="btn btn-primary">
      </div>  
      <?=form_close()?>


  </div>
  </div>
</div>

<?php }?>


<!-- edit reason -->
<?php foreach($reason as $reasonRow){?>
<div id="delete_reason<?=$reasonRow->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
   
  
     <label>Are you sure you want to delete this reason?</label>
    <?=form_open(base_url().'superadmin_controller/delete_reason')?>
     
    <input type="hidden" name="reason_id" value="<?=$reasonRow->id?>">
     <input type="submit" name="submit" value="YES" class="btn btn-danger">
     <a href="#" data-dismiss="modal" class="btn btn-primary">NO</a>
   
      <?=form_close()?>

    </div> 
  </div>
  </div>
</div>

<?php }?>

<div id="diactivate_system" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
   
  
     <label>Are you sure you want to DEACTIVATE the system?</label>
     
     <a  href="#confirm_deactivate_system" data-toggle="modal" class="btn btn-danger">YES</a>
     <a href="#" data-dismiss="modal" class="btn btn-primary">NO</a>
   

    </div> 
  </div>
  </div>
</div>


<div id="confirm_deactivate_system" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
   
  
     <label>Are you sure you want to DEACTIVATE the system?</label>
     <?=form_open(base_url().'superadmin_controller/deactivate_system')?>
    <input type="hidden" name="status" value="1">
     <input type="submit" name="submit" value="YES" class="btn btn-danger">
     <a href="#" data-dismiss="modal" class="btn btn-primary">NO</a>
    <?=form_close()?>

    </div> 
  </div>
  </div>
</div>

<!-- para sa activate -->
<div id="activate_system" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
   
  
     <label>Are you sure you want to ACTIVATE the system?</label>
     
     <a  href="#confirm_activate_system" data-toggle="modal" class="btn btn-danger">YES</a>
     <a href="#" data-dismiss="modal" class="btn btn-primary">NO</a>
   

    </div> 
  </div>
  </div>
</div>


<div id="confirm_activate_system" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
   
  
     <label>Are you sure you want to ACTIVATE the system?</label>
     <?=form_open(base_url().'superadmin_controller/deactivate_system')?>
    <input type="hidden" name="status" value="0">
     <input type="submit" name="submit" value="YES" class="btn btn-danger">
     <a href="#" data-dismiss="modal" class="btn btn-primary">NO</a>
    <?=form_close()?>

    </div> 
  </div>
  </div>
</div>