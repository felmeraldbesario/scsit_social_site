<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-12">
                        <!-- content -->                      
                        <div class="row">
                        <?=$this->session->flashdata('admin_announcement')?>
                       <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="header_fold sa_page">
                              <h2 style="background-color: #337ab7;">
                              ANNOUNCEMENT 
                              </h2>
                            </div>
                            <a href="#announcement" data-toggle="modal"class="pull-right text-success"><span class="glyphicon glyphicon-plus"></span> Announcement</a>
                          </div>
                        </div>
                       </div>
<!-- news -->
<?php 
              foreach($getAnnouncement as $announcementRow){

?>   
                       <div class="col-sm-12 col-md-12 col-lg-12">

                       <div class="panel panel-default">
                       <div class="panel-body">
<!-- for dropdown ni -->
 <ul class="list-inline">
  <li class=" dropdown pull-right">
                                
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
    <ul class="dropdown-menu">
    <li><a href="#update_announcement<?=$announcementRow->announcement_id?>"data-toggle="modal">
    <span class="glyphicon glyphicon-edit"></span> 
     <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
    </li>
     <li><a href="#delete_announcement<?=$announcementRow->announcement_id?>"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
     <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
     </li>    
    </ul>
    </li>
  </ul>
    <!-- end sa dropdown dre hehe -->                       <span class="glyphicon glyphicon-user"></span><strong> Published by admin:</strong>
                       <?php 
                       if($announcementRow->admin_id == "0"){
                        echo "SUPER ADMIN";
                       }
                       ?>
                       <?=ucfirst(strtolower($announcementRow->fname))." ".ucfirst(strtolower($announcementRow->lname))?>
                       <br>
                       <h6 class="text-primary"><span class="glyphicon glyphicon-time"></span><strong> Date Published:</strong><?=$announcementRow->announcement_date?></h6>
                       
                       <h6 class="text-primary">
                       <?php if($announcementRow->announcement_date_update != null):?>
                        <span class="glyphicon glyphicon-time"></span><strong>
                        Date Updated:</strong><?=$announcementRow->announcement_date_update?>
                      <?php endif;?>
                        </h6>
                      
                       <!-- body -->
                       <div class="col-sm-12 col-md-12 col-lg-12">
                       <div class="panel-body overflow-body">
                       <h6>
                       <?=nl2br($announcementRow->announcement)?>
                       </h6>
                       </div>
                       <br>
                       <!-- end -->


                       </div>


                      
                       <!-- end sa body -->

                       <div class="col-sm-12">
                        <!-- for table -->
                       <div class="table-responsive">
                       <table class="table table-hover table-bordered">
                       <tbody>
                           <tr>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who like">Like:</a></strong> <a href="#admin_view_like"data-toggle="modal">100</a></td>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who comment">Comments:</a></strong> <a href="#admin_view_comments" data-toggle="modal">100</a></td>
                           </tr>
                       </tbody>
                           
                       </table>
                       </div>
                       <!-- end -->
                       </div>



                       </div>
                       </div>

                       </div>
                       <?php } ?>
<!-- end sa news -->

                       
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
    </div>


<div id="announcement" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
<div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
        ANNOUNCEMENT
      </h2>
   </div>
 
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
      <?=form_open_multipart(base_url().'superadmin_controller/add_announcement')?>
      <label>Announcement</label>
      <textarea class="form-control" cols="5" rows="5" required placeholder="What's on your mind?" name="announcement"></textarea>
      <br>
      <input type="submit" class="btn btn-primary btn-sm pull-right" >
      <?=form_close()?>
      </div>  
  </div>
  </div>
</div>

<?php 
              foreach($getAnnouncement as $announcementRow){

?> 
<div id="update_announcement<?=$announcementRow->announcement_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
<div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
        ANNOUNCEMENT
      </h2>
   </div>
 
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
      <?=form_open_multipart(base_url().'superadmin_controller/update_announcement')?>
      <label>Announcement</label>
      <input type="hidden" name="announcement_id" value="<?=$announcementRow->announcement_id?>">
      <textarea class="form-control" cols="5" rows="5" required placeholder="What's on your mind?" name="announcement"><?=$announcementRow->announcement?></textarea>
      <br>
      <input type="submit" class="btn btn-primary btn-sm pull-right" >
      <?=form_close()?>
      </div>  
  </div>
  </div>
</div>
<?php } ?>


<?php 
              foreach($getAnnouncement as $announcementRow){

?> 
<div id="delete_announcement<?=$announcementRow->announcement_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
<div class="modal-header">
        
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
          <br>
          <label >Are you sure you want to delete this announcement?</label>
      <?=form_open_multipart(base_url().'superadmin_controller/delete_announcement')?>
      <input type="hidden" name="announcement_id" value="<?=$announcementRow->announcement_id?>"><br>
      <input type="submit" value="YES"class="btn btn-danger btn-sm"> <a href="#" data-dismiss="modal" class="btn btn-primary btn-sm">NO</a>
      <?=form_close()?>
      
  </div>
  </div>
</div>
<?php } ?>