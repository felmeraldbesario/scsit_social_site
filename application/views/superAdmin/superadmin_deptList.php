<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
               
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">

                    <div class="full col-sm-9">

                        <!-- content -->                      
                        <div class="row">
                          <?php echo $this->session->userdata('DEPARTMENT_EXIST');?>
                          <?php echo $this->session->userdata('DEPARTMENT_ADD_SUCCESS');?>
                          <?php echo $this->session->userdata('DEPARTMENT_NAME_NOT_AVAILABLE');?>
                          <?php echo $this->session->userdata('COURSE_SUCCESS');?>
                          <?php echo $this->session->userdata('COURSE_EXIST');?>
                          <?php echo $this->session->userdata('SUCCESSFULLY_UPDATED');?>
                          <?php echo $this->session->userdata('SUCCESSFULLY_DELETED');?>
                          <?=$this->session->flashdata('update_course');?>
                          <?=$this->session->flashdata('update_course_error');?>
                          <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="panel panel-default">
                          <div class="panel-body">
                            <div class="header_fold sa_page">
                              <h2 style="background-color: #337ab7;">
                                  DEPARTMENT
                              </h2>
                            </div>
                          <a href="#add_department" data-toggle="modal" class="text-success pull-right"><span class="glyphicon glyphicon-plus"></span>Add Department</a>
                          </div>
                          </div>

                          <?php 
                          $message = array(
                              'DEPARTMENT_ADD_SUCCESS' => '',
                              'DEPARTMENT_NAME_NOT_AVAILABLE' => '',
                              'DEPARTMENT_EXIST' => '',
                              'COURSE_EXIST' => '',
                              'COURSE_SUCCESS' => '',
                              'SUCCESSFULLY_UPDATED' => '',
                              'SUCCESSFULLY_DELETED'=>''
                            );
                          $this->session->unset_userdata($message);
                          ?>
                         

                          </div>


                          <div class="col-sm-12 col-md-12 col-lg-12">
                          <div class="row">
                          <!-- !!!!!!!!! -->
                          <?php foreach ($query as $department_list_row):?>
                          <div class="col-sm-6 col-md-6 col-lg-6">
                          <div class="panel panel-default">
                          <div class="panel-heading"><span class="glyphicon glyphicon-tags"></span> <?=ucfirst(strtolower($department_list_row->department));?>

                        <!-- dropdown -->
                        <div class=" dropdown pull-right">   
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <i class="glyphicon glyphicon-triangle-bottom"></i></a>
                        <ul class="dropdown-menu">
                        <li><a href="#fel_edit_added_department<?=$department_list_row->id;?>" data-toggle="modal" class="text-success" >
                       <span class="glyphicon glyphicon-edit"></span>
                        </a>
                        </li>
                        <li><a href="#fel_del_added_department<?=$department_list_row->id;?>" data-toggle="modal" class="text-danger">
                        <span class="glyphicon glyphicon-trash"></span>
                        </a>
                        </li>    
                        </ul>
                        </div>
                        <!-- end -->


                          </div>
                          <div class="panel-body">

                          <!-- information sa admin -->
                          <span class="glyphicon glyphicon-check"></span>Admin:
                        <?php 
                       
                        foreach($dept_personel_query->result() as $dept_personel_row):
                          if($department_list_row->id == $dept_personel_row->assigned_department):
                          ?>
                          <?=ucfirst(strtolower($dept_personel_row->fname)." ".ucfirst(strtolower($dept_personel_row->lname)))?>
                          
                        <?php
                          endif;
                          endforeach;
                          ?>

                          <br>
                          <span class="glyphicon glyphicon-check"></span>Status: Active
                          <br>
                          <br>
                          <a class="btn btn-sm btn-primary" href="#fel_add_course<?=$department_list_row->id;?>" data-toggle="modal" style="margin-left:20px;"><span class="glyphicon glyphicon-plus"></span>Add Course</a>
                          
                          <!-- end sa information sa admin -->

                          <!-- table -->
                          <div class="table-responsive" style="overflow-y:scroll;height:150px">
                          <table class="table table-hover">
                          <thead>
                              <tr>
                                  <th>Course</th>
                                  <th>Date Added</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                              <?php foreach($getCourseQuery as $CourseRow){

                              if($CourseRow->department_id == $department_list_row->id){
                              ?>
                              <tr>
                                  <td><?=$CourseRow->course?></td>
                                  <td><?=$CourseRow->date_added?></td>
                                  <td>
                                      <a href="#fel_edit_course<?=$CourseRow->id ?>"  class="text-success" data-toggle="modal"><span class="glyphicon glyphicon-edit"></span></a>
                                      &nbsp
                                      <a href="#fel_delete_course<?=$CourseRow->id ?>" data-toggle="modal" class="text-danger" ><span class="glyphicon glyphicon-trash"></span></a>
                                  </td>
                              </tr>
                              <?php }

                            } ?>
                          </tbody>
                              
                          </table>
                          </div>
                          <!-- end table -->

                          </div>
                          </div>
                          </div>
                        <?php endforeach;?>
                          <!-- !!!!!!!!! -->
                          </div>
                          </div>
                        
 
                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
    </div>
