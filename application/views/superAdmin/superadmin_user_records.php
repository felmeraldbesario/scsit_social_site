<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->                      
                        <div class="row">

                        <div class="panel panel-default">
                        <div class="panel-body">

                         <a href="<?=base_url();?>superadmin"class="btn btn-primary btn-sm" data-toggle="tooltip" title="Back to home page?"><span class="glyphicon glyphicon-arrow-left"></span></a>
                        <h5 class="text-danger pull-right">User's records</h5>
                        <br><br>

                        <div class="table-responsive">
                        <table class="table table-hover table-bordered" id="dataTables-example">
                        <thead>
                            <tr>
                                <th>
                                    ID#
                                </th>
                                <th>Firstname</th>
                                <th>Lastname</th>
                                <th>Department</th>
                                <th>Records</th>
                            </tr>
                        </thead>
                        <tbody>
                           <?php foreach($records_query as $recordsRow):?>
                            <tr>
                                <td><?=$recordsRow->id_number?></td>
                                <td><?=ucfirst(strtolower($recordsRow->firstname))?></td>
                                <td><?=ucfirst(strtolower($recordsRow->lastname))?></td>
                                <td><?=ucfirst(strtolower($recordsRow->department_name))?></td>
                                
                                <td>
                                    
<?php if($recordsRow->blocked_id == null):?>
                                    
                                <a href="#fel-pop-block<?=$recordsRow->user_id?>" data-toggle="modal"class="text-danger">BLOCK</a>
                                    <?php else: ?>
                                <a href="superadmin_blocked?id=<?=$recordsRow->dept_id?>" class="text-success">UNBLOCK</a>
                                    </td>
                                    <?php endif;?>                               

                            </tr>
                            <?php endforeach;?>
                        </tbody>
                            
                        </table>







                        </div>

                        </div>
                        </div>
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
    </div>
