<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->                      
                        <div class="row">
 <!-- start para sa post-->
 
                        <div class="col-sm-6">
                          <?php foreach($department_query as $department_list_row):?>
                     <h5 class="label label-primary"><?=ucfirst(strtolower($department_list_row->department));?> Department</h5><br><br>
                     <?php $department_name_selected = $department_list_row->department; ?>
                          <?php endforeach;?>
                        <?php foreach($departmentPostQuery as $departmentPostRow):?>
                            
                               <div class="panel panel-default">
                                 <div class="panel-heading">

                                 <!-- profile sa nag post okay -->
                                <ul class="list-inline">
                                  <li>
                                  <?php if($this->superadmin_department_post->department_post_admin($departmentPostRow->department_user_id,$departmentPostRow->admin_id_number) == true){
                                    if($this->superadmin_department_post->department_admin_image($departmentPostRow->admin_id_number)){
                                    ?>
                                 <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->department_admin_image($departmentPostRow->admin_id_number)?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                    <?php }else{
                                      ?>
                                 <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                      <?php
                                      } ?>
                                 </li>
                                 <li>
                                 <h5><strong>Admin</strong></h5>
                                 <?php }else if($departmentPostRow->department_user_id != "0" ){
                                  if($this->superadmin_department_post->get_user_image($departmentPostRow->department_user_id)){
                                  ?>
                                  <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_user_image($departmentPostRow->department_user_id)?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php } ?>
                                 </li>
                                 <li>
                                 <h5><strong><?=ucfirst(strtolower($departmentPostRow->firstname))." ".ucfirst(strtolower($departmentPostRow->lastname))?></strong></h5>
                                 
                                  <?php
                                  }else{
                                  if($this->superadmin_department_post->get_superadmin_image() != "" || $this->superadmin_department_post->get_superadmin_image() != "no image"){
                                  ?>
                                  <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_superadmin_image()?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php } ?>
                                  </li>
                                 <li>
                                 <h5><strong>Super Admin</strong></h5>
                                   
                                    <?php
                                    } ?>
                                 <h6 class="text-warning"> <span class="glyphicon glyphicon-time"></span> <?=$departmentPostRow->department_topic_date?></h6>
                                 </li>
  <!-- for dropdown ni -->
  <li class=" dropdown pull-right">
                                
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
    <ul class="dropdown-menu">
    <li><a href="#fel_dep_edit<?=$departmentPostRow->department_topic_id?>"data-toggle="modal">
    <span class="glyphicon glyphicon-edit"></span> 
     <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
    </li>
     <li><a href="#fel_dep_delete<?=$departmentPostRow->department_topic_id?>"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
     <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
     </li>    
    </ul>
    </li>
    <!-- end sa dropdown dre hehe -->
                                 </ul>
                               
                                 <!-- end -->


                                 </div>
                                  <div class="panel-body">
                                    
                                    <!-- coditional if true naay image select ni ha if false ang select only text ra -->
                                    <?php if($departmentPostRow->department_topic_image != "no image"){
                                    ?>
                                    <img src="<?php base_url();?>upload/user/<?=$departmentPostRow->department_topic_image?>"class="img-responsive"alt="img"style="width:740px;height:380px;">
                                    
                                    <?php

                                     }else{
                                    ?>

                                    <?php

                                      }?>
                                    <!-- end -->
                                    
                                    <!-- if naa or wala -->
                                    <p style="margin-top:2px;"> <?=$departmentPostRow->department_topic;?></p>
                                    
                                    <div class="table-responsive">
                       <table class="table table-hover table-bordered">
                       <tbody>
                           <tr>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who like">Like:</a></strong> <a href="#fel_cam_like<?=$departmentPostRow->department_topic_id?>"data-toggle="modal"><?=$this->superadmin_department_post->getLikes($departmentPostRow->department_topic_id)?></a></td>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who comment">Comments:</a></strong> <a href="#fel_cam_comment<?=$departmentPostRow->department_topic_id?>"data-toggle="modal"><?=$this->superadmin_department_post->getComments($departmentPostRow->department_topic_id)?></a></td>
                           </tr>
                       </tbody>
                           
                       </table>
                       </div>



                                    <!-- end -->
                                     <span id="acj_department_topic<?=$departmentPostRow->department_topic_id?>">
                                    <hr>

                              <?php foreach($this->superadmin_department_post->showComments($departmentPostRow->department_topic_id) as $commentRow):?>
                                     
                                    

                                    <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                    <div class="col-sm-1">
                                    </div>

                                    <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <div class="media-left">

                                    

                                      <?php if($this->superadmin_department_post->admin_personel_id($commentRow->department_admin_id_number) == true){
                                        if($this->superadmin_department_post->department_admin_image($commentRow->department_admin_id_number)){
                                        ?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->department_admin_image($commentRow->department_admin_id_number)?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php }else{
                                          ?>
                                      <img src="<?php base_url();?>img/logo.png"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                          <?php
                                          }?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong> <?=ucfirst(strtolower($department_name_selected))?> - Administrator </strong>
                                      <?php
                                        }else if($commentRow->commenter_id != 0){?>
                                        <?php if($this->superadmin_department_post->get_user_image($commentRow->comment_id) != false){?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_user_image($commentRow->comment_id)?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                      <?php }else{
                                        ?>
                                      <img src="<?php base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php
                                        } ?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>
                                      <?=ucfirst(strtolower($commentRow->firstname))." ".ucfirst(strtolower($commentRow->lastname))?>
                                      <?php }else{?>
                                        <?php
                                      if($this->superadmin_department_post->get_superadmin_image() != "" || $this->superadmin_department_post->get_superadmin_image() != "no image"){
                                         ?>                                        
                                        <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_superadmin_image()?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php}else{
                                          ?>
                                        
                                          <?php
                                          }?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>
                                      Super Administrator
                                      <?php }?>
                                    </strong>

                  <!-- for dropdown ni -->
               <li class=" dropdown pull-right">
                                
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                     
                        <li><a href="#fel_dept_com_edit"data-toggle="modal" onclick="return department_comment_modal(<?=$commentRow->comment_id?>);">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#fel_dept_com_edit" onclick="return department_delete_modal(<?=$commentRow->comment_id?>);" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
              <!-- end sa dropdown dre hehe --> 

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <?=$commentRow->department_date?></h6>

                                    <p><em><?=$commentRow->comment?></em></p>
                                    <hr>

                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                                    <?php endforeach;?>
                                    <!-- end sa comment -->
                                    <!-- /////////////////////////end sa comment nga media -->



                                    <!-- for View all comments -->
                                    <div class="panel-group" id="accordion">
                                    <ul class="list-inline">
                                    <li class="pull-right">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseViewComment<?=$departmentPostRow->department_topic_id?>" class="collapsed " style="text-decoration:none">
                                        View Comments <span class="glyphicon glyphicon-comment"></span>
                                    </a>
                                     </li>
                                     <br>
                                     <br>
                                     <!-- id ni sa view comment -->
                                     <div id="collapseViewComment<?=$departmentPostRow->department_topic_id?>" class="panel-collapse collapse" style="height: 0px;">
                                    
                                        <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                  <?php foreach($this->superadmin_department_post->viewAllComments($departmentPostRow->department_topic_id) as $viewAllCommentsRow):?>
                                    <div class="col-sm-2">
                                    </div>
                                    <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <div class="media-left">
                                     <?php if($this->superadmin_department_post->admin_personel_id($viewAllCommentsRow->department_admin_id_number) == true){
                                        if($this->superadmin_department_post->department_admin_image($viewAllCommentsRow->department_admin_id_number)){
                                        ?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->department_admin_image($viewAllCommentsRow->department_admin_id_number)?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php }else{
                                          ?>
                                      <img src="<?php base_url();?>img/logo.png"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                          <?php
                                          }?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong> <?=ucfirst(strtolower($department_name_selected))?> - Administrator </strong>
                                      <?php
                                        }else if($viewAllCommentsRow->commenter_id != 0){?>
                                        <?php if($this->superadmin_department_post->get_user_image($viewAllCommentsRow->comment_id) != false){?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_user_image($viewAllCommentsRow->comment_id)?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                      <?php }else{
                                        ?>
                                      <img src="<?php base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php
                                        } ?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>
                                      <?=ucfirst(strtolower($viewAllCommentsRow->firstname))." ".ucfirst(strtolower($viewAllCommentsRow->lastname))?>
                                      <?php }else{?>
                                        <?php
                                      if($this->superadmin_department_post->get_superadmin_image() != "" || $this->superadmin_department_post->get_superadmin_image() != "no image"){
                                         ?>                                        
                                        <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_superadmin_image()?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                        <?php}else{
                                          ?>
                                        
                                          <?php
                                          }?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>
                                      Super Administrator
                                      <?php }?></strong>

                  <!-- for dropdown ni -->
               <li class=" dropdown pull-right">
                                
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#feleditcampusComment"data-toggle="modal">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#FeldeletecampusComment"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
              <!-- end sa dropdown dre hehe --> 

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span><?=$viewAllCommentsRow->department_date?></h6>

                                    <p><em><?=$viewAllCommentsRow->comment?></em></p>
                                    <hr>

                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                                    <?php endforeach;?>
                                    <!-- end sa comment -->
                                    <!-- /////////////////////////end sa comment nga media -->
                                   
                                     </div>

                                    </ul>
                                    </div>
                                    </span>
                                    <!-- end -->


                           
                                    <div class="col-sm-2">
                                    <?php
                                        if($this->superadmin_department_post->get_superadmin_image() != "" || $this->superadmin_department_post->get_superadmin_image() != "no image"){
                                  ?>
                                  <img src="<?php base_url();?>upload/user/<?=$this->superadmin_department_post->get_superadmin_image()?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php } ?>
                                  
                                    </div>
                                      <div class="input-group">
                                     
                                      <input type="text" id="acj_sa_department_comment<?=$departmentPostRow->department_topic_id?>" class="form-control" placeholder="Add a comment..">
                                       <div class="input-group-btn">
                                       <input type="submit" value="comment" class="btn btn-primary" onclick="return sa_department_comment(<?=$departmentPostRow->department_topic_id?>)">
                                       
                                       </div>
                                      
                                       </div>

                       
                                   
                                  </div>
                               </div>



<?php endforeach;?>
                        
                        </div>

                <!-- end para sa post-->

                        <div class="col-sm-6">
                        <div class="panel panel-default">
                        <div class="panel-body">
                         <?=form_open_multipart(base_url().'superadmin_department_post')?>
                        <a href="#image"  class="hover" style="text-decoration:none"><label for="get_image"><span class="glyphicon glyphicon-picture text-success"></span> Add Image</label></a>
                        <input type="hidden" name="department_id" value="<?=$department_id?>">
                        <input type="file" id="get_image" style="display:none" name="userfile">
                        <textarea class="form-control" cols="5" rows="5" required placeholder="What's on your mind?" name="sa_department_post"></textarea>
                        <br>
                        <input type="hidden" id="submit">
                        <button for="submit" class="btn btn-sm btn-primary pull-right"><span class="glyphicon glyphicon-send"></span></button>
                        
                        <?=form_close()?>
                        </div>
                        </div>

                        <div class="well">

                        <span class="glyphicon glyphicon-time"> </span> Time Log's

                        <div class="table-responsive">
                        <br>
                        <table class="table table-hover table-responsive">
                           <tbody>
                               <?php foreach($this->superadmin_department_post->getLastpostOn($department_id) as $post_dateRow){ ?>
                               <tr>
                                   <td><strong>Last Post On: </strong></td>

                                   <td><?=$post_dateRow->date?></td>
                                <?php } ?>
                                <?php foreach($this->superadmin_department_post->getPreviuspostOn($department_id) as $post_dateRow){ ?>
                               <tr>
                                   <td><strong>Previus Post On: </strong></td>

                                   <td><?=$post_dateRow->date?></td>
                                <?php } ?>
                               </tr>
                           </tbody> 
                        </table>
                        </div>

                        </div>

                        </div>
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>

            <!-- /main -->

         
        </div>

    </div>
    </div>
