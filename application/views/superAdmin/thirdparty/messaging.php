
            <?php if($show_sms != ''):?>
                            <?php foreach($show_sms as $convo):?>
                              <?php if($convo['blocked'] == 0):?>
<!-- receiver conversation --> <?php if($convo['sender_id'] != $this->session->userdata('login_id')):?>
                              <div class="left">
                                    <span class="pull-left">
                                      <?php if($convo['profile_pic'] == ''):?>
                                        <img src="<?= base_url();?>img/default_profile.jpg" alt="User" class="img-circle" />
                                      <?php else:?>
                                        <img src="<?php echo base_url().'upload/user/'.$convo['profile_pic'];?>" alt="User" class="img-circle" />
                                      <?php endif;?>
                                      </span>
                                                                         
                                            <strong ><?php echo ucwords(strtolower($convo['firstname'].' '.$convo['lastname']));?></strong>
                                            <small class="pull-right text-success">
                                                <i class="glyphicon glyphicon-time"></i> <?php echo $convo['date'];?>
                                            </small>                                       
                                        <h5>
                                           <?php echo $convo['message'];?>
                                        </h5>
                                </div>
                                <!-- end -->

                               <br>
                             <?php else: ?>
<!-- sender conversation -->

                                   <div class="right">
                                    <span class="pull-right">
                                    <?php if($convo['profile_pic'] == ''):?>
                                        <img src="<?= base_url();?>img/default_profile.jpg" alt="User" class="img-circle" />
                                      <?php else:?>
                                        <img src="<?php echo base_url().'upload/user/'.$convo['profile_pic'];?>" alt="User" class="img-circle" />
                                      <?php endif;?>
                                    </span>
                                  
                                        
                                            <small class=" text-warning">
                                                <i class="glyphicon glyphicon-time"></i> <?php echo $convo['date'];?></small>
                                            <strong class="pull-right">Me</strong>
                                      
                                        <h5>
                                           <?php echo $convo['message'];?>
                                        </h5>
                                        </div>
                                        <!-- end -->
                                      <?php endif; ?>
                                      <br/><br/><br/>
                                    <?php endif;?>
                                  <?php endforeach; ?>
                                <?php endif; ?>
                              
                              