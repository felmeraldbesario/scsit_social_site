<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->                      
                        <div class="row">
 <!-- start para sa post-->
 
                        <div class="col-sm-6">
                        <?php foreach($campusPostQuery as $campusPostRow):?>
                          
                               <div class="panel panel-default">
                                 <div class="panel-heading">

                                 <!-- profile sa nag post okay -->
                                <ul class="list-inline">
                                  
                                   <?php if($campusPostRow->user_id != 0):?>
                                    <li>
                                  <?php if($this->superadmin_campus_post->get_user_image($campusPostRow->user_id) != false){?>
                                 <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_user_image($campusPostRow->user_id)?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }else{?>
                                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                  <?php }?>
                                 </li>
                                 <li>
                                 <h5><strong> 
                                    <?=ucfirst(strtolower($campusPostRow->firstname))." ".ucfirst(strtolower($campusPostRow->lastname))?>
                                    <?php else:?>
                                      <li>
                                      <?php if($this->superadmin_campus_post->get_image() != "" || $this->superadmin_campus_post->get_image() != "no image" ){?>
                                 <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_image()?>"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
                                 <?php }else{
?>
                  <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;">
<?php

                                  } ?>
                                 </li>
                                 <li>
                                 <h5><strong> 
                                      Super Admin
                                      <?php endif;?>
                                  </strong></h5>
                                 <h6 class="text-warning"> <span class="glyphicon glyphicon-time"></span> <?=$campusPostRow->date?></h6>
                                 </li>
  <!-- for dropdown ni -->
  <li class=" dropdown pull-right">
                                
  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
    <ul class="dropdown-menu">
    <li><a href="#fel_cam_edit<?=$campusPostRow->post_id?>"data-toggle="modal">
    <span class="glyphicon glyphicon-edit"></span> 
     <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
    </li>
     <li><a href="#fel_cam_delete<?=$campusPostRow->post_id?>"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
     <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
     </li>    
    </ul>
    </li>
    <!-- end sa dropdown dre hehe -->
                                 </ul>
                               
                                 <!-- end -->


                                 </div>
                                  <div class="panel-body">
                                    
                                    <!-- coditional if true naay image select ni ha if false ang select only text ra -->
                                    <?php if($campusPostRow->image == "no image"){
                                    ?>
                                   
                                    <?php
                                     }else{
                                    ?>
                                    <img src="<?php base_url();?>upload/user/<?=$campusPostRow->image?>"class="img-responsive"alt="img"style="width:740px;height:380px;">
                                    <?php

                                      }?>
                                    <!-- end -->
                                    
                                    <!-- if naa or wala -->
                                    <p style="margin-top:2px;"> <?=$campusPostRow->campus_post;?></p>
                                    
                                    <div class="table-responsive">
                       <table class="table table-hover table-bordered">
                       <tbody>
                           <tr>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who like">Like:</a></strong> <a href="#fel_cam_like<?=$campusPostRow->post_id?>"data-toggle="modal"><?=$this->superadmin_campus_post->getLikes($campusPostRow->post_id)?></a></td>
                           <td><strong><a href="#" data-toggle="tooltip" title="Views person who comment">Comments:</a></strong> <a href="#fel_cam_comment<?=$campusPostRow->post_id?>"data-toggle="modal"><?=$this->superadmin_campus_post->getComments($campusPostRow->post_id)?></a></td>
                           </tr>
                       </tbody>
                           
                       </table>
                       </div>
                                    <!-- end -->
                                    <div id="acj_link_<?=$campusPostRow->post_id?>"></div>
                                    <span id="acj_campus_post<?=$campusPostRow->post_id?>">
                                    <hr>
                              <?php foreach($this->superadmin_campus_post->showComments($campusPostRow->post_id) as $commentRow):?>
                                     
                                    <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                   
                                    <div class="col-sm-1">

                                    </div>
                                    <span id="delete_comment<?=$commentRow->comment_id?>">
                                    <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <div class="media-left">

                                      <?php if($commentRow->campus_commenter_id != 0):?>
                                      <?php if($this->superadmin_campus_post->get_user_image($commentRow->campus_commenter_id) != false){?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_user_image($commentRow->campus_commenter_id)?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                      <?php }
                                    ?>
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong><?=ucfirst(strtolower($commentRow->firstname))." ".ucfirst(strtolower($commentRow->lastname))?>
                                      <?php else:?>
                                        <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_image()?>"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>
                                      Super Admin
                                      <?php endif;?>
                                    </strong>

                  <!-- for dropdown ni -->
               <li class=" dropdown pull-right">
                              
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#fel_cam_com_edit" onclick="return edit_campus_comment(<?=$commentRow->comment_id?>);" data-toggle="modal">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#fel_cam_com_edit" onclick="return campus_modal_delete(<?=$commentRow->comment_id?>);" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
              <!-- end sa dropdown dre hehe --> 

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span> <?=$commentRow->date?></h6>

                                    <p><em><?=$commentRow->comment?></em></p>
                                    <hr>

                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                                    </span>
                                    <?php endforeach;?>
                                    <!-- end sa comment -->
                                    <!-- /////////////////////////end sa comment nga media -->



                                    <!-- for View all comments -->
                                    <div class="panel-group" id="accordion">
                                    <ul class="list-inline">
                                    <li class="pull-right">
                                  <a data-toggle="collapse" data-parent="#accordion" href="#collapseViewComment<?=$campusPostRow->post_id?>" class="collapsed " style="text-decoration:none">
                                        View Comments <span class="glyphicon glyphicon-comment"></span>
                                    </a>
                                     </li>
                                     <br>
                                     <br>
                                     <!-- id ni sa view comment -->
                                     <div id="collapseViewComment<?=$campusPostRow->post_id?>" class="panel-collapse collapse" style="height: 0px;">
                                    
                                        <!-- for comment ni dre hehe -->
                                    <!-- /////////////////////////for comment na media  note: apilag copy ang col-sm-1 para ma align tanan okay>-->
                                  <?php foreach($this->superadmin_campus_post->viewAllComments($campusPostRow->post_id) as $viewAllCommentsRow):?>
                                    <div class="col-sm-2">
                                    </div>
                                    <ul class="media-list">
                                    <li class="media">
                                    <!-- for profile -->
                                    <div class="media-left">
                                    <?php if($commentRow->campus_commenter_id != "0"){?>

                                    <img src="<?php base_url();?>img/default_profile.jpg"class="media-object img-circle"alt="profile"style="height:40px;width:40px;margin-top:-0px;">
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong><?=ucfirst(strtolower($viewAllCommentsRow->firstname))." ".ucfirst(strtolower($viewAllCommentsRow->lastname))?></strong>
                                    <?php }else{
                                      ?>
                                      <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_image()?>"alt="profile"style="height:40px;width:40px;margin-top:-0px;" class="img-object img-circle">
                                    </div>
                                    <!-- end for profile -->
                                    <!-- for body -->
                                    <div class="media-body">
                                    <h5 class="media-heading"><strong>Super Admin</strong>
                                      <?php


                                      }?>

                  <!-- for dropdown ni -->
               <li class=" dropdown pull-right">
                                
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                     <ul class="dropdown-menu">
                        <li><a href="#fel_cam_com_edit"onclick="return edit_campus_comment(<?=$viewAllCommentsRow->comment_id?>);" data-toggle="modal">
                          <span class="glyphicon glyphicon-edit"></span> 
                            <p class="text-success"style="margin-left:20px;margin-top:-20px;">Edit</p></a>
                        </li>
                      <li><a href="#fel_cam_com_edit" onclick="return campus_modal_delete(<?=$viewAllCommentsRow->comment_id?>);"data-toggle="modal"><span class="glyphicon glyphicon-trash"></span> 
                 <p class="text-danger"style="margin-left:20px;margin-top:-20px;">Delete</p></a>
                    </li>    
                 </ul>
               </li>
              <!-- end sa dropdown dre hehe --> 

                                    </h5>
                                    <h6 class="text-warning"><span class="glyphicon glyphicon-time"></span><?=$viewAllCommentsRow->date?></h6>

                                    <p><em><?=$viewAllCommentsRow->comment?></em></p>
                                    <hr>

                                    </div>
                                    <!-- end for body -->
                                    </li>
                                    </ul>
                                    <?php endforeach;?>
                                    <!-- /////////////////////////end sa comment nga media -->
                                   
                                     </div>

                                    </ul>
                                    </div>
                                  </span>
                                    <!-- end for comments-->
                                    <div class="col-sm-2">
                                    <img src="<?php base_url();?>upload/user/<?=$this->superadmin_campus_post->get_image()?>"class="img-responsive"alt="user profile"style="height:auto;width:40px;">
                                    </div>
                                      <div class="input-group">
                                     
                                      <input type="text" id="acj_sa_campus_comment<?=$campusPostRow->post_id?>" class="form-control" placeholder="Add a comment..">
                                       <div class="input-group-btn">
                                       
                                       <input type="submit" value="comment" class="btn btn-primary" onclick="return sa_campus_comment(<?=$campusPostRow->post_id?>)">
                                       </div>
                                      
                                       </div>

                                   
                                   
                                  </div>
                               </div>



<?php endforeach;?>
                        
                        </div>

                <!-- end para sa post-->

                        <div class="col-sm-6">
                        <div class="panel panel-default">
                        <div class="panel-body">
                        <?=form_open_multipart(base_url().'superadmin_campus_post')?>
                       
                        <a href="#image"  class="hover" style="text-decoration:none"><label for="get_image"><span class="glyphicon glyphicon-picture text-success"></span> Add Image</label></a>
                         <input type="file" id="get_image" style="display:none" name="userfile">
                        <textarea class="form-control" cols="5" rows="5" required placeholder="What's on your mind?" name="sa_campus_post"></textarea>
                        <br>
                        <input type="hidden" id="submit">
                        <button for="submit" class="btn btn-sm btn-primary pull-right"><span class="glyphicon glyphicon-send"></span></button>
                        
                        <?=form_close()?>
                        </div>
                        </div>

                        <div class="well">

                        <span class="glyphicon glyphicon-time"> </span> Time Log's

                        <div class="table-responsive">
                        <br>
                        <table class="table table-hover table-responsive">
                           <tbody>
                               <?php foreach($last_post_date as $post_dateRow){ ?>
                               <tr>
                                   <td><strong>Last Post On: </strong></td>

                                   <td><?=$post_dateRow->date?></td>
                                <?php } ?>
                                <?php foreach($previus_post_date as $post_dateRow){ ?>
                               <tr>
                                   <td><strong>Previus Post On: </strong></td>

                                   <td><?=$post_dateRow->date?></td>
                                <?php } ?>
                               </tr>
                           </tbody> 
                        </table>
                        </div>

                        </div>

                        </div>
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>

            <!-- /main -->

         
        </div>

    </div>
    </div>
