<div class="row print-row-margin">


<div class="col-sm-2 col-md-2 col-lg-2"></div>

<div class="col-sm-8 col-md-8 col-lg-8">

<div class="panel panel-default">
<div class="panel-body">

<div class="col-sm-2 col-md-2 col-lg-2">
<img src="<?= base_url();?>img/logo.png" class="img-resposive img-logo-size" alt="Logo">
</div>

<div class="col-sm-8 col-md-8 col-lg-8">
<h4 class="text-muted text-center">
	Salazar Colleges of Science and Institute of Technology
</h4>
<h5 class="text-muted text-center">
211 N. Bacalso., Avenue Cebu City
</h5>
<h5 class="text-primary text-center">
Social Site User's Record
</h5>
<center>
<img src="<?= base_url();?>img/default_profile.jpg" class="img-responsive default-profile-size">
</center>
</div>

<div class="col-sm-6">
<br>

<div class="table-responsive">
<?php foreach($admin_records as $recordsRow){?>
<table class="table table-hover">
	
	<tbody>
		<tr>
			<td><b>ID Number:</b></td>
			<td><?=$recordsRow->admin_id_number?></td>
		</tr>
		<tr>
			<td><b>Firstname:</b></td>
			<td><?=$recordsRow->fname?></td>
		</tr>
		<tr>
			<td><b>Lastname:</b></td>
			<td><?=$recordsRow->lname?></td>
		</tr>
		<tr>
			<td><b>Middle name:</b></td>
			<td><?=$recordsRow->mname?></td>
		</tr>
		<tr>
			<td><b>Email:</b></td>
			<td></td>
		</tr>
	</tbody>
</table>

</div>



</div>

<div class="col-sm-6">
<br>
<div class="table-responsive">
<table class="table table-hover">
	<tbody>
	    <tr>
			<td><b>Email Address:</b></td>
			<td><?=$recordsRow->email?></td>
		</tr>
	    <tr>
			<td><b>Department:</b></td>
			<td><?=$recordsRow->department_name?></td>
		</tr>
		
		<tr>
			<td></td>
			<td><button data-toggle="tooltip" title="Do you want to print this record?" class="btn btn-primary btn-sm pull-right" onclick="javascript:window.print()"><span class="glyphicon glyphicon-print"></span></button></td>
		</tr>
		
	</tbody>
</table>
</div>

</div>


<?php } ?>

</div>
</div>

</div>









</div>