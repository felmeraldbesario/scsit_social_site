
<?php 
foreach($department_comments as $commentRow):
?>
<div id="fel_dept_com_edit<?=$commentRow->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

            <?php foreach($this->superadmin_department_post->getdeptCommenttoUpdate($commentRow->id) as $getComment){?>
        <!-- body -->
       <div class="form-group">
       <label> Update Comment? </label>
       <textarea id="acj_dept_update_comment<?=$getComment->id?>" class="form-control" cols="3" rows="3"><?=$getComment->comment?></textarea>
       </div>
       <br>
       <a href="#" onclick="department_comment_update(<?=$getComment->id?>,<?=$getComment->department_topic_id?>);" class="btn btn-sm btn-primary" type="submit"  data-dismiss="modal">Update</a>
        <!-- end -->
      <?php }?>


      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>

<?php 
foreach($department_comments as $commentRow):
?>
<div id="fel_dept_com_del<?=$commentRow->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
            <?php foreach($this->superadmin_campus_post->getCommenttoUpdate($commentRow->id) as $getComment){?>
          
        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Comment?</em>
        
        <br><br>
        <a href="#" onclick="return campus_delete_comment(<?=$getComment->id?>,<?=$getComment->campus_post_id?>);" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->

          <?php }?>
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>















