<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SCSIT SOCIAL SITE-ADMINISTRATOR</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?= base_url();?>css/bootstrap.min.css" rel="stylesheet">
		<link href="<?= base_url();?>css/styles.css" rel="stylesheet">
		<link href="<?= base_url();?>css/header_fold.css" rel="stylesheet">
		<link href="<?= base_url();?>css/ribbon_fel.css" rel="stylesheet">

		<!-- for data tables style -->
		<link href="<?= base_url();?>css/dataTables.bootstrap.css"ref="stylesheet">
		<!-- custom style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/custom.css">
		<!-- font awesome -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/font-awesome.css">
		<link rel="shortcut icon" href="<?= base_url();?>img/logo.png">
		<!-- felmerald style -->
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald.css">
	</head>
	<body class="bodycolor">