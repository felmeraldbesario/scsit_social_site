<!-- block -->
<div id="fel_userrecord_block" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-ban-circle"></span> Are you sure you want to block this user <em class="text-danger">Felmerald besario</em>
       	
       	<br><br>
        <a type="submit" href="#fel_userrecord_block_notification" class="btn btn-danger btn-sm" data-toggle="modal" data-dismiss="modal">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>



<div id="fel_userrecord_block_notification" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-success glyphicon glyphicon-ok-sign"></span> Successfully block user <em class="text-danger"> felmerald besario</em>
        <br><br>
        <a href="<?= base_url();?>superadmin_blocked"> <u>Click this link to see all block user's</u></a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>