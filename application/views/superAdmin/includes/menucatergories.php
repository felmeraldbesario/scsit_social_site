                        <div class="col-sm-3 col-md-3 col-lg-3">

                        <div class="panel panel-danger text-center no-boder bg-color-brown">
                        <div class="panel-body">
                        <center><span class="glyphicon glyphicon-print fa-3x"></span></center>
                        <h3>User's Record </h3>
                         </div>
                        <div class="panel-footer back-footer-brown" style="background:#f0ad4e;">
                        <a href="<?= base_url();?>superadmin_user_records"data-toggle="tooltip" title="Print user record" data-placement="bottom"class="btn btn-warning btn-sm"><span class="glyphicon glyphicon-equalizer"></span> Manage</a>
                            
                        </div>
                         </div>

                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3">

                        <div class="panel panel-danger text-center no-boder bg-color-blue">
                        <div class="panel-body">
                        <center><span class="glyphicon glyphicon-lock fa-3x"></span></center>
                        <h3>Log's </h3>
                         </div>
                        <div class="panel-footer back-footer-blue"style="background:#286090;">
                        <a href="<?= base_url();?>superadmin_logs"data-toggle="tooltip" title="See all department log's" data-placement="bottom"class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-equalizer"></span> Manage</a>
                            
                        </div>
                         </div>

                        </div>

                        <div class="col-sm-3 col-md-3 col-lg-3">

                        <div class="panel panel-danger text-center no-boder bg-color-green">
                        <div class="panel-body">
                        <center><span class="glyphicon glyphicon-plus fa-3x"></span></center>
                        <h3>Departments </h3>
                         </div>
                        <div class="panel-footer back-footer-green">
                        <a href="<?= base_url();?>superadmin_deptList"data-toggle="tooltip" title="Add Department" data-placement="bottom"class="btn btn-success btn-sm"><span class="glyphicon glyphicon-equalizer"></span> Manage</a>
                            
                        </div>
                         </div>

                        </div>

                         <div class="col-sm-3 col-md-3 col-lg-3">

                        <div class="panel panel-danger text-center no-boder bg-color-red">
                        <div class="panel-body">
                        <center><span class="glyphicon glyphicon-trash fa-3x"></span></center>
                        <h3>Blocked User's</h3>
                         </div>
                        <div class="panel-footer back-footer-red">
                        <a href="<?=base_url();?>superadmin_blocked" data-toggle="tooltip" title="See all department blocked" data-placement="bottom" class="btn btn-danger btn-sm"><span class="glyphicon glyphicon-equalizer"></span> Manage</a>
                            
                        </div>
                         </div>


                        

                        </div>
