

<?php foreach($departmentPostQuery as $departmentPostRow){?>
<div id="fel_dep_edit<?=$departmentPostRow->department_topic_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
      
        <!-- body -->
        <!-- photo -->
        <?=form_open_multipart(base_url().'update_department_post')?>
        <?php foreach($this->superadmin_department_post->updateDepartmentPost($departmentPostRow->department_topic_id) as 
        $departmentUpdateRow){?>
        
        <img src="<?php base_url();?>upload/user/<?=$departmentUpdateRow->image?>"class="img-responsive post-img"alt="user profile">
        <div class="form-group">
       
        <br>
        
        <input type="file" name="userfile">
        </div>
        <input type="hidden" name="post_id" value="<?=$departmentUpdateRow->id?>">
        <input type="hidden" name="department_id" value="<?=$departmentUpdateRow->department_id?>">
        <div class="form-group">
        <textarea cols="5" rows="5" name="department_post" class="form-control"><?=$departmentPostRow->department_topic?></textarea>
        </div>
        <br>
        <input type="submit" value="Update" class="btn btn-primary btn-sm"/>
        <!-- end -->
        <?=form_close()?>
        <?php } ?>

      </div>
     
  </div>
  </div>
</div>
<?php } ?>


<!-- delete -->
<?php foreach($departmentPostQuery as $departmentPostRow){?>
<div id="fel_dep_delete<?=$departmentPostRow->department_topic_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <?=form_open(base_url().'delete_department_post');?>
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Post</em>
        <input type="hidden" name="department_id" value="<?=$departmentPostRow->dept_id?>">
        <input type="hidden" name="post_id" value="<?=$departmentPostRow->department_topic_id?>">
        <br><br>
        <input type="submit" value="Yes" class="btn btn-danger btn-sm">
        <!-- end -->
        <?=form_close()?>
      </div>
     
  </div>
  </div>
</div>
<?php } ?>


<div id="fel_dept_com_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

          <div id="ant_department_comment">
        <!-- body -->
        
        <!-- end -->
          </div>


      </div>
     
  </div>
  </div>
</div>



<?php 
foreach($department_comments as $commentRow):
?>
<div id="fel_dept_com_del<?=$commentRow->id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
            <?php foreach($this->superadmin_campus_post->getCommenttoUpdate($commentRow->id) as $getComment){?>
          
        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Comment?</em>
        
        <br><br>
        <a href="#" onclick="return campus_delete_comment(<?=$getComment->id?>,<?=$getComment->campus_post_id?>);" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->

          <?php }?>
      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>












<!-- like views -->

<!-- like -->
<div id="fel_computer_like" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/like.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Like</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>


<!-- dislike -->

<div id="fel_computer_dislike" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/dislike.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Dislike</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- comment -->

<div id="fel_computer_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Comment</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>



<!-- for own post -->
<!-- ///////////////////////////////////////// -->
<!-- edit -->

<div id="fel_computer_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <!-- photo -->
        <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive post-img"alt="user profile">
        <br>
        <div class="form-group">
        <label class="text-muted">Change Image?</label>
        <input type="file">
        </div>

        <div class="form-group">
        <textarea cols="5" rows="5" class="form-control"></textarea>
        </div>
        <br>
        <a href="#fel_computer_update_notify" data-toggle="modal" data-dismiss="modal" type="submit" value="Update" class="btn btn-primary btn-sm">Update</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>


<!-- notify  for change-->
<div id="fel_computer_update_notify" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Status has been successfully change!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- delete -->
<div id="fel_computer_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Status</em>
        
        <br><br>
        <a href="#fel_computer_del_not" data-toggle="modal" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- del not -->
<div id="fel_computer_del_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Status has been successfully deleted!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>


<!-- //////////////////////////// -->

<!-- comment -->


<!-- edit comment -->
<div id="fel_computer_comment_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
       <div class="form-group">
       <label> Update Comment?</label>
       <textarea class="form-control" cols="3" rows="3"></textarea>
       </div>
       <br>
       <a href="#fel_computer_comment_not" class="btn btn-sm btn-primary" type="submit"  data-toggle="modal" data-dismiss="modal">Update</a>
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- edit not -->

<div id="fel_computer_comment_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Comment has been successfully change!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- delete comment -->

<div id="fel_computer_comment_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Comment?</em>
        
        <br><br>
        <a href="#fel_computer_comment_del_not" data-toggle="modal" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- del not -->
<div id="fel_computer_comment_del_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Comment has been successfully deleted!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>
