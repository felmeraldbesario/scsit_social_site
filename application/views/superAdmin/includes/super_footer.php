	
<!-- update profile pic -->
<div id="felupdateProfile" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
 Update Profile
      </div>
      <div class="modal-body">
      <div class="panel-body">
      
          <!-- current profile photo -->
          <div class="col-sm-6">

          <label class="text-muted"><span class="glyphicon glyphicon-picture"></span></label>
          <span id="acj_profile_update">
           <?php
             foreach($superadminInfo as $superadminRow){
              if($superadminRow->image == "no image" || $superadminRow->image == ""){
              ?>
           <img src="<?= base_url();?>img/default_profile.jpg"id="ant_profile_id"class="media-object"alt="profile"style="height:200px;width:200px;">
              <?php
            }else{
              ?>
           <img src="<?= base_url();?>upload/superadmin/campus_post/<?=$superadminRow->image?>"id="ant_profile_id"class="media-object"alt="profile"style="height:150px;width:150px;">
              <?php
            }
            }
            ?>
          </span>
           </div>
            <div class="col-sm-6">
            <?=form_open_multipart(base_url().'superadmin_upload/update_profile')?>
           <p class="text-muted">Do you want to change? <em class="text-warning">Please select a new photo</em> </p>
           <div class="form-group">
           <input type="file" name="userfile" onchange="ant_profile_update(event)">
           </div>
            
           </div>
       
          </div>
      </div>
      <div class="modal-footer">
          <div>
           <input type="submit" value="Update"class="btn btn-primary btn-sm pull-right" >
            <?=form_close()?>
      </div>  
      </div>
  </div>
  </div>
</div>
<!-- end here -->


<!-- notifcation -->

<div id="changepassword" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Change Password
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
     
      <div style="color:red" id="output_message" ></div>
      <br>
<label>Current Password</label><br>
<span class="text-danger" id="current_password_label"></span>
<input type="password" id="current_password" class="form-control">
<label>New Password</label><br>
<span class="text-danger" id="new_password_label"></span>
<input type="password" id="new_password" class="form-control">

<br>
<input type="submit" onclick="return change_password();" class="btn btn-primary btn-sm pull-right" >
       </div>  


     
  </div>
  </div>
</div>

<!-- para sa pages -->

<div id="pages" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">
<span id="add_post_pages">

</span>
  </div>
  </div>
</div>


<!-- para sa username change -->

<div id="changeusername" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Username Password
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
     
      <div id="username_alert"></div>
      <br>
<label>Current Username</label><br>
<span class="text-danger" id="current_username_label"></span>
<input type="text" id="current_username" class="form-control">
<label>New Username</label><br>
<span class="text-danger" id="new_username_label"></span>
<input type="text" id="new_username" class="form-control">

<br>
<input type="submit" onclick="return change_username();" class="btn btn-primary btn-sm pull-right" >
       </div>  
  </div>
  </div>
</div>


<div id="update_sa_personal_info" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    Update Personal Info
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
     <span id="ant_personal_update_id" class="text-danger"></span>
     <?php foreach($superadmin_info as $infoRow):?>
      <br>
<label>First Name</label><br>
<input type="text" id="ant_firstname" name="firstname" value="<?=$infoRow->firstname?>"class="form-control">
<label>Last Name</label><br>
<input type="text" id="ant_lastname" name="lastname" value="<?=$infoRow->lastname?>"class="form-control">
<label>Middle Name</label><br>
<input type="text" id="ant_middlename" name="middlename" value="<?=$infoRow->middlename?>" class="form-control">
<label>Faculty ID Number</label><br>
<input type="text" id="ant_faculty_id" name="faculty_id" value="<?=$infoRow->faculty_id?>" class="form-control">
<label>Email</label><br>
<input type="email" id="ant_email" name="email" value="<?=$infoRow->email?>" class="form-control">
<label>Contact Number</label><br>
<input type="int" id="ant_contact_number" name="contact_number" value="<?=$infoRow->contact_number?>" class="form-control">

<br>
<input type="submit" onclick="return change_personel_info();" class="btn btn-primary btn-sm pull-right" >
       </div>  
<?php endforeach;?>

     
  </div>
  </div>
</div>




  <!-- script references for bootstrap orayt haha -->
		<script src="<?php echo base_url();?>js/jquery.min.js"></script>
		<script src="<?php echo base_url();?>js/bootstrap.min.js"></script>
		<script src="<?php echo base_url();?>js/scripts.js"></script>
    <script src="<?php echo base_url();?>superadmin_js/superadmin.js"></script>
    <script src="<?php echo base_url();?>superadmin_js/jquery.js"></script>
    
		<script src="<?= base_url();?>js/bootbox.min.js"></script>
	

		<!-- for data tables jquery -->
		<script src="<?php echo base_url();?>js/jquery.dataTables.js"></script>
        <script src="<?php echo base_url();?>js/dataTables.bootstrap.js"></script>
        <script>
            $(document).ready(function () {
                $('#dataTables-example').dataTable();
            });
       </script>

       <script type="text/javascript">$(function(){$("[data-toggle='popover']").popover();});</script>
       <script type="text/javascript">$(function(){$("[data-toggle='tooltip']").tooltip();});</script>
        
	</body>
</html>