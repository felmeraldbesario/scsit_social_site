
<div id="create_message" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <!-- para sa ribbon -->
      <div class="header_fold sa_pages">
     <h2 style="background-color: #337ab7;">
        <span class="glyphicon glyphicon-envelope"></span> Create Message
      </h2>
   </div>
   <!-- end para sa ribbon -->
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
          <br>
      </div>
     <div class="panel-body">
     <?=form_open(base_url().'superadmin_controller/send_new_message')?>
       <div class="form-group">
        <label>TO:</label>
        
        <select class="form-control" required name="send_to">
        <?php foreach($query as $departmentRow){?>
          <option value="<?=$departmentRow->id?>"><?=$departmentRow->department?></option>
          <?php } ?>
        </select>
        
        </div>
        <div class="form-group">
        <!-- kailang kung unsa ang naa sa message nga text ma butang sad dre diritsu murag update gani.. -->
        <textarea class="form-control" rows="5" cols="5" name="new_message"></textarea>
        </div>

        <div class="form-group">
        <input type="submit" value="Send" class="btn btn-primary btn-sm">
        </div>
        <?=form_close()?>
       </div>  


     
  </div>
  </div>
</div>

<div id="reply_message_modal" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

  <div id="ant_reply_message">
  </div>
     
  </div>
  </div>
</div>


<!-- delete -->

<div id="fel_message_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this message from <em class="text-danger">12/5/15</em>

       <div class="form-group">
       <br>
       <input type="submit" value="Yes" class="btn btn-danger btn-sm">
       </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- forward -->

<div id="fel_forwarning_message" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <div class="form-group">
        <label>Forward this message to?</label>
        <select class="form-control" required>
          <option>Department</option>
          <option></option>
        </select>
        </div>

        <div class="form-group">
        <!-- kailang kung unsa ang naa sa message nga text ma butang sad dre diritsu murag update gani.. -->
        <textarea class="form-control" rows="5" cols="5"></textarea>
        </div>

        <div class="form-group">
        <a href="#fel_forward_message_confirmation" type="submit" class="btn btn-primary btn-sm" data-toggle="modal" data-dismiss="modal">Send</a>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- forward message confirmation -->
<div id="fel_forward_message_confirmation" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       <span class="text-success glyphicon glyphicon-check"></span> You have successfuly forwarded your message to <em class="text-success">Computer Departmentk</em>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>