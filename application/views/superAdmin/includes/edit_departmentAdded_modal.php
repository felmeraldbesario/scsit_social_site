
<div id="add_department" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
        <div class="header_fold">
     <h2 style="background-color: #337ab7;">
    <span class="glyphicon glyphicon-plus"></span> DEPARTMENT
      </h2>
   </div>
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
        <!-- body -->
        <?=form_open_multipart(base_url().'superadmin_upload/superadmin_add_department'); ?>
       <br>
       <div class="form-group">
       <label>Choose Image:</label>
       <input type="file" name="userfile">
       </div>
       <div class="form-group">
       <label>Department:</label>
       <input type="text" name="department_name"class="form-control">
       </div>
       <div class="form-group">
       <input type="submit" value="SUBMIT" class="btn btn-primary btn-sm">
       </div>
        
        <!-- end -->

        <?=form_close()?>
      </div>
     
  </div>
  </div>
</div>




    <?php foreach($query as $department_list_row):?>
    <!-- edit -->
    <div id="fel_edit_added_department<?=$department_list_row->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

          <div class="modal-header">
             <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
            <br>

            <!-- body -->
           <div class="form-group">
            <?=form_open_multipart(base_url().'superadmin_upload/update_department_name');?>

           <label>Update Image:</label>
           <img src="<?=base_url()?>upload/superadmin/campus_post/<?=$department_list_row->image;?>" style="height:150px;width:150px"class="img-responsive" >
           <input type="file" name="userfile">
           </div>
           <label>Please enter new name</label>
           <input type="hidden" name="department_id" value="<?=$department_list_row->id;?>">
           <input type="text" class="form-control" value="<?=$department_list_row->department;?>" name="updated_department_name">
           </div>
           <div class="form-group">
           <input type="submit" value="update" class="btn btn-primary btn-sm">
           </div>
            <?=form_close();?>
            <!-- end -->


          </div>
         
      </div>
      </div>
    </div>
    <?php endforeach;?>
    <!-- delete -->
    <?php foreach($query as $department_list_row):?>
    <div id="fel_del_added_department<?=$department_list_row->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

          <div class="modal-header">
             <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
            <br>

            <!-- body -->
           
            <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Department name</em>
           	<br>
            <br>
            <a href="#fel_del_confirm_department<?=$department_list_row->id;?>" data-toggle="modal" class="btn btn-danger btn-sm">YES</a>
           <a href="#" data-dismiss="modal" class="btn btn-primary btn-sm pull-right">NO</a>


          </div>
         
      </div>
      </div>
    </div>
    <?php endforeach;?>

<?php foreach($query as $department_list_row):?>
    <div id="fel_del_confirm_department<?=$department_list_row->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

          <div class="modal-header">
             <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
            <br>

            <!-- body -->
           
            <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Department name</em>
            <?=form_open(base_url().'delete_department');?>
            <br><br>
            <input type="hidden" name="department_id" value="<?=$department_list_row->id;?>">
            <input type="submit" value="Yes" class="btn btn-danger btn-sm">
            <?=form_close();?>
            <!-- end -->


          </div>
         
      </div>
      </div>
    </div>
    <?php endforeach;?>



    <?php foreach($query as $department_list_row):?>
    <div id="fel_add_course<?=$department_list_row->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

      <div class="modal-header"><span class="glyphicon glyphicon-check"></span>ADD COURSE
       <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
       <br><br>

       <form action="<?=base_url().'superadmin_add_course'?>" method="post">
        <input type="hidden" value="<?=$department_list_row->id?>" name="department_id">
         <div class="form-group">
         
         <input type="text" name="course" class="form-control">
         </div>
         <div class="form-group">
         <input type="submit" value="Add Course" class="btn btn-primary btn-sm pull-left">
         </div>

         <br><br>
       </form>



      </div>

      </div>
      </div>
      </div>
    <?php endforeach;?>

    <?php foreach($getCourseQuery as $courseRow):?>
    <div id="fel_edit_course<?=$courseRow->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

      <div class="modal-header"><span class="glyphicon glyphicon-check"></span>UPDATE COURSE
       <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
       <br><br>

       <form action="<?=base_url().'superadmin_controller/superadmin_update_course'?>" method="post">
         <div class="form-group">
         <input type="hidden" name="course_id" value="<?=$courseRow->id?>">
         <input type="text" name="course" value="<?=$courseRow->course?>" class="form-control">
         </div>
         <div class="form-group">
         <input type="submit" value="UPDATE" class="btn btn-primary btn-sm pull-left">
         </div>

         <br><br>
       </form>



      </div>

      </div>
      </div>
      </div>
    <?php endforeach;?>

    <?php foreach($getCourseQuery as $courseRow):?>
    <div id="fel_delete_course<?=$courseRow->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

      <div class="modal-header">
       <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
        <label>Are you sure you want to delete this course?</label>
<br><br>
        <a href="#fel_delete_confirm<?=$courseRow->id;?>" data-toggle="modal" class=" btn btn-danger btn-sm">YES</a>
        <a href="#" data-dismiss="modal" class="pull-right btn btn-primary btn-sm">NO</a>
       </div>

       </div>
      </div>
      </div>
      </div>
    <?php endforeach;?>

    <?php foreach($getCourseQuery as $courseRow):?>
    <div id="fel_delete_confirm<?=$courseRow->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
      <div class="modal-dialog">
      <div class="modal-content">

      <div class="modal-header">
       <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        
        <label>Are you sure you want to delete this course?</label>
    
       <form action="<?=base_url().'superadmin_controller/superadmin_delete_course'?>" method="post">
         <div class="form-group">
         <input type="hidden" name="course_id" value="<?=$courseRow->id?>">
         </div>
         <input type="submit" value="YES" class="btn btn-danger btn-sm">
         <a href="#" data-dismiss="modal" class="pull-right btn btn-primary btn-sm">NO</a>
         <br><br>
       </form>
        
    </div>

      </div>
      </div>
      </div>
    <?php endforeach;?>
