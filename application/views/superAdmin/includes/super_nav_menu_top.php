<div class="navbar navbar-blue navbar-static-top">  
                    <div class="navbar-header">
                      <button class="navbar-toggle" type="button" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle</span>
                        <span class="icon-bar"></span>
          				     <span class="icon-bar"></span>
          				      <span class="icon-bar"></span>
                      </button>
                      <img src="<?= base_url();?>img/logo.png" class="img-responsive hover" alt="logo" style="height:30px;margin-left:10px;margin-top:7px;">
                  	</div>
                  	<nav class="collapse navbar-collapse" role="navigation">
                    
                    <ul class="nav navbar-nav">
                      <li class="active">
                        <a href="<?= base_url();?>superadmin" class="active"><i class="glyphicon glyphicon-home"></i> Home</a>
                      </li>
                      
                      <li>
                      <a href="<?= base_url();?>superadmin_campus" data-toggle="tooltip" title="Manage All Department Post" data-placement="bottom"><span class="glyphicon glyphicon-map-marker"></span> Campus</a>
                      </li>
                      <li>
                        <a href="<?= base_url();?>superadmin_message" data-toggle="tooltip" title="Want to send message?" data-placement="bottom" role="button"><i class="glyphicon glyphicon-envelope"></i> Message</a>
                      </li>
                      
                      
                    </ul>
                    <ul class="nav navbar-nav navbar-right"style="margin-right:20px;">
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><i class="glyphicon glyphicon-cog"></i></a>
                        <ul class="dropdown-menu">
                          <li><a href="superadmin_settings"style="color:black"><span class="glyphicon glyphicon-lock"></span> Settings</a></li>
                          <li><a href="account_settings"style="color:black"><span class="glyphicon glyphicon-user"></span> Account Settings</a></li>
                          <li><a href="<?= base_url();?>superadmin_logout"style="color:black"><span class="glyphicon glyphicon-log-out"></span> Logout</a></li>
            
                        </ul>
                      </li>
                    </ul>
                  	</nav>
                </div>
                <!-- end haheheh -->