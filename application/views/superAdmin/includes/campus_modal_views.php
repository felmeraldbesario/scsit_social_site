<!-- like views -->

<!-- like -->]
<?php foreach($campusPostQuery as $campusPostRow):?>
<div id="fel_cam_like<?=$campusPostRow->post_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/like.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Like</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
         <?php foreach($this->superadmin_campus_post->listLikes($campusPostRow->post_id) as $listCommentsRow):?>
          <tr>
            <td style="height:60px;width:60px;"><img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;"></td>
            <td><?=ucfirst(strtolower($listCommentsRow->firstname))." ".ucfirst(strtolower($listCommentsRow->lastname))?></td>
          </tr>
        <?php endforeach;?>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>

<?php foreach($campusPostQuery as $campusPostRow):?>
<div id="fel_cam_comment<?=$campusPostRow->post_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Comment</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
       
        <table class="table table-hover table-bordered">
        <tbody>
         <?php foreach($this->superadmin_campus_post->listComments($campusPostRow->post_id) as $listCommentsRow):?>
          <tr>
            <td style="height:60px;width:60px;"><img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive"alt="user profile"style="height:40px;width:40px;"></td>
            <td><?=ucfirst(strtolower($listCommentsRow->firstname))." ".ucfirst(strtolower($listCommentsRow->lastname))?></td>
          </tr>
        <?php endforeach;?>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>
<?php endforeach;?>