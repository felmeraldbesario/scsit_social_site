<!-- like views -->

<!-- like -->
<div id="fel_business_like" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/like.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Like</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>


<!-- dislike -->

<div id="fel_business_dislike" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/dislike.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Dislike</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- comment -->

<div id="fel_business_comment" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <img src="<?= base_url();?>img/circle_icon/message.png" class="img-responsive news-image-height">
        <h5 class="like-margin">Person Who Comment</h5>
        <!-- end -->
        <!-- table -->
        <div class="table-responsive">
        <table class="table table-hover table-bordered">
        <tbody>
          <tr>
            <td>Name of a Person</td>

          </tr>
        </tbody>
          
        </table>
        </div>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>



<!-- for own post -->
<!-- ///////////////////////////////////////// -->
<!-- edit -->

<div id="fel_business_post_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <!-- photo -->
        <img src="<?php base_url();?>img/default_profile.jpg"class="img-responsive post-img"alt="user profile">
        <br>
        <div class="form-group">
        <label class="text-muted">Change Image?</label>
        <input type="file">
        </div>

        <div class="form-group">
        <textarea cols="5" rows="5" class="form-control"></textarea>
        </div>
        <br>
        <a href="#fel_business_update_notify" data-toggle="modal" data-dismiss="modal" type="submit" value="Update" class="btn btn-primary btn-sm">Update</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>


<!-- notify  for change-->
<div id="fel_business_update_notify" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Status has been successfully change!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- delete -->
<div id="fel_business_post_delete" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Status</em>
        
        <br><br>
        <a href="#fel_business_del_not" data-toggle="modal" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- del not -->
<div id="fel_business_del_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Status has been successfully deleted!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>


<!-- //////////////////////////// -->

<!-- comment -->


<!-- edit comment -->
<div id="fel_business_comment_edit" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
       <div class="form-group">
       <label> Update Comment?</label>
       <textarea class="form-control" cols="3" rows="3"></textarea>
       </div>
       <br>
       <a href="#fel_business_comment_not" class="btn btn-sm btn-primary" type="submit"  data-toggle="modal" data-dismiss="modal">Update</a>
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- edit not -->

<div id="fel_business_comment_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Comment has been successfully change!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

<!-- delete comment -->

<div id="fel_business_comment_delte" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Comment?</em>
        
        <br><br>
        <a href="#fel_business_comment_del_not" data-toggle="modal" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>

<!-- del not -->
<div id="fel_business_comment_del_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Comment has been successfully deleted!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>
