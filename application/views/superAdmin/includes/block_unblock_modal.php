<!-- block -->
<?php foreach($records_query as $recordsRow){?>
<div id="fel-pop-block<?=$recordsRow->user_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-warning glyphicon glyphicon-warning-sign"></span> Are you sure you want to block 
        <strong class="text-danger"><?=ucfirst(strtolower($recordsRow->firstname))." ".ucfirst(strtolower($recordsRow->lastname))?></strong>
        <?=form_open(base_url().'superadmin_block_user')?>
        <div class="form-group">
        <br>
        <input type="hidden" name="blocked_id" value="<?=$recordsRow->user_id?>">
        <label>Select Reason</label>
        <select name="reason" class="form-control" required>
        <?php foreach($reason as $reasonRow):?>
          <option value="<?=$reasonRow->id?>"><?=$reasonRow->reason?></option>
        <?php endforeach;?>
        </select>
        </div>

        <div class="form-group">
        <input type="submit"  value="Yes" class="btn btn-danger btn-sm">
        </div>
        <?=form_close()?>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>
<?php } ?>
<!-- unblock -->
<?php foreach($records_query as $recordsRow){?>
<div id="fel-pop-unblock<?=$recordsRow->user_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-warning glyphicon glyphicon-warning-sign"></span> Are you sure you want to unblock 
        <strong class="text-danger"><?=ucfirst(strtolower($recordsRow->firstname))." ".ucfirst(strtolower($recordsRow->lastname))?></strong>
        <br>

        <h6>Block Reason:<em class="text-danger"> Hacker</em></h6>
        <h6>Date Blocked:<em class="text-muted"><?=$recordsRow->blocked_date?></em></h6>
       
        <?=form_open(base_url().'superadmin_unblock_user')?>
        <input type="hidden" name="department_id" value="<?=$recordsRow->dept_id?>">
        <input type="hidden" name="user_id" value="<?=$recordsRow->user_id?>">
        <div class="form-group">
        <input type="submit" value="Yes" class="btn btn-danger btn-sm">
        </div>
        <?=form_close()?>
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>
<?php } ?>