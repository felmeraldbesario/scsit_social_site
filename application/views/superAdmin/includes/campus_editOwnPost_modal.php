<!-- edit -->
<?php foreach($campusPostQuery as $campusPostRow){?>
<div id="fel_cam_edit<?=$campusPostRow->post_id ?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>
      
        <!-- body -->
        <!-- photo -->
        <?=form_open_multipart(base_url().'update_campus_post')?>
        <?php foreach($this->superadmin_campus_post->updateCampusPost($campusPostRow->post_id) as $campusUpdateRow){?>
        <?php if($campusPostRow->image != "no image"):?>
        <img src="<?php base_url();?>upload/user/<?=$campusUpdateRow->image?>"class="img-responsive post-img"alt="user profile">
        <div class="form-group">
        <label class="text-muted">Change Image</label>
        <?php else:?>
        <div class="form-group">
        <label class="text-muted">Add Image</label>
        <?php endif;?>
        <br>
        
        <input type="file" name="userfile">
        </div>
        <input type="hidden" name="post_id" value="<?=$campusUpdateRow->id?>">
        <div class="form-group">
        <textarea cols="5" rows="5" name="campus_post" class="form-control"><?=$campusUpdateRow->campus_post?></textarea>
        </div>
        <br>
        <input type="submit" value="Update" class="btn btn-primary btn-sm"/>
        <!-- end -->
        <?=form_close()?>
        <?php } ?>

      </div>
     
  </div>
  </div>
</div>
<?php } ?>

<!-- delete -->
<?php foreach($campusPostQuery as $campusPostRow){?>
<div id="fel_cam_delete<?=$campusPostRow->post_id?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <?=form_open(base_url().'delete_campus_post');?>
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Post</em>
        <input type="hidden" name="post_id" value="<?=$campusPostRow->post_id?>">
        <br><br>
        <input type="submit" value="Yes" class="btn btn-danger btn-sm">
        <!-- end -->
        <?=form_close()?>
      </div>
     
  </div>
  </div>
</div>
<?php } ?>
<!-- del not -->
<div id="fel_cam_del_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> Status has been successfully deleted!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>

