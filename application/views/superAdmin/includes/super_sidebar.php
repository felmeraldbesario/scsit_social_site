<!-- ////////////////////////////////////sidebar -->
            <!-- sidebar -->
            <div class="column col-sm-2 col-xs-1 sidebar-offcanvas" id="sidebar"style="background:#cccccc;">
              
              <!-- kani sa toggle sidebar okay -->
                <ul class="nav">
                <a href="#" data-toggle="offcanvas" class="visible-xs text-center">
                <span style="background:grey"class="glyphicon glyphicon-chevron-right"></span></a>
              </ul>
              <!-- end for toggle side bar -->
              <?php foreach($superadminInfo as $superadminRow){?>
              	<!-- profile pics here -->
                <a href="#felupdateProfile" title="Update Profile?" data-toggle="modal" data-placement="bottom">
                <?php if($superadminRow->image == "no image" || $superadminRow->image == ""){
              ?>
           <img src="<?= base_url();?>img/default_profile.jpg"class="media-object"alt="profile"style="height:120px;width:120px;">
              <?php
            }else{
              ?>
           <img src="<?= base_url();?>upload/user/<?=$superadminRow->image?>"class="media-object"alt="profile"style="height:150px;width:150px;">
              <?php
            }?></a>
                <!--end of profile pics-->
                <!-- ////////////////////
  
                Modal found in super_footer sa update profile okay?

                 /////////////-->
                <!-- list sa user details here, name, idnumber, location, department -->
                <br>

                <ul class="nav hidden-xs" id="lg-menu">
                   <img src="<?= base_url();?>img/circle_icon/user.png"class="img-responsive hover newsheight"> <h6 class="h6sidebarmargin"><?=ucfirst(strtolower($superadminRow->firstname))." ".ucfirst(strtolower($superadminRow->lastname)) ?></h6>

                </ul>
                <?php } ?>
                <hr>
                <ul class="nav hidden-xs" id="xs-menu">
                <p class="Pcolor">News</p>
                    <a href="<?= base_url();?>superadmin_news" data-toggle="tooltip" title="Manage News"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-edit"></span> News</h6></a>

                    <a href="<?= base_url();?>superadmin_sports" data-toggle="tooltip" title="Manage Sports"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-edit"></span> Sports</h6></a>

                    <a href="<?= base_url();?>superadmin_entertainment" data-toggle="tooltip" title="Manage Entertainment"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-edit"></span> Entertainment</h6></a>

                    <a href="<?= base_url();?>superadmin_editorial" data-toggle="tooltip" title="Manage Editorial"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-edit"></span> Editorial</h6></a>

                    <a href="<?= base_url();?>superadmin_announcement" data-toggle="tooltip" title="Manage Editorial"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-edit"></span> Announcement</h6></a>

                  <p class="Pcolor">Departments</p>
                  <?php foreach($query as $department_list_row):?>
                  <a href="<?= base_url();?>superadmin_department?id=<?=$department_list_row->id;?>"data-toggle="tooltip" title="Manage <?=$department_list_row->department;?> Department"><h6 class="MarginLeftNegative"><span class="glyphicon glyphicon-tags"></span> <?=$department_list_row->department;?></h6></a>
                  <?php endforeach;?>

                </ul>
                

              
            </div>
            <!-- ////////////////////////////////////sidebar -->

            