<div id="fel_it_unblock" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
        <span class="text-warning glyphicon glyphicon-warning-sign"></span> Are you sure you want to unblock 
        <strong class="text-danger">felmerald besario</strong>
        <br>

        <h6>Block Reason:<em class="text-danger"> Hacker</em></h6>
        <h6>Date Blocked:<em class="text-muted"> Date here</em></h6>
       

        <div class="form-group">
        <a href="#fel_it_block_not" data-toggle="modal" data-dismiss="modal" type="submit" value="Yes" class="btn btn-danger btn-sm">Yes</a>
        </div>
        
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>


<!-- notofy -->
<div id="fel_it_block_not" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br><br>
       

      
        <!-- body -->
        <span class="text-success glyphicon glyphicon-check"></span> User has been successfully Unblock!
        <!-- end -->
        


      </div>
     
  </div>
  </div>
</div>
