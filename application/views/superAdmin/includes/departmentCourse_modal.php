<!-- edit -->

<!-- delete -->

<div id="fel_ed_dept_course" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
  <div class="modal-dialog">
  <div class="modal-content">

      <div class="modal-header">
         <img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
        <br>

        <!-- body -->
       
        <span class="text-danger glyphicon glyphicon-trash"></span> Are you sure you want to delete this <em class="text-danger">Course name</em>

        <br><br>
        <input type="submit" value="Yes" class="btn btn-danger btn-sm">
        <!-- end -->


      </div>
     
  </div>
  </div>
</div>