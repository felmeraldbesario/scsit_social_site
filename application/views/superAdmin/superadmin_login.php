<div class="container-fluid">
<div class="row">

 <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">

 <div class="panel panel-default">
 <div class="panel-body">
 <img src="<?= base_url();?>img/circle_icon/add_status.png" class="img-responsive superloginstatus">
 <p class="text-muted superlogin_paragraph">Login yourself to get access</p>
 <hr>

 <div style="color:red;margin-left:25px;margin-top:-10px;padding-bottom:10px"><?= $error_message; ?></div>

<form name="login_form" action="<?= base_url().'login_superadmin'?>" method="post">
<div class="form-group input-group">
 <span class="input-group-addon"><i class="glyphicon glyphicon-tag"  ></i></span>
 <input type="text" class="form-control" required placeholder="Your Username "  name="username"/>
</div>

<div class="form-group input-group">
<span class="input-group-addon"><i class="glyphicon glyphicon-lock"></i></span>
<input type="password" class="form-control" required placeholder="Your Password" name="password">
</div>

<div class="form-group">
<input type="submit" value="login" class="btn btn-sm btn-primary pull-right">
</div>
</form>




 </div>
 </div>

 </div>




</div>
</div>