<div class="wrapper">
    <div class="box">
        <div class="row row-offcanvas row-offcanvas-left">
                      
          
            <?php include'includes/super_sidebar.php';?>
            <!-- /end sa sidebar  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design- -->
          
            <!-- main right col -->
            <div class="column col-sm-10 col-xs-11" id="main">
                
                <!-- top nav  ayaw kuhaa ni nga include ky kung eh butang sa controller maguba ang design-->
                <?php include'includes/super_nav_menu_top.php';?>
                
                <!-- /top nav -->
              
                <div class="padding">
                    <div class="full col-sm-9">
                        <!-- content -->                      
                        <div class="row">


                        <div class="col-sm-12 col-md-12 col-lg-12">
                        <div class="panel panel-default">

                        <div class="panel-body">
                        <?=$this->session->flashdata('messaging_alert')?>
                         <div class="header_fold sa_page">
                          <h2 style="background-color: #337ab7;">
                            <span class="glyphicon glyphicon-envelope"></span> MESSAGE INBOX
                          </h2>
                        </div>
                        <a href="#create_message" data-toggle="modal"class="text-success pull-right"><span class="glyphicon glyphicon-envelope"></span> Create Message</a>
                        <br>

                        <div class="table-responsive">
                        <table class="table table-hover table-bordered">
                        <thead>
                            <tr>
                                <th>Date</th><th>Sender</th><th>Message</th><th>Action</th>
                            </tr>
                        </thead>
                        <tbody>

                        <?php foreach($messageInbox as $inboxRow){?>
                            <?php if($this->check_delete_message->checkDeleteMessage($inboxRow->message_id) == true){?>
                        <tr id="message_row<?=$inboxRow->message_id?>" >
                            <td><?=$inboxRow->message_date?></td>
                            <td><?=ucfirst(strtolower($inboxRow->department))?></td>
                            <td>
                               <?= nl2br($inboxRow->message)?>
                            </td>
                            <td>
                                <a href="#reply_message_modal" onclick="return reply_message(<?=$inboxRow->message_id?>);"class="text-danger" data-toggle="modal" ><span class="glyphicon glyphicon-envelope" data-toggle="tooltip" title="Reply"></span></a>&nbsp
                                <a href="#reply_message_modal" onclick="return delete_message(<?=$inboxRow->message_id?>);" class="text-danger" data-toggle="modal"><span class="glyphicon glyphicon-trash"></span></a>&nbsp
                                <a href="#reply_message_modal" onclick="return forward_message(<?=$inboxRow->message_id?>);" class="text-success" data-toggle="modal"><span class="glyphicon glyphicon-send"></span></a>
                            </td>
                        </tr>
                            <?php }} ?>
                        </tbody>
                            
                        </table>
                        </div>

                        </div>
                        </div>

                        </div>


                       
                        
                        


                      




                       </div><!--/row-->
                      <!-- ayaw nig kuhaa ky maguba ang design okay -->
                        <?php include'includes/superadmin_copyrights.php';?>
                    </div><!-- /col-9 -->
                   
                </div><!-- /padding -->

            </div>
            <!-- /main -->

         
        </div>

    </div>
    </div>
