<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SCSIT SOCIAL SITE</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?=base_url().'css/bootstrap.min.css'?>" rel="stylesheet">
		<link href="<?=base_url().'css/styles.css'?>" rel="stylesheet">
		<link rel="shortcut icon" href="<?=base_url().'img/logo.png'?>">
	</head>
<body sstyle="background-image: url(img/Map.jpg);background-repeat: no-repeat;background-size: 100% 100%;height:100%;background-attachment: fixed;">

<!-- navbar -->
<nav class="navbar navbar-default" role="navigation"style="background:#336699">
<div class="navbar-header"> 
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<img src="<?=base_url().'img/logo.png'?>"class="img-responsive hover"alt="logo"style="height:30px;margin-left:10px;margin-top:7px;"> </div>
<div class="collapse navbar-collapse " id="example-navbar-collapse">
     <!-- login inline -->

</div> 

 </nav>
   <!-- end -->

<div class="col-sm-3">

</div>

<div class="col-sm-6" style="margin-top:150px;">
	<form action="<?php echo base_url().'user_controller/verify'?>" method="post">
		<div class="panel panel-primary">
			<div class="panel-body">
				<div class="form-group" >
					<h4>Please enter your verification code which was sent to your email.</h4>
					<p class="text-danger">
						<?php echo form_error('verification');?>
						<?php echo $this->session->flashdata('IncorrectVerification');?>
					</p>
					<input type="text" name="verification" class="form-control" placeholder="verification code"/>
				</div>
			</div>

			<div class="panel-footer">
				<button class="btn btn-warning">Cancel</button>
				<input type="submit" class="btn btn-info pull-right" value="submit"/>	
			</div>
		</div>
	</form>
</div>

<div class="col-sm-3">
<?php
echo "Current timezone: ". date_default_timezone_set('Asia/Manila')."</ br>
      Current time: ".date("d-m-Y A H:i:s");
?>
</div>