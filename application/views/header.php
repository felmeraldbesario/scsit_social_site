<!-- 
#scsit social site developer's
#Maturan Jermainer User's Back End Programmer
#Felmerald Besario User's ,Administrator and Super Administrator Front End developer and Back End Programmer in Administrator.
#Antonio Canillas Jr Back End Programmer in Super Administrator
#Roy Jim Back End Coding for News Pages
#Sarol Cecilio The researcher
 -->
<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $title;?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<!-- for SEO META TAGS -->
		<meta name="robots" content="index,follow" />
		<meta name="distribution" content="global" />
		<meta name="copyright" content="Copyright © 2016 SCSIT SOCIAL SITE" />
		<meta name="rating" content="general" />
		<meta name="description" content="Salazar Colleges of Science and Institute of Technology Social Site helps students connect and updates our events" />
		<meta name="keywords" content="scsit,scsitsocialsite, socialsite, salazar colleges" />
		<!-- /end sa meta tags -->
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link rel="shortcut icon" href="img/logo.png">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald_buttons.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/chrisraldforever.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/panel_index_login.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>rald_js/date_picker/jquery-ui.css">
		<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/ribbon_fel.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald_button.css">
	</head>
<body style="background-image: url(img/Map.jpg);background-repeat: no-repeat;background-size: 100% 100%;height:100%;background-attachment: fixed;">

<!-- navbar -->
<nav class="navbar navbar-default" role="navigation"style="background:#336699">
<div class="navbar-header"> 
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<img src="<?php echo base_url(); ?>img/logo.png"class="img-responsive hover"alt="logo"style="height:30px;margin-left:10px;margin-top:7px;"> 
<h3 style="color:white; margin-left: 50px; margin-top: -25px; font-family: Facebook Letter Faces">Social Site</h3>
</div>
<div class="collapse navbar-collapse " id="example-navbar-collapse">
     <!-- login inline -->
<div class="pull-right">
<!-- form sa login -->
<form  action="<?php echo base_url().'user_controller/login'?>" method="post" enctype="multipart/form-data"class="form-inline" role="form"style="margin-top:5px;">
<div class="form-group">
<input type="text"name="login_username"class="form-control"placeholder="Username"required="">
</div>
<div class="form-group">
<input type="password"name="login_password"class="form-control"placeholder="Password"required="">
</div>
<input type="submit" value="Login"class="btn btn-default btn-md"style="margin-top:-1px;">
</form>

<!-- end sa form sa login -->

</div>
<!-- end -->
</div> 

 </nav>
   <!-- end -->
