<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title>SCSIT SOCIAL SITE</title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="<?php echo base_url().'css/bootstrap.min.css'?>" rel="stylesheet">
		<link href="<?php echo base_url();?>css/styles.css" rel="stylesheet">
		<link rel="shortcut icon" href="<?php echo base_url();?>img/logo.png">
	</head>
<body style="background-image: url(img/Map.jpg);background-repeat: no-repeat;background-size: 100% 100%;height:100%;background-attachment: fixed;">
<?php 

	// if($this->session->flashdata("admin_block") == "" || $this->session->flashdata("incorrectPassOrUsername") == "" || $this->session->flashdata("system_status") == ""){
	// 	redirect(base_url().'index');
	// }
?>
<!-- navbar -->
<nav class="navbar navbar-default" role="navigation"style="background:#336699">
<div class="navbar-header"> 
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<img src="<?php echo base_url();?>img/logo.png"class="img-responsive hover"alt="logo"style="height:30px;margin-left:10px;margin-top:7px;"> </div>
<div class="collapse navbar-collapse " id="example-navbar-collapse">
</div> 
</nav>
<!-- end -->

<!-- body -->
<div class="container-fluid">
<br><br><br><br><br><br>

<div class="col-sm-3">
</div>
<div class="col-sm-6">
<div class="well">
<!-- dismissable alert -->
<!-- pwede nmung queryhan ang dismissable nga alert after sa user eh press and close ky d na mu blik nig refresh -->
<div class="alert alert-danger alert-dismissable">
<button type="button"class="close"data-dismiss="alert"aria-hidden="true">x</button>
<?php echo $this->session->flashdata("admin_block");?>
<?php echo $this->session->flashdata("incorrectPassOrUsername");?>
<?=$this->session->flashdata("system_status")?>
<a href="#"class="alert-link">
</div>
<!-- end -->

<!-- login panel -->
<form action="<?php echo base_url().'user_controller/login'?>" method="post">
<div class="form-group">
<input type="text"name="login_username"class="form-control input-lg"placeholder="Username"required="">
</div>
<div class="form-group">
<input type="password"name="login_password"class="form-control input-lg"placeholder="Password"required="">
</div>
<div class="form-group">
<input type="submit"value="Login"class="btn btn-primary">
</div>
</form>
<!-- end -->


</div>
</div>
<div class="col-sm-3">
</div>



</div>
<!-- end -->
<br><br><br><br><br><br>

<?php include'footer.php';?>