

<!-- ====================================================QUERY=============================================== -->


<div class="container-fluid">
	<div class="col s12">
		<div class="panel-body">
			<ol class="breadcrumb pull-right panel-shadow">
					<li class="text-muted">
						News Page
					</li>
						<li>
							<a href="<?= base_url();?>adver_sports">Sports Page</a>
						</li>
							<li>
								<a href="<?= base_url();?>adver_editorial">Editorial Page</a>
							</li>
								<li>
									<a href="<?= base_url();?>adver_entertainment">Entertainment Page</a>
								</li>
									<li>
										<a href="<?= base_url();?>index">Login</a>
									</li>	
			
			</ol>
		</div>
	</div>


<div class="row">
<?php if($queryRecord->num_rows() !=0){ ?>
	<?php foreach($queryRecord->result() as $newsRow):?>

		
       	 <div class="col s6">
          <div class="card" style="margin-top: -15px;">
          
            <div class="card-image">
            <?php if($newsRow->image !="no image"){ ?>
              <img src="<?= base_url();?>upload/user/<?= $newsRow->image;?>" style="height: 250px;">
              <span class="card-title"><?php echo ucwords(strtoupper($newsRow->title));?></span>
             <?php } ?>
            </div>
          
            <div class="card-content">
              <p>
            <?php 
              $newsContent = character_limiter(nl2br(ucfirst($newsRow->newsContent)),200);
              echo $newsContent;
              ?>
               <a class="text-danger" data-toggle="collapse" data-target="#demo<?= $newsRow->id; ?>">Click me to view all</a>
             </p>

              <!-- sa slide ni ha.. -->
  			<div id="demo<?= $newsRow->id; ?>" class="collapse">
  				
    			<?= nl2br(substr("$newsRow->newsContent",200)); ?>
    			<br><br>
    			<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $newsRow->id; ?>">Click me to hide
    			</a>
  			</div>

  		  <!-- end sa slide ni ha  -->
             
            </div>
            <div class="card-action">
              <h6 class="text-danger" style="font-size: 12px;"><i class="glyphicon glyphicon-time text-danger"></i> Date Published <?php echo $newsRow->date;?></h6>
            </div>



            
          </div>

         

        </div>
     

	<?php endforeach;?>
<?php }else{ ?>
<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h4 class="text-muted">
					<span class="text-danger glyphicon glyphicon-pushpin"></span>
					No News Published!
				</h4>
			</div>
		</div>
	</div>
	<?php } ?>

 </div>



<?php include'adver_copyright.php';?>

</div>



<!-- ====================================================QUERY=============================================== -->

<!-- modal -->


