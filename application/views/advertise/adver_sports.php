<div class="container-fluid">
	<div class="col s12">
		<div class="panel-body">

			<ol class="breadcrumb pull-right panel-shadow">
					<li >
						<a href="<?= base_url();?>adver_news">News Page</a>
					</li>
						<li class="text-muted">
							Sports Page
						</li>
							<li>
								<a href="<?= base_url();?>adver_editorial">Editorial Page</a>
							</li>
								<li>
									<a href="<?= base_url();?>adver_entertainment">Entertainment Page</a>
								</li>
									<li>
										<a href="<?= base_url();?>index">Login</a>
									</li>	
			
			</ol>


		</div>
	</div>


	<div class="row">

	<?php if($querySports->num_rows() !=0){?>
		<?php foreach($querySports->result() as $RowSport): ?>
		<div class="col s6">
			<div class="card" style="margin-top:-15px;">
				<div class="card-image">
				<?php if($RowSport->picture != "no image"){?>
					<img src="<?=base_url();?>upload/user/<?= $RowSport->picture; ?>" class="img-responsive" style="height: 250px;">
						<span class="card-title"><?= ucwords(strtoupper($RowSport->title)); ?></span>
				<?php } ?>
				</div>
					<div class="card-content">
						<p>
						<?php 
						$content = character_limiter(nl2br(ucfirst($RowSport->content)),200);
						echo $content;
						?>
						<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $RowSport->id; ?>">Click me to view all</a>
						</p>

						  <!-- sa slide ni ha.. -->
  			<div id="demo<?= $RowSport->id; ?>" class="collapse">
  				
    			<?= nl2br(substr("$RowSport->content",200)); ?>
    			<br><br>
    			<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $RowSport->id; ?>">Click me to hide
    			</a>
  			</div>

  		  <!-- end sa slide ni ha  -->


					</div>
						<div class="card-action">
							<h6 class="text-danger" style="font-size: 12px;"><i class="glyphicon glyphicon-time text-danger"></i> Date Published <?= $RowSport->date;?>
							</h6>
						</div>
			</div>
		</div>
	<?php endforeach; ?>
	<?php }else{?>

		<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h4 class="text-muted">
					<span class="text-danger glyphicon glyphicon-pushpin"></span>
					No Sport News Published!
				</h4>
			</div>
		</div>
	</div>

		<?php } ?>


	</div>



<?php include'adver_copyright.php';?>
</div>
