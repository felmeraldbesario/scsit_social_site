<div class="container-fluid">
	<div class="col s12">
		<div class="panel-body">

			<ol class="breadcrumb pull-right panel-shadow">
					<li >
						<a href="<?= base_url();?>adver_news">News Page</a>
					</li>
						<li >
							<a href="<?= base_url();?>adver_sports">Sports Page</a>
						</li>
							<li class="text-muted">
								Editorial Page
							</li>
								<li>
									<a href="<?= base_url();?>adver_entertainment">Entertainment Page</a>
								</li>
									<li>
										<a href="<?= base_url();?>index">Login</a>
									</li>	
			
			</ol>


		</div>
	</div>


	<div class="row">
<?php if($queryEditorial->num_rows() !=0){?>
	<?php foreach($queryEditorial->result() as $RowEditorial):?>
		<div class="col s6">
			<div class="card" style="margin-top:-15px;">
				<div class="card-image">
				<?php if($RowEditorial->picture !="no image"){ ?>
					<img src="<?= base_url(); ?>upload/user/<?= $RowEditorial->picture; ?>" class="img-responsive" style="height: 250px;">
						<span class="card-title"><?= ucwords(strtoupper($RowEditorial->title)); ?></span>
				<?php } ?>
			
				</div>
					<div class="card-content">
						<p>
							<?php 
							$content = character_limiter(nl2br(ucfirst($RowEditorial->content)),200);
							echo $content;
							?>
							<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $RowEditorial->id; ?>">Click me to view all</a>
							  <!-- sa slide ni ha.. -->
  			<div id="demo<?= $RowEditorial->id; ?>" class="collapse">
  				
    			<?= nl2br(substr("$RowEditorial->content",200)); ?>
    			<br><br>
    			<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $RowEditorial->id; ?>">Click me to hide
    			</a>
  			</div>

  		  <!-- end sa slide ni ha  -->
						</p>
					</div>
						<div class="card-action">
							<h6 class="text-danger" style="font-size: 12px;"><i class="glyphicon glyphicon-time text-danger"></i> Date Published <?= $RowEditorial->date; ?>
							</h6>
						</div>
			</div>
		</div>
	<?php endforeach;?>
<?php }else{ ?>

	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h4 class="text-muted">
					<span class="text-danger glyphicon glyphicon-pushpin"></span>
					No Editorial News Published!
				</h4>
			</div>
		</div>
	</div>


<?php } ?>


	</div>


<?php include'adver_copyright.php';?>	

</div>
