<div class="container-fluid">
	<div class="col s12">
		<div class="panel-body">

			<ol class="breadcrumb pull-right panel-shadow" style="word-break: break-all;">
					<li >
						<a href="<?= base_url();?>adver_news">News Page</a>
					</li>
						<li >
						<a href="<?= base_url();?>adver_sports">Sports Page</a>
							
						</li>
							<li>
								<a href="<?= base_url();?>adver_editorial">Editorial Page</a>
							</li>
								<li class="text-muted">
									Entertainment Page
								</li>
									<li>
										<a href="<?= base_url();?>index">Login</a>
									</li>	
			
			</ol>


		</div>
	</div>


	<div class="row">
<?php if($queryEnter->num_rows() !=0){?>
	<?php foreach($queryEnter->result() as $entertainmentRow):?>
		<div class="col s6">
			<div class="card" style="margin-top:-15px;">
				<div class="card-image">
				<?php if($entertainmentRow->picture !="no image"):?>
					<img src="<?= base_url(); ?>upload/user/<?= $entertainmentRow->picture;?>" class="img-responsive" style="height: 250px;">
						<span class="card-title"><?= strtoupper($entertainmentRow->title); ?></span>
				<?php endif;?>
				</div>
					<div class="card-content">
						<p>
							<?php 
							$content = character_limiter(nl2br(ucfirst($entertainmentRow->content)),200);
							echo $content;
							?>
						</p>
						<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $entertainmentRow->id; ?>">Click me to view all</a>
						 <!-- sa slide ni ha.. -->
  			<div id="demo<?= $entertainmentRow->id; ?>" class="collapse">
  				
    			<?= nl2br(substr("$entertainmentRow->content",200)); ?>
    			<br><br>
    			<a class="text-danger" data-toggle="collapse" data-target="#demo<?= $entertainmentRow->id; ?>">Click me to hide
    			</a>
  			</div>

  		  <!-- end sa slide ni ha  -->
						</p>
					</div>
						<div class="card-action">
							<h6 class="text-danger" style="font-size: 12px;"><i class="glyphicon glyphicon-time text-danger"></i> Date Published <?php echo $entertainmentRow->date;?>
							</h6>
						</div>
			</div>
		</div>
	<?php endforeach; ?>
<?php }else{?>

	<div class="col-sm-2"></div>
	<div class="col-sm-8">
		<div class="panel panel-default panel-shadow">
			<div class="panel-body">
				<h4 class="text-muted">
					<span class="text-danger glyphicon glyphicon-pushpin"></span>
					No Entertainment News Published!
				</h4>
			</div>
		</div>
	</div>

<?php } ?>

	</div>



<?php include'adver_copyright.php';?>
</div>
