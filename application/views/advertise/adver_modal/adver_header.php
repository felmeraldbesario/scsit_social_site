<!DOCTYPE html>
<html lang="en">
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8">
		<meta charset="utf-8">
		<title><?php echo $title; ?></title>
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
		<link href="css/bootstrap.css" rel="stylesheet">
		<link href="css/styles.css" rel="stylesheet">
		<link rel="shortcut icon" href="img/logo.png">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/felmerald_buttons.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/chrisraldforever.css">
		<link rel="stylesheet" type="text/css" href="<?= base_url();?>css/materialize/materialize.css">
		<link href="<?= base_url();?>css/dataTables.bootstrap.css"ref="stylesheet">
	</head>
<body style="background-image: url(img/Map.jpg);background-repeat: no-repeat;background-size: 100% 100%;height:100%;background-attachment: fixed;">
<!-- navbar -->
<nav class="navbar navbar-default" role="navigation"style="background:#336699;border:0px;border-radius: 0px;height: 40px;"> 
<div class="navbar-header"> 
<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#example-navbar-collapse"> 
<span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> 
<span class="icon-bar"></span>
<span class="icon-bar"></span>
</button>
<img src="img/logo.png"class="img-responsive hover"alt="logo"style="height:30px;margin-left:10px;margin-top:10px;"> </div>

</nav>
