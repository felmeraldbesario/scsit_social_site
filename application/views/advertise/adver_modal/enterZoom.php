<!-- entertianment Property -->
<?php foreach($entertainementRead as $select_department){ ?>
<div id="enter<?= $select_department->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">
				<img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
				<h4 class="text-primary"><span class="glyphicon glyphicon-pushpin"></span><strong> Latest Published Entertainment News</strong></h4>
			</div>
				<div class="modal-body">
					<div class="panel-body">
						<div class="panel panel-default panel-shadow">
							<div class="panel-body">
					<?php $this->db->where('id',$select_department->id); $queryEnter = $this->db->get('entertainment_news'); foreach($queryEnter->result() as $rowEnter){?>
						<address>
						<strong><?= ucwords(strtolower($rowEnter->title));?></strong>
						<h6 class="text-danger"><span class="glyphicon glyphicon-time"></span> <?= $rowEnter->date; ?></h6>
						</address>
						<hr>
						<h5>
							<?= nl2br(ucfirst(strtolower($rowEnter->entertainment))); ?>
						</h5>
					<?php } ?>
							</div>
						</div>

							
					</div>
				</div>
					<div class="modal-footer">
						<?= $newsAdverFooter;?>
					</div>
		</div>
	</div>
</div>
<?php } ?>