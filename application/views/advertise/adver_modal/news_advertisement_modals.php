
<!-- news ni dre -->
<?php foreach($newsRead as $select_department){?>
<div id="newsRead<?= $select_department->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">	
			<div class="modal-header">

				<img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
				<h4 class="text-primary"><span class="glyphicon glyphicon-pushpin"></span><strong> Latest Published News</strong></h4>
       

			</div>
				<div class="modal-body">
					<div class="panel-body">
						<div class="panel panel-default panel-shadow">
							<div class="panel-body">
					<?php $this->db->where('id',$select_department->id); $query = $this->db->get('news'); foreach($query->result() as $row){?>
						<address>
						<strong><?= ucwords(strtolower($row->title)); ?></strong>
						<h6 class="text-danger"><span class="glyphicon glyphicon-time"></span> <?= $row->date;?></h6>
						</address>
						<hr>
						<h5>
							<?= nl2br(ucfirst($row->news_body)); ?>
						</h5>
					<?php } ?>
							</div>
						</div>

							
					</div>
				</div>
					<div class="modal-footer">
					<?= $newsAdverFooter;?>
					</div>
		</div>
	</div>
</div>
<?php } ?>

<!-- sports property -->
<?php foreach($sportsRead as $select_department){?>
<div id="sportsRead<?php echo $select_department->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true" >
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
				<h4 class="text-primary"><span class="glyphicon glyphicon-pushpin"></span><strong> Latest Published Sports News</strong></h4>

			</div>
				<div class="modal-body">
					<div class="panel-body">
						<div class="panel panel-default panel-shadow">
							<div class="panel-body">
					<?php $this->db->where('id',$select_department->id); $query = $this->db->get('sports'); foreach($query->result() as $data){?>
								<address>
									<strong><?= ucwords(strtolower($data->title)); ?></strong>
									<h6 class="text-danger"><?= $data->date;?></h6>
								</address>
									<hr>
									<h5>
										<?= nl2br(ucfirst($data->sports_body)); ?>
									</h5>
							<?php } ?>

							</div>
						</div>
					</div>
				</div>
					<div class="modal-footer">
					<?= $newsAdverFooter; ?>
					</div>
		</div>
	</div>
</div>
<?php } ?>




<!-- editorial Property -->
<?php foreach($editorialRead as $select_department){?>
<div id="editorialRead<?php echo $select_department->id;?>" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content">
			<div class="modal-header">

				<img src="<?= base_url();?>img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
				<h4 class="text-primary"><span class="glyphicon glyphicon-pushpin"></span><strong> Latest Published Editorial News</strong></h4>

			</div>
				<div class="modal-body">
					<div class="panel-body">
						<div class="panel panel-default panel-shadow">
							<div class="panel-body">
		<?php $this->db->where('id',$select_department->id); $query = $this->db->get('editorial_new'); foreach($query->result() as $data){?>
								<address>
									<strong><?= ucwords(strtolower($data->title)); ?></strong>
									<h6 class="text-danger"><?= $data->date;?></h6>
								</address>
									<hr>
									<h5>
										<?= nl2br(ucfirst($data->editorial)); ?>
									</h5>
				<?php } ?>
							</div>
						</div>
					</div>
				</div>
					<div class="modal-footer">
					</div>

		</div>
	</div>
</div>
<?php } ?>


<!-- entertainement property -->



