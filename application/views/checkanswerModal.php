<!-- check answer -->
<div id="felcheckanswer" class="modal fade" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">

            <div class="modal-header">
                <img src="img/circle_icon/close.png"class="img-responsive hover pull-right"alt="x"data-dismiss="modal"style="height:17px;">
                <br>
                <label class="text-success">
                    <span class="glyphicon glyphicon-check"></span> ID Number Valid</label>
                <br>
                <label class="text-danger">
                    <span class="glyphicon glyphicon-warning-sign"></span> Invalid ID Number
                </label>
                <div class="table-responsive">
                    <table class="table table-hover"style="border:1px red">
                        <thead>
                            <tr>
                                <th class="text-danger"><span class="glyphicon glyphicon-warning-sign"></span> Note:</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><span class="glyphicon glyphicon-question-sign"></span> User must request their Department Administrator to put their ID number to database, before user can register.<br><br>
                                    <span class="glyphicon glyphicon-question-sign"></span> Each Department has one Administrator
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                <!-- 
                READ PLEASE 
                
                Guide for query
                Hello good day, my group kanang eh if else ang Id number valid ug Invalid Id Number
                kailangan if true usa ray mu print which is Id number Valid if False print Invalid Id nUmber
                
                So meaning ang user nga wala pa nka request sa Department Admin to put their Id Number to the database
                para secure nga taga SCSIT ra jud ang mka register..
                
                Afterwards eh butang nang Note nga naka table kuyog sa Invalid Id Number
                
                
                -->

            </div>

        </div>
    </div>
</div>