<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "user_controller";
$route['index']="user_controller/index";




// for advertise
// for adver
$route['adver_news']="adver_controller/adver_news";
$route['adver_entertainment']="adver_controller/adver_entertainment";
$route['adver_editorial']="adver_controller/adver_editorial";
$route['adver_sports']="adver_controller/adver_sports";
// end adver

// for admin routing
// admin_controller

$route['administrator']="admin_controller/administrator";
$route['admin_login']="admin_controller/admin_login";
$route['login']="admin_controller/login";
$route['admin_sidebar']="admin_controller/admin_sidebar";
$route['admin_nav_menu_top']="admin_controller/admin_nav_menu_top";
$route['admin_signup']="admin_controller/admin_signup";
$route['department_selected']="upload/department_selected";
$route['admin_printuser']="admin_controller/admin_printuser";
$route['department_message']="upload/department_message";
$route['admin_news']="upload/admin_news";
$route['admin_sports']="upload/admin_sports";
$route['admin_entertainment']="upload/admin_entertainment";
$route['admin_editorial']="upload/admin_editorial";
$route['success']="admin_controller/success";
$route['admin_registration']="upload/admin_registration";
$route['change_password']="admin_controller/change_password";

// php queries

$route['logout']="admin_controller/logout";
$route['add_course']="upload/add_course";
$route['Update_profile_dept_admin']="upload/Update_profile_dept_admin";
$route['account_update']="admin_controller/account_update";
$route['insert_news']="upload/insert_news";
$route['insert_sports']="upload/insert_sports";
$route['insert_intertainment']="upload/insert_intertainment";
$route['insert_editorial']="upload/insert_editorial";
$route['fel_updating_news']="upload/fel_updating_news";
$route['fel_updating_sports']="upload/fel_updating_sports";
$route['fel_updating_entertainment']="upload/fel_updating_entertainment";
$route['fel_updating_editorial']="upload/fel_updating_editorial";
$route['delete_news']="admin_controller/delete_news";
$route['delete_sports']="admin_controller/delete_sports";
$route['delete_entertainment']="admin_controller/delete_entertainment";
$route['delete_enditorial']="admin_controller/delete_enditorial";
$route['dept_message']="upload/dept_message";
$route['delete_message']="admin_controller/delete_message";
$route['delete_course']="admin_controller/delete_course";
$route['Updating_course']="admin_controller/Updating_course";
$route['block_user']="admin_controller/block_user";
$route['unblock_user_blocked_dept']="admin_controller/unblock_user_blocked_dept";
$route['delete_unblock']="admin_controller/delete_unblock";
$route['add_department_status']="upload/add_department_status";
$route['department_announcement']="admin_controller/department_announcement";
$route['announcement_insert']="admin_controller/announcement_insert";
$route['dept_message_forward']="upload/dept_message_forward";
$route['reply_message']="upload/reply_message";
$route['update_announcement']="admin_controller/update_announcement";
$route['delete_announcement']="admin_controller/delete_announcement";
$route['admin_department_comment']="admin_controller/admin_department_comment";
$route['delete_dept_status']="admin_controller/delete_dept_status";
$route['edit_departmentPost']="upload/edit_departmentPost";
// end

//superadmin routes
$route['superadmin_login']="superadmin_controller/superadmin_login";
$route['login_superadmin']="superadmin_controller/login_superadmin"; 
$route['superadmin_logout']="superadmin_controller/logout_superadmin";
$route['superadmin_add_course']="superadmin_controller/superadmin_add_course";
$route['delete_department']="superadmin_controller/delete_department";
$route['superadmin']="superadmin_controller/superadmin";
$route['superadmin_block_user']="superadmin_controller/superadmin_block_user";
$route['superadmin_unblock_user']="superadmin_controller/superadmin_unblock_user";
$route['update_campus_post']="superadmin_upload/update_campus_post";
$route['delete_campus_post']="superadmin_controller/delete_campus_post";


$route['superadmin_blocked']="superadmin_controller/superadmin_blocked/0";

$route['superadmin_department_post']="superadmin_upload/superadmin_department_post";
$route['update_department_post']="superadmin_upload/update_department_post";
$route['delete_department_post']="superadmin_controller/delete_department_post";

$route['account_settings']="superadmin_controller/account_settings";
$route['superadmin_logs']="superadmin_controller/superadmin_logs";

$route['superadmin_deptList']="superadmin_controller/superadmin_deptList";
$route['superadmin_user_records']="superadmin_controller/superadmin_user_records";
$route['superadmin_print_records']="superadmin_controller/superadmin_print_records";
$route['superadmin_message']="superadmin_controller/superadmin_message";
$route['superadmin_campus']="superadmin_controller/superadmin_campus";
$route['superadmin_campus_post'] = "superadmin_upload/superadmin_campus_post";
$route['superadmin_department']="superadmin_controller/superadmin_department";
$route['superadmin_faculty']="superadmin_controller/superadmin_faculty";
$route['superadmin_news']="superadmin_controller/superadmin_news";
$route['superadmin_sports']="superadmin_controller/superadmin_sports";
$route['superadmin_entertainment']="superadmin_controller/superadmin_entertainment";
$route['superadmin_editorial']="superadmin_controller/superadmin_editorial";
$route['superadmin_settings']="superadmin_controller/superadmin_settings";
$route['superadmin_announcement']="superadmin_controller/superadmin_announcement";



// =====================user================================//


$route['home']="user_controller/home";
$route['user_wall_modal']="user_controller/user_wall_modal";
$route['delete_confirmation_modal']="user_controller/delete_confirmation_modal";
$route['sidebar']="user_controller/sidebar";
$route['nav_menu_top']="user_controller/nav_menu_top";
$route['user_header']="user_controller/user_header";
$route['edits_popup_modal']="user_controller/edits_popup_modal";
$route['user_department_modal']="user_controller/user_department_modal";
$route['user_messaging']="user_controller/user_messaging";
$route['profile']="user_controller/profile";
$route['department']="user_controller/department";
$route['index']="user_controller/index";
$route['header']="user_controller/header";
$route['footer']="user_controller/footer";
$route['auth']="user_controller/auth";
$route['signup']="user_controller/signup";
$route['checkanswerModal']="user_controller/checkanswerModal";
$route['newspageModal']="user_controller/newspageModal";
$route['newspageEditCommentModal']="user_controller/newspageEditCommentModal";
$route['newspageDeleteConfirmationModal']="user_controller/newspageDeleteConfirmationModal";
$route['departmentEditandDeleteModal']="user_controller/departmentEditandDeleteModal";
$route['debate']="user_controller/debate";
$route['left_sidebar']="user_controller/left_sidebar";
$route['search_result_modal']="user_controller/search_result_modal";
$route['search_result_friendlist']="user_controller/search_result_friendlist";
$route['search_result_blocklist']="user_controller/search_result_blocklist";
$route['search_result_unfollow']="user_controller/search_result_unfollow";
$route['change_pass_modal']="user_controller/change_pass_modal";
$route['verification']="user_controller/verification";
$route['userlogout'] = "user_controller/userlogout";

//user_roycontroller
$route['newspage']="user_roycontroller/newspage";
$route['newspagemodaltwocomment']="user_roycontroller/newspagemodaltwocomment";
$route['newspagemodalallcomment']="user_roycontroller/newspagemodalallcomment";
$route['newspagemodalview_like_comment']="user_roycontroller/newspagemodalview_like_comment";
$route['newspagemodal_readmore']="user_roycontroller/newspagemodal_readmore";
$route['sports']="user_roycontroller/sports";
$route['sportsmodaltwocomment']="user_roycontroller/sportsmodaltwocomment";
$route['sportsmodalallcomment']="user_roycontroller/sportsmodalallcomment";
$route['entertainment']="user_roycontroller/entertainment";
$route['editorial']="user_roycontroller/editorial";
$route['announcement']="user_roycontroller/announcement";
//end user_roycontroller






$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */