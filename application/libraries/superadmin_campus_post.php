<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class superadmin_campus_post {

    public function __construct()
    {
        $this->CI =& get_instance();
	}

    public function getLikes($id){
    	$this->CI->db->where('topic_id',$id);
        $feedback = $this->CI->db->count_all_results('campus_post_feedback');
    	return $feedback;
    }
    public function getComments($id){
    	$this->CI->db->where('campus_post_id',$id);
        $comments = $this->CI->db->count_all_results('campus_post_comment');
        return $comments;
    }
    public function showComments($post_id){
        
        $query = "SELECT campus_post_comment.id as comment_id,firstname,lastname,campus_commenter_id,campus_post_id,comment,date FROM campus_post_comment LEFT JOIN user ON campus_post_comment.campus_commenter_id = user.id 
        WHERE campus_post_comment.campus_post_id = $post_id ORDER BY campus_post_comment.id DESC LIMIT 2";
        $showComments = $this->CI->db->query($query);

        return $showComments->result();
    }
    public function viewAllComments($id){
        $this->CI->db->where('campus_post_id',$id);
        $count = $this->CI->db->count_all_results('campus_post_comment');

        $query = "SELECT campus_post_comment.id as comment_id,firstname,lastname,campus_commenter_id,campus_post_id,comment,date FROM campus_post_comment LEFT JOIN user ON user.id = campus_post_comment.campus_commenter_id
                  WHERE campus_post_comment.campus_post_id = $id ORDER BY campus_post_comment.id DESC LIMIT $count offset 2";
        $showComments = $this->CI->db->query($query);

        return $showComments->result();

    }
    public function listComments($id){
        $this->CI->db->select('*');
        $this->CI->db->from('campus_post_comment');
        $this->CI->db->join('user','user.id = campus_post_comment.campus_commenter_id','left');
        $this->CI->db->where('campus_post_comment.campus_post_id',$id);
        $this->CI->db->group_by('user.id');
        $listComments = $this->CI->db->get();

        return $listComments->result();
    }
    public function listLikes($id){
        $this->CI->db->select('*');
        $this->CI->db->from('campus_post_feedback');
        $this->CI->db->join('user','user.id = campus_post_feedback.student_id','left');
        $this->CI->db->where('topic_id',$id);
        $this->CI->db->group_by('user.id');
        $listLikes = $this->CI->db->get();

        return $listLikes->result();
    }

    public function getCommenttoUpdate($comment_id){
        $this->CI->db->where('id',$comment_id);
        $currentComment = $this->CI->db->get('campus_post_comment');

        return $currentComment->result();
    }

    public function updateCampusPost($id){
        $this->CI->db->where('id',$id);
        $getPost = $this->CI->db->get('campus_post');

        return $getPost->result();
    }
    public function get_image(){
        $query ="SELECT image FROM super_admin_info";
        $getImage = $this->CI->db->query($query);
        foreach($getImage->result() as $imageRow){
            return $imageRow->image;
        }
    }
    public function get_user_image($user_id){
        $this->CI->db->where('user_id',$user_id);
        $check_user = $this->CI->db->get('user_profile_pic');

        if($check_user->num_rows() > 0){
            foreach($check_user->result() as $userRow){
            return $userRow->image;
            }
        }else{
            return false;
        }

    }
}

?>