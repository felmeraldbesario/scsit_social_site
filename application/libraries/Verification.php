<?php
    /**
     * Author: Jermaine Maturan
     * @param: none
     * purpose: for verification codes
     */
    class Verification extends CI_Model{
        
        /**
         * Name: verificationCode
         * @param: none
         * @return string
         * purpose: for generating of 6 characters verification code 
         */
        public function verificationCode(){
            $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $charLength = strlen($characters);
            $randomChar = "";
            for($i = 0; $i <= 6; $i++){
                $randomChar = $randomChar.$characters[rand(0, $charLength -1)];
            }
            return $randomChar;
        }

         /**
         * Name: question
         * @param: none
         * @return string
         * purpose: for generating answer and question
         */
        public function question(){
            $question1 = "Who is the founder of SCSIT?";
            $question2 = "Who is Jermaine Maturan?";
            $question3 = "Who is me?";

            $answer1 = "Duterte";
            $answer2 = "Robot";
            $answer3 = "I";

            $questionArray = array(
                    '1' => "Who is the founder of SCSIT?",
                    '2' => "Who is Jermaine Maturan?",
                    '3' => "Who is me?"
                );

            $answer = array(
                    '1' => 'Duterte',
                    '2' => 'Robot',
                    '3' => 'I'
                );
            $number = rand(1, 3);
            $array = array(
                    'question' => $questionArray[$number],
                    'answer' => $answer[$number]
                );
            return $array;          
        }

        public function tokenizer($count){

           $characters = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
            $charLength = strlen($characters);
            $randomChar = "";
            for($i = 0; $i <= $count; $i++){
                $randomChar = $randomChar.$characters[rand(0, $charLength -1)];
            }
           $query = $this->db->query("SELECT token FROM user WHERE token='".$randomChar."'");
           if($query->num_rows() == 0){
             return $randomChar;
           }else{
                $this->tokenizer($count);
           }
        }
    }