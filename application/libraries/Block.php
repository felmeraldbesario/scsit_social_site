<?php
    /**
     * Author: Jermaine Maturan
     * @param: none
     * purpose: check session
     */
    class Block extends CI_Model{

    	public function checkSession(){
    		if(!$this->session->userdata('pre_username') && !$this->session->userdata('pre_firstname')){
    			redirect(base_url().'index');
    		}
    	}

        public function checkUserSession(){
            if(!$this->session->userdata('session_id')  && !$this->session->userdata('session_token')){
               redirect(base_url().'index');
            }
            
        }
    }