<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class admin_department_post {

    public function __construct()
    {
        $this->CI =& get_instance();
	}
    

    public function department_post_admin($department_user_id,$department_admin_id_number){
        

        $this->CI->db->where('id',$department_user_id);
        $this->CI->db->where('admin_id_number',$department_admin_id_number);
        $check_admin_personel = $this->CI->db->get('admin_personel');
        if($check_admin_personel->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    public function department_admin_image($admin_id_number){

        $this->CI->db->where('admin_id_number',$admin_id_number);
        $check_admin_personel = $this->CI->db->get('admin_personel');
        
        if($check_admin_personel->num_rows() > 0){
            foreach($check_admin_personel->result() as $adminRow){
            return $adminRow->image;
        }
        }else{
            return false;
        }

    }

    public function get_user_image($user_id){
        $this->CI->db->where('user_id',$user_id);
        $check_user = $this->CI->db->get('user_profile_pic');

        if($check_user->num_rows() > 0){
            foreach($check_user->result() as $userRow){
            return $userRow->image;
        }
        }else{
            return false;
        }

    }
        public function get_superadmin_image(){
        $query ="SELECT image FROM super_admin_info";
        $getImage = $this->CI->db->query($query);
        foreach($getImage->result() as $imageRow){
            return $imageRow->image;
        }
    }
    public function admin_personel_id($admin_id_number){

            $this->CI->db->where('admin_id_number',$admin_id_number);
            $query = $this->CI->db->get('admin_personel');
            if($query->num_rows() > 0){
                return true;
            }else{
                return false;
            }
        }

}

?>