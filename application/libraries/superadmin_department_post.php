<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class superadmin_department_post {

    public function __construct()
    {
        $this->CI =& get_instance();
	}

    public function getLikes($id){
    	$this->CI->db->where('department_topic_id',$id);
        $feedback = $this->CI->db->count_all_results('department_topic_feedback');
    	return $feedback;
    }
    public function getComments($id){
    	$this->CI->db->where('department_topic_id',$id);
        $comments = $this->CI->db->count_all_results('department_topic_comments');
        return $comments;
    }
   public function showComments($post_id){
        
        $query = "SELECT department_topic_comments.admin_id_number as department_admin_id_number,department_topic_comments.id as comment_id,firstname,lastname,commenter_id,department_topic_id,comment,department_topic_comments.date as department_date FROM department_topic_comments 
        LEFT JOIN user ON department_topic_comments.commenter_id = user.id 
        LEFT JOIN admin_personel ON admin_personel.admin_id_number = department_topic_comments.admin_id_number 
        WHERE department_topic_comments.department_topic_id = '$post_id' ORDER BY department_topic_comments.id DESC LIMIT 2";
        $showComments = $this->CI->db->query($query);

        return $showComments->result();
    }
    
  
     public function viewAllComments($id){
        $this->CI->db->where('department_topic_id',$id);
        $count = $this->CI->db->count_all_results('department_topic_comments');

        $query = "SELECT department_topic_comments.admin_id_number as department_admin_id_number,department_topic_comments.id as comment_id,firstname,lastname,commenter_id,department_topic_id,comment,department_topic_comments.date as department_date FROM department_topic_comments 
        LEFT JOIN user ON department_topic_comments.commenter_id = user.id 
        LEFT JOIN admin_personel ON admin_personel.admin_id_number = department_topic_comments.admin_id_number 
        WHERE department_topic_comments.department_topic_id = '$id' ORDER BY department_topic_comments.id DESC LIMIT $count offset 2";
        $showComments = $this->CI->db->query($query);

        return $showComments->result();

    }

    public function listComments($id){
        $this->CI->db->select('*');
        $this->CI->db->from('department_topic_comments');
        $this->CI->db->join('user','user.id = department_topic_comments.commenter_id','left');
        $this->CI->db->where('department_topic_comments.department_topic_id',$id);
        $this->CI->db->group_by('user.id');
        $listComments = $this->CI->db->get();

        return $listComments->result();
    }
    public function listLikes($id){
        $this->CI->db->select('*');
        $this->CI->db->from('department_topic_feedback');
        $this->CI->db->join('user','user.id = department_topic_feedback.user_id','left');
        $this->CI->db->where('department_topic_id',$id);
        $this->CI->db->group_by('user.id');
        $listLikes = $this->CI->db->get();

        return $listLikes->result();
    }

     public function updateDepartmentPost($id){
        $this->CI->db->where('id',$id);
        $getPost = $this->CI->db->get('department_topic');

        return $getPost->result();
    }

     public function getdeptCommenttoUpdate($comment_id){
        $this->CI->db->where('id',$comment_id);
        $currentComment = $this->CI->db->get('department_topic_comments');

        return $currentComment->result();
    }

    public function getLastpostOn($id){
        $query = "SELECT date FROM department_topic WHERE department_id='$id' ORDER BY id DESC LIMIT 1";
        $datePost = $this->CI->db->query($query);

        return $datePost->result();
    }
     public function getPreviuspostOn($id){
        $this->CI->db->where('department_id',$id);
        $count = $this->CI->db->count_all_results('department_topic');
        
        $query = "SELECT date FROM department_topic WHERE department_id='$id' ORDER BY id DESC  LIMIT $count OFFSET 1 ";
        $datePost = $this->CI->db->query($query);

        return $datePost->result();
    }
    public function department_post_admin($department_user_id,$department_admin_id_number){
        

        $this->CI->db->where('id',$department_user_id);
        $this->CI->db->where('admin_id_number',$department_admin_id_number);
        $check_admin_personel = $this->CI->db->get('admin_personel');
        
        if($check_admin_personel->num_rows() > 0){
            return true;
        }else{
            return false;
        }

    }

    public function department_admin_image($admin_id_number){

        $this->CI->db->where('admin_id_number',$admin_id_number);
        $check_admin_personel = $this->CI->db->get('admin_personel');
        
        if($check_admin_personel->num_rows() > 0){
            foreach($check_admin_personel->result() as $adminRow){
            return $adminRow->image;
        }
        }else{
            return false;
        }

    }

    public function get_user_image($user_id){
        $this->CI->db->where('user_id',$user_id);
        $check_user = $this->CI->db->get('user_profile_pic');

        if($check_user->num_rows() > 0){
            foreach($check_user->result() as $userRow){
            return $userRow->image;
        }
        }else{
            return false;
        }

    }
        public function get_superadmin_image(){
         $query ="SELECT image FROM super_admin_info";
        $getImage = $this->CI->db->query($query);
        if($getImage->num_rows() > 0){
            foreach($getImage->result() as $imageRow){
                return $imageRow->image;
            }
        }else{
            return false;
        }
    }
    public function admin_personel_id($admin_id_number){

            $this->CI->db->where('admin_id_number',$admin_id_number);
            $query = $this->CI->db->get('admin_personel');
            if($query->num_rows() > 0){
                return true;
            }else{
                return false;
            }
        }

}

?>