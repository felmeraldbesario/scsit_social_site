<?php 
class admin_library {
	public function __construct()
    {
        $this->CI =& get_instance();
	}

	public function joinedblockedname($id){
		// gi library nlu para makuha nku ang iyang id nga dad.on sa block user until 
		//print blocked user
		$user_id = $this->CI->session->userdata('login_id');
		$this->CI->db->select('user.id AS member_id, admin_block_list.id AS member_block_id, firstname, lastname, middlename, age, address, gender AS member_sex, department_id, reason, admin_block_list.date AS blocked_date, admin_personel.email AS adminEmailAddress ');
		$this->CI->db->from('admin_block_list');
		$this->CI->db->join('user','user.id = admin_block_list.blocked_id','left');
		$this->CI->db->join('block_reason','block_reason.id = admin_block_list.reason_id','left');
		$this->CI->db->join('admin_personel','admin_personel.id = admin_id','left');
		$this->CI->db->where('user.id',$id);
		$this->CI->db->where('admin_id',$user_id);
		$this->CI->db->order_by('member_id','desc');
		$blockedUserInformationQuery = $this->CI->db->get();
		return $blockedUserInformationQuery->result();
	}
	public function newsAdvertise($id){
		$this->CI->db->select('*');
		$this->CI->db->from('news');
		$adverModalRead = $this->CI->db->get();
		return $adverModalRead->result();
	}
	// ==========delete message ====================//
	public function check_delete_message($message_id){
		$admin_id = $this->CI->session->userdata('login_id');
		$query = "SELECT * FROM admin_delete_messages WHERE message_id ='$message_id' AND deleter_id='$admin_id'";
		$checkQuery = $this->CI->db->query($query);
		if($checkQuery->num_rows() == 1){
			return false;
		}else{
			return true;
		}
	}
	// para sa comment
	public function showComment($id){
		$this->CI->db->select('admin_personel.image as admin_image,department_topic_comments.admin_id_number as department_admin_id_number,department_topic_comments.id as comment_id,firstname,lastname,commenter_id,department_topic_id,comment,department_topic_comments.date as department_date');
		$this->CI->db->from('department_topic_comments');
		$this->CI->db->join('user','department_topic_comments.commenter_id = user.id','left');
		$this->CI->db->join('admin_personel','admin_personel.admin_id_number = department_topic_comments.admin_id_number','left');
		$this->CI->db->where('department_topic_comments.department_topic_id',$id);
		$queryShowComment = $this->CI->db->get();
		return $queryShowComment->result();
	}
	// @param
	// select data from department_topic nga post
	// DESC
	public function Selecting_department_profile_status($id){
		
		$this->CI->db->select('admin_id_number,department_topic.id as department_topic_id,department_topic.department_id as dept_id,firstname,lastname,department_topic.user_id_number as user_number,department_topic.user_id as department_user_id,department_topic.image as department_topic_image,department_topic,department_topic.date as department_topic_date, admin_personel.image as picture');
		$this->CI->db->from('department_topic');
		$this->CI->db->join('user','department_topic.user_id = user.id','left');
		$this->CI->db->join('admin_personel','department_topic.user_id_number = admin_personel.admin_id_number','left');
		$this->CI->db->where('department_topic.department_id',$id);
		$this->CI->db->order_by('department_topic_id','desc');
		$departmentPostQuery = $this->CI->db->get();
		return $departmentPostQuery->result();
	

	}

	public function admin_personel_post($admin_id_number){
		$this->CI->db->where('admin_id_number',$admin_id_number);
            $query = $this->CI->db->get('admin_personel');
            if($query->num_rows() > 0){
                return true;
            }else{
                return false;
            }
	}
	public function get_superadmin_image(){
        $query ="SELECT image FROM super_admin_info";
        $getImage = $this->CI->db->query($query);
        if($getImage->num_rows() > 0){
        foreach($getImage->result() as $imageRow){
            return $imageRow->image;
        }
        }else{
        	return false;
        }
    }

    public function get_admin_image($admin_id_number){
    	  $this->CI->db->where('admin_id_number',$admin_id_number);
            $query = $this->CI->db->get('admin_personel');
            if($query->num_rows() > 0){
                return true;
            }else{
                return false;
            }
    }
    public function get_user_image($commenter_id){
    	$this->CI->db->where('user_id',$commenter_id);
        $check_user = $this->CI->db->get('user_profile_pic');

        if($check_user->num_rows() > 0){
            foreach($check_user->result() as $userRow){
            return $userRow->image;
        }
        }else{
            return false;
        }
    }
}

?>