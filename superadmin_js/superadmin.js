
function sa_campus_comment(campus_post_id){
	
	sa_campus_comments = $('input#acj_sa_campus_comment'+campus_post_id).val();
	$.post("superadmin_controller/sa_campus_comments",{campus_comment:sa_campus_comments,campus_post_id:campus_post_id},function(data){
		
		$('span#acj_campus_post'+campus_post_id).html(data);
		$('input#acj_sa_campus_comment'+campus_post_id).val("");
	});
}

function campus_comment_update(comment_id,campus_post_id){
	
	comment = $('textarea#acj_update_comment'+comment_id).val();

	$.post("superadmin_controller/campus_comment_update",{comment_id:comment_id,comment:comment,campus_post_id:campus_post_id},function(data){
		
		$('span#acj_campus_post'+campus_post_id).html(data);
	});
}
///--- para sa modal
function edit_campus_comment(comment_id){
	$.post("superadmin_controller/modal_campus_comment",{comment_id:comment_id},function(data){
		$('div#edit_camp_comment').html(data);
	});
}

function campus_modal_delete(comment_id){
	$.post("superadmin_controller/modal_campus_delete",{comment_id:comment_id},function(data){
	
		$('div#edit_camp_comment').html(data);
	});
}

function campus_delete_comment(comment_id,campus_post_id){
	$.post("superadmin_controller/campus_comment_delete",{comment_id:comment_id,campus_post_id:campus_post_id},function(data){
		$('span#acj_campus_post'+campus_post_id).html(data);
	});
}

/////////////==========department============////////////////


function sa_department_comment(department_post_id){
	

	sa_department_comments = $('input#acj_sa_department_comment'+department_post_id).val();

	$.post("superadmin_controller/sa_department_comments",{department_topic:sa_department_comments,department_post_id:department_post_id},function(data){
		$('span#acj_department_topic'+department_post_id).html(data);
		$('input#acj_sa_department_comment'+department_post_id).val("");
	});
}


function department_comment_modal(comment_id){
	$.post("superadmin_controller/department_comment_modal",{comment_id:comment_id},function(data){
		$("div#ant_department_comment").html(data);
	});
}

function department_comment_update(comment_id,department_topic_id){
	comment = $('textarea#acj_dept_update_comment').val();
	$.post("superadmin_controller/department_comment_update",{comment_id:comment_id,comment:comment,department_topic_id:department_topic_id},function(data){
		$('span#acj_department_topic'+department_topic_id).html(data);
	});
}
function department_delete_modal(comment_id){
	$.post("superadmin_controller/department_delete_modal",{comment_id:comment_id},function(data){
		$("div#ant_department_comment").html(data);
	});
}
function department_comment_delete(comment_id,department_topic_id){
	$.post("superadmin_controller/department_comment_delete",{comment_id:comment_id,department_topic_id:department_topic_id},function(data){
		$('span#acj_department_topic'+department_topic_id).html(data);
	});
}

/// change password

function change_password(){
	
	current_password = $('input#current_password').val();
	new_password = $('input#new_password').val();
if(current_password.length >= 8 ){
	if(new_password.length >=8){
			
$.post("superadmin_controller/change_password",{current_password:current_password,new_password:new_password},function(data){
		$("div#output_message").html(data);
	 	$('input#current_password').val("");
		$('input#new_password').val("");
	});
		
	}else{
		$('span#new_password_label').text("Input at least 8 letters");
	 	document.getElementById('new_password').style="border:1px solid red";
	}
}else{
		$('span#current_password_label').text("Input at least 8 letters");
	 	document.getElementById('current_password').style="border:1px solid red";
}
}
// change username

function change_username(){
	
	current_username = $('input#current_username').val();
	new_username = $('input#new_username').val();
if(current_username.length >=6 ){
	if(new_username.length >=6){
			
$.post("superadmin_controller/change_username",{current_username:current_username,new_username:new_username},function(data){
		$("div#username_alert").html(data);
	 	$('input#current_username').val("");
		$('input#new_username').val("");

	});
		
	}else{
		$('span#new_username_label').text("Input at least 6 letters");
	 	document.getElementById('new_username').style="border:1px solid red";
	}
}else{
		$('span#current_username_label').text("Input at least 6 letters");
	 	document.getElementById('current_username').style="border:1px solid red";
}
}


		

// para sa pages

function add_post_modal(temp_id){
	$.post("superadmin_controller/get_add_post_modal",{temp_id:temp_id},function(data){
		$('span#add_post_pages').html(data);
	});
}

function edit_post_modal(temp_id,post_id){
	$.post("superadmin_controller/get_edit_post_modal",{temp_id:temp_id,post_id:post_id},function(data){
		$('span#add_post_pages').html(data);
	});
}

function delete_post_modal(temp_id,post_id){
	$.post("superadmin_controller/get_delete_post_modal",{temp_id:temp_id,post_id:post_id},function(data){
		$('span#add_post_pages').html(data);
	});
}

function delete_pages_post(temp_id,post_id){
	$.post("superadmin_controller/delete_pages_post",{temp_id:temp_id,post_id:post_id},function(data){
		$('span#post_pages'+post_id).hide();
	});	
}
// message

function reply_message(message_id){
	
	$.post("superadmin_controller/reply_message_modal",{message_id:message_id},function(data){
		$('div#ant_reply_message').html(data);
	
	});
}

function forward_message(message_id){
	
	$.post("superadmin_controller/forward_message_modal",{message_id:message_id},function(data){
		$('div#ant_reply_message').html(data);
	
	});
}
function delete_message(message_id){
	
	$.post("superadmin_controller/delete_message_modal",{message_id:message_id},function(data){
		$('div#ant_reply_message').html(data);
	
	});
}
function delete_action_message(message_id){
	$.post("superadmin_controller/delete_action_message",{message_id:message_id},function(data){

		$('tr#message_row'+message_id).hide();
	});
}


/// para sa picture
  var image_update = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('image_id_update');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
  };

  var ant_profile_update = function(event) {
    var input = event.target;

    var reader = new FileReader();
    reader.onload = function(){
      var dataURL = reader.result;
      var output = document.getElementById('ant_profile_id');
      output.src = dataURL;
    };
    reader.readAsDataURL(input.files[0]);
  };

// para sa update personal info
  function change_personel_info(){
  		firstname = $('#ant_firstname').val();
  		lastname = $('#ant_lastname').val();
  		middlename = $('#ant_middlename').val();
  		faculty_id = $('#ant_faculty_id').val();
  		email = $('#ant_email').val();
  		contact_number = $('#ant_contact_number').val();

  		if(
  			(firstname!= "" || $.trim(firstname).length != 0)   && (lastname!= "" || $.trim(lastname).length != 0) &&
  			(middlename!= "" || $.trim(middlename).length != 0) &&	(faculty_id!= "" || $.trim(faculty_id).length != 0) &&
  			(email!= "" || $.trim(email).length != 0) && (contact_number != "" || $.trim(contact_number).length != 0 ) &&
  			(faculty_id != 0 && contact_number != 0)
		  ){

  $.post("superadmin_controller/change_personel_info",{firstname:firstname,lastname:lastname,middlename:middlename,faculty_id:faculty_id,email:email,contact_number:contact_number},function(data){
  location.reload();
  	});
  		}else{
  			$("span#ant_personal_update_id").text("All Fields are required!!");
  		}
  }

  