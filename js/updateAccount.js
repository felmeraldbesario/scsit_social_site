$(document).ready(function(){
	var call = false;
	$('#jam_updateAccount').click(function(){
		var jam_emailupdate = $('#jam_emailupdate').val();
		var jam_usernameupdate = $('#jam_usernameupdate').val();
		var oldpass = $('#jam_oldPassUpdate').val();
		var jam_newPassUpdate = $('#jam_newPassUpdate').val();
		var jam_retypePassUpdate = $('#jam_retypePassUpdate').val();
		if(oldpass == ''){
			$("#jam_changeAccount").html("<label class='text-warning'>Please type your old password..</label>");
		}
		else if(jam_newPassUpdate == ''){
			$("#jam_changeAccount").html("<label class='text-warning'>Please enter new password..</label>");
		}
		else if(jam_retypePassUpdate == ''){
			$("#jam_changeAccount").html("<label class='text-warning'>Please retype your new password..</label>");
		}
		else if(jam_newPassUpdate != '' && jam_retypePassUpdate != ''){
			if(jam_newPassUpdate == jam_retypePassUpdate){
				$.post("user_controller/updateSecurityAccount", {oldpass:oldpass, newPassword:jam_newPassUpdate, email:jam_emailupdate, username:jam_usernameupdate}, function(data){
					$("#jam_changeAccount").html(data);
				});
			}else{
				$("#jam_changeAccount").html("<label class='text-warning'>password mismatch..</label>");
			}
		}
		return call;

	});

	$('#jam_dismissUpAccount').click(function(){
		$('#jam_oldPassUpdate').val("");
		$('#jam_newPassUpdate').val("");
		$('#jam_retypePassUpdate').val("")
		$("#jam_changeAccount").html("");
	});
});