$(document).ready(function(){
	$('#jam_campus_img').hide();
	$('#jam_campus_userfile').hide();
    $('#jam_campus_userfile').change(function(e){
         filename = e.target.files[0].name;
         output = document.getElementById('jam_campus_img');
         $('#jam_campus_img').show();
         output.src = URL.createObjectURL(e.target.files[0]);
   		
    });
});

function delPost(id){
	$.post("user_controller/delPost", {id:id}, function(data){
		$('#jam_campus_post' + id).hide();
	});
	
}


function clikePost(id){
	$.post("user_controller/clikePost", {id:id}, function(data){
		$('#clikePost' + id).html(data);
	});
}

function commentCPost(id, commentCount){
	var comment = $('#jam_toCPostComment' + id).val();
	$.post("user_controller/commentCPost", {id:id, comment:comment}, function(data){
		$('#jam_cpost_comment' + id).html(data);
		$('#jam_toCPostComment' + id).val('');
		getCampusCommentCount(id, comment);
	});
}

function jam_delComment(id, post_id){
	$.post("user_controller/delComment", {id:id}, function(data){
		$('#cpost_comment_id' + id).hide();
		minusCPostCommentCount(post_id);
	});
}

function getCampusCommentCount(id, comment){
	if(comment != ''){
		$.post("user_controller/getCampusCommentCount", {id:id}, function(data){
			$('#jam_commenter_counts' + id).html(' ' + data + ' ' + 'Comments');

		});
	}
}

function minusCPostCommentCount(id){
	$.post("user_controller/getCampusCommentCount", {id:id}, function(data){
		$('#jam_commenter_counts' + id).html(' ' + data + ' ' + 'Comments');
	});
}

function cpost_viewAllComments(id){
	$.post("user_controller/cpost_viewAllComments", {id:id}, function(data){
		$('#jam_cpost_comment' + id).html(data).fadeIn("slow");
		$('#c_viewAllComments' + id).hide();
	});
}

function getAllCPostCommenters(id){
	$.post("user_controller/getAllCPostCommenters", {id:id}, function(data){
		$('#jam_cpostCommenter_modal').html(data);
	});
	
}

function jam_cpost_likers(id){
	$.post("user_controller/jam_cpost_likers", {id:id}, function(data){
		$('#cpost_likers_names').html(data);
	});
}

function editCPost(post_id){
	$('#jam_updateCPost_image').hide();

	$.post("user_controller/editCPost", {id:post_id}, function(data){
	  $('#update_cpost_jam').html(data);
		$('#jam_updateCPost_image').change(function(e){
	         filename = e.target.files[0].name;
	         output = document.getElementById('jam_target_cpost_image');
	         output.src = URL.createObjectURL(e.target.files[0]);
		   		$(document).keyup(function(event){
				    if(event.keyCode == 27){
				        output.src = 'img/default_profile.jpg';
				    }
				    if(event.keyCode == 8){
				    	 output.src = 'img/default_profile.jpg';
				    }
				});
	    });
	});
}


function jam_viewFullImage(id){
	$.post("user_controller/jam_viewFullImage", {id:id}, function(data){
		$('#jam_target_viewFullImage').html(data);
	});
}


function commentFullImge(id){
	comment = $('#jam_cpfullImage' + id).val();

	if(comment != " "){
		$.post("user_controller/commentFullImge", {id:id, comment:comment}, function(data){
			$("#jam_vfComments").html(data);
			$('#jam_cpfullImage' + id).val(" ");
		});
	}
}

function delPostFullImage(id){
	$.post("user_controller/delPost", {id:id}, function(data){
		location.reload();
	});
}

function viewfullimagecommentDel(id){
	$.post("user_controller/viewfullimagecommentDel", {id:id}, function(data){
		$('#view_full_comment_div' + id).hide();
	});
	
}

function viewFullImageEdit(id){
	
}

function editComment(id){
	$.post("user_controller/editComment", {id:id}, function(data){
		$('#target_div_cpostCommentEdit').html(data);
	});
}

