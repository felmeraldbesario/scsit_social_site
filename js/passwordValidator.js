
var submitCall;
var passValidator;
var label;

$(document).ready(function(){
    $('#password').click(function(){
        passValidator  = setInterval(function(){
            password = $('#password').val();
            cpassword = $('#cpassword').val();
            if(password == '' && cpassword == ''){
                this.label = "";
                this.submitCall = false;
            }else if(password != cpassword){
                this.label = "<span class='text-danger'>Password mismatch..</span>";
                this.submitCall = false;
            }else if(password == cpassword){
                 this.label = "<span class='text-success'>Password match..</span>";
                this.submitCall = true;
            }
            $('#passwordVal').html(this.label);
  
        },1000);
    });
});

$(document).ready(function(){
    $('#cpassword').click(function(){
        passValidator  = setInterval(function(){
            password = $('#password').val();
            cpassword = $('#cpassword').val();
            if(password == '' && cpassword == ''){
                this.label = "";
                this.submitCall = false;
            }else if(password != cpassword){
                this.label = "<span class='text-danger'>Password mismatch..</span>";
                this.submitCall = false;
            }else if(password == cpassword){
                 this.label = "<span class='text-success'>Password match..</span>";
                this.submitCall = true;
            }
            $('#passwordVal').html(this.label);
  
        },1000);
    });
});

$(document).ready(function(){
    $('#submit').click(function(){
        clearInterval(passValidator);
        $('#passwordVal').text(this.label);
        return submitCall;
    });
});

/*
$('document').ready(function(){
       
    passValidator  = setInterval(function(){
        password = $('#password').val();
        cpassword = $('#cpassword').val();
        if(password == '' && cpassword == ''){
            this.label = "";
            this.submitCall = false;
        }else if(password != cpassword){
            this.label = "<span class='text-danger'>Password mismatch..</span>";
            this.submitCall = false;
        }else if(password == cpassword){
            this.label = "<span class='text-success'>Password match..</span>";
            this.submitCall = true;
        }
        $('#passwordVal').html(this.label);
  
    },1000);
   
});
*/